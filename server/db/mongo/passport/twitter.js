import User from '../models/user';

/* eslint-disable no-param-reassign */
export default (req, accessToken, refreshToken, profile, done) => {
  if (req.user) {
    return User.findOne({ twitter: profile.id }, (findOneErr, existingUser) => {
      if (existingUser) {
        return done(null, false, { message: 'There is already a Twitter account that belongs to you. Sign in with that account or delete it, then link it with your current account.' });
      }
      return User.findById(req.user.id, (findByIdErr, user) => {
        user.role = 'subscriber'; // only manage their profile
        user.twitter = profile.id;
        user.tokens.push({ kind: 'twitter', accessToken });
        user.profile.username = user.profile.name || profile.username;
        user.profile.name = user.profile.name || profile.displayName;
        user.profile.gender = user.profile.gender || profile.gender;
        user.profile.picture = user.profile.picture || profile.photos[0].value;
        user.save((err) => {
          done(err, user, { message: 'Twitter account has been linked.' });
        });
      });
    });
  }
  return User.findOne({ twitter: profile.id }, (findByTwitterIdErr, existingUser) => {
    if (existingUser) return done(null, existingUser);
    return User.findOne({ email: profile._json.emails[0].value }, (findByEmailErr, existingEmailUser) => {
      if (existingEmailUser) {
        return done(null, false, { message: 'There is already an account using this email address. Sign in to that account and link it with Twitter manually from Account Settings.' });
      }
      const user = new User();
      user.role = 'subscriber'; // only manage their profile
      user.email = profile.email;
      user.twitter = profile.id;
      user.tokens.push({ kind: 'twitter', accessToken });
      user.profile.username = profile.username;
      user.profile.name = profile.displayName;
      user.profile.gender = profile.gender;
      user.profile.picture = profile.photos[0].value;
      return user.save((err) => {
        done(err, user);
      });
    });
  });
};
/* eslint-enable no-param-reassign */
