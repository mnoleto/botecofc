import User from '../models/user';
// import md5 from 'spark-md5';

/* eslint-disable no-param-reassign */
export default (req, accessToken, refreshToken, profile, done) => {
  if (req.user) {
    return User.findOne({ google: profile.id }, (findOneErr, existingUser) => {
      if (existingUser) {
        return done(null, false, { message: 'Já existe uma conta do Google+ cadastrada que pertence a você. Faça o login usando essa conta ou apague.' });
      }
      return User.findById(req.user.id, (findByIdErr, user) => {
        // user._id = md5.hash(profile.displayName);
        user.role = 'subscriber'; // only manage their profile
        user.google = profile.id;
        user.tokens.push({ kind: 'google', accessToken });
        user.name = user.profile.name || profile.displayName;
        user.profile.gender = user.profile.gender || profile._json.gender;
        user.profile.picture = user.profile.picture || profile._json.image.url;
        user.save((err) => {
          console.log('Google account has been linked.');
          done(err, user, { message: 'Google account has been linked.' });
        });
      });
    });
  }
  return User.findOne({ google: profile.id }, (findByGoogleIdErr, existingUser) => {
    if (existingUser) return done(null, existingUser);
    return User.findOne({ email: profile._json.emails[0].value }, (findByEmailErr, existingEmailUser) => {
      if (existingEmailUser) {
        return done(null, false, { message: 'Já existe uma conta usando esse endereço de e-mail. Faça o login usando essa conta.' });
      }
      const user = new User();
      // user._id = profile.id;
      user.role = 'subscriber'; // only manage their profile
      user.email = profile._json.emails[0].value;
      user.google = profile.id;
      user.tokens.push({ kind: 'google', accessToken });
      user.name = profile.displayName;
      user.profile.gender = profile._json.gender;
      user.profile.picture = profile._json.image.url;
      return user.save((err) => {
        console.log('Google account has been created.');
        done(err, user);
      });
    });
  });
};
/* eslint-enable no-param-reassign */
