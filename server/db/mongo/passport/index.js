import deserializeUser from './deserializeUser';
import facebook from './facebook';
import google from './google';

export { deserializeUser, facebook, google };

export default {
  deserializeUser,
  facebook,
  google,
};
