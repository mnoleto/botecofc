import User from '../models/user';
// import md5 from 'spark-md5';

/* eslint-disable no-param-reassign */
export default (req, accessToken, refreshToken, profile, done) => {
  if (req.user) {
    return User.findOne({ facebook: profile.id }, (findOneErr, existingUser) => {
      if (existingUser) {
        return done(null, false, { status: 'error', message: 'Já existe uma conta de Facebook cadastrada que pertence a você. Faça o login usando essa conta ou apague.' });
      }
      return User.findById(req.user.id, (findByIdErr, user) => {
        // user._id = ObjectID(profile.id);
        user.role = 'subscriber'; // only manage their profile
        user.facebook = profile.id;
        user.tokens.push({ kind: 'facebook', accessToken });
        user.name = user.profile.name || profile.name;
        user.profile.gender = user.profile.gender || profile.gender;
        user.profile.picture = user.profile.photos[0].value || profile.photos[0].value;
        user.save((err) => {
          done(err, user, { status: 'first',  message: 'Facebook account has been linked.' });
        });
      });
    });
  }
  return User.findOne({ facebook: profile.id }, (findByFacebookIdErr, existingUser) => {
    if (existingUser) return done(null, existingUser);
    return User.findOne({ email: profile.emails[0].value }, (findByEmailErr, existingEmailUser) => {
      if (existingEmailUser) {
        return done(null, false, { status: 'error',  message: 'Já existe uma conta usando esse endereço de e-mail. Faça o login usando essa conta.' });
      }
      const user = new User();
      // user._id = ObjectID(profile.id);
      user.role = 'subscriber'; // only manage their profile
      user.email = profile.emails[0].value;
      user.facebook = profile.id;
      user.tokens.push({ kind: 'facebook', accessToken });
      user.name = profile.displayName;
      user.profile.gender = profile.gender;
      user.profile.picture = profile.photos[0].value;
      return user.save((err) => {
        done(err, user, { status: 'logged' });
      });
    });
  });
};
/* eslint-enable no-param-reassign */
