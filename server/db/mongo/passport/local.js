import User from '../models/user';

export default (email, password, done) => {
  User.findOne({ email }, (findErr, user) => {
    if (!user) return done(null, false, { message: `O e-mail ${email}. não pode ser encontrado.` });
    return user.comparePassword(password, (passErr, isMatch) => {
      if (isMatch) {
        return done(null, user);
      }
      return done(null, false, { message: 'O E-mail ou senha não conferem.' });
    });
  });
};
