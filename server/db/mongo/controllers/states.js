import _ from 'lodash';
import States from '../models/states';
// import statesData from '../data/states.json'

const statesData = [{
  "id": 11,
  "name": "Rondônia",
  "abbr": "RO"
}, {
  "id": 12,
  "name": "Acre",
  "abbr": "AC"
}, {
  "id": 13,
  "name": "Amazonas",
  "abbr": "AM"
}, {
  "id": 14,
  "name": "Roraima",
  "abbr": "RR"
}, {
  "id": 15,
  "name": "Pará",
  "abbr": "PA"
}, {
  "id": 16,
  "name": "Amapá",
  "abbr": "AP"
}, {
  "id": 17,
  "name": "Tocantins",
  "abbr": "TO"
}, {
  "id": 21,
  "name": "Maranhão",
  "abbr": "MA"
}, {
  "id": 22,
  "name": "Piauí",
  "abbr": "PI"
}, {
  "id": 23,
  "name": "Ceará",
  "abbr": "CE"
}, {
  "id": 24,
  "name": "Rio Grande do Norte",
  "abbr": "RN"
}, {
  "id": 25,
  "name": "Paraíba",
  "abbr": "PB"
}, {
  "id": 26,
  "name": "Pernambuco",
  "abbr": "PE"
}, {
  "id": 27,
  "name": "Alagoas",
  "abbr": "AL"
}, {
  "id": 28,
  "name": "Sergipe",
  "abbr": "SE"
}, {
  "id": 29,
  "name": "Bahia",
  "abbr": "BA"
}, {
  "id": 31,
  "name": "Minas Gerais",
  "abbr": "MG"
}, {
  "id": 32,
  "name": "Espírito Santo",
  "abbr": "ES"
}, {
  "id": 33,
  "name": "Rio de Janeiro",
  "abbr": "RJ"
}, {
  "id": 35,
  "name": "São Paulo",
  "abbr": "SP"
}, {
  "id": 41,
  "name": "Paraná",
  "abbr": "PR"
}, {
  "id": 42,
  "name": "Santa Catarina",
  "abbr": "SC"
}, {
  "id": 43,
  "name": "Rio Grande do Sul",
  "abbr": "RS"
}, {
  "id": 50,
  "name": "Mato Grosso do Sul",
  "abbr": "MS"
}, {
  "id": 51,
  "name": "Mato Grosso",
  "abbr": "MT"
}, {
  "id": 52,
  "name": "Goiás",
  "abbr": "GO"
}, {
  "id": 53,
  "name": "Distrito Federal",
  "abbr": "DF"
}]



/**
 * List
 */
export function all(req, res) {
  States.find({}).exec((err, states) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }

    return res.json(states.sort((a, b) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;

    }));
  });
}

export function active(req, res) {
  States.find({ isActive: true }).exec((err, states) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }
    return res.json(states.sort((a, b) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;

    }));

  });
}

/**
 * Add a States
 */
export function add(req, res) {
  States.create(req.body, (err) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }

    return res.status(200).send('OK');
  });
}

/**
 * Update a topic
 */
export function update(req, res) {
  const query = { id: req.params.id };
  const omitKeys = ['id', '_id', '_v'];
  const data = _.omit(req.body, omitKeys);

  States.findOneAndUpdate(query, data, (err) => {
    if (err) {
      console.log('Error on save!');
      return res.status(500).send('We failed to save for some reason');
    }

    return res.status(200).send('Updated successfully');
  });
}

/**
 * Remove a topic
 */
export function remove(req, res) {
  const query = { id: req.params.id };
  States.findOneAndRemove(query, (err) => {
    if (err) {
      console.log('Error on delete');
      return res.status(500).send('We failed to delete for some reason');
    }

    return res.status(200).send('Removed Successfully');
  });
}

export function addAllStates(req, res) {
  States.create(statesData, (err) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }

    return res.status(200).send('OK');
  });
}

export function removeAllStates(req, res) {
  const query = {};
  States.deleteMany(query, (err) => {
    if (err) {
      console.log('Error on delete');
      return res.status(500).send('We failed to delete for some reason');
    }

    return res.status(200).send('Removed Successfully');
  });
}

export default {
  active,
  add,
  addAllStates,
  all,
  remove,
  removeAllStates,
  update,
};

