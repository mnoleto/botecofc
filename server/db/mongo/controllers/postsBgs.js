import _ from 'lodash';
import PostsBgs from '../models/posts-bgs';

/**
 * List
 */
export function all(req, res) {
  PostsBgs.find({}).exec((err, bgs) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }

    return res.json(bgs);
  });
}

/**
 * Add a Post
 */
export function add(req, res) {
  PostsBgs.create(req.body, (err, bg) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }

    return res.status(200).json(bg);
  });
}

/**
 * Update a post
 */
export function update(req, res) {
  const query = { id: req.params.id };
  const omitKeys = ['id', '_id', '_v'];
  const data = _.omit(req.body, omitKeys);
  PostsBgs.findOneAndUpdate(query, data, (err) => {
    if (err) {
      console.log('Error on save!');
      return res.status(500).send('We failed to save for some reason');
    }

    return res.status(200).send('Updated successfully');
  });
}

/**
 * Remove a post
 */
export function remove(req, res) {
  const query = { _id: req.params.id };
  PostsBgs.findOneAndRemove(query, (err) => {
    if (err) {
      console.log('Error on delete');
      return res.status(500).send('We failed to delete for some reason');
    }

    return res.status(200).send('Removed Successfully');
  });
}

export default {
  all,
  add,
  update,
  remove
};
