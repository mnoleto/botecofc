import _ from 'lodash';
import Championships from '../models/championships';
import moment from 'moment';
import 'moment/locale/pt-br';

/**
 * List
 */
export function all(req, res) {
  moment.locale('pt-BR');
  const filterDate = moment().subtract(1, 'day');
  Championships.find({ 'dateEnd': { $gte: filterDate } }).exec((err, championships) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }
    return res.json(championships);
  });
}

/**
 * Add a Championships
 */
export function add(req, res) {
  Championships.create(req.body, (err) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }

    return res.status(200).send('OK');
  });
}

/**
 * Update a topic
 */
export function update(req, res) {
  const query = { id: req.params.id };
  const omitKeys = ['id', '_id', '_v'];
  const data = _.omit(req.body, omitKeys);

  Championships.findOneAndUpdate(query, data, (err) => {
    if (err) {
      console.log('Error on save!');
      return res.status(500).send('We failed to save for some reason');
    }

    return res.status(200).send('Updated successfully');
  });
}

/**
 * Remove a topic
 */
export function remove(req, res) {
  const query = { id: req.params.id };
  Championships.findOneAndRemove(query, (err) => {
    if (err) {
      console.log('Error on delete');
      return res.status(500).send('We failed to delete for some reason');
    }

    return res.status(200).send('Removed Successfully');
  });
}

// export function updateLogo(req, res) {
//   Championships.find((err, championships) => {
//     championships.forEach((doc) => {
//       const updatedValue = doc.logo.responseText.replace('app/images/uploads', 'https://botecofc.s3.amazonaws.com/uploads');
//       Championships.update({ _id: doc._id }, { $set: { logo: { responseText: updatedValue } } }, (err, result) => {
//         if (err) {
//           console.log('Error on update logo');
//           return res.status(500).send('We failed to update the championship logo');
//         }
//       });
//     });

//     return res.status(200).send('Updated successfully');
//   })
// }

export default {
  all,
  add,
  update,
  remove,
  // updateLogo,
};
