import banners from './banners';
import championships from './championships';
import channels from './channels';
import cities from './cities';
import countries from './countries';
import featureRequest from './featureRequest';
import games from './games';
import ownerships from './ownerships';
import places from './places';
import placesSuggestions from './placesSuggestions';
import placesTemp from './placesTemp';
import posts from './posts';
import postsBgs from './postsBgs';
import postsFigures from './postsFigures';
import sports from './sports';
import states from './states';
import teams from './teams';
import users from './users';

export {
  banners,
  championships,
  channels,
  cities,
  countries,
  featureRequest,
  games,
  ownerships,
  places,
  placesSuggestions, 
  placesTemp,
  posts,
  postsBgs,
  postsFigures,
  sports,
  states,
  teams,
  users,
};

export default {
  banners,
  championships,
  channels,
  cities,
  countries,
  featureRequest,
  games,
  ownerships,
  places,
  placesSuggestions,
  placesTemp,
  posts,
  postsBgs,
  postsFigures,
  sports,
  states,
  teams,
  users,
};
