import _ from 'lodash';
import PlacesSuggestions from '../models/placesSuggestions';

/**
 * List
 */
export function all(req, res) {
  PlacesSuggestions.find({}).exec((err, placesSuggestions) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }
    return res.json(placesSuggestions);
  });
}

/**
 * Add a PlacesSuggestions
 */
export function add(req, res) {
  PlacesSuggestions.create(req.body, (err) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }

    return res.status(200).send('OK');
  });
}

/**
 * Remove a topic
 */
export function remove(req, res) {
  const query = { id: req.params.id };
  PlacesSuggestions.findOneAndRemove(query, (err) => {
    if (err) {
      console.log('Error on delete');
      return res.status(500).send('We failed to delete for some reason');
    }

    return res.status(200).send('Removed Successfully');
  });
}

export default {
  all,
  add,
  remove
};
