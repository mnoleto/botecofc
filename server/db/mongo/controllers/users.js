import _ from 'lodash';
import User from '../models/user';
import passport from 'passport';

/**
 * List
 */
export function all(req, res) {
  User.find({}).exec((err, users) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }
    return res.json(users);
  });
}

/**
 * POST /login
 */
export function login(req, res, next) {
  // Do email and password validation for the server
  passport.authenticate('local', (authErr, user, info) => {
    if (authErr) return next(authErr);
    if (!user) {
      return res.status(401).json({ message: info.message });
    }
    // Passport exposes a login() function on req (also aliased as
    // logIn()) that can be used to establish a login session
    return req.logIn(user, (loginErr) => {
      if (loginErr) return res.status(401).json({ message: loginErr });
      if(req.cookies.returnTo) {
        res.redirect(req.cookies.returnTo);
        delete req.cookies.returnTo;
      } else {
        return res.status(200).json({
          message: 'Usuário logado com sucesso.'
        });
      }
    });
  })(req, res, next);
}

/**
 * POST /logout
 */
export function logout(req, res) {
  // Do email and password validation for the server
  req.logout();
  res.redirect('/');
}

/**
 * POST /signup
 * Create a new local account and login
 */
export function signUp(req, res, next) {
  const user = new User({
    email: req.body.email,
    password: req.body.password
  });
  
  User.findOne({ email: req.body.email }, (findErr, existingUser) => {
    if (existingUser) {
      return res.status(409).json({ message: 'Já existe uma conta com esse e-mail!' });
    }
    
    return user.save((saveErr) => {
      if (saveErr) return next(saveErr);
      return req.logIn(user, (loginErr) => {
        if (loginErr) return res.status(401).json({ message: loginErr });
        if(req.cookies.returnTo) {
          res.redirect(req.cookies.returnTo);
          delete req.cookies.returnTo;
        } else {
          return res.status(200).json({
            message: 'Usuário logado com sucesso.'
          });
        }
      });
    });
  });
}

/**
 * POST /register
 * Create a new local account without login
 */
export function register(req, res, next) {
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    role: (req.body.role) ? req.body.role : 'subscriber', // only manage their profile
    password: req.body.password
  });

  User.findOne({ email: req.body.email }, (findErr, existingUser) => {
    if (existingUser) {
      return res.status(409).json({ message: 'Já existe uma conta com esse e-mail!' });
    }

    return user.save((saveErr) => {
      if (saveErr) return next(saveErr);
      return res.status(200).json({
        message: 'Usuário registrado com sucesso!'
      });
    });
  });
}

/**
 * Remove a user
 */
export function remove(req, res) {
  const query = { _id: req.params.id };
  User.findOneAndRemove(query, (err) => {
    if (err) {
      console.log('Error on delete');
      return res.status(500).send('We failed to delete for some reason');
    }

    return res.status(200).send('Removed Successfully');
  });
}

/**
 * Update a user
 */
export function update(req, res) {
  const query = { _id: req.params.id };
  const omitKeys = ['id', '_id', '_v'];
  const data = _.omit(req.body, omitKeys);
  
  User.findOneAndUpdate(query, data, (err) => {
    if (err) {
      console.log('Error on save!');
      return res.status(500).send('We failed to save for some reason');
    }

    return res.status(200).send('Updated successfully');
  });
}

export function addToFavorite(req, res) {
  const query = { _id: req.params.id };
  const omitKeys = ['id', '_id', '_v'];
  const data = _.omit(req.body, omitKeys);
  User.findOneAndUpdate(query, { $push: { 'favorites': data.favorite } }, {upsert:true}, (err) => {
    if (err) {
      console.log('Error on update favorites!');
      return res.status(500).send('We failed to update favorites for some reason');
    }
    return res.status(200).send('Updated successfully');
  });
}

export function removeFromFavorite(req, res) {
  const query = { _id: req.params.id };
  const omitKeys = ['id', '_id', '_v'];
  const data = _.omit(req.body, omitKeys);
  User.update(query, { $pull: { 'favorites': { 'id': data.favorite.id } } }, (err) => {
    if (err) {
      console.log('Error on update favorites!');
      return res.status(500).send('We failed to update favorites for some reason');
    }

    return res.status(200).send('Updated successfully');
  });
}

export default {
  all,
  login,
  logout,
  signUp,
  register,
  remove,
  update,
  addToFavorite,
  removeFromFavorite,
};
