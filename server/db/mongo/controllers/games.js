import _ from 'lodash';
import Games from '../models/games';
import moment from 'moment';

/**
 * List
 */
export function all(req, res) {
  const filterDate = moment().subtract(1, 'days').toDate();

  Games.find({'gameDate': { $gte: filterDate } }).lean().exec((err, games) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }

    return res.json(games);
  });
}

/**
 * Add a Games
 */
export function add(req, res) {
  Games.create(req.body, (err) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }

    return res.status(200).send('OK');
  });
}

/**
 * Update a topic
 */
export function update(req, res) {
  const query = { id: req.params.id };
  const omitKeys = ['id', '_id', '_v'];
  const data = _.omit(req.body, omitKeys);

  Games.findOneAndUpdate(query, data, (err) => {
    if (err) {
      console.log('Error on save!');
      return res.status(500).send('We failed to save for some reason');
    }

    return res.status(200).send('Updated successfully');
  });
}

/**
 * Remove a topic
 */
export function remove(req, res) {
  const query = { id: req.params.id };
  Games.findOneAndRemove(query, (err) => {
    if (err) {
      console.log('Error on delete');
      return res.status(500).send('We failed to delete for some reason');
    }

    return res.status(200).send('Removed Successfully');
  });
}

export function updateGamesId(req, res) {
  const filterDate = moment().subtract(1, 'days').toDate();

  Games.find({'gameDate': { $gte: filterDate } }).lean().exec((err, games) => {
    games.forEach((doc) => {
      Games.update({ _id: doc._id }, { $set: { id: doc._id } }, (error, result) => {
        if (error) {
          console.log('Error on update game');
          return res.status(500).send('We failed to update the game id');
        }
        console.error(result);
      });
    })

    return res.status(200).send('Updated successfully');
  });
}

export default {
  all,
  add,
  update,
  remove,
  updateGamesId,
};
