import _ from 'lodash';
import PostsFigures from '../models/posts-figures';

/**
 * List
 */
export function all(req, res) {
  PostsFigures.find({}).exec((err, figures) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }

    return res.json(figures);
  });
}

/**
 * Add a Post
 */
export function add(req, res) {
  const figures = req.body.map((item) => {
    return {
      image: item
    }
  });
  PostsFigures.create(figures, (err, figures) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }

    return res.status(200).json(figures);
  });
}

/**
 * Update a post
 */
export function update(req, res) {
  const query = { id: req.params.id };
  const omitKeys = ['id', '_id', '_v'];
  const data = _.omit(req.body, omitKeys);
  PostsFigures.findOneAndUpdate(query, data, (err) => {
    if (err) {
      console.log('Error on save!');
      return res.status(500).send('We failed to save for some reason');
    }

    return res.status(200).send('Updated successfully');
  });
}

/**
 * Remove a post
 */
export function remove(req, res) {
  const query = { _id: req.params.id };
  PostsFigures.findOneAndRemove(query, (err) => {
    if (err) {
      console.log('Error on delete');
      return res.status(500).send('We failed to delete for some reason');
    }

    return res.status(200).send('Removed Successfully');
  });
}

export default {
  all,
  add,
  update,
  remove
};
