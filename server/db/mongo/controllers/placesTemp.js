import _ from 'lodash';
import PlacesTemp from '../models/placesTemp';

/**
 * List
 */
export function all(req, res) {
  PlacesTemp.find({}).exec((err, placesTemp) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }
    return res.json(placesTemp);
  });
}

/**
 * Add a PlacesTemp
 */
export function add(req, res) {
  PlacesTemp.create(req.body, (err) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }

    return res.status(200).send('OK');
  });
}

/**
 * Update a topic
 */
export function update(req, res) {
  const query = { id: req.params.id };
  const omitKeys = ['id', '_id', '_v'];
  const data = _.omit(req.body, omitKeys);

  PlacesTemp.findOneAndUpdate(query, data, (err) => {
    if (err) {
      console.log('Error on save!');
      return res.status(500).send('We failed to save for some reason');
    }

    return res.status(200).send('Updated successfully');
  });
}

/**
 * Remove a topic
 */
export function remove(req, res) {
  const query = { id: req.params.id };
  PlacesTemp.findOneAndRemove(query, (err) => {
    if (err) {
      console.log('Error on delete');
      return res.status(500).send('We failed to delete for some reason');
    }

    return res.status(200).send('Removed Successfully');
  });
}

export function cleanPlace(req, res) {
  PlacesTemp.find((err, places) => {
    places.forEach((doc) => {
      if (doc.id === '' || doc.id == null) {
        const query = { _id: doc._id };
        PlacesTemp.findOneAndRemove(query, (err) => {
          if (err) {
            console.log('Error on delete');
            return res.status(500).send('We failed to delete for some reason');
          }
        });
      }
    });

    return res.status(200).send('Updated successfully');
  })
}

export default {
  all,
  add,
  update,
  remove,
  cleanPlace,
};
