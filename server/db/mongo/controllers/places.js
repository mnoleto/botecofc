import _ from 'lodash';
import moment from 'moment';
import Places from '../models/places';

/**
 * List
 */
export function all(req, res) {
  const filterDate = moment().subtract(1, 'days').toDate();
  Places.find({}).lean().exec((err, places) => {
    if (err) {
      console.log('Error in first query');
      return res.status(500).send('Something went wrong getting the data');
    }
    const filteredPlaces = places.map((place) => {
      if(!place.broadcastGames) return place;
      return Object.assign({}, place, {
        broadcastGames: place.broadcastGames
          ? place.broadcastGames.filter((game) => {
            if (!game || game.gameDate == undefined) return false;
            const gameDate = new Date(game.gameDate);
            return gameDate > filterDate;
          })
          : [],
      });
    });
    return res.json(filteredPlaces);
  });
}

/**
 * Add a Places
 */
export function add(req, res) {
  Places.create(req.body, (err) => {
    if (err) {
      console.log(err);
      return res.status(400).send(err);
    }

    return res.status(200).send('OK');
  });
}

/**
 * Update a topic
 */
export function update(req, res) {
  const query = { id: req.params.id };
  const omitKeys = ['id', '_id', '_v'];
  const isIncrementPosts = req.body.isIncrementPosts;
  const data = _.omit(req.body, omitKeys);

  if (isIncrementPosts) {
    Places.findOneAndUpdate(query, {
      $inc: { numberOfPosts: 1 },
      $push: { posts: { date: new Date() } } ,
    }, (err) => {
      if (err) {
        console.log('Error on save!');
        return res.status(500).send('We failed to save for some reason');
      }
      return res.status(200).send('Updated successfully');
    });
  } else {
    Places.findOneAndUpdate(query, data, (err) => {
      if (err) {
        return res.status(500).send('We failed to save for some reason');
      }
      return res.status(200).send('Updated successfully');
    });
  }
}

/**
 * Remove a topic
 */
export function remove(req, res) {
  const query = { id: req.params.id };
  Places.findOneAndRemove(query, (err) => {
    if (err) {
      console.log('Error on delete');
      return res.status(500).send('We failed to delete for some reason');
    }

    return res.status(200).send('Removed Successfully');
  });
}

export function resetCounter(req, res) {
  Places.find((err, places) => {
    places.forEach((doc) => {
      Places.update({ _id: doc._id }, { $set: { numberOfPosts: 0 } }, (err, result) => {
        if (err) {
          console.log('Error on update numberOfPosts');
          return res.status(500).send('We failed to update the championship numberOfPosts');
        }
      });
    });

    return res.status(200).send('Updated successfully');
  })
}

export function updateLocale(req, res) {
  Places.find((err, places) => {
    places.forEach((doc) => {
      Places.update({ _id: doc._id }, { $set: { cityId: '5300108', stateId: '53' } }, (err, result) => {

        if (err) {
          console.log('Error on update logo');
          return res.status(500).send('We failed to update the place locale');
        }
        console.log(result);
      });
    });

    return res.status(200).send('Updated successfully');
  })
}

export function cleanPlace(req, res) {
  Places.find((err, places) => {
    places.forEach((doc) => {
      if (doc.id === '' || doc.id == null) {
        const query = { _id: doc._id };
        Places.findOneAndRemove(query, (err) => {
          if (err) {
            console.log('Error on delete');
            return res.status(500).send('We failed to delete for some reason');
          }
        });
      }
    });

    return res.status(200).send('Updated successfully');
  })
}

export function updatePosts(req, res) {
  Places.find((err, places) => {
    places.forEach((doc) => {
      Places.update({ _id: doc._id }, { $set: { posts: [] } }, (err, result) => {
        if (err) {
          console.log('Error on update place posts');
          return res.status(500).send('We failed to update the place posts');
        }
        console.error(result);
      });
    });

    return res.status(200).send('Updated successfully');
  })
}

export function updateBroadcast(req, res) {
  Places.find((err, places) => {
    places.forEach((doc) => {
      const broadcastGamesLength = doc.broadcastGames ? doc.broadcastGames.length : 0
      if (broadcastGamesLength > 0) {
        const broadcastGames = doc.broadcastGames.map(item => ({
          ...item,
          id: item._id,
        }));
        Places.update({ _id: doc._id }, { $set: { broadcastGames } }, (err, result) => {
          if (err) {
            console.log('Error on update broadcast');
            return res.status(500).send('We failed to update the game broadcast');
          }
          console.error(result);
        });
      }
    });

    return res.status(200).send('Updated successfully');
  })
}

// export function updateLogo(req, res) {
//   Places.find((err, places) => {
//     places.forEach((doc) => {
//       const updatedValue = doc.logo.responseText.replace('app/images/uploads', 'https://botecofc.s3.amazonaws.com/uploads');
//       Places.update({ _id: doc._id }, { $set: { logo: { responseText: updatedValue } } }, (err, result) => {
//         if (err) {
//           console.log('Error on update logo');
//           return res.status(500).send('We failed to update the championship logo');
//         }
//         console.error(result);
//       });
//     });

//     return res.status(200).send('Updated successfully');
//   })
// }

// export function updateGallery(req, res) {
//   Places.find((err, places) => {
//     places.forEach(doc => {
//       const newGalery = doc.gallery && doc.gallery.length > 1
//         ? doc.gallery.map(gallery => {
//           if (gallery && gallery.responseText) {
//             const updatedValue = gallery.responseText.replace('app/images/uploads', 'https://botecofc.s3.amazonaws.com/uploads');
//             gallery.responseText = updatedValue;
//           }
//           return gallery;
//         })
//         : [];

//       Places.update({ _id: doc._id }, { $set: { gallery: newGalery } }, (err, result) => {
//         if (err) {
//           console.error('Error on update gallery');
//           return res.status(500).send('We failed to update the place gallery');
//         }
//         console.error(result);
//       });
//     });

//     return res.status(200).send('Updated successfully');
//   })
// }

export default {
  add,
  all,
  cleanPlace,
  remove,
  resetCounter,
  update,
  updateLocale,
  updatePosts,
  updateBroadcast,
  // updateGallery,
  // updateLogo,
};
