/**
 * Defining a User Model in mongoose
 * Code modified from https://github.com/sahat/hackathon-starter
 */

import bcrypt from 'bcrypt-nodejs';
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

/*
 User Schema
 */

const UserSchema = new mongoose.Schema({
    // id: { type: String, unique: true, default: '' },
    email: { type: String, unique: true, lowercase: true },
    name: { type: String, required: false, default: '' },
    password: String,
    role: { type: String, required: true, default: 'subscriber' },
    tokens: Array,
    resetPasswordToken: String,
    resetPasswordExpires: Date,
    facebook: {},
    google: {},
    twitter: {},
    profile: {
        birthday: { type: String, default: '' },
        city: { type: String, default: '' },
        cityId: String,
        stateId: String,
        gender: { type: String, default: '' },
        phone: { type: String, default: '' },
        picture: { type: String, default: '' },
        team: { type: Object, required: false, default: {} },
        terms: { type: Boolean, required: true, default: false },
    },
    favorites: { type: Array, required: false, default: [] },
    ownerships: { type: Array, required: false, default: [] },
    watch: { type: Array, required: false, default: [] },
    date: { type: Date, default: Date.now }
});

function encryptPassword(next) {
    const user = this;
    if (!user.isModified('password')) return next();
    return bcrypt.genSalt(5, (saltErr, salt) => {
        if (saltErr) return next(saltErr);
        return bcrypt.hash(user.password, salt, null, (hashErr, hash) => {
            if (hashErr) return next(hashErr);
            user.password = hash;
            return next();
        });
    });
}

/**
 * Password hash middleware.
 */
UserSchema.pre('save', encryptPassword);

/*
 Defining our own custom document instance method
 */
UserSchema.methods = {
  comparePassword(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
      if (err) return cb(err);
      return cb(null, isMatch);
    });
  }
};

/**
 * Statics
 */

UserSchema.statics = {};

export default mongoose.model('User', UserSchema);
