/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const StatesSchema = new mongoose.Schema({
  abbr: String,
  id: String,
  isActive: { type: Boolean, default: false },
  name: String,
});

export default mongoose.model('States', StatesSchema);

