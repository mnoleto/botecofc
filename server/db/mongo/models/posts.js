/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const PostsSchema = new mongoose.Schema({
  id: String,
  image: Object,
  font: String,
  games: [],
  place_id: String,
  text: String,
  date: { type: Date, default: Date.now }
});

// Compiles the schema into a model, opening (or creating, if nonexistent) the 'Posts' collection in the MongoDB database
export default mongoose.model('Posts', PostsSchema);