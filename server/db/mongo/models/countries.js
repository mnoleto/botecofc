/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const CountriesSchema = new mongoose.Schema({
  id: String,
  name: String,
  slug: String,
  date: { type: Date, default: Date.now }
});

// Compiles the schema into a model, opening (or creating, if nonexistent) the 'Countries' collection in the MongoDB database
export default mongoose.model('Countries', CountriesSchema);