
/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const BannerSchema = new mongoose.Schema({
  cityId: String,
  date: { type: Date, default: Date.now },
  endDate: { type: Date, default: Date.now },
  id: String,
  image: { type: Object, default: {}},
  imageMobile: Object,
  link: String,
  name: String,
  stateId: String,
  startDate: { type: Date, default: Date.now },
  tracking: String,
});

export default mongoose.model('Banners', BannerSchema);