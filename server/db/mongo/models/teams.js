/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const TeamsSchema = new mongoose.Schema({
	id: String,
	name: String,
	slug: String,
	sport: { id: String, name: String },
	country: { id: String, name: String },
	logo: Object,
	color: { type: String, default: '#000000' },
	textColor: { type: String, default: '#ffffff' },
	score: { type: Number, default: 0} , // Everytime a user say theywatch games of a team at a place, the score will raise
	date: { type: Date, default: Date.now }
});

// Compiles the schema into a model, opening (or creating, if nonexistent) the 'Teams' collection in the MongoDB database
export default mongoose.model('Teams', TeamsSchema);