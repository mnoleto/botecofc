/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const PostsBgsSchema = new mongoose.Schema({
  date: { type: Date, default: Date.now },
  portrait: Object,
  rect: Object,
});

// Compiles the schema into a model, opening (or creating, if nonexistent) the 'PostsBgs' collection in the MongoDB database
export default mongoose.model('PostsBgs', PostsBgsSchema);