/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const ChannelsSchema = new mongoose.Schema({
    id: String,
    logo: Object,
    name: String,
    date: { type: Date, default: Date.now }
});

// Compiles the schema into a model, opening (or creating, if nonexistent) the 'Channels' collection in the MongoDB database
export default mongoose.model('Channels', ChannelsSchema);