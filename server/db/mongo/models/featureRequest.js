/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const FeatureRequestSchema = new mongoose.Schema({
  id: String,
  place_id: String,
  feature: String,
  date: { type: Date, default: Date.now }
});

// Compiles the schema into a model, opening (or creating, if nonexistent) the 'FeatureRequest' collection in the MongoDB database
export default mongoose.model('FeatureRequest', FeatureRequestSchema);