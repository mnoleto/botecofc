/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const OwnershipsSchema = new mongoose.Schema({
  id: String,
  place: {
  	id: String,
  	name: String,
  },
  user: {
  	name: String,
    email: String,
  },
  date: { type: Date, default: Date.now }
});

// Compiles the schema into a model, opening (or creating, if
//	nonexistent) the 'OwnershipsSchema' collection in the MongoDB database
export default mongoose.model('Ownerships', OwnershipsSchema);

