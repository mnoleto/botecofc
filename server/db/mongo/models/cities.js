/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const CitiesSchema = new mongoose.Schema({
  id: String,
  isActive: { type: Boolean, default: false },
  isDefault: { type: Boolean, default: false },
  name: String,
  state_id: String,
});

export default mongoose.model('Cities', CitiesSchema);


