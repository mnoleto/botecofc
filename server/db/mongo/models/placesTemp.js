/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const PlacesTempSchema = new mongoose.Schema({
	about: String,
	address: String,
	cards: Object,
	city: String,
	cityId: String,
	cnpj: String,
	date: { type: Date, default: Date.now },
	email: String,
	facebook: String,
	gallery: [],
	googleplus: String,
	id: String,
	instagram: String,
	logo: Object,
	name: String,
	opening: Object,
	ownerName: String,
	ownerships: { type: Object, required: false, default: {} },
	phone: String,
	services: Object,
	slug: String,
	stateId: String,
	teamOficial: Object,
	twitter: String,
	website: String,
	zipcode: String,
});

export default mongoose.model('PlacesTemp', PlacesTempSchema);