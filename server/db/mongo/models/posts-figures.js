/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const PostsFiguresSchema = new mongoose.Schema({
  date: { type: Date, default: Date.now },
  image: Object,
});

// Compiles the schema into a model, opening (or creating, if nonexistent) the 'PostsFigures' collection in the MongoDB database
export default mongoose.model('PostsFigures', PostsFiguresSchema);