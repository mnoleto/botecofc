/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const SportsSchema = new mongoose.Schema({
  id: String,
  name: String,
  slug: String,
  date: { type: Date, default: Date.now }
});

// Compiles the schema into a model, opening (or creating, if nonexistent) the 'Sports' collection in the MongoDB database
export default mongoose.model('Sports', SportsSchema);