/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const PlacesSchema = new mongoose.Schema({
	about: String,
	address: String,
	broadcastGames: { type: Array, required: false, default: [] },
	canGeneratePosts: { type: Boolean, default: false },
	cards: Object,
	city: String,
	cityId: String,
	cnpj: String,
	date: { type: Date, default: Date.now },
	email: String,
	facebook: String,
	gallery: [],
	googleplus: String,
	id: String,
	instagram: String,
	logo: Object,
	name: String,
	numberOfPosts: { type: Number, min: 0 },
	opening: Object,
	ownerName: String,
	ownerships: { type: Object, required: false, default: {} },
	phone: String,
	posts: { type: Array, required: false, default: [] },
	recomendations: { type: Array, required: false, default: [] },
	services: Object,
	slug: String,
	stateId: String,
	status: { type: Boolean, default: true },
	teamOficial: Object,
	twitter: String,
	website: String,
	zipcode: String,
});

// Compiles the schema into a model, opening (or creating, if nonexistent) the 'Places' collection in the MongoDB database
export default mongoose.model('Places', PlacesSchema);