/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const PlacesSuggestionsSchema = new mongoose.Schema({
  date: { type: Date, default: Date.now },
  id: String,
  place: {
  	address: String,
  	name: String,
    city: String,
    cityId: String,
    reference: String,
    stateId: String,
  },
  user: {
  	id: String,
  	name: String,
    email: String,
  },
});

export default mongoose.model('PlacesSuggestions', PlacesSuggestionsSchema);

