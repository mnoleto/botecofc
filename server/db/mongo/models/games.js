/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const GamesSchema = new mongoose.Schema({
	id: String,
	city: String,
	stadium: String,
	channels: [],
	championship: {
		id: String,
		name: String,
	},
	homeTeam: {
		id: String,
		name: String,
	},
	visitingTeam: {
		id: String,
		name: String,
	},
	places: [],
	gameDate: { type: Date, default: Date.now },
	date: { type: Date, default: Date.now }
});

// Compiles the schema into a model, opening (or creating, if nonexistent) the 'Games' collection in the MongoDB database
export default mongoose.model('Games', GamesSchema);