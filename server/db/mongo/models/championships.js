/**
 * Schema Definitions
 *
 */
import mongoose from 'mongoose';

const ChampionshipsSchema = new mongoose.Schema({
  id: String,
  name: String,
  slug: String,
  dateBegin: Date,
  dateEnd: Date,
  logo: Object,
  countries: Object,
  sports: Object,
  teams: Object,
  date: { type: Date, default: Date.now }
});

// Compiles the schema into a model, opening (or creating, if nonexistent) the 'Championships' collection in the MongoDB database
export default mongoose.model('Championships', ChampionshipsSchema);