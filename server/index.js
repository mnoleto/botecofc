import express from 'express';
import webpack from 'webpack';
import { ENV } from './config/appConfig';
import { connect } from './db';
import passportConfig from './config/passport';
import expressConfig from './config/express';
import routesConfig from './config/routes';
import webpackDevConfig from '../webpack/webpack.config.dev-client';
import fs from 'fs';
import http from 'http';
import https from 'https';

const App = require('../public/assets/server');
const app = express();

/*
 * Database-specific setup
 * - connect to MongoDB using mongoose
 * - register mongoose Schema
 */
connect();

/*
 * REMOVE if you do not need passport configuration
 */
passportConfig();

if (ENV === 'development') {
  const compiler = webpack(webpackDevConfig);
  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: webpackDevConfig.output.publicPath
  }));

  app.use(require('webpack-hot-middleware')(compiler));
}

expressConfig(app);

routesConfig(app);

app.get('*', App.default);

app.get('*', App.default);

if (ENV === 'development') {
  const httpServer = http.createServer(app)
  httpServer.listen(app.get('port'));
} else {
  const privateKey = fs.readFileSync('/etc/letsencrypt/live/botecofc.com.br/privkey.pem', 'utf8');
  const certificate = fs.readFileSync('/etc/letsencrypt/live/botecofc.com.br/fullchain.pem', 'utf8')
  const ca = fs.readFileSync('/etc/letsencrypt/live/botecofc.com.br/chain.pem', 'utf8')
  const credentials = { key: privateKey, cert: certificate, ca }

  const httpsServer = https.createServer(credentials, app)
  httpsServer.listen(app.get('port'));

  http.createServer((req, res) => {
    res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
    res.end();
  }).listen(8080);

  // app.use (function (req, res, next) {
  //   if (req.secure) {
  //     // request was via https, so do no special handling
  //     next();
  //   } else {
  //     // request was via http, so redirect to https
  //     res.redirect('https://' + req.headers.host + req.url);
  //   }
  // });
}
