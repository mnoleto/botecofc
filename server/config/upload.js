import aws from 'aws-sdk';
import multer from 'multer';
import multerS3 from 'multer-s3';

import { ENV } from '../config/appConfig';
import { iamUser } from '../config/secrets';

const getFileName = (path, file, folder) => {
  let ext;
  switch (file.mimetype) { // *Mimetype stores the file type, set extensions according to filetype
    case 'image/jpeg':
      ext = '.jpeg';
      break;
    case 'image/png':
      ext = '.png';
      break;
    case 'image/gif':
      ext = '.gif';
      break;
  }
  const fileName = Date.now();
  return `${path}${folder}${fileName}${ext}`;
};

export default (app, folder, router, isSingle = true) => {
  let fileStorage;
  if (ENV === 'development') {
    const uploadPath = './app/images/uploads';
    fileStorage = multer.diskStorage({
      destination: `${uploadPath}/${folder}`, // Specifies upload location...
      filename: (req, file, cb) => {
        cb(null, getFileName('', file, ''));
      },
    });
  } else {
    const s3 = new aws.S3({
      accessKeyId: iamUser.ACCESS_KEY,
      secretAccessKey: iamUser.SECRET_KEY,
    });

    fileStorage = multerS3({
      s3,
      bucket: 'botecofc',
      metadata: function (req, file, cb) {
        cb(null, { fieldName: file.fieldname });
      },
      key: (req, file, cb) => {
        cb(null, getFileName('uploads/', file, folder));
      },
      cacheControl: 'max-age=31536000',
      acl: 'public-read',
      contentType: multerS3.AUTO_CONTENT_TYPE,
    })
  }

  const upload = multer({
    storage: fileStorage,
    limits: { fileSize: 41943040 },
  });

  if (isSingle) {
    app.post(router, upload.single('file'), function (req, res) {
      if (ENV === 'development') {
        res.send({
          responseText: `/${req.file.path}`,
          fileName: req.file.filename,
          fileType: req.file.mimetype,
        });
      } else {
        res.send({
          responseText: req.file.location,
          fileName: req.file.key,
          fileType: req.file.mimetype,
        });
      }
    });
  } else {
    app.post(router, upload.array('files[]', 20), function (req, res) {
      if (ENV === 'development') {
        res.send(req.files.map((file) => {
          return {
            responseText: `/${file.path}`,
            fileName: file.filename,
            fileType: file.mimetype,
          }
        }));
      } else {
        res.send(req.files.map((file) => {
          return {
            responseText: file.location,
            fileName: file.key,
            fileType: file.mimetype,
          }
        }));
      }
    });
  }
};