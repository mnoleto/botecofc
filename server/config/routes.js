/**
 * Routes for express app
 */
import passport from 'passport';
import nodemailer from 'nodemailer';
import unsupportedMessage from '../db/unsupportedMessage';
import { controllers, passport as passportConfig } from '../db';
import { gmail } from '../config/secrets';
import uploadFile from '../config/upload';
import jimp from '../jimp';


const bannersController = controllers && controllers.banners;
const championshipsController = controllers && controllers.championships;
const channelsController = controllers && controllers.channels;
const citiesController = controllers && controllers.cities;
const countriesController = controllers && controllers.countries;
const featureRequestController = controllers && controllers.featureRequest;
const gamesController = controllers && controllers.games;
const ownershipsController = controllers && controllers.ownerships;
const placesController = controllers && controllers.places;
const placesSuggestionsController = controllers && controllers.placesSuggestions;
const placesTempController = controllers && controllers.placesTemp;
const postsController = controllers && controllers.posts;
const postsBgsController = controllers && controllers.postsBgs;
const postsFiguresController = controllers && controllers.postsFigures;
const sportsController = controllers && controllers.sports;
const statesController = controllers && controllers.states;
const teamsController = controllers && controllers.teams;
const usersController = controllers && controllers.users;

export default (app) => {
	if (usersController) {
		app.get('/users', usersController.all);
		app.post('/login', usersController.login);
		app.post('/signup', usersController.signUp);
		app.put('/signup/:id', usersController.update);
		app.put('/user/:id', usersController.update);
		app.post('/register', usersController.register);
		app.post('/logout', usersController.logout);
		app.delete('/user/:id', usersController.remove);

		app.put('/addfavorite/:id', usersController.addToFavorite);
		app.put('/removefavorite/:id', usersController.removeFromFavorite);
	} else {
		console.warn(unsupportedMessage('users routes'));
	}

	// =====================================
	// GOOGLE ROUTES ======================
	// =====================================
	// route for google authentication and login
	if (passportConfig && passportConfig.google) {
		// google auth
		// Redirect the user to Google for authentication. When complete, Google
		// will redirect the user back to the application at
		// /auth/google/return
		// Authentication with google requires an additional scope param, for more info go
		// here https://developers.google.com/identity/protocols/OpenIDConnect#scope-param
		app.get('/auth/google', passport.authenticate('google', {
			scope: [
				'https://www.googleapis.com/auth/userinfo.profile',
				'https://www.googleapis.com/auth/userinfo.email'
			]
		}));

		// Google will redirect the user to this URL after authentication. Finish the
		// process by verifying the assertion. If valid, the user will be logged in.
		// Otherwise, the authentication has failed.
		app.get('/auth/google/callback',
			passport.authenticate('google', {
				failureRedirect: '/login-error',
			}),
			function (req, res, next) {
				if (!req.user.profile.terms) {
					res.redirect('/cadastro');
				} else {
					const returnTo = req.cookies.returnTo;
					delete req.cookies.returnTo;
					res.redirect(returnTo || '/');
				}
			}
		);
	}

	// =====================================
	// FACEBOOK ROUTES ======================
	// =====================================
	// route for facebook authentication and login
	if (passportConfig && passportConfig.facebook) {
		app.get('/auth/facebook', passport.authenticate('facebook', {
			scope: [
				'email'
			]
		}));

		app.get('/auth/facebook/callback',
			passport.authenticate('facebook', {
				failureRedirect: '/login-error'
			}),
			function(req, res, next) {
				if (!req.user.profile.terms) {
					res.redirect('/cadastro');
				} else {
					const returnTo = req.cookies.returnTo;
					delete req.cookies.returnTo;
					res.redirect(returnTo || '/');
				}
			}
		);
	}

	// championships routes
	if (championshipsController) {
		app.get('/championship', championshipsController.all);
		app.post('/championship/:id', championshipsController.add);
		app.put('/championship/:id', championshipsController.update);
		app.delete('/championship/:id', championshipsController.remove);

		// upload
		uploadFile(app, 'championships/', '/uploadChampionship');
	} else {
		console.warn(unsupportedMessage('championships routes'));
	}

	// countries routes
	if (countriesController) {
		app.get('/country', countriesController.all);
		app.post('/country/:id', countriesController.add);
		app.put('/country/:id', countriesController.update);
		app.delete('/country/:id', countriesController.remove);
	} else {
		console.warn(unsupportedMessage('countries routes'));
	}

	// games routes
	if (gamesController) {
		app.get('/game', gamesController.all);
		app.post('/game/:id', gamesController.add);
		app.put('/game/:id', gamesController.update);
		app.delete('/game/:id', gamesController.remove);
		app.get('/game-id', gamesController.updateGamesId);
	} else {
		console.warn(unsupportedMessage('games routes'));
	}

	// places routes
	if (placesController) {
		app.get('/place', placesController.all);
		app.post('/place/:id', placesController.add);
		app.put('/place/:id', placesController.update);
		app.delete('/place/:id', placesController.remove);

		app.get('/resetCounter', placesController.resetCounter)
		app.get('/update-locale', placesController.updateLocale)
		app.get('/update-posts', placesController.updatePosts)
		app.get('/place-clean', placesController.cleanPlace);
		app.get('/place-broadcast', placesController.updateBroadcast);

		// upload
		uploadFile(app, 'places/', '/uploadPlace');
	} else {
		console.warn(unsupportedMessage('places routes'));
	}

	// places suggestions routes
	if (placesSuggestionsController) {
		app.get('/placeSuggestion', placesSuggestionsController.all);
		app.post('/placeSuggestion/:id', placesSuggestionsController.add);
		app.delete('/placeSuggestion/:id', placesSuggestionsController.remove);
	} else {
		console.warn(unsupportedMessage('places suggestions routes'));
	}

	// places routes
	if (placesTempController) {
		app.get('/placeTemp', placesTempController.all);
		app.post('/placeTemp/:id', placesTempController.add);
		app.put('/placeTemp/:id', placesTempController.update);
		app.delete('/placeTemp/:id', placesTempController.remove);
		app.get('/placeTemp-clean', placesTempController.cleanPlace);
	} else {
		console.warn(unsupportedMessage('placesTemp routes'));
	}

	// ownership routes
	if (ownershipsController) {
		app.get('/ownership', ownershipsController.all);
		app.post('/ownership/:id', ownershipsController.add);
		app.delete('/ownership/:id', ownershipsController.remove);
	} else {
		console.warn(unsupportedMessage('ownerships routes'));
	}

	// sports routes
	if (sportsController) {
		app.get('/sport', sportsController.all);
		app.post('/sport/:id', sportsController.add);
		app.put('/sport/:id', sportsController.update);
		app.delete('/sport/:id', sportsController.remove);
	} else {
		console.warn(unsupportedMessage('sports routes'));
	}

	// teams routes
	if (teamsController) {
		app.get('/team', teamsController.all);
		app.post('/team/:id', teamsController.add);
		app.put('/team/:id', teamsController.update);
		app.delete('/team/:id', teamsController.remove);

		// upload
		uploadFile(app, 'teams/', '/uploadTeam');
	} else {
		console.warn(unsupportedMessage('teams routes'));
	}

	// channels routes
	if (channelsController) {
		app.get('/channel', channelsController.all);
		app.post('/channel/:id', channelsController.add);
		app.put('/channel/:id', channelsController.update);
		app.delete('/channel/:id', channelsController.remove);
	} else {
		console.warn(unsupportedMessage('channels routes'));
	}

	// cities routes
	if (citiesController) {
	  app.get('/active-city', citiesController.active);
	  app.get('/city', citiesController.all);
	  app.post('/city/:id', citiesController.add);
	  app.get('/add-all-cities', citiesController.addAllCities);
	  app.put('/city/:id', citiesController.update);
	  app.delete('/city/:id', citiesController.remove);
	  app.get('/remove-all-cities', citiesController.removeAllCities);
	} else {
	  console.warn(unsupportedMessage('cities routes'));
	}

	// states routes
	if (statesController) {
	  app.get('/state', statesController.all);
	  app.get('/active-state', statesController.active);
	  app.post('/state/:id', statesController.add);
	  app.get('/add-all-states', statesController.addAllStates);
	  app.put('/state/:id', statesController.update);
	  app.delete('/state/:id', statesController.remove);
	  app.get('/remove-all-states', statesController.removeAllStates);
	} else {
	  console.warn(unsupportedMessage('states routes'));
	}

	// posts routes
	if (postsController) {
		app.get('/post', postsController.all);
		app.post('/post/:id', postsController.add);
		app.put('/post/:id', postsController.update);
		app.delete('/post/:id', postsController.remove);

		// upload
		uploadFile(app, 'posts/', '/uploadPost');
	} else {
		console.warn(unsupportedMessage('posts routes'));
	}

	// posts bgs routes
	if (postsBgsController) {
		app.get('/bgs', postsBgsController.all);
		app.post('/post-bgs', postsBgsController.add);
		app.put('/post-bgs/:id', postsBgsController.update);
		app.delete('/post-bgs/:id', postsBgsController.remove);

		// upload
		uploadFile(app, 'posts-bgs/', '/uploadPostBgs');
	} else {
		console.warn(unsupportedMessage('posts bgs routes'));
	}

	// posts figures routes
	if (postsFiguresController) {
		app.get('/figures', postsFiguresController.all);
		app.post('/post-figures', postsFiguresController.add);
		app.put('/post-figures/:id', postsFiguresController.update);
		app.delete('/post-figures/:id', postsFiguresController.remove);

		// upload
		uploadFile(app, 'posts-figures/', '/uploadPostFigures', false);
	} else {
		console.warn(unsupportedMessage('posts figures routes'));
	}

	// feature request routes
	if (featureRequestController) {
		app.get('/featureRequest', featureRequestController.all);
		app.post('/featureRequest/:id', featureRequestController.add);
		app.put('/featureRequest/:id', featureRequestController.update);
		app.delete('/featureRequest/:id', featureRequestController.remove);
	} else {
		console.warn(unsupportedMessage('features routes'));
	}

	// banners routes
	if (bannersController) {
		app.get('/banner', bannersController.all);
		app.post('/banner/:id', bannersController.add);
		app.put('/banner/:id', bannersController.update);
		app.delete('/banner/:id', bannersController.remove);

		// upload
		uploadFile(app, 'banners/', '/uploadBanner');
	} else {
		console.warn(unsupportedMessage('banner routes'));
	}

	
	app.post('/createPost', jimp);

	/*
	 * E-MAIL FEEDBACKS
	 */
	// Cre­at­ing a trans­port object in your route handler.
	const transporter = nodemailer.createTransport({
		service: 'Gmail',
		debud: true,
		auth: {
			// XOAuth2: {
			// 	user: gmail.account,
			// 	clientId: gmail.clientId,
			// 	clientSecret: gmail.clientSecret,
			// 	refreshToken: gmail.refreshToken
			// }
			user: gmail.mailUser, // Your email id
			pass: gmail.mailPass // Your password
		}
	});

	// router for send the email with place suggestion status
	app.post('/sendSuggestionStatus', function(req, res) {
		var mailOptions = {
			from: 'contato@botecofc.com.br', // sender address
			to: req.body.email, // list of receivers
			subject: 'Status da sua sugestão', // Subject line
			html:
			'<center><table width="598px" cellspacing="0" cellpadding="0">' +
				'<tr height="40px"><td width="35px">&nbsp;</td><td>&nbsp;</td><td width="15px">&nbsp;</td></tr>' +
				'<tr><td colspan="3"><img src="http://35.161.121.80:3000/images/emails/header.jpg" alt="BotecoFC"></td></tr>' +
				'<tr><td width="35px"></td><td>' +
					'<h1>Oi, ' + req.body.name + '</h1>' +
					'Prepara aí que temos uma boa notícia!<br />Você sugeriu o estabelecimento ' + req.body.placename + '  ele já está publicado na rede do Boteco FC! Obrigado pela ajuda, hein? ' +
					'Assim você nos ajuda a criar um ambiente bem melhor pra todo mundo. E se precisar de alguma coisa, por favor, nos avise em atendimento@botecofc.com.br.</p>' +
					'<p>:)</p>' +
				'</td><td width="15px"></td></tr>' +
				'<tr><td colspan="3"><img src="http://35.161.121.80:3000/images/emails/footer.jpg" alt="BotecoFC"></td></tr>' +
				'<tr><td width="35px"></td><td>' +
					'<p>Esta mensagem foi enviada para ' + req.body.email + '. Caso não pretenda receber emails do Boteco FC anule aqui sua subscrição.</p>' +

					'Termos de utilização | Política de privacidade | Contato' +
					'<p>Brasília DF</p>' +
				'</td><td width="15px"></td></tr>' +
				'<tr height="30px"><td width="35px">&nbsp;</td><td>&nbsp;</td><td width="15px">&nbsp;</td></tr>' +
			'</table></center>'
		};

		transporter.sendMail(mailOptions, function(error, info){
			if(error){
				console.log(error);
				res.json({yo: 'error'});
			} else {
				res.json({yo: info.response});
			};
			transporter.close();
		});
	});

	// router for send the email with place register status
	app.post('/sendPlaceStatus', function(req, res) {
		var mailOptions = {
			from: 'contato@botecofc.com.br', // sender address
			to: req.body.email, // list of receivers
			subject: 'Status do Registro do bar', // Subject line
			html: '<h1>Oi, ' + req.body.name + '</h1>' +
			'Tudo bem? Vim aqui dizer que o cadastro do seu estabelecimento ' + req.body.placename + ' acaba de ser aprovado! É um grande prazer ter você como parceiro. Já é possível editar seus dados, fazer posts de fotos, promoções e mostrar para seus clientes tudo que seu local oferece. ' +
			'<a href="http://botecofc.com.br/editar-bar">CLIQUE AQUI</a> e acesse seu perfil no Boteco FC agora mesmo. E se precisar de alguma coisa, por favor, nos avise em atendimento@botecofc.com.br.</p>' +
			'<p>:)</p>' +
			'<p>Um abraço,<br /> Pedro Lucena<br />Fundador do Boteco FC<br />botecofc.com.br</p>'
		};

		transporter.sendMail(mailOptions, function(error, info){
			if(error){
				console.log(error);
				res.json({yo: 'error'});
			}else{
				res.json({yo: info.response});
			};
		});
	});

	// router for send the email with the user role information
	app.post('/sendUserRoleChange', function(req, res) {
		var mailOptions = {
			from: 'contato@botecofc.com.br', // sender address
			to: req.body.email, // list of receivers
			subject: 'Dono de bar', // Subject line
			html: '<h1>Oi, ' + req.body.name + '</h1>' +
			'<p>Tudo bem? Conforme contato que fizemos com seu estabelecimento informamos que você já pode gerenciar o perfil do ' + req.body.place +
			' no Boteco FC! É um prazer ter você como parceiro.Já é possível editar seus dados, fazer posts de fotos, promoções e mostrar para seus clientes tudo que seu local oferece.' +
			' <a href="http://botecofc.com.br/editar-bar">CLIQUE AQUI</a> e acesse seu perfil agora mesmo. E se precisar de alguma coisa, por favor, nos avise em atendimento@botecofc.com.br.</p>' +
			'<p>Um abraço,<br /> Pedro Lucena<br />Fundador do Boteco FC<br />botecofc.com.br</p>'
		};

		transporter.sendMail(mailOptions, function(error, info){
			if(error){
				console.log(error);
				res.json({yo: 'error'});
			}else{
				res.json({yo: info.response});
			};
		});
	});
};
