import { Strategy as TwitterStrategy } from 'passport-twitter';
import { twitter } from '../secrets';
import unsupportedMessage from '../../db/unsupportedMessage';
import { passport as dbPassport } from '../../db';

export default (passport) => {
  if (!dbPassport || !dbPassport.twitter || ! typeof dbPassport.twitter === 'function') {
    console.warn(unsupportedMessage('passport-twitter'));
    return;
  }

  passport.use(new TwitterStrategy({
    consumerKey: twitter.consumerKey,
    consumerSecret: twitter.consumerSecret,
    callbackURL: twitter.callbackURL
  }, dbPassport.twitter));
};
