import Jimp from 'jimp';

export default class ImagesConfig {
  constructor(containerHeight, containerWidth, numberOfGames) {
    this.containerHeight = containerHeight;
    this.containerWidth = containerWidth;
    this.numberOfGames = numberOfGames;

    this.margin = 40;
    this.AUTO = Jimp.AUTO;
    this.BOTTOM = this.containerHeight - this.margin;
    this.CENTER = (this.containerWidth / 2) + (this.margin / 2);
    this.LEFT = this.margin;
    this.MIDDLE = this.containerWidth / 2;
    this.RIGHT = this.containerWidth - this.margin;
    this.TOP = this.margin;

    this.boteco = {
      w: this.AUTO,
      h: 40,
      x: this.LEFT + 15,
      y: this.BOTTOM - 50,
    };

    this.figure = {
      w: 210,
      h: this.AUTO,
      x: this.LEFT + 65,
      y: this.BOTTOM - 280,
    };

    this.homeTeam = {
      x: 0,
      y: 0,
    };

    this.text = {
      w: this.AUTO,
      h: 95,
      x: this.CENTER - 190,
      y: this.TOP + 50,
    }

    this.logo = {
      w: 170,
      h: this.AUTO,
      x: this.RIGHT - 170 - 20,
      y: this.BOTTOM - 180,
    };

    this.gameBase = {
      boteco: this.boteco,
      figure: this.figure,
      logo: this.logo,
      text: this.text,
    }

    this.team1 = {
      w: 170,
      h: this.AUTO,
    };

    this.team2 = {
      w: 110,
      h: this.AUTO,
    };

    this.visiting1 = {
      x: 290,
      y: 0,
    };

    this.visiting2 = {
      x: 200,
      y: 0,
    };

    this.versus1 = {
      x: 215,
      y: 50,
    };

    this.versus2 = {
      x: 145,
      y: 15,
    };

    this.game1Rect = {
      team: this.team1,
      homeTeam: this.homeTeam,
      visitingTeam: this.visiting1,
      versus: this.versus1,
      day: {
        w: 250,
        h: this.AUTO,
        x: 210,
        y: 260,
      },
      time: {
        w: 275,
        h: this.AUTO,
        x: 170,
        y: 360,
      },
      championship: {
        w: this.AUTO,
        h: 50,
        x: this.RIGHT,
        y: 190,
      },
    }

    this.game1Portrait = {
      team: this.team1,
      homeTeam: this.homeTeam,
      visitingTeam: this.visiting1,
      versus: {
        x: 205,
        y: 50,
      },
      day: {
        w: 250,
        h: this.AUTO,
        x: 210,
        y: 260,
      },
      time: {
        w: 275,
        h: this.AUTO,
        x: 170,
        y: 360,
      },
      championship: {
        w: this.AUTO,
        h: 50,
        x: this.RIGHT,
        y: 190,
      },
    }

    this.game2 = {
      team: this.team2,
      homeTeam: this.homeTeam,
      visitingTeam: this.visiting2,
      versus: this.versus2,
      day: {
        w: 130,
        h: this.AUTO,
        x: 393,
        y: 20,
      },
      time: {
        w: 140,
        h: this.AUTO,
        x: 373,
        y: 60,
      },
      championship: {
        w: this.AUTO,
        h: 30,
        x: 0,
        y: 125,
      },
    }

    this.rectConfig = {
      // template for 1 game
      1: {
        ...this.gameBase,
        game: {
          ...this.game1Rect,
        },
        games: [
          {
            x: this.RIGHT - 440,
            y: this.TOP + 200,
          },
        ],
      },
      // template for 2 games
      2: {
        ...this.gameBase,
        game: {
          ...this.game2,
        },
        games: [
          {
            x: 237,
            y: this.TOP + 240,
          },
          {
            x: 237,
            y: this.TOP + 440,
          },
        ],
      },
      // template for 3 games
      3: {
        ...this.gameBase,
        game: {
          ...this.game2,
        },
        games: [
          {
            x: 237,
            y: 250,
          },
          {
            x: 237,
            y: 450,
          },
          {
            x: 237,
            y: 650,
          },
        ],
      },
    };

    this.portraitConfig = {
      // template for 1 game
      1: {
        ...this.gameBase,
        game: {
          ...this.game1Portrait,
        },
        games: [
          {
            x: this.RIGHT - 440,
            y: this.TOP + 300,
          },
        ],
      },
      // template for 2 games
      2: {
        ...this.gameBase,
        game: {
          ...this.game2,
        },
        games: [
          {
            x: 100,
            y: this.TOP + 350,
          },
          {
            x: 100,
            y: this.TOP + 610,
          },
        ],
      },
      // template for 3 games
      3: {
        ...this.gameBase,
        game: {
          ...this.game2,
        },
        games: [
          {
            x: 120,
            y: 300,
          },
          {
            x: 120,
            y: 500,
          },
          {
            x: 120,
            y: 700,
          },
        ],
      },
    };
  }

  basic(object, property, index) {
    if (this.containerWidth > 1000) {
      return this.rectConfig[this.numberOfGames][object][property];
    }
    return this.portraitConfig[this.numberOfGames][object][property];
  }

  games(property, index) {
    if (this.containerWidth > 1000) {
      return this.rectConfig[this.numberOfGames]['games'][index][property];
    }
    return this.portraitConfig[this.numberOfGames]['games'][index][property];
  }

  game(object, property) {
    if (this.containerWidth > 1000) {
      return this.rectConfig[this.numberOfGames]['game'][object][property];
    }
    return this.portraitConfig[this.numberOfGames]['game'][object][property];
  }
}
