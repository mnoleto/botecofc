import Jimp from 'jimp';
import * as filestack from 'filestack-js'
import { ENV } from '../config/appConfig';
import ImagesConfig from './config';

const filestackClient = filestack.init('AaI8a4ETgy6MQdb1KEywIz');
const rectSize = { w: 1024, h: 1024 };
const portraitSize = { w: 750, h: 1334 };

const loadImage = (url, type) => {
  return Jimp.read({
    url,
    headers: { rejectUnauthorized: false, strictSSL: false },
  }).catch(err => console.error(type, err));
}

const createImage = ({
  bg,
  botecoLogo,
  fcLogo,
  emptyJimpBase,
  figure,
  games,
  isStory,
  size,
  text,
  versus,
}) => {
  const config = new ImagesConfig(size.h, size.w, games.length);

  const finalImage =
    emptyJimpBase
      // .clone()
      // background image
      .composite(
        bg.cover(size.w, size.h),
        0,
        0
      )
      // text
      .composite(
        text.resize(config.basic('text', 'w'), config.basic('text', 'h')),
        config.basic('text', 'x'),
        config.basic('text', 'y')
      )
      // botecoFC logo
      .composite(
        fcLogo.resize(config.basic('boteco', 'w'), config.basic('boteco', 'h')),
        config.basic('boteco', 'x'),
        config.basic('boteco', 'y')
      )
      // boteco logo
      .composite(
        botecoLogo.resize(config.basic('logo', 'w'), config.basic('logo', 'h')),
        config.basic('logo', 'x'),
        config.basic('logo', 'y')
      );
    
    // figure
    if (isStory && figure != null) {
      finalImage.composite(
        figure.resize(config.basic('figure', 'w'), config.basic('figure', 'h')),
        config.basic('figure', 'x'),
        config.basic('figure', 'y')
      )
    }
    
    games.forEach((game, index) => {
      finalImage
        .composite(
          game.homeTeam.image.resize(config.game('team', 'w'), config.game('team', 'h')),
          config.games('x', index) + config.game('homeTeam', 'x'),
          config.games('y', index) + config.game('homeTeam', 'y')
        )
        .composite(
          game.day.image.resize(config.game('day', 'w'), config.game('day', 'h')),
          config.games('x', index) + config.game('day', 'x'),
          config.games('y', index) + config.game('day', 'y'),
        )
        .composite(
          game.hour.image.resize(config.game('time', 'w'), config.game('time', 'h')),
          config.games('x', index) + config.game('time', 'x'),
          config.games('y', index) + config.game('time', 'y'),
        )
        .composite(
          game.championship.image.resize(config.game('championship', 'w'), config.game('championship', 'h')),
          (games.length > 1)
            ? config.games('x', index) + ((310 - game.championship.image.bitmap.width) / 2) // centered with the team logos
            : config.game('championship', 'x') - game.championship.image.bitmap.width,
          config.games('y', index) + config.game('championship', 'y'),
        )
        // versus
        .composite(
          versus,
          config.games('x', index) + config.game('versus', 'x'),
          config.games('y', index) + config.game('versus', 'y')
        )
        .composite(
          game.visitingTeam.image.resize(config.game('team', 'w'), config.game('team', 'h')),
          config.games('x', index) + config.game('visitingTeam', 'x'),
          config.games('y', index) + config.game('visitingTeam', 'y')
        );
    });

  return finalImage;
};

const createRectImage = (result, games, typeOfImage) => {
  return createImage({
    bg: typeOfImage === 'custom' ? result[0] : result[1],
    botecoLogo: result[7],
    emptyJimpBase: result[4],
    fcLogo: result[6],
    figure: result[9],
    games,
    isStory: false,
    size: rectSize,
    text: result[5],
    versus: result[8],
  });
};

const createPortraitImage = (result, games, typeOfImage) => {
  return createImage({
    bg: typeOfImage === 'custom' ? result[0] : result[2],
    botecoLogo: result[7],
    emptyJimpBase: result[3],
    fcLogo: result[6],
    figure: result[9],
    games,
    isStory: true,
    result,
    size: portraitSize,
    text: result[5],
    versus: result[8],
  });
};

// Jimp#getBase64 still doesn't return promise, this method helps us
const encodeJimpImage = (image, mime) => {
  return new Promise((fulfil, reject) => {
    image.getBase64(mime, (err, data) => {
      if (err) reject(err);
      fulfil(data);
    }).quality(70);
  });
};

const imagePath = ENV === 'production' ? 'https://botecofc.com.br' : 'http://localhost:3000';
const getPath = (path) => {
  return ENV === 'production' ? path : `${imagePath}${path}`;
}

export default async (req, res) => {
  const {
    background,
    figure,
    games,
    logo,
    logoBoteco,
    portrait,
    rect,
    text,
    typeOfImage,
    versus
  } = req.body;

  // empty Jimp image
  const basePortraitPromise = new Jimp(portraitSize.w, portraitSize.h).background(0x00000000);
  const baseRectPromise = new Jimp(rectSize.w, rectSize.h).background(0x00000000);

  const botecoLogoPromise = Jimp.read(
    Buffer.from(logoBoteco.replace(/^data:image\/png;base64,/, ''), 'base64')
  );

  let backgroundPromise = null;
  let rectPromise = null;
  let portraitPromise = null;
  if (typeOfImage === 'custom') {
    const bgPath = getPath(background);
    backgroundPromise = loadImage(bgPath, 'background');
  } else {
    const rectPath = getPath(rect);
    rectPromise = loadImage(rectPath, 'background');
    
    const portraitPath = getPath(portrait);
    portraitPromise = loadImage(portraitPath, 'background');
  }

  const figurePath = getPath(figure);

  const figurePromise = loadImage(figurePath, 'figure');
  const versusPromise = loadImage(`${imagePath}${versus}`, 'versus');
  const textPromise = loadImage(`${imagePath}${text}`, 'text');
  
  const transformedLogoUrl = logo.replace(/ /g, "%20");
  const options = {
    circle: true,
  }
  const transformedUrl = filestackClient.transform(
    transformedLogoUrl,
    options,
    true
  )
  const logoPromise = loadImage(transformedUrl, 'logo');

  const promisesArr = [
    backgroundPromise,
    rectPromise,
    portraitPromise,
    basePortraitPromise,
    baseRectPromise,
    textPromise,
    botecoLogoPromise,
    logoPromise,
    versusPromise,
    figurePromise,
  ];

  const visitingTeams = games.map(game => loadImage(game.visitingTeam.logo.replace(/ /g, "%20"), 'visiting'));
  const homeTeams = games.map(game => loadImage(game.homeTeam.logo.replace(/ /g, "%20"), 'home'));
  const gameDay = games.map(game => loadImage(`${imagePath}/app/images/post/days/${game.day}.png`), 'day');
  const gameHour = games.map(game => loadImage(`${imagePath}/app/images/post/hours/${game.hour}.png`), 'hours');
  const championship = games.map(game => loadImage(`${imagePath}/app/images/post/championships/${game.championship.slug}.png`, 'championship'));

  let rectImage;
  let basicElements;
  let normalizedGames;

  return Promise.all([
    ...promisesArr,
    ...homeTeams,
    ...visitingTeams,
    ...gameDay,
    ...gameHour,
    ...championship,
  ])
    .then((result) => {
      basicElements = result.slice(0, 10);
      const gamesImages = result.slice(10);
      normalizedGames = games.map((game, index) => {
        return {
          ...game,
          homeTeam: {
            ...game.homeTeam,
            image: gamesImages[index],
          },
          visitingTeam: {
            ...game.visitingTeam,
            image: gamesImages[index + games.length],
          },
          day: {
            ...game.day,
            image: gamesImages[index + (games.length * 2)],
          },
          hour: {
            ...game.hour,
            image: gamesImages[index + (games.length * 3)],
          },
          championship: {
            ...game.championship,
            image: gamesImages[index + (games.length * 4)],
          }
        }
      });
      return createRectImage(
        basicElements,
        normalizedGames,
        typeOfImage
      );
    })
    .then(compositedImage => encodeJimpImage(compositedImage, Jimp.MIME_JPEG))
    .then(jpgImage => {
      rectImage = jpgImage;
      return createPortraitImage(
        basicElements,
        normalizedGames,
        typeOfImage
      );
    })
    .then(compositedImage => encodeJimpImage(compositedImage, Jimp.MIME_JPEG))
    .then(jpgImage => res.status(200).json({ horizontal: jpgImage, retangulo: rectImage}))
    .catch(err => {
      console.error(err);
      console.error(err.stack);
      res.status(500).send('We failed to create your post for some reason');
    });
};
