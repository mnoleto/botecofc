import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './containers/App';

// PUBLIC AREA
import BroadcastList from './containers/BroadcastList';
import BroadcastManage from './containers/BroadcastManage';
import Championship from './containers/Championship';
import Championships from './containers/Championships';
import Game from './containers/Game';
import Home from './containers/Home';
import Login from './containers/Login';
import LoginError from './components/login/LoginError';
import Place from './containers/Place';
import PlaceEdit from './containers/PlaceEdit';
import Places from './containers/Places';
import Policy from './components/policy';
import PostManage from './containers/PostManage';
import Register from './containers/Register';
import RegisterPlace from './containers/RegisterPlace';
import Team from './containers/Team';
import Teams from './containers/Teams';
import Terms from './components/terms';

// ADMIN
import Admin from './containers/admin/Admin';
import AdminBanners from './containers/admin/AdminBanners';
import AdminBroadcasts from './containers/admin/Broadcasts';
import AdminChampionships from './containers/admin/Championships';
import AdminChannels from './containers/admin/Channels';
import AdminCountries from './containers/admin/Countries';
import AdminCities from './containers/admin/Cities';
import AdminGames from './containers/admin/Games';
import AdminLogin from './containers/admin/Login';
import AdminPlaces from './containers/admin/Places';
import AdminPlacesSuggestions from './containers/admin/PlacesSuggestions';
import AdminPlacesTemp from './containers/admin/PlacesTemp';
import AdminSports from './containers/admin/Sports';
import AdminStates from './containers/admin/States';
import AdminTeams from './containers/admin/Teams';
import Dashboard from './containers/admin/Dashboard';
import FeatureToggle from './containers/admin/FeatureToggle';
import PostFigures from './containers/admin/PostFigures';
import PostBgs from './containers/admin/PostBgs';
import OwnershipsRequests from './containers/admin/OwnershipsRequests';
import Users from './containers/admin/Users';

/*
 * @param {Redux Store}
 * We require store as an argument here because we wish to get
 * state from the store after it has been authenticated.
 */
export default (store) => {
	const requireAuth = (nextState, replace, callback) => {
		const { user: { authenticated }} = store.getState();
		if (!authenticated) {
			replace({
				pathname: '/login',
				state: { nextPathname: nextState.location.pathname }
			});
		}
		callback();
	};

	const requireAdmin = (nextState, replace, callback) => {
		const { user: { authenticated, account }} = store.getState();
		if (!authenticated) {
			replace({
				pathname: '/bfc-admin/login',
				state: { nextPathname: nextState.location.pathname }
			});
		}
		if (account.role !== 'admin') {
			replace({
				pathname: '/',
				state: { nextPathname: nextState.location.pathname }
			});
		}
		callback();
	};

	const requireOwner = (nextState, replace, callback) => {
		const { user: { authenticated, account }} = store.getState();
		if (!authenticated) {
			replace({
				pathname: '/login',
				state: { nextPathname: nextState.location.pathname }
			});
		}
		if (account.role !== 'admin' && account.role !== 'owner') {
			replace({
				pathname: '/',
				state: { nextPathname: nextState.location.pathname }
			});
		}
		callback();
	};

	const redirectAuth = (nextState, replace, callback) => {
		const { user: { authenticated }} = store.getState();
		if (authenticated) {
			replace({
				pathname: '/bfc-admin'
			});
		}
		callback();
	};

	return (
		<Route
			path="/"
			component={App}
		>
			<IndexRoute component={Home} />

			<Route path="home" component={Home} />
			<Route path="campeonatos" component={Championships} />
			<Route path="campeonatos/:slug" component={Championship} />
			<Route path="bares" component={Places} />
			<Route path="bares/:slug" component={Place} />
			<Route path="times" component={Teams} />
			<Route path="times/:slug" component={Team} />
			<Route path="jogo/:id" component={Game} />
			<Route path="cadastro" onEnter={requireAuth} component={Register} />
			<Route path="login" component={Login} />
			<Route path="login-error" component={LoginError} />
			<Route path="cadastrar-bar" component={RegisterPlace} />
			<Route path="termos" component={Terms} />
			<Route path="politica-de-privacidade" component={Policy} />
			{/* OWNERS */}
			<Route path="editar-bar" onEnter={requireOwner} component={PlaceEdit} />
			<Route path="gerenciar-jogos" onEnter={requireOwner} component={BroadcastManage} />
			<Route path="gerenciar-jogos/confirmados" onEnter={requireOwner} component={BroadcastList} />
			<Route path="gerenciar-post" onEnter={requireOwner} component={PostManage} />

			<Route path="bfc-admin" component={Admin}>
				<IndexRoute component={Dashboard} onEnter={requireAdmin} />
				{/* ADMIN */}
				<Route path="login" component={AdminLogin} onEnter={redirectAuth} />
				<Route path="usuarios" component={Users} onEnter={requireAdmin} />
				<Route path="usuarios/solicitacoes" component={OwnershipsRequests} onEnter={requireAdmin} />
				<Route path="campeonatos" component={AdminChampionships} onEnter={requireAdmin} />
				<Route path="cidades" component={AdminCities} onEnter={requireAdmin} />
				<Route path="estados" component={AdminStates} onEnter={requireAdmin} />
				<Route path="paises" component={AdminCountries} onEnter={requireAdmin} />
				<Route path="jogos" component={AdminGames} onEnter={requireAdmin} />
				<Route path="bares" component={AdminPlaces} onEnter={requireAdmin} />
				<Route path="bares/aprovados" component={AdminPlaces} onEnter={requireAdmin} />
				<Route path="bares/pendentes" component={AdminPlacesTemp} onEnter={requireAdmin} />
				<Route path="bares/sugestoes" component={AdminPlacesSuggestions} onEnter={requireAdmin} />
				<Route path="bares/transmissoes" component={AdminBroadcasts} onEnter={requireAdmin} />
				<Route path="bares/post" component={FeatureToggle} onEnter={requireAdmin} />
				<Route path="figurinhas" component={PostFigures} onEnter={requireAdmin} />
				<Route path="bgs" component={PostBgs} onEnter={requireAdmin} />
				<Route path="esportes" component={AdminSports} onEnter={requireAdmin} />
				<Route path="times" component={AdminTeams} onEnter={requireAdmin} />
				<Route path="canais" component={AdminChannels} onEnter={requireAdmin} />
				<Route path="banners" component={AdminBanners} onEnter={requireAdmin} />
			</Route>
		</Route>
	);
};
