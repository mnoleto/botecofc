/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import * as types from '../types';

polyfill();

const makePostsRequest = (method, id, data, api = '/post-bgs') => {
  return request[method](api + (id ? ('/' + id) : ''), data);
}

const createBgsSuccess = (data) => {
	return {
		type: types.CREATE_BGS_SUCCESS,
		data,
	};
}
const createBgsFailure = (data) => {
	return {
		type: types.CREATE_BGS_FAILURE,
		error: data.error,
};
}

const createBgsRequest = (data) => {
	return {
		type: types.CREATE_BGS_REQUEST,
		bgs: data,
	};
}

const removeBgsSuccess = (req) => {
    return {
        type: types.REMOVE_BGS_SUCCESS,
        req,
    };
}
const removeBgsFailure = (data) => {
    return {
        type: types.REMOVE_BGS_FAILURE,
        error: data.error,
    };
}

const removeBgsRequest = (id) => {
    return {
        type: types.REMOVE_BGS_REQUEST,
        id,
    };
}

export function createBgs(data) {
	return (dispatch) => {
		dispatch(createBgsRequest(data));
		
		return makePostsRequest('post', null, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(createBgsSuccess(res));
				}
			})
			.catch((err) => {
				return dispatch(createBgsFailure({ error: 'Oops! Something went wrong and we couldn\'t create your post' }));
			});
	};
}

export function removeBg(id) {
	return (dispatch) => {
		dispatch(removeBgsRequest(id));
		
		return makePostsRequest('delete', id)
			.then(res => {
				if (res.status === 200) {
					return dispatch(removeBgsSuccess(res));
				}
			})
			.catch((err) => {
				return dispatch(removeBgsFailure({ error: 'Oops! Something went wrong and we couldn\'t remove your bg' }));
			});
	};
}

export function fetchPostsBgs() {
	return {
		type: types.GET_BGS,
		promise: makePostsRequest('get', null, null, '/bgs')
	};
}