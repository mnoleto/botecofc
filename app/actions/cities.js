/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

const makeCityRequest = (method, id, data, api = '/city') => {
  return request[method](api + (id ? ('/' + id) : ''), data);
}

/*
 * @param data
 * @return a simple JS object
 */
const createCityRequest = (data) => {
  return {
    type: types.CREATE_CITY_REQUEST,
    ...data,
  };
}

const createCitySuccess = () => {
  return {
    type: types.CREATE_CITY_SUCCESS
  };
}

const createCityFailure = (data) => {
  return {
    type: types.UPDATE_CITY_FAILURE,
    id: data.id,
    error: data.error
  };
}

const destroy = (id) => {
  return { type: types.DESTROY_CITY, id };
}

const updateCityRequest = (data) => {
  return {
    type: types.UPDATE_CITY_REQUEST,
    ...data,
  };
}

const updateCitySuccess = () => {
  return {
    type: types.UPDATE_CITY_SUCCESS
  };
}

const updateCityFailure = (data) => {
  return {
    type: types.UPDATE_CITY_FAILURE,
    id: data.id,
    error: data.error
  };
}

export const createCity = (obj) => {
  return (dispatch) => {
    // If the name box is empty
    if (obj.name.trim().length <= 0) return;

    const id = md5.hash(obj.name);
    const data = {
      ...obj,
      id: id,
    };

    // First dispatch an optimistic update
    dispatch(createCityRequest(data));

    return makeCityRequest('post', id, data)
      .then(res => {
        if (res.status === 200) {
          return dispatch(createCitySuccess());
        }
      })
      .catch(() => {
        return dispatch(createCityFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your city' }));
      });
  };
}

export const updateCity = (obj) => {
  return (dispatch) => {
    dispatch(updateCityRequest(obj));

    const data = {
      id: obj.id,
      isDefault: obj.isDefault,
      isActive: obj.isActive,
      name: obj.name,
      state_id: obj.stateId,
    }

    return makeCityRequest('put', data.id, data)
      .then(res => {
        if (res.status === 200) {
          return dispatch(updateCitySuccess());
        }
      })
      .catch(() => {
        return dispatch(updateCityFailure({ id: obj.id, error: 'Oops! Something went wrong and we couldn\'t update your city' }));
      });
  }
}


// Fetch posts logic
export const fetchCities = () => {
  return {
    type: types.GET_CITIES,
    promise: makeCityRequest('get')
  };
}

export const fetchActiveCities = () => {
  return {
    type: types.GET_ACTIVE_CITIES,
    promise: makeCityRequest('get', null, null, '/active-city')
  };
}


export const destroyCity = (id) => {
  return dispatch => {
    dispatch(destroy(ID));
    return makeCityRequest('delete', id)
  };
}

