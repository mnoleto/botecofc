/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

function makePlaceRequest(method, id, data, api = '/place') {
	return request[method](api + (id ? ('/' + id) : ''), data);
}

/*
 * @param data
 * @return a simple JS object
 */
function createPlaceRequest(data) {
	return {
		type: types.CREATE_PLACE_REQUEST,
		...data,
	};
}

function createPlaceSuccess() {
	return {
		type: types.CREATE_PLACE_SUCCESS
	};
}

function createPlaceFailure(data) {
	return {
		type: types.CREATE_PLACE_FAILURE,
		id: data.id,
		error: data.error
	};
}

export function createPlace(obj) {
	return (dispatch) => {
		if (obj.name.trim().length <= 0) return;

		const id = md5.hash(obj.name);

		dispatch(createPlaceRequest({...obj, id}));

		return makePlaceRequest('post', id, { ...obj, id })

			.then(res => {
				if (res.status === 200) {
					return dispatch(createPlaceSuccess());
				}
			})
			.catch(() => {
				return dispatch(createPlaceFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your place'}));
			});
	};
}

/*
 * UPDATE PLACE
 */
function updatePlaceRequest(data) {
	return {
		type: types.UPDATE_PLACE_REQUEST,
		...data,
	};
}

function updatePlaceSuccess(message) {
	return {
		type: types.UPDATE_PLACE_SUCCESS,
		message,
	};
}

function updatePlaceFailure(message) {
	return {
		type: types.UPDATE_PLACE_FAILURE,
		message,
	};
}

export function updatePlace(obj) {
	return (dispatch) => {

		const data = {
			...obj,
			id: obj.id === '' || obj.id == null ? md5.hash(obj.name) : obj.id,
			isIncrementPosts: false,
		};

		dispatch(updatePlaceRequest(data));

		return makePlaceRequest('put', data.id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(updatePlaceSuccess('success'));
				}
			})
			.catch(() => {
				return dispatch(updatePlaceFailure('error'));
			});
	};
}

/*
 * UPDATE OWNERSHIP
 */
function updateOwnershipRequest(index, data) {
	return {
		type: types.PLACE_OWNERSHIP_REQUEST,
		index: index,
		id: data.id,
		ownerships: data.ownerships
	};
}

function updateOwnershipSuccess() {
	return {
		type: types.PLACE_OWNERSHIP_SUCCESS
	};
}

function updateOwnershipFailure(data) {
	return {
		type: types.PLACE_OWNERSHIP_FAILURE,
		id: data.id,
		error: data.error
	};
}

export function updatePlaceOwnership(index, obj, callback) {
	return (dispatch) => {
		const data = {
			id: obj.id,
			ownerships: obj.user,
			isIncrementPosts: false,
		};

		dispatch(updateOwnershipRequest(index, data));

		return makePlaceRequest('put', data.id, data)
			.then(res => {
				if (res.status === 200) {
					if (callback) callback();
					return dispatch(updateOwnershipSuccess());
				}
			})
			.catch(() => {
				return dispatch(updateOwnershipFailure({ id: data.id, error: 'Oops! Something went wrong and we couldn\'t update your place'}));
			});
	};
}



/*
 * ADD BROADCAST GAMES
 */
function updateBroadcastGameRequest(data) {
	return {
		type: types.UPDATE_BROADCASTGAMES_REQUEST,
		id: data.id,
		broadcastGames: data.broadcastGames
	};
}

function updateBroadcastGameSuccess() {
	return {
		type: types.UPDATE_BROADCASTGAMES_SUCCESS
	};
}

function updateBroadcastGameFailure(data) {
	return {
		type: types.UPDATE_BROADCASTGAMES_FAILURE,
		id: data.id,
		error: data.error
	};
}

export function updateBroadcastGame(obj) {
	return (dispatch) => {
		const data = {
			...obj,
			isIncrement: false,
		}

		dispatch(updateBroadcastGameRequest(data));

		return makePlaceRequest('put', data.id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(updateBroadcastGameSuccess());
				}
			})
			.catch(() => {
				return dispatch(updateBroadcastGameFailure({ id: data.id, error: 'Oops! Something went wrong and we couldn\'t update your place'}));
			});
	};
}

export function approvePlace(obj, callback) {
	return (dispatch) => {
		if (obj.name.trim().length <= 0) return;

		const id = md5.hash(obj.name);
		const data = {
			...obj,
			id: id,
		};

		dispatch(createPlaceRequest(data));

		return makePlaceRequest('post', id, data)
			.then(res => {
				if (res.status === 200) {
					dispatch(createPlaceSuccess());
					return callback('success');
				}
			})
			.catch(() => {
				dispatch(createPlaceFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your place'}));
				return callback('error');
			});
	};
}

// RECOMENDATION
function userRecomendTeamRequest(data) {
	return {
		type: types.UPDATE_RECOMENDATION_REQUEST,
		id: data.id,
		recomendations: data.recomendations
	};
}

function userRecomendTeamSuccess() {
	return {
		type: types.UPDATE_RECOMENDATION_SUCCESS
	};
}

function userRecomendTeamFailure(data) {
	return {
		type: types.UPDATE_RECOMENDATION_FAILURE,
		id: data.id,
		error: data.error
	};
}

export function userRecomendTeam(obj) {
	return (dispatch) => {
		const data = {
			id: obj.id,
			recomendations: obj.recomendations,
			isIncrementPosts: false,
		};

		dispatch(userRecomendTeamRequest(data));

		return makePlaceRequest('put', data.id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(userRecomendTeamSuccess());
				}
			})
			.catch(() => {
				return dispatch(userRecomendTeamFailure({ id: data.id, error: 'Oops! Something went wrong and we couldn\'t update your place'}));
			});
	};
}

// Fetch posts logic
export function fetchPlaces() {
	return {
		type: types.GET_PLACES,
		promise: makePlaceRequest('get')
	};
}

/*
 * DESTROY
 */
function destroy(id) {
	return { type: types.DESTROY_PLACE, id };
}

export function destroyPlace(id, index) {
	return dispatch => {
		dispatch(destroy(id));
		return makePlaceRequest('delete', id);
	};
}

function visibilityFilter(filter, city ) {
	return {
		type: types.SET_PLACE_VISIBILITY_FILTER,
		show: filter,
		city
	};
}

export function setVisibilityFilter(filter, city) {
	return dispatch => {
		dispatch(visibilityFilter(filter, city));
	};
}

export function dismissMessage() {
	dispatch({
		type: types.DISMISS_MESSAGE,
		message: ''
	});
}

/* FEATURE REQUEST */
export function updateGeneratePostRequest({ id, canGeneratePosts }) {
	return {
		type: types.UPDATE_GENERATE_POSTS_REQUEST,
		id,
		canGeneratePosts,
	}
}

export function updateGeneratePostSuccess() {
	return {
		type: types.UPDATE_GENERATE_POSTS_SUCCESS,
	}
}

export function updateGeneratePostFailure({ id, error }) {
	return {
		type: types.UPDATE_GENERATE_POSTS_FAILURE,
		id,
		error,
	}
}

export function updateGeneratePosts(placeId, canGeneratePosts) {
	return dispatch => {
		const data = {
			id: placeId,
			canGeneratePosts,
			isIncrementPosts: false,
		};

		dispatch(updateGeneratePostRequest(data));

		return makePlaceRequest('put', data.id, data)
			.then(() => {
				return dispatch(updateGeneratePostSuccess());
			})
			.catch(() => {
				return dispatch(updateGeneratePostFailure({ id: data.id, error: 'Oops! Something went wrong and we couldn\'t update your place' }));
			});
	}
}

function increment(id) {
	return { type: types.INCREMENT_POSTS, id };
}

function incrementFail(id) {
	return { type: types.INCREMENT_POSTS_FAIL, id };
}

export function incrementPosts(id) {
	return (dispatch) => {
		return makePlaceRequest('put', id, {
			id,
			isIncrementPosts: true,
		})
			.then(() => dispatch(increment(id)))
			.catch(() => dispatch(incrementFail(id)));
	};
}