/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

function makeBannerRequest(method, id, data, api = '/banner') {
  return request[method](api + (id ? ('/' + id) : ''), data);
}

/*
 * @param data
 * @return a simple JS object
 */
function createBannerRequest(data) {
  return {
    type: types.CREATE_BANNER_REQUEST,
    ...data
  };
}

function createBannerSuccess() {
  return {
    type: types.CREATE_BANNER_SUCCESS
  };
}

function createBannerFailure(data) {
  return {
    type: types.UPDATE_BANNER_FAILURE,
    id: data.id,
    error: data.error
  };
}

function destroy(id) {
  return { type: types.DESTROY_BANNER, id };
}

function updateBannerRequest(data) {
  return {
    type: types.UPDATE_BANNER_REQUEST,
    ...data
  };
}

function updateBannerSuccess() {
  return {
    type: types.UPDATE_BANNER_SUCCESS
  };
}

function updateBannerFailure(data) {
  return {
    type: types.UPDATE_BANNER_FAILURE,
    id: data.id,
    error: data.error
  };
}

// This action creator returns a function,
// which will get executed by Redux-Thunk middleware
// This function does not need to be pure, and thus allowed
// to have side effects, including executing asynchronous API calls.
export function createBanner(obj) {
  return (dispatch) => {
    if (obj.name.trim().length <= 0) return;
    const id = md5.hash(obj.name);
    // Redux thunk's middleware receives the store methods `dispatch`
    // and `getState` as parameters
    const data = {
      ...obj,
      id: id,
    };

    // First dispatch an optimistic update
    dispatch(createBannerRequest(data));

    return makeBannerRequest('post', id, data)
      .then(res => {
        if (res.status === 200) {
          return dispatch(createBannerSuccess());
        }
      })
      .catch(() => {
        return dispatch(createBannerFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your banner' }));
      });
  };
}

export function updateBanner(obj) {
  return (dispatch) => {
    const data = {
      ...obj
    };

    dispatch(updateBannerRequest(data));

    return makeBannerRequest('put', data.id, data)
      .then(res => {
        if (res.status === 200) {
          return dispatch(updateBannerSuccess());
        }
      })
      .catch(() => {
        return dispatch(updateBannerFailure({ id: data.id, error: 'Oops! Something went wrong and we couldn\'t update your banner' }));
      });
  }
}

// Fetch posts logic
export function fetchBanners() {
  return {
    type: types.GET_BANNERS,
    promise: makeBannerRequest('get')
  };
}

export function destroyBanner(id) {
  return dispatch => {
    dispatch(destroy(id));
    return makeBannerRequest('delete', id);
  };
}
