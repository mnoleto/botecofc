/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

function makeTeamRequest(method, id, data, api = '/team') {
	return request[method](api + (id ? ('/' + id) : ''), data);
}

/*
 * @param data
 * @return a simple JS object
 */
function createTeamRequest(data) {
	return {
		type: types.CREATE_TEAM_REQUEST,
		id: data.id,
		name: data.name,
		slug: data.slug,
		sport: data.sport,
		country: data.country,
		color: data.color,
		textColor: data.textColor,
		logo: data.logo
	};
}

function createTeamSuccess() {
	return {
		type: types.CREATE_TEAM_SUCCESS
	};
}

function createTeamFailure(data) {
	return {
		type: types.CREATE_TEAM_FAILURE,
		id: data.id,
		error: data.error
	};
}

export function createTeam(obj) {
	return (dispatch, getState) => {
		// If the name box is empty
		if (obj.name.trim().length <= 0) return;

		const id = md5.hash(obj.name);
		// Redux thunk's middleware receives the store methods `dispatch`
		// and `getState` as parameters
		const { team } = getState();
		const data = {
			id: id,
			name: obj.name,
			slug: obj.slug,
			sport: obj.sport,
			country: obj.country,
			color: obj.color,
			textColor: obj.textColor,
			logo: obj.logo
		};

		// First dispatch an optimistic update
		dispatch(createTeamRequest(data));

		return makeTeamRequest('post', id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(createTeamSuccess());
				}
			})
			.catch(() => {
				return dispatch(createTeamFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your team'}));
			});
	};
}

function updateTeamRequest(index, data) {
	return {
		type: types.UPDATE_TEAM_REQUEST,
		index: index,
		id: data.id,
		name: data.name,
		slug: data.slug,
		sport: data.sport,
		country: data.country,
		color: data.color,
		textColor: data.textColor,
		logo: data.logo
	};
}

function updateTeamSuccess() {
	return {
		type: types.UPDATE_TEAM_SUCCESS
	};
}

function updateTeamFailure(data) {
	return {
		type: types.UPDATE_TEAM_FAILURE,
		id: data.id,
		error: data.error
	};
}

export function updateTeam(index, obj) {
	return (dispatch, getState) => {
		// If the name box is empty
		if (obj.name.trim().length <= 0) return;

		const { team } = getState();
		const data = {
			id: obj.id,
			name: obj.name,
			slug: obj.slug,
			sport: obj.sport,
			country: obj.country,
			color: obj.color,
			textColor: obj.textColor,
			logo: obj.logo
		};

		dispatch(updateTeamRequest(index, data));

		return makeTeamRequest('put', data.id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(updateTeamSuccess());
				}
			})
			.catch(() => {
				return dispatch(updateTeamFailure({ id: data.id, error: 'Oops! Something went wrong and we couldn\'t update your team'}));
			});
	};
}

// Fetch posts logic
export function fetchTeams() {
	return {
		type: types.GET_TEAMS,
		promise: makeTeamRequest('get')
	};
}

function destroy(index) {
	return { type: types.DESTROY_TEAM, index };
}

export function destroyTeam(id, index) {
	return dispatch => {
		dispatch(destroy(index));
		return makeTeamRequest('delete', id);
		// do something with the ajax response
		// You can also dispatch here
		// E.g.
		// .then(response => {});
	};
}

function teamsVisibilityFilter(filter, countries) {
	return {
		type: types.SET_TEAMS_VISIBILITY_FILTER,
		show: filter,
		countries: countries
	};
}

export function setTeamsVisibilityFilter(filter, countries) {
	return dispatch => {
		dispatch(teamsVisibilityFilter(filter, countries));
	};
}

function teamVisibilityFilter(filter, team) {
	return {
		type: types.SET_TEAM_VISIBILITY_FILTER,
		show: filter,
		team: team
	};
}

export function setTeamVisibilityFilter(filter, team) {
	return dispatch => {
		dispatch(teamVisibilityFilter(filter, team));
	};
}

// SCORE
export function increment(id) {
  return { type: types.INCREMENT_SCORE, id };
}

export function decrement(id) {
  return { type: types.DECREMENT_SCORE, id };
}

export function incrementScore(id) {
  return dispatch => {
    return makeTeamRequest('put', id, {
        isFull: false,
        isIncrement: true
      })
      .then(() => dispatch(increment(id)))
      .catch(() => dispatch(createTeamFailure({id, error: 'Oops! Something went wrong and we couldn\'t add your vote'})));
  };
}

export function decrementScore(id) {
  return dispatch => {
    return makeTeamRequest('put', id, {
        isFull: false,
        isIncrement: false
      })
      .then(() => dispatch(decrement(id)))
      .catch(() => dispatch(createTeamFailure({id, error: 'Oops! Something went wrong and we couldn\'t add your vote'})));
  };
}