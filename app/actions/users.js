import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import { push } from 'react-router-redux';

import * as types from 'types';

polyfill();

function makeUserRequest(method, data, api = '/login') {
	return request({
		url: api,
		method,
		data,
		withCredentials: true
	});
}

function makeUser(method, id, data, api = '/user') {
	return request[method](api + (id ? ('/' + id) : ''), data);
}

export function exportUsers() {
	return {
		type: types.EXPORT_USERS,
	};
}

export function toggleLoading() {
	return {
		type: types.TOGGLE_LOADING,
	};
}

const selectPreferredCitySuccess = (data) => {
  return {
    type: types.SELECT_LOCALE_CITY,
    selectedCity: data.selectedCity,
  };
}

export const selectPreferredCity = (obj) => {
  return (dispatch) => {
    dispatch(selectPreferredCitySuccess(obj));
  };
}

/*
 * FETCH
 */
export function fetchUsers() {
	return {
		type: types.GET_USERS,
		promise: makeUserRequest('get', null, '/users')
	};
}

/*
 * DESTROY
 */
function destroy(index) {
	return { type: types.DESTROY_USER, index };
}

export function destroyUser(id, index) {
	return dispatch => {
		dispatch(destroy(index));
		return makeUser('delete', id);
		// do something with the ajax response
		// You can also dispatch here
		// E.g.
		// .then(response => {});
	};
}

/*
 * LOGOUT
 */
function beginLogout() {
	return { type: types.LOGOUT_USER};
}

function logoutSuccess() {
	return { type: types.LOGOUT_SUCCESS_USER };
}

function logoutError() {
	return { type: types.LOGOUT_ERROR_USER };
}

export function logOut() {
	return dispatch => {
		dispatch(beginLogout());

		return makeUserRequest('post', null, '/logout')
			.then(response => {
				if (response.status === 200) {
					dispatch(logoutSuccess());
				} else {
					dispatch(logoutError());
				}
			});
	};
}

/*
 * UPDATE REGISTER
 */
function updateRegisterRequest(data) {
	return {
		type: types.UPDATE_SIGNUP_REQUEST,
		...data,
	};
}

function updateSignUpSuccess(message) {
	return {
		type: types.UPDATE_SIGNUP_SUCCESS,
		message
	};
}

function updateSignUpFailure(data) {
	return {
		type: types.UPDATE_SIGNUP_FAILURE,
		_id: data.id,
		error: data.error
	};
}

export function updateRegister(obj, callback) {
	return (dispatch) => {
		const data = {
			...obj,
		};

		dispatch(updateRegisterRequest(data));

		return makeUser('put', obj._id, obj)
			.then(res => {
				if (res.status === 200) {
					dispatch(updateSignUpSuccess('success'));
					return callback('success');
				}
			})
			.catch(() => {
				dispatch(updateSignUpFailure({ id, error: 'Oops! Something went wrong and we couldn\'t update your user'}));
				return callback('error');
			});
	};
}

/*
 * REGISTER
 */
function beginRegister() {
	return { type: types.REGISTER_USER };
}

function registerSuccess(message) {
	return {
		type: types.REGISTER_SUCCESS_USER,
		message
	};
}

function registerError(message) {
	return {
		type: types.REGISTER_ERROR_USER,
		message
	};
}

export function register(data) {
	return dispatch => {
		dispatch(beginRegister());

		return makeUserRequest('put', data, '/register')
			.then(response => {
				if (response.status === 200) {
					dispatch(registerSuccess(response.data.message));
					// dispatch(push('/'));
				} else {
					dispatch(registerError('Oops! Something went wrong'));
				}
			})
			.catch(err => {
				dispatch(registerError(err.data.message));
			});
	};
}


/*
 * ROLE
 */
function updateRoleRequest(data) {
	return {
		type: types.UPDATE_ROLE_REQUEST,
		_id: data.id,
		role: data.role
	};
}

function updateRoleSuccess(message) {
	return {
		type: types.UPDATE_ROLE_SUCCESS,
		message
	};
}

function updateRoleFailure(data) {
	return {
		type: types.UPDATE_ROLE_FAILURE,
		_id: data.id,
		error: data.error
	};
}

export function updateRole(obj) {
	return (dispatch) => {
		const data = {
			id: obj.id,
			role: obj.role
		};

		dispatch(updateRoleRequest(data));

		return makeUser('put', data.id, data, '/signup')
			.then(res => {
				if (res.status === 200) {
					return dispatch(updateRoleSuccess('Success'));
				}
			})
			.catch(() => {
				return dispatch(updateRoleFailure({ id, error: 'Oops! Something went wrong and we couldn\'t update your user'}));
			});
	};
}

/*
 * OWNERSHIPS
 */
function updateOwnershipRequest(data) {
	return {
		type: types.USER_OWNERSHIP_REQUEST,
		_id: data.id,
		ownerships: data.ownerships,
		role: data.role
	};
}

function updateOwnershipSuccess() {
	return {
		type: types.USER_OWNERSHIP_SUCCESS
	};
}

function updateOwnershipFailure(data) {
	return {
		type: types.USER_OWNERSHIP_FAILURE,
		_id: data.id,
		error: data.error
	};
}

export function updateUserOwnership(obj, callback) {
	return (dispatch) => {
		const data = {
			id: obj.id,
			ownerships: obj.place,
			role: obj.role
		};

		dispatch(updateOwnershipRequest(data));

		return makeUser('put', data.id, data)
			.then(res => {
				if (res.status === 200) {
					if (callback) callback();
					return dispatch(updateOwnershipSuccess());
				}
			})
			.catch(() => {
				return dispatch(updateOwnershipFailure({ id, error: 'Oops! Something went wrong and we couldn\'t update your user'}));
			});
	};
}


/*
 * PLACES TO WATCH GAMES
 */
function addPlaceToWatchRequest(data) {
	return {
		type: types.ADD_PLACETOWATCH_REQUEST,
		_id: data.id,
		watch: data.watch
	};
}

function addPlaceToWatchSuccess() {
	return {
		type: types.ADD_PLACETOWATCH_SUCCESS
	};
}

function addPlaceToWatchFailure(data) {
	return {
		type: types.ADD_PLACETOWATCH_FAILURE,
		_id: data.id,
		error: data.error
	};
}

function removePlaceToWatchRequest(data) {
	return {
		type: types.REMOVE_PLACETOWATCH_REQUEST,
		_id: data.id,
		watch: data.watch
	};
}

function removePlaceToWatchSuccess() {
	return {
		type: types.REMOVE_PLACETOWATCH_SUCCESS
	};
}

function removePlaceToWatchFailure(data) {
	return {
		type: types.REMOVE_PLACETOWATCH_FAILURE,
		_id: data.id,
		error: data.error
	};
}

export function addPlaceToWatch(id, obj) {
	return (dispatch, getState) => {
		const { user } = getState();

		const data = {
			id: id,
			watch: obj
		};

		dispatch(addPlaceToWatchRequest(data));

		return makeUser('put', data.id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(addPlaceToWatchSuccess());
				}
			})
			.catch(() => {
				return dispatch(addPlaceToWatchFailure({ id, error: 'Oops! Something went wrong and we couldn\'t update your user'}));
			});
	};
}

export function removePlaceToWatch(id, obj) {
	return (dispatch, getState) => {
		const { user } = getState();

		const data = {
			id: id,
			watch: obj
		};

		dispatch(removePlaceToWatchRequest(data));

		return makeUser('put', data.id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(removePlaceToWatchSuccess());
				}
			})
			.catch(() => {
				return dispatch(removePlaceToWatchFailure({ id, error: 'Oops! Something went wrong and we couldn\'t update your user'}));
			});
	};
}


/*
 * FAVORITES
 */
function addToFavoritesRequest(data) {
	return {
		type: types.ADD_TOFAVORITES_REQUEST,
		_id: data.id,
		favorite: data.favorite
	};
}

function addToFavoritesSuccess() {
	return {
		type: types.ADD_TOFAVORITES_SUCCESS
	};
}

function addToFavoritesFailure(data) {
	return {
		type: types.ADD_TOFAVORITES_FAILURE,
		_id: data.id,
		error: data.error
	};
}

function removeFromFavoritesRequest(data) {
	return {
		type: types.REMOVE_FROMFAVORITES_REQUEST,
		_id: data.id,
		favorite: data.favorite
	};
}

function removeFromFavoritesSuccess() {
	return {
		type: types.REMOVE_FROMFAVORITES_SUCCESS
	};
}

function removeFromFavoritesFailure(data) {
	return {
		type: types.REMOVE_FROMFAVORITES_FAILURE,
		_id: data.id,
		error: data.error
	};
}

export function addTeamToFavorites(obj) {
	return (dispatch, getState) => {
		const { user } = getState();

		const id = obj.id;
		const data = {
			id: obj.id,
			favorite: obj.favorite
		};

		dispatch(addToFavoritesRequest(data));

		return makeUser('put', data.id, data, '/addfavorite')
			.then(res => {
				if (res.status === 200) {
					return dispatch(addToFavoritesSuccess());
				}
			})
			.catch(() => {
				return dispatch(addToFavoritesFailure({ id, error: 'Oops! Something went wrong and we couldn\'t update your user'}));
			});
	};
}

export function removeTeamFromFavorites(obj) {
	return (dispatch, getState) => {
		const { user } = getState();

		const id = obj.id;
		const data = {
			id: obj.id,
			favorite: obj.favorite
		};

		dispatch(removeFromFavoritesRequest(data));

		return makeUser('put', data.id, data, '/removefavorite')
			.then(res => {
				if (res.status === 200) {
					return dispatch(removeFromFavoritesSuccess());
				}
			})
			.catch(() => {
				return dispatch(removeFromFavoritesFailure({ id, error: 'Oops! Something went wrong and we couldn\'t update your user'}));
			});
	};
}
