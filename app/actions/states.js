/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

const makeStateRequest = (method, id, data, api = '/state') => {
  return request[method](api + (id ? ('/' + id) : ''), data);
}

/*
 * @param data
 * @return a simple JS object
 */
const createStateRequest = (data) => {
  return {
    type: types.CREATE_STATE_REQUEST,
    abbr: data.abbr,
    id: data.id,
    isActive: data.isActive,
    name: data.name,
  };
}

const createStateSuccess = () => {
  return {
    type: types.CREATE_STATE_SUCCESS
  };
}

const createStateFailure = (data) => {
  return {
    type: types.UPDATE_STATE_FAILURE,
    id: data.id,
    error: data.error
  };
}

const destroy = (index) => {
  return { type: types.DESTROY_STATE, index };
}

const updateStateRequest = (data) => {
  return {
    type: types.UPDATE_STATE_REQUEST,
    ...data,
  };
}

const updateStateSuccess = () => {
  return {
    type: types.UPDATE_STATE_SUCCESS
  };
}

const updateStateFailure = (data) => {
  return {
    type: types.UPDATE_STATE_FAILURE,
    id: data.id,
    error: data.error
  };
}

export const createState = (obj) => {
  return (dispatch) => {
    if (obj.name.trim().length <= 0) return;

    const id = md5.hash(obj.name);
    const data = {
      obj,
      id,
    };

    dispatch(createStateRequest(data));

    return makeStateRequest('post', id, data)
      .then(res => {
        if (res.status === 200) {
          return dispatch(createStateSuccess());
        }
      })
      .catch(() => {
        return dispatch(createStateFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your state' }));
      });
  };
}

export const updateState = (obj) => {
  return (dispatch) => {
    dispatch(updateStateRequest(obj));

    return makeStateRequest('put', obj.id, obj)
      .then(res => {
        if (res.status === 200) {
          return dispatch(updateStateSuccess());
        }
      })
      .catch(() => {
        return dispatch(updateStateFailure({ id: obj.id, error: 'Oops! Something went wrong and we couldn\'t update your state' }));
      });
  }
}

// Fetch posts logic
export const fetchStates = () => {
  return {
    type: types.GET_STATES,
    promise: makeStateRequest('get')
  };
}

export const fetchActiveStates = () => {
  return {
    type: types.GET_ACTIVE_STATES,
    promise: makeStateRequest('get', null, null, '/active-state')
  };
}


export const destroyState = (id, index) => {
  return dispatch => {
    dispatch(destroy(index));
    return makeStateRequest('delete', id)
  };
}

