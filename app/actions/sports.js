/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

function makeSportRequest(method, id, data, api = '/sport') {
	return request[method](api + (id ? ('/' + id) : ''), data);
}

/*
 * @param data
 * @return a simple JS object
 */
function createSportRequest(data) {
	return {
		type: types.CREATE_SPORT_REQUEST,
		id: data.id,
		name: data.name,
		slug: data.slug
	};
}

function createSportSuccess() {
	return {
		type: types.CREATE_SPORT_SUCCESS
	};
}

function createSportFailure(data) {
	return {
		type: types.CREATE_SPORT_FAILURE,
		id: data.id,
		error: data.error
	};
}

function destroy(index) {
	return { type: types.DESTROY_SPORT, index };
}

function updateSportRequest(index, data) {
	return {
		type: types.UPDATE_SPORT_REQUEST,
		index: index,
		id: data.id,
		name: data.name,
		slug: data.slug
	};
}

function updateSportSuccess() {
	return {
		type: types.UPDATE_SPORT_SUCCESS
	};
}

function updateSportFailure(data) {
	return {
		type: types.UPDATE_SPORT_FAILURE,
		id: data.id,
		error: data.error
	};
}

// This action creator returns a function,
// which will get executed by Redux-Thunk middleware
// This function does not need to be pure, and thus allowed
// to have side effects, including executing asynchronous API calls.
export function createSport(obj) {
	return (dispatch, getState) => {
		// If the name box is empty
		if (obj.name.trim().length <= 0) return;

		const id = md5.hash(obj.name);
		// Redux thunk's middleware receives the store methods `dispatch`
		// and `getState` as parameters
		const { sport } = getState();
		const data = {
			id: id,
			name: obj.name,
			slug: obj.slug
		};

		// First dispatch an optimistic update
		dispatch(createSportRequest(data));

		return makeSportRequest('post', id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(createSportSuccess());
				}
			})
			.catch(() => {
				return dispatch(createSportFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your sport'}));
			});
	};
}

export function updateSport(index, obj) {
	return (dispatch, getState) => {
		if (obj.name.trim().length <= 0) return;

		const { sport } = getState();
		const data = {
			id: obj.id,
			name: obj.name,
			slug: obj.slug
		};

		dispatch(updateSportRequest(index, data));

		return makeSportRequest('put', data.id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(updateSportSuccess());
				}
			})
			.catch(() => {
				return dispatch(updateSportFailure({ id: data.id, error: 'Oops! Something went wrong and we couldn\'t update your sport'}));
			});
	};
}

// Fetch posts logic
export function fetchSports() {
	return {
		type: types.GET_SPORTS,
		promise: makeSportRequest('get')
	};
}

export function destroySport(id, index) {
	return dispatch => {
		dispatch(destroy(index));
		return makeSportRequest('delete', id);
		// do something with the ajax response
		// You can also dispatch here
		// E.g.
		// .then(response => {});
	};
}
