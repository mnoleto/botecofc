import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
	fetch: null,
	fetchSuccess: ['data'],
	fetchError: ['error']
}, {});