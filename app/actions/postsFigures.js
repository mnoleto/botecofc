/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import * as types from '../types';

polyfill();

const makePostsRequest = (method, id, data, api = '/post-figures') => {
    return request[method](api + (id ? ('/' + id) : ''), data);
}

const createFiguresSuccess = (req) => {
    return {
        type: types.CREATE_FIGURES_SUCCESS,
        req,
    };
}
const createFiguresFailure = (data) => {
    return {
        type: types.CREATE_FIGURES_FAILURE,
        error: data.error,
    };
}

const createFiguresRequest = (data) => {
    return {
        type: types.CREATE_FIGURES_REQUEST,
        images: data,
    };
}

const removeFiguresSuccess = (req) => {
    return {
        type: types.REMOVE_FIGURES_SUCCESS,
        req,
    };
}
const removeFiguresFailure = (data) => {
    return {
        type: types.REMOVE_FIGURES_FAILURE,
        error: data.error,
    };
}

const removeFiguresRequest = (id) => {
    return {
        type: types.REMOVE_FIGURES_REQUEST,
        id,
    };
}

export function createFigures(data) {
	return (dispatch) => {
		dispatch(createFiguresRequest(data));
		
		return makePostsRequest('post', null, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(createFiguresSuccess(res));
				}
			})
			.catch((err) => {
				return dispatch(createFiguresFailure({ error: 'Oops! Something went wrong and we couldn\'t create your post' }));
			});
	};
}

export function removeFigure(id) {
	return (dispatch) => {
		dispatch(removeFiguresRequest(id));
		
		return makePostsRequest('delete', id)
			.then(res => {
				if (res.status === 200) {
					return dispatch(removeFiguresSuccess(res));
				}
			})
			.catch((err) => {
				return dispatch(removeFiguresFailure({ error: 'Oops! Something went wrong and we couldn\'t remove your figure' }));
			});
	};
}

export function fetchPostsFigures() {
	return {
		type: types.GET_FIGURES,
		promise: makePostsRequest('get', null, null, '/figures')
	};
}