/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

function makePlaceTempRequest(method, id, data, api = '/placeTemp') {
	return request[method](api + (id ? ('/' + id) : ''), data);
}

/*
 * CREATE PLACE TEMP
 */
function createPlaceTempRequest(data) {
	return {
		type: types.CREATE_PLACETEMP_REQUEST,
		...data
	};
}

function createPlaceTempSuccess(message) {
	return {
		type: types.CREATE_PLACETEMP_SUCCESS,
		message
	};
}

function createPlaceTempFailure(data) {
	return {
		type: types.CREATE_PLACETEMP_FAILURE,
		...data
	};
}

export function dismissMessage() {
	dispatch({
		type: types.DISMISS_MESSAGE,
		message: ''
	});
}

export function createPlaceTemp(obj) {
	return (dispatch) => {
		// If the name box is empty
		if (obj.name.trim().length <= 0) return;

		const id = md5.hash(obj.name);
		
		const data = {
			...obj,
			id,
		};

		// First dispatch an optimistic update
		dispatch(createPlaceTempRequest(data));

		return makePlaceTempRequest('post', id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(createPlaceTempSuccess('success'));
				}
			})
			.catch(() => {
				return dispatch(createPlaceTempFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your place'}));
			});
	};
}


/*
 * UPDATE PLACE TEMP
 */
function updatePlaceTempRequest(index, data) {
	return {
		type: types.UPDATE_PLACETEMP_REQUEST,
		index: index,
		...data
	};
}

function updatePlaceTempSuccess() {
	return {
		type: types.UPDATE_PLACETEMP_SUCCESS
	};
}

function updatePlaceTempFailure(data) {
	return {
		type: types.UPDATE_PLACETEMP_FAILURE,
		...data
	};
}

export function updatePlaceTemp(index, obj) {
	return (dispatch) => {
		// If the name box is empty
		if (obj.name.trim().length <= 0) return;

		const data = {
			...obj
		};

		dispatch(updatePlaceTempRequest(index, data));

		return makePlaceTempRequest('put', data.id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(updatePlaceTempSuccess());
				}
			})
			.catch(() => {
				return dispatch(updatePlaceTempFailure({ id: data.id, error: 'Oops! Something went wrong and we couldn\'t update your place'}));
			});
	};
}


/*
 * APPROVE PLACE TEMP
 */
function approvePlaceTempRequest(data) {
	return {
		type: types.APPROVE_PLACETEMP_REQUEST,
		id: data.id,
		name: data.name,
		address: data.address,
		city: data.city
	};
}


export function approvePlaceTemp(obj, callback) {
	return (dispatch) => {
		// If the name box is empty
		if (obj.name.trim().length <= 0) return;

		const id = md5.hash(obj.name);
		
		const data = {
			...obj,
			id: id,
		};

		// First dispatch an optimistic update
		dispatch(approvePlaceTempRequest(data));

		return makePlaceTempRequest('post', null, data, '/sendSuggestionStatus');
	};
}

/*
 * FETCH PLACE TEMP
 */
export function fetchPlacesTemp() {
	return {
		type: types.GET_PLACESTEMP,
		promise: makePlaceTempRequest('get')
	};
}


/*
 * DESTROY PLACE TEMP
 */
function destroy(id) {
	return { type: types.DESTROY_PLACETEMP, id };
}

export function destroyPlaceTemp(id, index, _id) {
	return dispatch => {
		const placeId = id !== '' ? id : _id
		dispatch(destroy(placeId));
		return makePlaceTempRequest('delete', id);
	};
}
