/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

function makeOwnershipRequest(method, id, data, api = '/ownership') {
	return request[method](api + (id ? ('/' + id) : ''), data);
}

/*
 * @param data
 * @return a simple JS object
 */
function createOwnershipRequest(data) {
	return {
		type: types.CREATE_OWNERSHIP_REQUEST,
		id: data.id,
		place: {
			id: data.place.id,
			name: data.place.name
		},
		user: {
			id: data.user.id,
			name: data.user.name,
			role: data.user.role
		}
	};
}

function createOwnershipSuccess() {
	return {
		type: types.CREATE_OWNERSHIP_SUCCESS
	};
}

function createOwnershipFailure(data) {
	return {
		type: types.CREATE_OWNERSHIP_FAILURE,
		id: data.id,
		error: data.error
	};
}

function destroy(id) {
	return { type: types.DESTROY_OWNERSHIP, id };
}

// This action creator returns a function,
// which will get executed by Redux-Thunk middleware
// This function does not need to be pure, and thus allowed
// to have side effects, including executing asynchronous API calls.
export function createOwnership(obj, callback) {
	return (dispatch, getState) => {
		// If the name box is empty
		// if (obj.name.trim().length <= 0) return;
		const date = new Date();
		const id = md5.hash(date.toString());

		// Redux thunk's middleware receives the store methods `dispatch`
		// and `getState` as parameters
		const { ownership } = getState();
		const data = {
			id,
			place: obj.place,
			user: obj.user
		};

		// First dispatch an optimistic update
		dispatch(createOwnershipRequest(data));

		return makeOwnershipRequest('post', id, data)
			.then(res => {
				if (res.status === 200) {
					if(callback) {
						dispatch(createOwnershipSuccess('success'));
						return callback();
					} else {
						return dispatch(createOwnershipSuccess('success'));
					}
				}
			})
			.catch(() => {
				return dispatch(createOwnershipFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your ownership'}));
			});
	};
}

export function destroyOwnership(id) {
	return dispatch => {
		dispatch(destroy(id));
		return makeOwnershipRequest('delete', id);
	};
}

// Fetch posts logic
export function fetchOwnerships() {
	return {
		type: types.GET_OWNERSHIPS,
		promise: makeOwnershipRequest('get')
	};
}

function visibilityFilter(filter, championship, initialDate, endDate, place) {
	return {
		type: types.SET_OWNER_VISIBILITY_FILTER,
		show: filter,
		championship,
		initialDate,
		endDate,
		place,
	};
}

export function setVisibilityFilter(filter, championship, initialDate, endDate, place) {
	return dispatch => {
		dispatch(visibilityFilter(filter, championship, initialDate, endDate, place));
	};
}