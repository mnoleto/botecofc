/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

function makePlaceSuggestionRequest(method, id, data, api = '/placeSuggestion') {
	return request[method](api + (id ? ('/' + id) : ''), data);
}

/*
 * @param data
 * @return a simple JS object
 */
function createPlaceSuggestionRequest(data) {
	return {
		type: types.CREATE_PLACESUGGESTION_REQUEST,
		id: data.id,
		place: {
			name: data.place.name,
			address: data.place.address,
			reference: data.place.reference,
			cityId: data.place.cityId,
			stateId: data.place.stateId
		},
		user: {
			id: data.user.id,
			name: data.user.name,
			email: data.user.email
		}
	};
}

function createPlaceSuggestionSuccess(message) {
	return {
		type: types.CREATE_PLACESUGGESTION_SUCCESS,
		message
	};
}

function createPlaceSuggestionFailure(data) {
	return {
		type: types.CREATE_PLACESUGGESTION_FAILURE,
		id: data.id,
		error: data.error
	};
}

function destroy(id) {
	return { type: types.DESTROY_PLACESUGGESTION, id };
}

export function dismissMessage() {
	return {
		type: types.DISMISS_MESSAGE,
		message: ''
	}
}

export function createPlaceSuggestion(obj) {
	return (dispatch) => {
		// If the name box is empty
		if (obj.place.name.trim().length <= 0) return;

		const id = md5.hash(obj.place.name);
		
		const data = {
			id: id,
			place: {
				name: obj.place.name,
				address: obj.place.address,
				reference: obj.place.reference,
				cityId: obj.place.cityId,
				stateId: obj.place.stateId
			},
			user: {
				id: obj.user.id,
				name: obj.user.name,
				email: obj.user.email
			}
		};

		// First dispatch an optimistic update
		dispatch(createPlaceSuggestionRequest(data));

		return makePlaceSuggestionRequest('post', id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(createPlaceSuggestionSuccess('success'));
				}
			})
			.catch(() => {
				return dispatch(createPlaceSuggestionFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your place suggestion'}));
			});
	};
}

// Fetch posts logic
export function fetchPlacesSuggestions() {
	return {
		type: types.GET_PLACESSUGGESTIONS,
		promise: makePlaceSuggestionRequest('get')
	};
}

export function destroyPlaceSuggestion(id) {
	return dispatch => {
		dispatch(destroy(id));
		return makePlaceSuggestionRequest('delete', id);
	};
}
