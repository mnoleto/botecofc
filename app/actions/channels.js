/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

function makeChannelRequest(method, id, data, api = '/channel') {
    return request[method](api + (id ? ('/' + id) : ''), data);
}

/*
 * @param data
 * @return a simple JS object
 */
function createChannelRequest(data) {
    return {
        type: types.CREATE_CHANNEL_REQUEST,
        id: data.id,
        name: data.name,
        // logo: data.logo,
    };
}

function createChannelSuccess() {
    return {
        type: types.CREATE_CHANNEL_SUCCESS
    };
}

function createChannelFailure(data) {
    return {
        type: types.CREATE_CHANNEL_FAILURE,
        id: data.id,
        error: data.error
    };
}

function destroy(index) {
    return { type: types.DESTROY_CHANNEL, index };
}

function updateChannelRequest(index, data) {
    return {
        type: types.UPDATE_CHANNEL_REQUEST,
        index: index,
        id: data.id,
        name: data.name,
        // logo: data.logo,
    };
}

function updateChannelSuccess() {
    return {
        type: types.UPDATE_CHANNEL_SUCCESS
    };
}

function updateChannelFailure(data) {
    return {
        type: types.UPDATE_CHANNEL_FAILURE,
        id: data.id,
        error: data.error
    };
}

// This action creator returns a function,
// which will get executed by Redux-Thunk middleware
// This function does not need to be pure, and thus allowed
// to have side effects, including executing asynchronous API calls.
export function createChannel(obj) {
    return (dispatch, getState) => {
        // If the name box is empty
        if (obj.name.trim().length <= 0) return;

        const id = md5.hash(obj.name);
        
        const data = {
            id: id,
            name: obj.name,
            // logo: obj.logo,
        };

        // First dispatch an optimistic update
        dispatch(createChannelRequest(data));

        return makeChannelRequest('post', id, data)
            .then(res => {
                if (res.status === 200) {
                    return dispatch(createChannelSuccess());
                }
            })
            .catch(() => {
                return dispatch(createChannelFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your channel' }));
            });
    };
}

export function updateChannel(index, obj) {
    return (dispatch, getState) => {
        // If the name box is empty
        if (obj.name.trim().length <= 0) return;

        const { channel } = getState();
        const data = {
            id: obj.id,
            name: obj.name,
            // logo: obj.logo,
        };

        // First dispatch an optimistic update
        dispatch(updateChannelRequest(index, data));

        return makeChannelRequest('put', data.id, data)
            .then(res => {
                if (res.status === 200) {
                    return dispatch(updateChannelSuccess());
                }
            })
            .catch(() => {
                return dispatch(updateChannelFailure({ id: data.id, error: 'Oops! Something went wrong and we couldn\'t update your channel' }));
            });
    };
}

// Fetch posts logic
export function fetchChannels() {
    return {
        type: types.GET_CHANNELS,
        promise: makeChannelRequest('get')
    };
}

export function destroyChannel(id, index) {
    return dispatch => {
        dispatch(destroy(index));
        return makeChannelRequest('delete', id);
        // do something with the ajax response
        // You can also dispatch here
        // E.g.
        // .then(response => {});
    };
}
