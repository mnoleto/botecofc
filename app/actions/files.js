/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import * as types from 'types';

polyfill();


/*
 * @param data
 * @return a simple JS object
 */
export function uploadFileRequest(data) {
  return {
    type: types.UPLOAD_FILE_REQUEST,
    data
  };
}

export function uploadFileSuccess() {
  return {
    type: types.UPLOAD_FILE_SUCCESS
  };
}

export function uploadFileFailure(data) {
  return {
    type: types.UPLOAD_FILE_FAILURE,
    error: data.error
  };
}

export function uploadFile(data, url, callback) {
  return (dispatch) => {
    // First dispatch an optimistic update
    dispatch(uploadFileRequest(data));

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
      if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
        dispatch(uploadFileSuccess());
        callback(xmlHttp);
      }
    }
    xmlHttp.open("post", url); 
    xmlHttp.send(data);
  };
}