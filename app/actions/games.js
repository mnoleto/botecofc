/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

function makeGameRequest(method, id, data, api = '/game') {
	return request[method](api + (id ? ('/' + id) : ''), data);
}

function destroy(index) {
	return { type: types.DESTROY_GAME, index };
}

function createGameRequest(data) {
	return {
		type: types.CREATE_GAME_REQUEST,
		...data,
	};
}

function createGameSuccess() {
	return {
		type: types.CREATE_GAME_SUCCESS
	};
}

function createGameFailure(data) {
	return {
		type: types.CREATE_GAME_FAILURE,
		...data
	};
}

function destroy(index) {
	return { type: types.DESTROY_GAME, index };
}

function updateGameRequest(index, data) {
	return {
		type: types.UPDATE_GAME_REQUEST,
		...data,
		index: index,
	};
}

function updateGameSuccess() {
	return {
		type: types.UPDATE_GAME_SUCCESS
	};
}

function updateGameFailure(data) {
	return {
		type: types.UPDATE_GAME_FAILURE,
		...data
	};
}

export function createGame(obj) {
	return (dispatch) => {
		const id = md5.hash(obj.homeTeam.id + obj.gameDate);
		
		const data = {
			...obj,
			id,
		};

		// First dispatch an optimistic update
		dispatch(createGameRequest(data));
		
		return makeGameRequest('post', id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(createGameSuccess());
				}
			})
			.catch(() => {
				return dispatch(createGameFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your game'}));
			});
	};
}

export function updateGame(index, obj) {
	return (dispatch) => {
		const data = {
			...obj
		};

		// First dispatch an optimistic update
		dispatch(updateGameRequest(index, data));

		return makeGameRequest('put', data.id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(updateGameSuccess());
				}
			})
			.catch(() => {
				return dispatch(updateGameFailure({ id: data.id, error: 'Oops! Something went wrong and we couldn\'t create your game'}));
			});
	};
}

// Fetch posts logic
export function fetchGames() {
	return {
		type: types.GET_GAMES,
		promise: makeGameRequest('get')
	};
}

export function destroyGame(id, index) {
	return dispatch => {
		dispatch(destroy(index));
		return makeGameRequest('delete', id);
	};
}

function visibilityFilter(filter, championship, city, date ) {
	return {
		type: types.SET_GAME_VISIBILITY_FILTER,
		show: filter,
		championship,
		city,
		date
	};
}

export function setVisibilityFilter(filter, championship, city, date) {
	return dispatch => {
		dispatch(visibilityFilter(filter, championship, city, date));
	};
}
