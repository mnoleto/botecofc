/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

function makeCountryRequest(method, id, data, api = '/country') {
	return request[method](api + (id ? ('/' + id) : ''), data);
}

/*
 * @param data
 * @return a simple JS object
 */
function createCountryRequest(data) {
	return {
		type: types.CREATE_COUNTRY_REQUEST,
		id: data.id,
		name: data.name,
		slug: data.slug
	};
}

function createCountrySuccess() {
	return {
		type: types.CREATE_COUNTRY_SUCCESS
	};
}

function createCountryFailure(data) {
	return {
		type: types.UPDATE_COUNTRY_FAILURE,
		id: data.id,
		error: data.error
	};
}

function destroy(index) {
	return { type: types.DESTROY_COUNTRY, index };
}

function updateCountryRequest(index, data) {
	return {
		type: types.UPDATE_COUNTRY_REQUEST,
		index: index,
		id: data.id,
		name: data.name,
		slug: data.slug
	};
}

function updateCountrySuccess() {
	return {
		type: types.UPDATE_COUNTRY_SUCCESS
	};
}

function updateCountryFailure(data) {
	return {
		type: types.UPDATE_COUNTRY_FAILURE,
		id: data.id,
		error: data.error
	};
}

// This action creator returns a function,
// which will get executed by Redux-Thunk middleware
// This function does not need to be pure, and thus allowed
// to have side effects, including executing asynchronous API calls.
export function createCountry(obj) {
	return (dispatch, getState) => {
		// If the name box is empty
		if (obj.name.trim().length <= 0) return;

		const id = md5.hash(obj.name);
		// Redux thunk's middleware receives the store methods `dispatch`
		// and `getState` as parameters
		const { country } = getState();
		const data = {
			id: id,
			name: obj.name,
			slug: obj.slug
		};

		// First dispatch an optimistic update
		dispatch(createCountryRequest(data));

		return makeCountryRequest('post', id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(createCountrySuccess());
				}
			})
			.catch(() => {
				return dispatch(createCountryFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your country'}));
			});
	};
}

export function updateCountry(index, obj) {
	return (dispatch, getState) => {
		if (obj.name.trim().length <= 0) return;

		const { country } = getState();
		const data = {
			id: obj.id,
			name: obj.name,
			slug: obj.slug
		};

		dispatch(updateCountryRequest(index, data));

		return makeCountryRequest('put', data.id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(updateCountrySuccess());
				}
			})
			.catch(() => {
				return dispatch(updateCountryFailure({ id: data.id, error: 'Oops! Something went wrong and we couldn\'t update your country'}));
			});
	}
}

// Fetch posts logic
export function fetchCountries() {
	return {
		type: types.GET_COUNTRIES,
		promise: makeCountryRequest('get')
	};
}

export function destroyCountry(id, index) {
	return dispatch => {
		dispatch(destroy(index));
		return makeCountryRequest('delete', id);
		// do something with the ajax response
		// You can also dispatch here
		// E.g.
		// .then(response => {});
	};
}
