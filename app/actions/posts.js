/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

function makePostsRequest(method, id, data, api = '/createPost') {
    return request[method](api + (id ? ('/' + id) : ''), data);
}

/*
 * @param data
 * @return a simple JS object
 */
function createPostRequest(data) {
    return {
        type: types.CREATE_POST_REQUEST,
        ...data,
    };
}

function createPostSuccess(image) {
    return {
        type: types.CREATE_POST_SUCCESS,
        image,
    };
}

function createPostFailure(data) {
    return {
        type: types.CREATE_POST_FAILURE,
        error: data.error,
    };
}

export function createPost(data) {
    return (dispatch) => {
        // First dispatch an optimistic update
        dispatch(createPostRequest(data));
        
        return makePostsRequest('post', null, data)
            .then(res => {
                if (res.status === 200) {
                    return dispatch(createPostSuccess(res));
                }
            })
            .catch((err) => {
                return dispatch(createPostFailure({ error: 'Oops! Something went wrong and we couldn\'t create your post' }));
            });
    };
}
