/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

function makeFeatureToggleRequest(method, id, data, api = '/featureRequest') {
  return request[method](api + (id ? ('/' + id) : ''), data);
}

/*
 * @param data
 * @return a simple JS object
 */
function createFeatureToggleRequest(data) {
  return {
    type: types.CREATE_FEATURE_TOGGLE_REQUEST,
    id: data.id,
    place_id: data.place_id,
    feature: data.feature,
  };
}

function createFeatureToggleSuccess() {
  return {
    type: types.CREATE_FEATURE_TOGGLE_SUCCESS
  };
}

function createFeatureToggleFailure(data) {
  return {
    type: types.CREATE_FEATURE_TOGGLE_FAILURE,
    id: data.id,
    error: data.error
  };
}

function destroy(id) {
  return { type: types.DESTROY_FEATURE_TOGGLE, id };
}

// This action creator returns a function,
// which will get executed by Redux-Thunk middleware
// This function does not need to be pure, and thus allowed
// to have side effects, including executing asynchronous API calls.
export function createFeatureToggle(obj) {
  return (dispatch, getState) => {
    // If the name box is empty
    // if (obj.name.trim().length <= 0) return;
    const date = new Date();
    const id = md5.hash(date.toString());
    
    // Redux thunk's middleware receives the store methods `dispatch`
    // and `getState` as parameters
    const { featureToggle } = getState();
    const data = {
      id,
      place_id: obj,
      feature: 'post',
    };

    // First dispatch an optimistic update
    dispatch(createFeatureToggleRequest(data));

    return makeFeatureToggleRequest('post', id, data)
      .then(res => {
        if (res.status === 200) {
          return dispatch(createFeatureToggleSuccess('success'));
        }
      })
      .catch(() => {
        return dispatch(createFeatureToggleFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your ownership' }));
      });
  };
}

export function destroyFeatureToggle(id) {
  return dispatch => {
    dispatch(destroy(id));
    return makeFeatureToggleRequest('delete', id);
  };
}

// Fetch posts logic
export function fetchFeatureToggles() {
  return {
    type: types.GET_FEATURE_TOGGLES,
    promise: makeFeatureToggleRequest('get')
  };
}
