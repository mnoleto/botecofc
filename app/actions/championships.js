/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

function makeChampionshipRequest(method, id, data, api = '/championship') {
	return request[method](api + (id ? ('/' + id) : ''), data);
}

/*
 * @param data
 * @return a simple JS object
 */
function createChampionshipRequest(data) {
	return {
		type: types.CREATE_CHAMPIONSHIP_REQUEST,
		id: data.id,
		name: data.name,
		slug: data.slug,
		dateBegin: data.dateBegin,
		dateEnd: data.dateEnd,
		logo: data.logo,
		countries: data.countries,
		sports: data.sports,
		teams: data.teams
	};
}

function createChampionshipSuccess() {
	return {
		type: types.CREATE_CHAMPIONSHIP_SUCCESS
	};
}

function createChampionshipFailure(data) {
	return {
		type: types.CREATE_CHAMPIONSHIP_FAILURE,
		id: data.id,
		error: data.error
	};
}

function destroy(index) {
	return { type: types.DESTROY_CHAMPIONSHIP, index };
}

function updateChampionshipRequest(index, data) {
	return {
		type: types.UPDATE_CHAMPIONSHIP_REQUEST,
		index: index,
		id: data.id,
		name: data.name,
		slug: data.slug,
		dateBegin: data.dateBegin,
		dateEnd: data.dateEnd,
		logo: data.logo,
		countries: data.countries,
		sports: data.sports,
		teams: data.teams
	};
}

function updateChampionshipSuccess() {
	return {
		type: types.UPDATE_CHAMPIONSHIP_SUCCESS
	};
}

function updateChampionshipFailure(data) {
	return {
		type: types.UPDATE_CHAMPIONSHIP_FAILURE,
		id: data.id,
		error: data.error
	};
}

// This action creator returns a function,
// which will get executed by Redux-Thunk middleware
// This function does not need to be pure, and thus allowed
// to have side effects, including executing asynchronous API calls.
export function createChampionship(obj) {
	return (dispatch, getState) => {
		// If the name box is empty
		if (obj.name.trim().length <= 0) return;

		const id = md5.hash(obj.name);
		// Redux thunk's middleware receives the store methods `dispatch`
		// and `getState` as parameters
		const { championship } = getState();
		const data = {
			id: id,
			name: obj.name,
			slug: obj.slug,
			dateBegin: obj.dateBegin,
			dateEnd: obj.dateEnd,
			logo: obj.logo,
			countries: obj.countries,
			sports: obj.sports,
			teams: obj.teams
		};

		// First dispatch an optimistic update
		dispatch(createChampionshipRequest(data));
		
		return makeChampionshipRequest('post', id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(createChampionshipSuccess());
				}
			})
			.catch(() => {
				return dispatch(createChampionshipFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your championship'}));
			});
	};
}

export function updateChampionship(index, obj) {
	return (dispatch, getState) => {
		// If the name box is empty
		if (obj.name.trim().length <= 0) return;

		const { championship } = getState();
		const data = {
			id: obj.id,
			name: obj.name,
			slug: obj.slug,
			dateBegin: obj.dateBegin,
			dateEnd: obj.dateEnd,
			logo: obj.logo,
			countries: obj.countries,
			sports: obj.sports,
			teams: obj.teams
		};

		// First dispatch an optimistic update
		dispatch(updateChampionshipRequest(index, data));

		return makeChampionshipRequest('put', data.id, data)
			.then(res => {
				if (res.status === 200) {
					return dispatch(updateChampionshipSuccess());
				}
			})
			.catch(() => {
				return dispatch(updateChampionshipFailure({ id: data.id, error: 'Oops! Something went wrong and we couldn\'t update your championship'}));
			});
	};
}

// Fetch posts logic
export function fetchChampionships() {
	return {
		type: types.GET_CHAMPIONSHIPS,
		promise: makeChampionshipRequest('get')
	};
}

export function destroyChampionship(id, index) {
	return dispatch => {
		dispatch(destroy(index));
		return makeChampionshipRequest('delete', id);
		// do something with the ajax response
		// You can also dispatch here
		// E.g.
		// .then(response => {});
	};
}


function visibilityFilter(filter, sports, countries) {
	return {
		type: types.SET_CHAMPIONSHIPS_VISIBILITY_FILTER,
		show: filter,
		sports: sports,
		countries: countries
	};
}

export function setVisibilityFilter(filter, sports, countries) {
	return dispatch => {
		dispatch(visibilityFilter(filter, sports, countries));
	};
}

function championshipVisibilityFilter(filter, championship) {
	return {
		type: types.SET_CHAMPIONSHIP_VISIBILITY_FILTER,
		show: filter,
		championship
	};
}

export function setChampionshipVisibilityFilter(filter, championship) {
	return dispatch => {
		dispatch(championshipVisibilityFilter(filter, championship));
	};
}
