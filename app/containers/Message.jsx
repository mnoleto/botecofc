import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames/bind';
import { dismissMessage } from '../actions/messages';
import styles from '../css/components/message';

const cx = classNames.bind(styles);

class Message extends Component {
	render() {
		const { message } = this.props;
		if(message.type) {
			const classname = message.type.toLowerCase();
			return (
				<div className={cx('message', classname)} onClick={dismissMessage}>{message.message}</div>
			);
		} else {
			return (
				<div/>
			);
		}
	}
}

Message.propTypes = {
  message: PropTypes.object
};

function mapStateToProps(state) {
  return { message: state.message};
}

export default connect(mapStateToProps, { dismissMessage })(Message);
