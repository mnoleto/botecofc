import { connect } from 'react-redux';
import { updateRegister } from '../actions/users';
import Register from '../components/register';

function mapStateToProps(state) {
	return {
		cities: state.city.cities,
		states: state.state.states,
		teams: state.team.teams,
		user: state.user,
	};
}

export default connect(mapStateToProps, { updateRegister })(Register);
