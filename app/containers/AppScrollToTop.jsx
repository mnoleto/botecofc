import React, { Component } from 'react';
import { withRouter } from 'react-router';
import classNames from 'classnames/bind';
import styles from '../css/main';
import navStyles from '../css/main';

const cx = classNames.bind(navStyles).bind(styles);

class AppScrollToTop extends Component {
  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0)
    }
  }

  render() {
    return (
      <div className={cx('app')}>
        {this.props.children}
      </div>
    )
  }
}

export default withRouter(AppScrollToTop)