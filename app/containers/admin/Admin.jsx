import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import MenuAdmin from 'components/header/MenuAdmin';
import styles from 'css/main';
import navStyles from 'css/admin/main';

const cx = classNames.bind(navStyles).bind(styles);

const Admin = ({children}) => {
  return (
    <div className={cx('admin')}>
    	<MenuAdmin />
    	{children}
    </div>
  );
};

Admin.propTypes = {
  children: PropTypes.object
};

export default Admin;
