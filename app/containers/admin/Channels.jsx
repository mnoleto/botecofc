import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import Channels from 'components/admin/channels';
import { createChannel, destroyChannel, updateChannel } from 'actions/channels';


function mapStateToProps(state) {
    return {
        channels: state.channel.channels,
    };
}

export default connect(mapStateToProps, { createChannel, destroyChannel, updateChannel})(Channels);
