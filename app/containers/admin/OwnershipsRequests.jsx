import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { fetchOwnerships, destroyOwnership } from 'actions/ownerships';
import { fetchPlaces, updatePlaceOwnership } from 'actions/places';
import { fetchUsers, updateUserOwnership } from 'actions/users';

import AdminTitle from 'components/admin/AdminTitle';
import OwnershipsRequestsTable from 'components/admin/OwnershipsRequestsTable';

import classNames from 'classnames/bind';
import styles from 'css/admin/main';
const cx = classNames.bind(styles);

class UsersRequests extends Component {
	static need = [
		fetchOwnerships,
		fetchPlaces,
		fetchUsers
	];

	constructor(props) {
		super(props);

		this.onApprove = this.onApprove.bind(this);
		this.onReprove = this.onReprove.bind(this);
	}

	onApprove(place, user, id) {
		const { destroyOwnership, updatePlaceOwnership, updateUserOwnership, places, users } = this.props;
		let placeIndex, placeId;

		places.forEach(function(value, index) {
			if(value.id === place.id) {
				placeIndex = index;
				placeId = value.id;
			}
		});
		let userSelected, userIndex;
		users.forEach(function(value, index) {
			if(value.email === user.email) {
				userSelected = value;
				userIndex = index;
			}
		});
		updatePlaceOwnership(placeIndex, {
			id: placeId,
			user: userSelected
		});
		const role = (userSelected.role === 'admin') ? 'admin' : 'owner';
		updateUserOwnership({
			id: userSelected._id,
			place: [...userSelected.ownerships, place],
			role: role
		});
		destroyOwnership(id);
	}

	onReprove(id) {
		const { destroyOwnership } = this.props;
		destroyOwnership(id);
	}

	render() {
		const { ownerships } = this.props;
		return (
			<div className={cx('row', 'admin-content')}>
				<AdminTitle main_title={"Solicitações de propriedade"} main_description={"Lista de usuários que querem ser donos de bares"} />

				{ownerships.length > 0 &&
					<OwnershipsRequestsTable
						ownerships={ownerships}
						onApprove={this.onApprove}
						onReprove={this.onReprove}
					/>
				}
			</div>
		);
	}
};

UsersRequests.propTypes = {
	ownerships: PropTypes.array.isRequired,
	places: PropTypes.array.isRequired,
	users: PropTypes.array.isRequired,
	destroyOwnership: PropTypes.func.isRequired,
	updatePlaceOwnership: PropTypes.func.isRequired,
	updateUserOwnership: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		ownerships: state.ownership.ownerships,
		places: state.place.places,
		users: state.users.users
	};
}

export default connect(mapStateToProps, { destroyOwnership, updatePlaceOwnership, updateUserOwnership })(UsersRequests);
