import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames/bind';
import { fetchCities } from '../../actions/cities'
import { fetchStates } from '../../actions/states'
import { fetchUsers, register, destroyUser, updateRole, exportUsers } from '../../actions/users';
import AdminTitle from '../../components/admin/AdminTitle';
import UsersForm from '../../components/admin/UsersForm';
import UsersList from '../../components/admin/UsersList';
import styles from '../../css/admin/main';

const cx = classNames.bind(styles);

class Users extends Component {
	static need = [
		fetchCities,
		fetchStates,
		fetchUsers
	];

	constructor(props) {
		super(props);

		this.state = {
			index: null,
			user: {}
		};

		this.clearChange = this.clearChange.bind(this);
		this.onDestroy = this.onDestroy.bind(this);
		this.onEdit = this.onEdit.bind(this);
		this.onEntryChange = this.onEntryChange.bind(this);
	};

	clearChange() {
		this.setState({
			index: null,
			user: {}
		});
	};

	onDestroy(id, index) {
	  const { destroyUser } = this.props;
	  destroyUser(id, index);
	};

	onEdit(data, index) {
		this.setState({
			index: index,
			user: data
		});
	};

	onEntryChange(data) {
	  const { updateRole } = this.props;
	  updateRole(data);
	  this.clearChange();
	};

	render() {
		const { cities, states, users, register } = this.props;
		return (
			<div className={cx('row', 'admin-content')}>
				<AdminTitle main_title={"Usuários"} main_description={"Responsável pelo gerenciamento de Usuários."} />

				<UsersForm
					cities={cities}
					states={states}
					user={this.state.user}
					onEntrySave={register}
					onEntryChange={this.onEntryChange}
				/>
				
				<UsersList
					users={users}
					onDestroy={this.onDestroy}
					onEdit={this.onEdit}
				/>
			</div>
		);
	}
};

Users.propTypes = {
	users: PropTypes.array,
	register: PropTypes.func.isRequired,
	destroyUser: PropTypes.func.isRequired,
	updateRole: PropTypes.func.isRequired,
	exportUsers: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		cities: state.city.cities,
		states: state.state.states,
		users: state.users.users
	};
}

export default connect(mapStateToProps, { register, destroyUser, updateRole, exportUsers })(Users);
