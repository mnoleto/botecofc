import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchCities, updateCity } from '../../actions/cities';
import { destroyPlaceTemp, updatePlaceTemp, fetchPlacesTemp } from 'actions/placesTemp';
import { fetchStates, updateState } from '../../actions/states';
import { createPlace, updatePlaceOwnership } from 'actions/places';
import { fetchTeams } from 'actions/teams';
import { dismissMessage } from 'actions/messages';
import { fetchUsers, updateUserOwnership } from 'actions/users';

import AdminTitle from 'components/admin/AdminTitle';
import PlacesForm from 'components/admin/PlacesForm';
import AdminList from 'components/admin/AdminList';

import classNames from 'classnames/bind';
import styles from 'css/admin/main';
const cx = classNames.bind(styles);

class PlacesTemp extends Component {
	static need = [
		fetchCities,
		fetchPlacesTemp,
		fetchStates,
		fetchTeams,
		fetchUsers,
	];

	constructor(props) {
		super(props);

		this.state = {
			index: null,
			placeTemp: {}
		};

		this.clearChange = this.clearChange.bind(this);
		this.onEdit = this.onEdit.bind(this);
		this.onEntrySave = this.onEntrySave.bind(this);
	};

	clearChange() {
		this.setState({
			index: null,
			placeTemp: {}
		});
	};

	onEdit(data, index) {
		this.setState({
			index: index,
			placeTemp: data
		});
	};

	onEntrySave(data) {
		const {
			cities,
			createPlace,
			destroyPlaceTemp,
			states,
			updateCity,
			updateState,
			updateUserOwnership,
			users,
		} = this.props;

		createPlace(data);
		destroyPlaceTemp(this.state.placeTemp.id);

		const city = cities.find(item => item.id === data.cityId)
		const state = states.find(item => item.id === data.stateId)
		if (city && !city.isActive) {
			updateCity({ id: data.cityId, isActive: true })
		}
		if (state && !state.isActive) {
			updateState({ id: data.stateId, isActive: true })
		}
		
		if(data.ownerships && data.ownerships.name && data.ownerships.name.length > 0) {
			const user = users
				.find((value) => value.email === this.state.placeTemp.ownerships.email);
			
			const userPlaces = [...user.ownerships, {
				id: this.state.placeTemp.id,
				name: this.state.placeTemp.name
			}];

			const role = (user.role === 'admin') ? 'admin' : 'owner';
			
			updateUserOwnership({
				id: user._id,
				place: userPlaces,
				role,
			});
		}
		this.clearChange();
	}

	onOwnershipRemove() {
		const {updateUserOwnership, updatePlaceOwnership, users} = this.props;
		updatePlaceOwnership(this.state.index, {id: this.state.place.id, user: {}});

		let index, user;
		users.forEach((value, i) => {
			if (value.email === this.state.place.ownerships.email) {
				user = value;
				index = i;
			}
		});
		
		const place = user.ownerships ? user.ownerships.filter((value, i) => {
			return (value.id !== this.state.place.id);
		}) : null;

		const role = (user.role === 'admin') ? 'admin' : (place.length > 0 ? 'owner' : 'subscriber');
		updateUserOwnership(index, {id: user._id, place, role});
	}

	render() {
		const { teams, dismissMessage, cities, states, destroyPlaceTemp } = this.props;
		const { placesTemp, message } = this.props.placeTemp;
		return (
			<div className={cx('row', 'admin-content')}>
				<AdminTitle 
					main_title={"Bares"} 
					main_description={"Responsável pelo gerenciamento de bares."} 
					/>

				<PlacesForm
					cities={cities}
					message={message}
					place={this.state.placeTemp}
					teams={teams}
					onDismissMessage={dismissMessage}
					onEntrySave={this.onEntrySave} 
					onEntryChange={this.onEntrySave}
					onOwnershipRemove={this.onOwnershipRemove}
					states={states}
				/>
				
				<AdminList
					data={placesTemp}
					headerTitle={"Botecos pendentes"}
					onDestroy={destroyPlaceTemp}
					onEdit={this.onEdit}
					/>
			</div>
		);
	}
	
};

PlacesTemp.propTypes = {
	cities: PropTypes.array.isRequired,
	user: PropTypes.object,
	placeTemp: PropTypes.object.isRequired,
	dismissMessage: PropTypes.func.isRequired,
	createPlace: PropTypes.func.isRequired,
	destroyPlaceTemp: PropTypes.func.isRequired,
	states: PropTypes.array.isRequired,
	updatePlaceTemp: PropTypes.func.isRequired,
	updatePlaceOwnership: PropTypes.func.isRequired,
	updateUserOwnership: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		cities: state.city.cities,
		placeTemp: state.placeTemp,
		teams: state.team.teams,
		states: state.state.states,
		user: state.user,
		users: state.users.users
	};
}

export default connect(mapStateToProps, {
	dismissMessage,
	destroyPlaceTemp,
	updatePlaceTemp,
	createPlace,
	updateCity,
	updateState,
	updateUserOwnership,
	updatePlaceOwnership
})(PlacesTemp);

