import classNames from 'classnames/bind';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchCities, updateCity } from '../../actions/cities';
import { createPlace, destroyPlace, updatePlace, fetchPlaces, updatePlaceOwnership } from '../../actions/places';
import { fetchStates, updateState } from '../../actions/states';
import { fetchTeams } from '../../actions/teams';
import { fetchUsers, updateUserOwnership } from '../../actions/users';
import { dismissMessage } from '../../actions/messages';
import AdminTitle from '../../components/admin/AdminTitle';
import PlacesForm from '../../components/admin/PlacesForm';
import AdminList from '../../components/admin/AdminList';
import styles from '../../css/admin/main';

const cx = classNames.bind(styles);

class Places extends Component {
	static need = [
		fetchCities,
		fetchPlaces,
		fetchStates,
		fetchTeams,
		fetchUsers
	];

	constructor(props) {
		super(props);

		this.state = {
			index: null,
			place: {}
		};

		this.clearChange = this.clearChange.bind(this);
		this.onEdit = this.onEdit.bind(this);
		this.onEntryChange = this.onEntryChange.bind(this);
		this.onEntrySave = this.onEntrySave.bind(this);
		this.onOwnershipRemove = this.onOwnershipRemove.bind(this);
		this.updateStateAndCity = this.updateStateAndCity.bind(this)
	};

	clearChange() {
		this.setState({
			index: null,
			place: {}
		});
		this.forceUpdate();
	};

	onEdit(data, index) {
		this.setState({
			index: index,
			place: data
		});
	};

	updateStateAndCity(data) {
		const { cities, states, updateCity, updateState } = this.props;
		const city = cities.find(item => item.id === data.cityId)
		const state = states.find(item => item.id === data.stateId)
		if (city && !city.isActive) {
			updateCity({ id: data.cityId, isActive: true })
		}
		if (state && !state.isActive) {
			updateState({ id: data.stateId, isActive: true })
		}
	}

	onEntryChange(data) {
		const { updatePlace } = this.props;
		this.clearChange();
		updatePlace(data);
		this.updateStateAndCity(data)
	}

	onEntrySave(data) {
		const { createPlace } = this.props;
		this.clearChange();
		createPlace(data);
		this.updateStateAndCity(data)
	}

	onOwnershipRemove() {
		const {updateUserOwnership, updatePlaceOwnership, users} = this.props;
		const _ = this;
		updatePlaceOwnership(this.state.index, {id: this.state.place.id, user: {}});

		let index, user;
		users.forEach((value, i) => {
			if (value.email === this.state.place.ownerships.email) {
				user = value;
				index = i;
			}
		});
		
		const place = user.ownerships.filter((value, i) => {
			return (value.id !== this.state.place.id);
		});
		
		const role = (user.role === 'admin') ? 'admin' : (place.length > 0 ? 'owner' : 'subscriber');
		
		updateUserOwnership({id: user._id, place, role}, () => {
			_.setState({
				index: null,
				place: {}
			});
		});
	}

	render() {
		const { cities, message, places, states, teams, dismissMessage, destroyPlace } = this.props;
		return (
			<div className={cx('row', 'admin-content')}>
				<AdminTitle
					main_title={"Bares"}
					main_description={"Responsável pelo gerenciamento de bares."}
					/>

				<PlacesForm
					cities={cities}
					message={message}
					onDismissMessage={dismissMessage}
					onEntryChange={this.onEntryChange}
					onEntrySave={this.onEntrySave}
					onOwnershipRemove={this.onOwnershipRemove}
					place={this.state.place}
					states={states}
					teams={teams}
					/>

				<AdminList
					data={places}
					headerTitle={"Botecos aprovados"}
					onDestroy={destroyPlace}
					onEdit={this.onEdit}
					/>
			</div>
		);
	}

};

Places.propTypes = {
	cities: PropTypes.array,
	dismissMessage: PropTypes.func.isRequired,
	message: PropTypes.object,
	places: PropTypes.array.isRequired,
	states: PropTypes.array,
	updatePlaceOwnership: PropTypes.func.isRequired,
	users: PropTypes.array.isRequired,
	updateUserOwnership: PropTypes.func.isRequired,
};

function mapStateToProps({
	city,
	message,
	place,
	state,
	team,
	users,
}) {
	return {
		cities: city.cities,
		message: message.message,
		places: place.places,
		states: state.states,
		teams: team.teams,
		users: users.users,
	};
}

export default connect(mapStateToProps, {
	dismissMessage,
	destroyPlace,
	updatePlace,
	createPlace,
	updateCity,
	updateState,
	updatePlaceOwnership,
	updateUserOwnership
})(Places);
