import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createCountry, destroyCountry, updateCountry, fetchCountries } from 'actions/countries';

import AdminTitle from 'components/admin/AdminTitle';
import CountriesForm from 'components/admin/CountriesForm';
import AdminList from 'components/admin/AdminList';

import classNames from 'classnames/bind';
import styles from 'css/admin/main';
const cx = classNames.bind(styles);

class Countries extends Component {
	static need = [
		fetchCountries
	];

	constructor(props) {
		super(props);

		this.state = {
			index: null,
			country: {}
		};

		this.clearChange = this.clearChange.bind(this);
		this.onDestroy = this.onDestroy.bind(this);
		this.onEdit = this.onEdit.bind(this);
		this.onEntryChange = this.onEntryChange.bind(this);
	};

	clearChange() {
		this.setState({
			index: null,
			country: {}
		});
	};

	onDestroy(id, index) {
	  const { destroyCountry } = this.props;
	  destroyCountry(id, index);
	};

	onEdit(data, index) {
		this.setState({
			index: index,
			country: data
		});
	};

	onEntryChange(data) {
		const { updateCountry } = this.props;
		updateCountry(this.state.index, data);
		this.clearChange();
	};

	render() {
		const { countries, destroyCountry, updateCountry, createCountry } = this.props;
		return (
			<div className={cx('row', 'admin-content')}>
				<AdminTitle
					main_title={"Paises"} 
					main_description={"Responsável pelo gerenciamento de paises."} 
					/>

				<CountriesForm 
					country={this.state.country} 
					onEntrySave={createCountry} 
					onEntryChange={this.onEntryChange} 
					/>

				<AdminList
					data={countries}
					headerTitle={"Paises cadastrados"}
					onDestroy={destroyCountry}
					onEdit={this.onEdit}
					/>
			</div>
		);
	}
	
};

Countries.propTypes = {
  countries: PropTypes.array.isRequired,
  destroyCountry: PropTypes.func.isRequired,
  updateCountry: PropTypes.func.isRequired,
  createCountry: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
  	countries: state.country.countries
  };
}

export default connect(mapStateToProps, { createCountry, updateCountry, destroyCountry })(Countries);
