import classNames from 'classnames/bind';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { createCity, destroyCity, updateCity, fetchCities } from '../../actions/cities';
import { fetchStates } from '../../actions/states';
import AdminTitle from '../../components/admin/AdminTitle';
import CitiesForm from '../../components/admin/CitiesForm';
import CityList from '../../components/admin/CityList';
import styles from '../../css/admin/main';

const cx = classNames.bind(styles);

class Cities extends Component {
  static need = [
    fetchCities,
    fetchStates,
  ];

  constructor(props) {
    super(props);

    this.state = {
      index: null,
      city: {}
    };

    this.clearChange = this.clearChange.bind(this);
    this.onDestroy = this.onDestroy.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onEntryChange = this.onEntryChange.bind(this);
  };

  clearChange() {
    this.setState({
      index: null,
      city: {}
    });
  };

  onDestroy(id, index) {
    const { destroyCity } = this.props;
    destroyCity(id, index);
  };

  onEdit(data, index) {
    this.setState({
      index: index,
      city: data
    });
  };

  onEntryChange(data) {
    const { updateCity } = this.props;
    updateCity(data);
    this.clearChange();
  };

  render() {
    const { cities, destroyCity, createCity, states } = this.props;
    return (
      <div className={cx('row', 'admin-content')}>
        <AdminTitle
          main_title={"Cidades"}
          main_description={"Responsável pelo gerenciamento de cidades."}
        />

        <CitiesForm
          city={this.state.city}
          cities={cities}
          states={states}
          onEntrySave={createCity}
          onEntryChange={this.onEntryChange}
        />

        <CityList
          cities={cities}
          states={states}
          headerTitle={"Cidades cadastradas"}
          onDestroy={destroyCity}
          onEdit={this.onEdit}
        />
      </div>
    );
  }
}

Cities.propTypes = {
  cities: PropTypes.array.isRequired,
  states: PropTypes.array.isRequired,
  destroyCity: PropTypes.func.isRequired,
  updateCity: PropTypes.func.isRequired,
  createCity: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  return {
    cities: state.city.cities,
    states: state.state.states,
  }
}

export default connect(mapStateToProps, { createCity, updateCity, destroyCity })(Cities);
