import classNames from 'classnames/bind';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createFigures, fetchPostsFigures, removeFigure } from '../../actions/postsFigures';
import { uploadFile } from '../../actions/files';
import AdminTitle from '../../components/admin/AdminTitle';
import styles from '../../css/admin/main';
import Figures from '../../components/admin/posts/Figures'
import { sortByDate } from '../../utils/sort'

const cx = classNames.bind(styles);

class PostFigures extends Component {
	static need = [
		fetchPostsFigures
	];

	constructor(props) {
		super(props);

		this.onUploadComplete = this.onUploadComplete.bind(this);
	}

	onUploadComplete(uploadedFiles) {
		this.props.createFigures(uploadedFiles);
	}

	render() {
		const { figures, uploadFile, removeFigure} = this.props
		return (
			<div className={cx('admin-content')}>
				<AdminTitle main_title={"Posts figurinhas"} main_description={""} />

				<div style={{ paddingLeft: 20, paddingRight: 20 }}>
					<Figures
						images={figures}
						onUploadComplete={this.onUploadComplete}
						uploadFile={uploadFile}
						removeFigure={removeFigure}
					/>
					<br/>
					Tamanho: 250 x 250 pixels
					<br/>
					Resolução: 72 dpis
					<br/>
					Alinhamento horizontal: centro / vertical: base
				</div>
			</div>
		);
	}
};

PostFigures.propTypes = {
	figures: PropTypes.array.isRequired,
};

function mapStateToProps(state) {
	return {
		figures: state.posts.figures.sort(sortByDate),
	};
}

export default connect(mapStateToProps, { createFigures, removeFigure, uploadFile })(PostFigures);
