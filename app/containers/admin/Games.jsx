import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createGame, destroyGame, updateGame, fetchGames, setVisibilityFilter } from 'actions/games';
import { fetchChampionships } from 'actions/championships';
import { fetchTeams } from 'actions/teams';
import { fetchPlaces } from 'actions/places';
import { fetchChannels } from 'actions/channels';
import AdminTitle from 'components/admin/AdminTitle';
import GamesForm from 'components/admin/GamesForm';
import GamesList from 'components/admin/GamesList';
import classNames from 'classnames/bind';
import styles from 'css/admin/main';
import { filterRecentGames } from '../../utils/filters'

const cx = classNames.bind(styles);

class Games extends Component {
	static need = [
		fetchGames,
		fetchChampionships,
		fetchTeams,
		fetchPlaces,
		fetchChannels,
	];

	constructor(props) {
		super(props);

		this.state = {
			index: null,
			game: {}
		};

		this.clearChange = this.clearChange.bind(this);
		this.onChangeChampionshipFilter = this.onChangeChampionshipFilter.bind(this);
		this.onDestroy = this.onDestroy.bind(this);
		this.onEdit = this.onEdit.bind(this);
		this.onEntryChange = this.onEntryChange.bind(this);
	};

	clearChange() {
		this.setState({
			index: null,
			game: {}
		});
	};

	onChangeChampionshipFilter(filter, championship) {
		const { setVisibilityFilter } = this.props;
		setVisibilityFilter(filter, championship, '', '');
	};

	onDestroy(id, index) {
	  const { destroyGame } = this.props;
	  destroyGame(id, index);
	};

	onEdit(data, index) {
		this.setState({
			index: index,
			game: data
		});
	};

	onEntryChange(data) {
		const { updateGame } = this.props;
		updateGame(this.state.index, data);
		this.clearChange();
	};

	render() {
		const { channels, games, championships, teams, createGame } = this.props;
		return (
			<div className={cx('row', 'admin-content')}>
				<AdminTitle 
					main_title={"Jogos"} 
					main_description={"Responsável pelo gerenciamento de jogos."} 
					/>

				<GamesForm 
					channels={channels}
					game={this.state.game}
					championships={championships}
					teams={teams}
					onEntrySave={createGame} 
					onEntryChange={this.onEntryChange} 
					onChangeChampionshipFilter={this.onChangeChampionshipFilter}
					/>
					
				<GamesList
					data={games}
					headerTitle={"Jogos cadastrados"}
					onDestroy={this.onDestroy}
					onEdit={this.onEdit}
					/>
			</div>
		);
	}
	
};

Games.propTypes = {
	channels: PropTypes.array.isRequired,
	games: PropTypes.array.isRequired,
	championships: PropTypes.array.isRequired,
	teams: PropTypes.array.isRequired,
	createGame: PropTypes.func.isRequired,
	updateGame: PropTypes.func.isRequired,
	destroyGame: PropTypes.func.isRequired,
	setVisibilityFilter: PropTypes.func.isRequired
};

function filterTeams(teams, championships, filters) {
	if(filters.show === 'SHOW_ALL') {
		return teams;
	} else {
		let seletedTeams = [];
		let selectedChampionship;

		championships.forEach(function(item, index) {
			if( item.id === filters.championship) selectedChampionship = item;
		});
		
		teams.forEach(function(team, index) {
			selectedChampionship.teams.forEach(function(tm, i) {
				if(team.id === tm.id) seletedTeams.push(team);
			});
		});

		return seletedTeams;
	}
}

function mapStateToProps(state) {
	return {
		channels: state.channel.channels,
		games: filterRecentGames(state.game.games),
		championships: state.championship.championships,
		teams: filterTeams(state.team.teams, state.championship.championships, state.visibilityFilter.gameVisibilityFilter)
	};
}

export default connect(mapStateToProps, { createGame, updateGame, destroyGame, setVisibilityFilter })(Games);

