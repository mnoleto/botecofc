import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createTeam, destroyTeam, fetchTeams, updateTeam } from 'actions/teams';
import { fetchCountries } from 'actions/countries';
import { fetchSports } from 'actions/sports';

import AdminList from 'components/admin/AdminList';
import AdminTitle from 'components/admin/AdminTitle';
import TeamsForm from 'components/admin/TeamsForm';

import classNames from 'classnames/bind';
import styles from 'css/admin/main';
const cx = classNames.bind(styles);

class Teams extends Component {
	//Data that needs to be called before rendering the component
	//This is used for server side rending via the fetchComponentDataBeforeRender() method
	static need = [
		fetchTeams,
		fetchCountries,
		fetchSports,
	];

	constructor(props) {
		super(props);

		this.state = {
			index: null,
			team: {}
		};

		this.clearChange = this.clearChange.bind(this);
		this.onDestroy = this.onDestroy.bind(this);
		this.onEdit = this.onEdit.bind(this);
		this.onEntryChange = this.onEntryChange.bind(this);
	};

	clearChange() {
		this.setState({
			index: null,
			team: {}
		});
	};

	onDestroy(id, index) {
	  const { destroyTeam } = this.props;
	  destroyTeam(id, index);
	};

	onEdit(data, index) {
		this.setState({
			index: index,
			team: data
		});
	};

	onEntryChange(data) {
	  const { updateTeam } = this.props;
	  updateTeam(this.state.index, data);
	  this.clearChange();
	};

	render() {
		const { teams, countries, sports, createTeam } = this.props;
		return (
			<div className={cx('row', 'admin-content')}>
				<AdminTitle 
					main_title={"Times"} 
					main_description={"Responsável pelo gerenciamento de times."} 
					/>

				<TeamsForm 
					team={this.state.team} 
					countries={countries} 
					sports={sports} 
					onEntrySave={createTeam} 
					onEntryChange={this.onEntryChange} 
					/>
				
				<AdminList
					data={teams}
					folder={'teams'}
					headerTitle={"Times cadastrados"}
					onDestroy={this.onDestroy}
					onEdit={this.onEdit}
					/>
			</div>
		);
	}
};

Teams.propTypes = {
	teams: PropTypes.array.isRequired,
	countries: PropTypes.array.isRequired,
	sports: PropTypes.array.isRequired,
	createTeam: PropTypes.func.isRequired,
	updateTeam: PropTypes.func.isRequired,
	destroyTeam: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		teams: state.team.teams,
		countries: state.country.countries,
		sports: state.sport.sports
	};
}

export default connect(mapStateToProps, { createTeam, updateTeam, destroyTeam })(Teams);
