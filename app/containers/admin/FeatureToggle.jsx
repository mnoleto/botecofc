import { connect } from 'react-redux';
import { updateGeneratePosts } from '../../actions/places';
import { destroyFeatureToggle } from '../../actions/featureToggle';
import FeatureToggle from '../../components/admin/featureToggle/';

function mapStateToProps(state) {
  return {
    places: state.place.places,
    featureToggles: state.featureToggle.featureToggles,
  };
}

export default connect(mapStateToProps, { updateGeneratePosts, destroyFeatureToggle })(FeatureToggle);
