import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames/bind';
import { fetchCities } from '../../actions/cities'
import { fetchStates } from '../../actions/states'
import { fetchPlacesSuggestions, destroyPlaceSuggestion } from '../../actions/placesSuggestions';
import { approvePlaceTemp} from '../../actions/placesTemp';
import AdminTitle from '../../components/admin/AdminTitle';
import PlacesSuggestionsTable from '../../components/admin/PlacesSuggestionsTable';
import styles from '../../css/admin/main';

const cx = classNames.bind(styles);

class PlacesSuggestions extends Component {
	static need = [
		fetchCities,
		fetchPlacesSuggestions
	];

	constructor(props) {
		super(props);
		this.onApprove = this.onApprove.bind(this);
	}

	onApprove(data, id) {
		const { approvePlaceTemp, destroyPlaceSuggestion } = this.props;
		
		approvePlaceTemp(data, function(status) {
			if(status === 'success') destroyPlaceSuggestion(id);
		});
	}

	render() {
		const { cities, states, placesSuggestions, destroyPlaceSuggestion } = this.props;
		return (
			<div className={cx('row', 'admin-content')}>
				<AdminTitle main_title={"Sugestão de bares"} main_description={"Lista de bares sugeridos pelos usuários."} />

				<PlacesSuggestionsTable
					cities={cities}
					states={states}
					placesSuggestions={placesSuggestions}
					onApprove={this.onApprove}
					onDisapprove={destroyPlaceSuggestion}
				/>
			</div>
		);
	}
};

PlacesSuggestions.propTypes = {
	placesSuggestions: PropTypes.array.isRequired,
	approvePlaceTemp: PropTypes.func.isRequired,
	destroyPlaceSuggestion: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		cities: state.city.cities,
		states: state.state.states,
		placesSuggestions: state.suggestion.placeSuggestions
	};
}

export default connect(mapStateToProps, { approvePlaceTemp, destroyPlaceSuggestion })(PlacesSuggestions);
