import classNames from 'classnames/bind';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createState, destroyState, updateState, fetchStates } from '../../actions/states';
import AdminTitle from '../../components/admin/AdminTitle';
import StatesForm from '../../components/admin/StatesForm';
import AdminList from '../../components/admin/AdminList';
import styles from '../../css/admin/main';

const cx = classNames.bind(styles);

class States extends Component {
  static need = [
    fetchStates
  ];

  constructor(props) {
    super(props);

    this.state = {
      index: null,
      state: {}
    };

    this.clearChange = this.clearChange.bind(this);
    this.onDestroy = this.onDestroy.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onEntryChange = this.onEntryChange.bind(this);
  };

  clearChange() {
    this.setState({
      index: null,
      state: {}
    });
  };

  onDestroy(id, index) {
    const { destroyState } = this.props;
    destroyState(id, index);
  };

  onEdit(data, index) {
    this.setState({
      index: index,
      state: data
    });
  };

  onEntryChange(data) {
    const { updateState } = this.props;
    updateState(data);
    this.clearChange();
  };

  render() {
    const { states, destroyState, createState } = this.props;
    return (
      <div className={cx('row', 'admin-content')}>
        <AdminTitle
          main_title={"Estados"}
          main_description={"Responsável pelo gerenciamento de estados."}
        />

        <StatesForm
          state={this.state.state}
          onEntrySave={createState}
          onEntryChange={this.onEntryChange}
        />

        <AdminList
          data={states}
          headerTitle={"Estados cadastrados"}
          onDestroy={destroyState}
          onEdit={this.onEdit}
        />
      </div>
    );
  }

};

States.propTypes = {
  states: PropTypes.array.isRequired,
  destroyState: PropTypes.func.isRequired,
  updateState: PropTypes.func.isRequired,
  createState: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    states: state.state.states
  };
}

export default connect(mapStateToProps, { createState, updateState, destroyState })(States);
