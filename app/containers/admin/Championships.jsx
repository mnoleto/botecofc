import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createChampionship, destroyChampionship, fetchChampionships, setVisibilityFilter, updateChampionship } from 'actions/championships';
import { fetchSports } from 'actions/sports';
import { fetchCountries } from 'actions/countries';
import { fetchTeams } from 'actions/teams';

import AdminList from 'components/admin/AdminList';
import AdminTitle from 'components/admin/AdminTitle';
import ChampionshipsForm from 'components/admin/ChampionshipsForm';

import classNames from 'classnames/bind';
import styles from 'css/admin/main';
const cx = classNames.bind(styles);

/*
 * CHAMPIONSHIPS
 */
class Championships extends Component {
	static need = [
		fetchChampionships,
		fetchSports,
		fetchCountries,
		fetchTeams
	];

	constructor(props) {
		super(props);

		this.state = {
			index: null,
			championship: {}
		};

		this.clearChange = this.clearChange.bind(this);
		this.onChangeTeamFilter = this.onChangeTeamFilter.bind(this);
		this.onDestroy = this.onDestroy.bind(this);
		this.onEdit = this.onEdit.bind(this);
		this.onEntryChange = this.onEntryChange.bind(this);
	};

	clearChange() {
		this.setState({
			index: null,
			championship: {}
		});
	};

	onChangeTeamFilter(filter, data) {
		const { setVisibilityFilter } = this.props;
		setVisibilityFilter(filter, data.sports, data.countries);
	};

	onDestroy(id, index) {
	  const { destroyChampionship } = this.props;
	  destroyChampionship(id, index);
	};

	onEdit(data, index) {
		this.setState({
			index: index,
			championship: data
		});
	};

	onEntryChange(data) {
	  const { updateChampionship } = this.props;
	  updateChampionship(this.state.index, data);
	  this.clearChange();
	};

	render() {
		const { championships, sports, countries, teams, createChampionship } = this.props;

		return (
			<div className={cx('row', 'admin-content')}>
				<AdminTitle
					main_title={"Campeonatos"}
					main_description={"Responsável pelo gerenciamento de campeonatos."}
					/>
				
				<ChampionshipsForm
					championship={this.state.championship}
					sports={sports}
					countries={countries}
					teams={teams}
					onEntrySave={createChampionship}
					onEntryChange={this.onEntryChange}
					onChangeTeamFilter={this.onChangeTeamFilter}
					/>
				
				<AdminList
					data={championships}
					folder={'championships'}
					headerTitle={"Campeonatos cadastrados"}
					onDestroy={this.onDestroy}
					onEdit={this.onEdit}
					/>
			</div>
		);
	}
	
};

Championships.propTypes = {
  championships: PropTypes.array.isRequired,
  sports: PropTypes.array.isRequired,
  countries: PropTypes.array.isRequired,
  teams: PropTypes.array.isRequired,
  createChampionship: PropTypes.func.isRequired,
  updateChampionship: PropTypes.func.isRequired,
  destroyChampionship: PropTypes.func.isRequired,
  setVisibilityFilter: PropTypes.func.isRequired
};

function filterTeamsByCountry(teams, filters) {
	if( filters.countries.length === 0) {
		return teams;
	} else {
		let countries = [];
		teams.forEach(function(team, index) {
			filters.countries.forEach(function(country, index) {
				if(team.country.id === country.id) countries.push(team);
			});
		});
		return countries;
	}
}

function filterTeamsBySport(teams, filters) {
	if (filters.sports.length === 0) {
		return teams;
	} else {
		let sports = [];
		teams.forEach(function(team, index) {
			filters.sports.forEach(function(sport, index) {
				if(team.sport.id === sport.id) sports.push(team);
			});
		});
		return sports;
	}
}

function filterTeams(teams, filters) {
	if (filters.show === 'SHOW_ALL'){
		return teams;
	} else {
		return filterTeamsBySport(filterTeamsByCountry(teams, filters), filters);
	}
}

function filterSports(sports, filters) {
	if (filters.show === 'SHOW_ALL') {
		return sports;
	} else {
		return sports.filter(function(sport) {
			return !filters.sports.some(function(fil) {
				return sport.id === fil.id;
			});
		});
	}
}

function filterCountries(countries, filters) {
	if (filters.show === 'SHOW_ALL') {
		return countries;
	} else {
		return countries.filter(function(country) {
			return !filters.countries.some(function(fil) {
				return country.id === fil.id;
			});
		});
	}
}

function mapStateToProps(state) {
	return {
		championships: state.championship.championships,
		sports: state.sport.sports,
		countries: state.country.countries,
		teams: filterTeams(state.team.teams, state.visibilityFilter.championshipsVisibilityFilter)
	};
}


export default connect(mapStateToProps, { createChampionship, updateChampionship, destroyChampionship, setVisibilityFilter })(Championships);

