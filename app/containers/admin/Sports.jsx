import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSport, destroySport, updateSport, fetchSports } from 'actions/sports';

import AdminTitle from 'components/admin/AdminTitle';
import SportsForm from 'components/admin/SportsForm';
import AdminList from 'components/admin/AdminList';

import classNames from 'classnames/bind';
import styles from 'css/admin/main';
const cx = classNames.bind(styles);

class Sports extends Component {
	static need = [
		fetchSports
	];

	constructor(props) {
		super(props);

		this.state = {
			index: null,
			sport: {}
		};

		this.clearChange = this.clearChange.bind(this);
		this.onDestroy = this.onDestroy.bind(this);
		this.onEdit = this.onEdit.bind(this);
		this.onEntryChange = this.onEntryChange.bind(this);
	};

	clearChange() {
		this.setState({
			index: null,
			sport: {}
		});
	};

	onEdit(data, index) {
		this.setState({
			index: index,
			sport: data
		});
	};

	onDestroy(id, index) {
	  const { destroySport } = this.props;
	  destroySport(id, index);
	};

	onEntryChange(data) {
	  const { updateSport } = this.props;
	  updateSport(this.state.index, data);
	  this.clearChange();
	};

	render() {
		const { sports, createSport } = this.props;
		return (
			<div className={cx('row', 'admin-content')}>
				<AdminTitle 
					main_title={"Modalidades esportivas"} 
					main_description={"Responsável pelo gerenciamento de modalidades esportivas."} 
					/>

				<SportsForm 
					sport={this.state.sport} 
					onEntrySave={createSport} 
					onEntryChange={this.onEntryChange} 
					/>

				<AdminList
					data={sports}
					headerTitle={"Esportes cadastrados"}
					onDestroy={this.onDestroy}
					onEdit={this.onEdit}
					/>
			</div>
		);
	}
	
};

Sports.propTypes = {
  sports: PropTypes.array.isRequired,
  createSport: PropTypes.func.isRequired,
  updateSport: PropTypes.func.isRequired,
  destroySport: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    sports: state.sport.sports
  };
}

export default connect(mapStateToProps, { createSport, updateSport, destroySport })(Sports);

