import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createBanner, destroyBanner, fetchBanners, updateBanner } from 'actions/banners';
import { fetchCities } from 'actions/cities';
import { fetchStates } from 'actions/states';

import AdminList from 'components/admin/AdminList';
import AdminTitle from 'components/admin/AdminTitle';
import BannersForm from 'components/admin/BannersForm';

import classNames from 'classnames/bind';
import styles from 'css/admin/main';
const cx = classNames.bind(styles);

class Banners extends Component {
  static need = [
    fetchBanners,
    fetchCities,
    fetchStates
  ];

  constructor(props) {
    super(props);

    this.state = {
      banner: {}
    };

    this.clearChange = this.clearChange.bind(this);
    this.onDestroy = this.onDestroy.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onEntryChange = this.onEntryChange.bind(this);
  };

  clearChange() {
    this.setState({
      banner: {}
    });
  };

  onDestroy(id) {
    const { destroyBanner } = this.props;
    destroyBanner(id);
  };

  onEdit(data) {
    this.setState({
      banner: data
    });
  };

  onEntryChange(data) {
    const { updateBanner } = this.props;
    updateBanner(data);
    this.clearChange();
  };

  render() {
    const { banners, cities, createBanner, states } = this.props;
    return (
      <div className={cx('row', 'admin-content')}>
        <AdminTitle
          main_title={"Banners"}
          main_description={"Responsável pelo gerenciamento de banners."}
        />

        <BannersForm
          banner={this.state.banner}
          cities={cities}
          states={states}
          onEntrySave={createBanner}
          onEntryChange={this.onEntryChange}
          onClear={this.clearChange}
        />

        <AdminList
          data={banners}
          folder={'banner'}
          headerTitle={"Banners cadastrados"}
          onDestroy={this.onDestroy}
          onEdit={this.onEdit}
        />
      </div>
    );
  }
};

Banners.propTypes = {
  banners: PropTypes.array.isRequired,
  createBanner: PropTypes.func.isRequired,
  updateBanner: PropTypes.func.isRequired,
  destroyBanner: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    banners: state.banners.banners,
    cities: state.city.cities,
    states: state.state.states,
  };
}

export default connect(mapStateToProps, { createBanner, updateBanner, destroyBanner })(Banners);
