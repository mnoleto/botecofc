import React from 'react';
import { connect } from 'react-redux';

import classNames from 'classnames/bind';
import styles from 'css/admin/main';

const cx = classNames.bind(styles);

/*
 * Note: This is kept as a container-level component,
 *  i.e. We should keep this as the container that does the data-fetching
 *  and dispatching of actions if you decide to have any sub-components.
 */
const Dashboard = () => <div className={cx('row', 'admin-content')}><h1 className={cx('welcome')}>Bem-vindo ao Painel de controle do Boteco FC.</h1></div>;

export default Dashboard;
