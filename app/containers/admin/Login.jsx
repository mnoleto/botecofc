import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { manualLogin } from 'actions/users';

import AdminTitle from 'components/admin/AdminTitle';
import LoginForm from 'components/admin/LoginForm';

import classNames from 'classnames/bind';
import styles from 'css/admin/main';
const cx = classNames.bind(styles);

class Login extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { message } = this.props.user;

    return (
      <div className={cx('row', 'admin-content')}>
        <AdminTitle
          main_title={"ÁREA RESTRITA"}
          />
        
        <div className={cx('admin-container')}>
          <LoginForm onLogin={manualLogin} message={message} />
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  user: PropTypes.object
};

function mapStateToProps(state) {
  return {
    user: state.user
  };
}

export default connect(mapStateToProps, {manualLogin})(Login);
