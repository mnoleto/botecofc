import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchChampionships } from 'actions/championships';
import { fetchGames, setVisibilityFilter } from 'actions/games';
import { fetchPlaces, updateBroadcastGame } from 'actions/places';
import AdminTitle from 'components/admin/AdminTitle';
import AdminList from 'components/admin/AdminList';
import BroadcastsForm from 'components/admin/BroadcastsForm';
import { filterRecentGames } from '../../utils/filters'

import classNames from 'classnames/bind';
import styles from 'css/admin/main';
const cx = classNames.bind(styles);

class Broadcasts extends Component {
	static need = [
		fetchChampionships,
		fetchGames,
		fetchPlaces
	];

	constructor(props) {
		super(props);
		this.state = {
			place: {},
			broadcastGames: []
		};
		this.filterConfirmed = this.filterConfirmed.bind(this);
		this.getPlaceById = this.getPlaceById.bind(this);
		this.removeGame = this.removeGame.bind(this);
		this.selectChampionship = this.selectChampionship.bind(this);
		this.selectGame = this.selectGame.bind(this);
		this.selectPlace = this.selectPlace.bind(this);
	}

	getPlaceById(id) {
		const { places } = this.props;
		return places.find(function(value, index) {
			return (value.id === id)
		});
	}

	removeGame(game) {
		const { updateBroadcastGame } = this.props;
		if(!game || game === null) return false;
		const index = this.state.broadcastGames.findIndex(function(value, i) {
			return (value.id === game);
		});
		this.state.broadcastGames.splice(index, 1);
		updateBroadcastGame({
			id: this.state.place.id,
			broadcastGames: this.state.broadcastGames
		});
	}

	selectChampionship(filter, championship) {
		const { setVisibilityFilter } = this.props;
		setVisibilityFilter(filter, championship, '', '');
	};

	selectGame(game) {
		const { updateBroadcastGame } = this.props;
		this.state.broadcastGames.push(game);
		updateBroadcastGame({
			id: this.state.place.id,
			broadcastGames: this.state.broadcastGames
		});
	}

	selectPlace(place) {
		this.setState({ place });
		if(place && place.broadcastGames && place.broadcastGames.length > 0) {
			this.setState({broadcastGames: place.broadcastGames.filter(function(game, index) {
				Date.prototype.removeHours= function(h){
			      this.setHours(this.getHours()-h);
			      return this;
			  }
				const now = new Date();
				const filterDate = new Date(now.toUTCString()).removeHours(2);
				if(game && new Date(game.gameDate) > filterDate) return game;
			})});
		} else {
			this.setState({broadcastGames: []});
		}
	}

	filterConfirmed(games) {
		if (this.state.broadcastGames.length === 0) return games;
		const notConfirmed = games.filter((game) => {
			let confirmed = false;
			this.state.broadcastGames.map((broadcast) => {
				if (game.id === broadcast.id) {
					confirmed = true;
				}
			});
			if (!confirmed) return game;
		});
		return notConfirmed;
	}

	render() {
		const { championships, games, places } = this.props;

		return (
			<div className={cx('row', 'admin-content')}>
				<AdminTitle main_title={"Transmissão de jogos"} main_description={""} />

				<BroadcastsForm
					place={this.state.place}
					championships={championships}
					games={this.filterConfirmed(games)}
					broadcastGames={this.state.broadcastGames}
					onRemoveGame={this.removeGame}
					onSelectChampionship={this.selectChampionship}
					onSelectGame={this.selectGame}
					onSelectPlace={this.selectPlace}
				/>

				<AdminList
					data={places}
					headerTitle={"Botecos aprovados"}
					onEdit={this.selectPlace}
				/>
			</div>
		);
	}
};

Broadcasts.propTypes = {
	championships: PropTypes.array.isRequired,
	games: PropTypes.array.isRequired,
	places: PropTypes.array.isRequired
};

function filterGames(games, filter) {
	if(filter.show === 'SHOW_ALL') {
		return games;
	} else {
		return games.filter(function(game, index) {
			if(game.championship && game.championship.id) {
				return (game.championship.id === filter.championship)
			}
		});
	}
}

function mapStateToProps(state) {
	return {
		championships: state.championship.championships,
		games: filterGames(filterRecentGames(state.game.games), state.visibilityFilter.gameVisibilityFilter),
		places: state.place.places
	};
}

export default connect(mapStateToProps, { setVisibilityFilter, updateBroadcastGame })(Broadcasts);
