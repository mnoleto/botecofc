import classNames from 'classnames/bind';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createBgs, fetchPostsBgs, removeBg } from '../../actions/postsBg';
import { uploadFile } from '../../actions/files';
import AdminTitle from '../../components/admin/AdminTitle';
import styles from '../../css/admin/main';
import Bgs from '../../components/admin/posts/Bgs'
import { sortByDate } from '../../utils/sort'

const cx = classNames.bind(styles);

class PostBg extends Component {
	static need = [
		fetchPostsBgs
	];

	constructor(props) {
		super(props);

		this.onAdd = this.onAdd.bind(this);
	}

	onAdd(bgs) {
		return this.props.createBgs(bgs);
	}

	render() {
		const { bgs, removeBg, uploadFile } = this.props
		return (
			<div className={cx('admin-content')}>
				<AdminTitle main_title={"Posts backgrounds"} main_description={""} />
				<Bgs bgs={bgs} onAdd={this.onAdd} uploadFile={uploadFile} removeBg={removeBg} />
			</div>
		);
	}
};

PostBg.propTypes = {
	bgs: PropTypes.array.isRequired,
};

function mapStateToProps(state) {
	return {
		bgs: state.posts.bgs.sort(sortByDate),
	};
}

export default connect(mapStateToProps, { createBgs, removeBg, uploadFile })(PostBg);
