import { connect } from 'react-redux';
import Places from '../components/places';
import { filterPlacesByCity } from '../utils/filters';
import { setVisibilityFilter } from '../actions/places';
import { createPlaceSuggestion, dismissMessage } from '../actions/placesSuggestions';

function mapStateToProps({
	championship,
	city,
	game,
	place,
	state,
	suggestion,
	team,
	user,
	visibilityFilter,
}) {
	return {
		championships: championship.championships,
		city: visibilityFilter.placesVisibilityFilter.city,
		cities: city.cities,
		games: game.games,
		places: filterPlacesByCity(place.places, user.selectedCity),
		states: state.states,
		suggestions: suggestion,
		teams: team.teams,
		user: user,
	};
}

export default connect(mapStateToProps, { createPlaceSuggestion, dismissMessage, setVisibilityFilter })(Places);
