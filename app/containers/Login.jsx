import { connect } from 'react-redux';
import Login from '../components/login';

function mapStateToProps(state) {
	return {
		user: state.user
	};
}

export default connect(mapStateToProps)(Login);
