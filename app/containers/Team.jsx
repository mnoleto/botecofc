import { connect } from 'react-redux';
import Team from '../components/team';
import {
  setTeamVisibilityFilter,
  incrementScore,
  decrementScore,
} from '../actions/teams';
import { userRecomendTeam } from '../actions/places';
import {
  addPlaceToWatch,
  removePlaceToWatch,
  addTeamToFavorites,
  removeTeamFromFavorites,
} from '../actions/users';
import {
  filterChampionshipsByTeam,
  filterPlacesByTeam,
  filterGamesByTeam,
  filterConfirmedGames,
  filterRecentGames,
} from '../utils/filters';

function mapStateToProps(state) {
  return {
    user: state.user,
    teams: state.team.teams,
    games: filterConfirmedGames(filterGamesByTeam(filterRecentGames(state.game.games), state.visibilityFilter.teamVisibilityFilter), state.place.places),
    championships: filterChampionshipsByTeam(state.championship.championships, state.visibilityFilter.teamVisibilityFilter),
    places: filterPlacesByTeam(state.place.places, state.visibilityFilter.teamVisibilityFilter),
    allplaces: state.place.places
  };
}

export default connect(mapStateToProps, {
  setTeamVisibilityFilter,
  addPlaceToWatch,
  removePlaceToWatch,
  addTeamToFavorites,
  removeTeamFromFavorites,
  userRecomendTeam,
  incrementScore,
  decrementScore
})(Team);
