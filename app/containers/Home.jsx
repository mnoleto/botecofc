import { connect } from 'react-redux';
import Home from '../components/home';
import { setVisibilityFilter } from '../actions/games';
import { toggleLoading } from '../actions/users';
import { filterGames, filterHomePlacesByCity, filterPlacesByCity , filterRecentGames} from '../utils/filters';

const filterBannersByLocation = (banners, selectedCity, cities) => {
	const city = cities.find(item => item.id === selectedCity)
	if (city) {
		const selectedState = city.state_id
		return banners.reduce((acc, banner) => {
			if (banner.cityId === '' && banner.stateId === '') {
				return [...acc, banner]
			}
			if (banner.cityId !== '' && banner.cityId === selectedCity) {
				return [...acc, banner]
			}
			if (banner.stateId !== '' && banner.stateId === selectedState) {
				return [...acc, banner]
			}
			return [...acc]
		}, [])
	}
	return banners
}

function mapStateToProps(state, props) {
	return {
		account: state.user.account,
		allgames: filterRecentGames(state.game.games),
		allplaces: filterPlacesByCity(state.place.places, state.user.selectedCity),
		authenticated: state.user.authenticated,
		banners: filterBannersByLocation(state.banners.banners, state.user.selectedCity, state.city.cities),
		championships: state.championship.championships,
		ga: props.ga,
		games: filterGames(filterRecentGames(state.game.games), state.visibilityFilter.gameVisibilityFilter),
		gameVisibilityFilter: state.visibilityFilter.gameVisibilityFilter,
		isLoading: state.user.isLoading,
		places: filterHomePlacesByCity(state.place.places, state.user.selectedCity, state.city.cities),
		teams: state.team.teams,
		termsAccepted : state.user.termsAccepted,
	};
}

export default connect(mapStateToProps, { setVisibilityFilter, toggleLoading })(Home);
