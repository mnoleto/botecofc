import { connect } from 'react-redux';
import { setTeamsVisibilityFilter } from '../actions/teams';
import { filterTeamsByCountry, filterRecentGames } from '../utils/filters';
import Teams from '../components/teams';

function mapStateToProps(state) {
	return {
		countries: state.country.countries,
		games: filterRecentGames(state.game.games),
		teams: filterTeamsByCountry(state.team.teams, state.visibilityFilter.teamsVisibilityFilter)
	};
}

export default connect(mapStateToProps, { setTeamsVisibilityFilter })(Teams);
