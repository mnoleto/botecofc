import { connect } from 'react-redux';
import BroadcastList from '../components/broadcast/list';
import { setVisibilityFilter } from '../actions/ownerships';
import { filterPlaces, filterRecentGames } from '../utils/filters';

function mapStateToProps(state) {
  return {
    allGames: filterRecentGames(state.game.games),
    places: filterPlaces(state.place.places, state.user.account.ownerships),
    teams: state.team.teams,
    channels: state.channel.channels,
    user: state.user,
    ownerManageFilter: state.visibilityFilter.ownerManageFilter,
  };
}

export default connect(mapStateToProps, { setVisibilityFilter })(BroadcastList);
