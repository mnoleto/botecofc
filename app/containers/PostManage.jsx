import { connect } from 'react-redux';
import PostManage from '../components/post/manage';
import { setVisibilityFilter } from '../actions/ownerships';
import { createFeatureToggle } from '../actions/featureToggle';
import { createPost } from '../actions/posts';
import { incrementPosts } from '../actions/places';
import { uploadFile } from '../actions/files';
import { filterPlaces, filterRecentGames } from '../utils/filters';
import { sortByDate } from '../utils/sort';

function mapStateToProps(state) {
  return {
    allGames: filterRecentGames(state.game.games),
    bgs: state.posts.bgs.sort(sortByDate).map((bg) => {
      return {
        medium: bg.portrait.responseText,
        large: bg.rect.responseText,
      }
    }),
    championships: state.championship.championships,
    channels: state.channel.channels,
    featureToggles: state.featureToggle.featureToggles,
    figures: state.posts.figures.sort(sortByDate).map((figure) => {
      return figure.image.responseText;
    }),
    ownerManageFilter: state.visibilityFilter.ownerManageFilter,
    places: filterPlaces(state.place.places, state.user.account.ownerships),
    post: state.post.image,
    teams: state.team.teams,
    user: state.user,
  };
}

export default connect(mapStateToProps, {
  setVisibilityFilter,
  createFeatureToggle,
  createPost,
  incrementPosts,
  uploadFile,
})(PostManage);
