import { connect } from 'react-redux';
import { createOwnership } from '../actions/ownerships';
import { selectPreferredCity } from '../actions/users'
import Place from '../components/place';

function mapStateToProps({
	city,
	state,
	game,
	place,
	team,
	user,
}) {
	return {
		cities: city.cities,
		states: state.states,
		games: game.games,
		places: place.places,
		teams: team.teams,
		user: user,
    selectedCity: user.selectedCity,
	};
}

export default connect(mapStateToProps, { createOwnership, selectPreferredCity })(Place);
