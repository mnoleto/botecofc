import { connect } from 'react-redux';
import Game from '../components/game';
import { filterPlacesByCity } from '../utils/filters';
import { setVisibilityFilter } from '../actions/games';

function mapStateToProps({
	city,
	place,
	game,
	team,
	user,
	visibilityFilter,
}) {
	return {
		allplaces: place.places,
		games: game.games,
		teams: team.teams,
		places: filterPlacesByCity(place.places, user.selectedCity, city.cities),
    gameVisibilityFilter: visibilityFilter.gameVisibilityFilter
	};
}

export default connect(mapStateToProps, { setVisibilityFilter })(Game);
