import moment from 'moment';
import { connect } from 'react-redux';
import ManageBroadcast from '../components/broadcast/manage';
import { updateBroadcastGame } from '../actions/places';
import { setVisibilityFilter } from '../actions/ownerships';
import { filterPlaces, filterRecentGames } from '../utils/filters';

function filterGames(games, filter) {
	if(filter.show === 'SHOW_ALL') {
		return games;
	} else {
		if(!games || !filter) return;
		return games.filter(game => {
			if(!filter.championship || filter.championship === '') {
				return true;
			}
			const gameDate = moment(game.gameDate);
			const isDate = (gameDate >= filter.initialDate && gameDate <= filter.endDate)
			const isChampionship = (game.championship && game.championship.id === filter.championship)
			return isDate && isChampionship
		});
	}
}

function mapStateToProps(state) {
	return {
		championships: state.championship.championships,
		allGames: filterRecentGames(state.game.games),
		games: filterGames(filterRecentGames(state.game.games), state.visibilityFilter.ownerManageFilter),
		places: filterPlaces(state.place.places, state.user.account.ownerships),
		teams: state.team.teams,
		channels: state.channel.channels,
		user: state.user,
		ownerManageFilter: state.visibilityFilter.ownerManageFilter,
	};
}

export default connect(mapStateToProps, {
	setVisibilityFilter,
	updateBroadcastGame,
})(ManageBroadcast);
