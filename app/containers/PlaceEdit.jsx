import { connect } from 'react-redux';
import { updatePlace, dismissMessage } from '../actions/places';
import { filterPlaces } from '../utils/filters';
import PlaceEdit from '../components/place/PlaceEdit';

function mapStateToProps(state) {
	return {
		cities: state.city.cities,
		states: state.state.states,
		message: state.place.message,
		places: filterPlaces(state.place.places, state.user.account.ownerships),
		user: state.user
	};
}

export default connect(mapStateToProps, { updatePlace, dismissMessage })(PlaceEdit);
