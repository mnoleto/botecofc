import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ga from 'react-ga';
import { fetchCities, fetchActiveCities } from '../actions/cities';
import { fetchStates, fetchActiveStates } from '../actions/states';
import Navigation from '../containers/Navigation';
import Footer from '../components/Footer';
import AppScrollToTop from '../containers/AppScrollToTop';

class App  extends Component {
	static need = [
		fetchActiveCities,
		fetchActiveStates,
		fetchCities,
		fetchStates,
	];

	componentDidMount() {
		ga.initialize('UA-93074779-1', { debug: false });
		ga.pageview(this.props.location.pathname);
	}

	UNSAFE_componentWillUpdate(nextProps) {
		if (nextProps.location.pathname !== this.props.location.pathname) {
			ga.pageview(nextProps.location.pathname);
		}
	}

	render () {
		const { children, location } = this.props;
		const isAdmin = location.pathname.includes('bfc-admin')
		return (
			<AppScrollToTop>
				<Navigation location={this.props.location} />
				{React.cloneElement(children, { ga })}
				{!isAdmin && <Footer />}
			</AppScrollToTop>
		);
	}
}

App.propTypes = {
  children: PropTypes.object
};

export default App;
