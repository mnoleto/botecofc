import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames/bind';
import { fetchCities } from '../actions/cities';
import { createPlaceTemp, dismissMessage } from '../actions/placesTemp';
import { fetchStates } from '../actions/states';
import PlaceRegisterForm from '../components/register/PlaceRegisterForm';
import styles from '../css/components/placeRegister';

const cx = classNames.bind(styles);

class RegisterPlace extends Component {
	static need = [
		fetchCities,
		fetchStates,
	];

	render() {
		const { cities, createPlaceTemp, message, states } = this.props;
		const { account } = this.props.user;

		return (
			<div className={cx('place-register')}>
				<header className={cx('register-header')}>
					<h1 className={cx('title')}>Cadastrar estabelecimento</h1>
				</header>

				<div className={cx('container')}>
					<p className={cx('welcome-phrase')}>Tem um estabelecimento? Aqui é seu lugar.<br />Insira as informações abaixo e aguarde nosso contato para confirmação dos seus dados. Muito obrigado. :)</p>
				</div>

				<PlaceRegisterForm
					cities={cities}
					dismissMessage={dismissMessage}
					entryUpdate={createPlaceTemp}
					message={message}
					states={states}
					user={account}
				/>
			</div>
		);
	}
}

function mapStateToProps({
	city,
	placeTemp,
	state,
	user
}) {
	return {
		cities: city.cities,
		message: placeTemp.message,
		states: state.states,
		user: user
	};
}

export default connect(mapStateToProps, { createPlaceTemp, dismissMessage })(RegisterPlace);
