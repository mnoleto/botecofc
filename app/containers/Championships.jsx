import { connect } from 'react-redux';
import Championships from '../components/championships';
import { filterChampionships, filterConfirmedGames } from '../utils/filters';
import { setVisibilityFilter } from '../actions/championships';

function mapStateToProps(state) {
	return {
		championships: filterChampionships(state.championship.championships, state.visibilityFilter.championshipsVisibilityFilter),
		countries: state.country.countries,
		teams: state.team.teams,
		games: filterConfirmedGames(state.game.games, state.place.places)
	};
}

export default connect(mapStateToProps, { setVisibilityFilter })(Championships);
