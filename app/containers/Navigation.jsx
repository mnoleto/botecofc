import { connect } from 'react-redux';
import { logOut } from '../actions/users';
import Header from '../components/header';

function mapStateToProps({
	championship,
	game,
	place,
	team,
	user,
}, props) {
	return {
		championships: championship.championships,
		games: game.games,
		location: props.location,
		places: place.places,
		teams: team.teams,
		user: user,
	};
}

export default connect(mapStateToProps, { logOut })(Header);
