import { connect } from 'react-redux';
import { selectPreferredCity } from '../actions/users'
import Locale from '../components/locale';

function mapStateToProps({
  city,
  state,
  user,
}) {
  return {
    activeCities: city.active,
    activeStates: state.active,
    selectedCity: user.selectedCity,
  };
}

export default connect(mapStateToProps, { selectPreferredCity })(Locale);
