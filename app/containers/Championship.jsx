import { connect } from 'react-redux';
import Championship from '../components/championship';
import { filterConfirmedGames, filterRecentGames } from '../utils/filters';

function mapStateToProps(state) {
	return {
		championships: state.championship.championships,
		games: filterConfirmedGames(filterRecentGames(state.game.games), state.place.places),
		teams: state.team.teams
	};
}

export default connect(mapStateToProps)(Championship);
