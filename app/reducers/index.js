import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import banners from './banners';
import championship from './championship';
import channel from './channel';
import city from './city';
import country from './country';
import featureToggle from './featureToggle';
import game from './game';
import message from './message';
import ownership from './ownership';
import place from './place';
import placeTemp from './placeTemp';
import post from './post';
import sport from './sport';
import state from './state';
import suggestion from './suggestion';
import team from './team';
import user from './user';
import users from './users';
import visibilityFilter from './visibilityFilter';
import posts from './posts';

const rootReducer = combineReducers({
  banners,
  championship,
  channel,
  city,
  country,
  featureToggle,
  game,
  message,
  ownership,
  place,
  placeTemp,
  post,
  posts,
  routing,
  sport,
  state,
  suggestion,
  team,
  user,
  users,
  visibilityFilter,
});

export default rootReducer;
