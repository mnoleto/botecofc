import {
	CREATE_CHAMPIONSHIP_REQUEST,
	CREATE_CHAMPIONSHIP_FAILURE,
	UPDATE_CHAMPIONSHIP_REQUEST,
	UPDATE_CHAMPIONSHIP_FAILURE,
	DESTROY_CHAMPIONSHIP,
	GET_CHAMPIONSHIPS_REQUEST,
	GET_CHAMPIONSHIPS_SUCCESS,
	GET_CHAMPIONSHIPS_FAILURE } from 'types';


export default function championship(state = {
	championships: []
}, action) {
	switch (action.type) {
		case GET_CHAMPIONSHIPS_REQUEST:
			return Object.assign({}, state, {
				isFetching: true
			});
		case GET_CHAMPIONSHIPS_SUCCESS:
			return Object.assign({}, state, {
				isFetching: false,
				championships: action.req.data
			});
		case GET_CHAMPIONSHIPS_FAILURE:
			return Object.assign({}, state, {
				isFetching: false
			});
		case CREATE_CHAMPIONSHIP_REQUEST:
			return {
				championships: [...state.championships, {
					id: action.id,
					name: action.name, 
					slug: action.slug,
					dateBegin: action.dateBegin, 
					dateEnd: action.dateEnd, 
					logo: action.logo, 
					sports: action.sports, 
					countries: action.countries, 
					teams: action.teams,
					date: new Date()
				}]
			};
		case CREATE_CHAMPIONSHIP_FAILURE:
			return {
				championships: [...state.championships.filter((tp) => tp.id !== action.id)]
			};
		case UPDATE_CHAMPIONSHIP_REQUEST:
			return {
				championships: [
					...state.championships.slice(0, action.index),
						Object.assign({}, state.championships[action.index], {
							id: action.id,
							name: action.name,
							slug: action.slug,
							dateBegin: action.dateBegin,
							dateEnd: action.dateEnd,
							logo: action.logo, 
							countries: action.countries,
							sports: action.sports,
							teams: action.teams
						}
					),
					...state.championships.slice(action.index + 1)
				]
			};
		case UPDATE_CHAMPIONSHIP_FAILURE:
			return {
				championships: [...state.championships.filter((tp) => tp.id !== action.id)]
			};
		case DESTROY_CHAMPIONSHIP:
			return {
				championships: [...state.championships.filter((tp, i) => i !== action.index)]
			};
		default:
			return state;
	}
}
