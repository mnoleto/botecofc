import { combineReducers } from 'redux';
import * as types from 'types';

const isFetching = (
	state = false,
	action
) => {
	switch (action.type) {
		case types.GET_PLACESTEMP_REQUEST:
			return true;
		case types.GET_PLACESTEMP_SUCCESS:
		case types.GET_PLACESTEMP_FAILURE:
			return false;
		default:
			return state;
	}
};

const message = (
	state = '',
	action
) => {
  switch (action.type) {
		case types.CREATE_PLACETEMP_FAILURE:
		case types.CREATE_PLACETEMP_SUCCESS:
			return action.message;
		case types.DISMISS_MESSAGE:
			return '';
		default:
			return state;
	}
};

const placeTemp = (
	state = {},
	action
) => {

	switch (action.type) {
		case types.APPROVE_PLACETEMP_REQUEST:
			return {
				id: action.id,
				name: action.name,
				address: action.address,
				city: action.city,
				date: new Date()
			};
		case types.CREATE_PLACETEMP_REQUEST:
			return {
				about: action.about,
				address: action.address,
				cards: action.cards,
				city: action.city,
				cnpj: action.cnpj,
				date: new Date(),
				email: action.email,
				facebook: action.facebook,
				gallery: action.gallery,
				googleplus: action.googleplus,
				id: action.id,
				instagram: action.instagram,
				logo: action.logo,
				name: action.name,
				opening: action.opening,
				ownerName: action.ownerName,
				phone: action.phone,
				services: action.services,
				slug: action.slug,
				teamOficial: action.teamOficial,
				twitter: action.twitter,
				website: action.website,
				zipcode: action.zipcode,
			};
		case types.UPDATE_PLACETEMP_REQUEST:
			return {
				about: action.about,
				address: action.address,
				cards: action.cards,
				city: action.city,
				cnpj: action.cnpj,
				email: action.email,
				facebook: action.facebook,
				gallery: action.gallery,
				googleplus: action.googleplus,
				id: action.id,
				instagram: action.instagram,
				logo: action.logo,
				name: action.name,
				opening: action.opening,
				ownerName: action.ownerName,
				phone: action.phone,
				services: action.services,
				slug: action.slug,
				teamOficial: action.teamOficial,
				twitter: action.twitter,
				website: action.website,
				zipcode: action.zipcode,
			};
		default:
			return state;
	}
};


const placesTemp = (
	state = [],
	action
) => {
	switch (action.type) {
		case types.GET_PLACESTEMP_SUCCESS:
			return action.req.data;
		case types.APPROVE_PLACETEMP_REQUEST:
			return [...state, placeTemp(undefined, action)]
		case types.CREATE_PLACETEMP_REQUEST:
			return [...state, placeTemp(undefined, action)]
		case types.CREATE_PLACETEMP_FAILURE:
			return state.filter((tp) => tp.id !== action.id);
		case types.UPDATE_PLACETEMP_REQUEST:
			return [
				...state.slice(0, action.index),
				Object.assign({}, state[action.index], placeTemp(undefined, action)),
				...state.slice(action.index + 1)
			];
		case types.UPDATE_PLACETEMP_FAILURE:
			return state.filter((tp) => tp.id !== action.id);
		case types.DESTROY_PLACETEMP:
			return state.filter((tp) => tp.id !== action.id);
		default:
			return state;
	}
};

const placeTempReducer = combineReducers({
	placesTemp,
	message,
	isFetching
});

export default placeTempReducer;
