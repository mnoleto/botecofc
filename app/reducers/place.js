import * as types from 'types';


export default function place(state = {
	places: []
}, action) {
	switch (action.type) {
		case types.DISMISS_MESSAGE:
			return Object.assign({}, state, {
				message: ''
			});
		case types.UPDATE_PLACE_SUCCESS:
		case types.PLACE_OWNERSHIP_SUCCESS:
		case types.UPDATE_PLACE_SUCCESS:
			return Object.assign({}, state, {
				message: 'success'
			});
		case types.GET_PLACES_REQUEST:
			return Object.assign({}, state, {
				message: '',
				isFetching: true
			});
		case types.GET_PLACES_SUCCESS:
			return Object.assign({}, state, {
				message: '',
				isFetching: false,
				places: action.req.data
			});
		case types.GET_PLACES_FAILURE:
			return Object.assign({}, state, {
				message: '',
				isFetching: false
			});
		case types.CREATE_PLACE_REQUEST:
			return {
				places: [...state.places, {
					id: action.id,
					...action,
					date: new Date()
				}]
			};
		case types.UPDATE_PLACE_REQUEST:
			return Object.assign({}, state, {
				message: '',
				places: state.places.map(function(value) {
					if(value.id === action.id) { 
						return Object.assign({}, value, {
							...action,
						});
					} else {
						return Object.assign({}, value);
					}
				})
			})
		case types.INCREMENT_POSTS:
			return Object.assign({}, state, {
				places: state.places.map(function (value) {
					if (value.id === action.id) {
						return Object.assign({}, value, {
							numberOfPosts: value.numberOfPosts + 1,
						});
					} else {
						return Object.assign({}, value);
					}
				})
			})
		case types.PLACE_OWNERSHIP_REQUEST:
			return Object.assign({}, state, {
				message: '',
				places: state.places.map(function(value) {
					if(value.id === action.id) { 
						return Object.assign({}, value, {
							ownerships: action.ownerships
						});
					} else {
						return Object.assign({}, value);
					}
				})
			})
		case types.UPDATE_BROADCASTGAMES_REQUEST:
			return Object.assign({}, state, {
				places: state.places.map(function(value, index) {
					if(value.id === action.id) { 
						return Object.assign({}, value, {
							broadcastGames: action.broadcastGames,
						});
					} else {
						return Object.assign({}, value);
					}
				})
			})
		case types.UPDATE_RECOMENDATION_REQUEST:
			return Object.assign({}, state, {
				places: state.places.map(function(value, index) {
					if(value.id === action.id) { 
						return Object.assign({}, value, {
							recomendations: action.recomendations
						});
					} else {
						return Object.assign({}, value);
					}
				})
			})
		case types.UPDATE_GENERATE_POSTS_REQUEST:
			return Object.assign({}, state, {
				places: state.places.map(function (value, index) {
					if (value.id === action.id) {
						return Object.assign({}, value, {
							canGeneratePosts: action.canGeneratePosts,
						});
					} else {
						return Object.assign({}, value);
					}
				})
			})
		case types.UPDATE_GENERATE_POSTS_FAILURE:
			return Object.assign({}, state, {
				places: state.places.map(function (value, index) {
					if (value.id === action.id) {
						return Object.assign({}, value, {
							canGeneratePosts: !value.canGeneratePosts,
						});
					} else {
						return Object.assign({}, value);
					}
				})
			})
		case types.CREATE_PLACE_FAILURE:
		case types.UPDATE_PLACE_FAILURE:
		case types.PLACE_OWNERSHIP_FAILURE:
		case types.UPDATE_BROADCASTGAMES_FAILURE:
			return {
				message: 'error',
				places: [...state.places.filter((tp) => tp.id !== action.id)]
			};
		case types.DESTROY_PLACE:
			return {
				places: [...state.places.filter((tp, i) => i !== action.index)]
			};
		default:
			return state;
	}
}
