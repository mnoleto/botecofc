import { combineReducers } from 'redux';
import * as types from 'types';

const isFetching = (
  state = false,
  action
) => {
  switch (action.type) {
    case types.CREATE_POST_REQUEST:
      return true;
    case types.CREATE_POST_SUCCESS:
    case types.CREATE_POST_FAILURE:
      return false;
    default:
      return state;
  }
};

const image = (
  state = {},
  action
) => {
  switch (action.type) {
    case types.CREATE_POST_SUCCESS:
      return action.image.data;
    default:
      return state;
  }
};

const featureToggleReducer = combineReducers({
  image,
  isFetching
});

export default featureToggleReducer;