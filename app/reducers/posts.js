import { combineReducers } from 'redux';
import * as types from '../types';

const bgs = (
  bgs = [],
  action
) => {
  switch (action.type) {
    case types.GET_BGS_SUCCESS:
      return action.req.data;
    case types.CREATE_BGS_SUCCESS:
      if (!action.data || !action.data.data || !action.data.data.rect) {
        return bgs;
      }
      const res = action.data.data;
      return [...bgs, res];
    case types.REMOVE_BGS_REQUEST:
      return bgs.filter(img => img._id !== action.id);
    default:
      return bgs;
  }
};

const figures = (
  images = [],
  action
) => {
  switch (action.type) {
    case types.GET_FIGURES_SUCCESS:
      return action.req.data;
    case types.CREATE_FIGURES_REQUEST:
      return [...images, ...action.images.map(item => ({ image: item }))]
    case types.CREATE_FIGURES_SUCCESS:
      if (!action.req.data || !action.req.data[0] || !action.req.data[0].image) {
        return images;
      }
      const res = action.req.data[0].image;
      return images.map(img => {
        if (img.responseText === res.responseText) {
          return res;
        }
        return img;
      });
    case types.REMOVE_FIGURES_REQUEST:
      return images.filter(img => img._id !== action.id);
    default:
      return images;
  }
};

const postsReducer = combineReducers({
  bgs,
  figures,
});

export default postsReducer;