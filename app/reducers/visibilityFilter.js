import { combineReducers } from 'redux';

import {
	SET_CHAMPIONSHIPS_VISIBILITY_FILTER,
	SET_CHAMPIONSHIP_VISIBILITY_FILTER,
	SET_GAME_VISIBILITY_FILTER,
	SET_TEAMS_VISIBILITY_FILTER,
	SET_TEAM_VISIBILITY_FILTER,
	SET_PLACE_VISIBILITY_FILTER,
	SET_OWNER_VISIBILITY_FILTER,
} from 'types';


const championshipsVisibilityFilter = (
	state = {
		show: 'SHOW_ALL',
		sports: [],
		countries: []
	},
	action
)  => {
	switch (action.type) {
	    case SET_CHAMPIONSHIPS_VISIBILITY_FILTER:
			return Object.assign({}, state, {
				show: action.show,
				sports: action.sports,
				countries: action.countries
			});

    default:
		return state;
  }
};

const championshipVisibilityFilter = (
	state = {
		show: 'SHOW_ALL',
		championship: ''
	},
	action
)  => {
	switch (action.type) {
	    case SET_CHAMPIONSHIP_VISIBILITY_FILTER:
			return Object.assign({}, state, {
				show: action.show,
				championship: action.championship
			});

    default:
		return state;
  }
};

const gameVisibilityFilter = (
	state = {
		show: 'SHOW_ALL',
		championship: '',
		city: '',
		date: ''
	},
	action
)  => {
	switch (action.type) {
		case SET_GAME_VISIBILITY_FILTER:
			return Object.assign({}, state, {
				show: action.show,
				championship: action.championship,
				city: action.city,
				date: action.date
			});

		default:
			return state;
	}
};

const placesVisibilityFilter = (
	state = {
		show: 'SHOW_ALL',
		city: ''
	},
	action
)  => {
	switch (action.type) {
		case SET_PLACE_VISIBILITY_FILTER:
			return Object.assign({}, state, {
				show: action.show,
				city: action.city
			});

		default:
			return state;
	}
};

const teamsVisibilityFilter = (
	state = {
		show: 'SHOW_ALL',
		countries: ''
	},
	action
)  => {
	switch (action.type) {
		case SET_TEAMS_VISIBILITY_FILTER:
			return Object.assign({}, state, {
				show: action.show,
				countries: action.countries
			});

		default:
			return state;
	}
};

const teamVisibilityFilter = (
	state = {
		show: 'SHOW_ALL',
		team: ''
	},
	action
)  => {
	switch (action.type) {
		case SET_TEAM_VISIBILITY_FILTER:
			return Object.assign({}, state, {
				show: action.show,
				team: action.team
			});

		default:
			return state;
	}
};

const ownerManageFilter = (
	state = {
		show: 'SHOW_ALL',
		championship: '',
		initialDate: '',
		endDate: '',
		place: {},
	},
	action
) => {
	switch (action.type) {
		case SET_OWNER_VISIBILITY_FILTER:
			return Object.assign({}, state, {
				show: action.show,
				championship: action.championship,
				initialDate: action.initialDate,
				endDate: action.endDate,
				place: action.place,
			});
			break;
	
		default:
			return state;
	}
};

const visibilityFilter = combineReducers({
	championshipsVisibilityFilter,
	championshipVisibilityFilter,
	gameVisibilityFilter,
	teamsVisibilityFilter,
	teamVisibilityFilter,
	placesVisibilityFilter,
	ownerManageFilter,
});

export default visibilityFilter;
