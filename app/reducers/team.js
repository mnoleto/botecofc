import {
    CREATE_TEAM_REQUEST,
    CREATE_TEAM_FAILURE,
    UPDATE_TEAM_REQUEST,
    UPDATE_TEAM_FAILURE,
    DESTROY_TEAM,
    GET_TEAMS_REQUEST,
    GET_TEAMS_SUCCESS,
    GET_TEAMS_FAILURE,
    INCREMENT_SCORE,
    DECREMENT_SCORE } from 'types';


export default function team(state = {
    teams: []
}, action) {
    switch (action.type) {
        case GET_TEAMS_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case GET_TEAMS_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                teams: action.req.data
            });
        case GET_TEAMS_FAILURE:
            return Object.assign({}, state, {
                isFetching: false
            });
        case CREATE_TEAM_REQUEST:
            return {
                teams: [
                    ...state.teams, { 
                        id: action.id, 
                        name: action.name, 
                        slug: action.slug,
                        sport: action.sport, 
                        country: action.country, 
                        color: action.color, 
                        textColor: action.textColor, 
                        logo: action.logo,
                        date: new Date()
                    }
                ]
            };
        case CREATE_TEAM_FAILURE:
            return {
                teams: [...state.teams.filter((tp) => tp.id !== action.id)]
            };
        case UPDATE_TEAM_REQUEST:
            return {
                teams: [
                    ...state.teams.slice(0, action.index),
                    Object.assign({}, state.teams[action.index], {
                        id: action.id,
                        name: action.name,
                        slug: action.slug,
                        sport: action.sport,
                        country: action.country,
                        color: action.color,
                        textColor: action.textColor,
                        logo: action.logo
                    }),
                    ...state.teams.slice(action.index + 1)
                ]
            };
        case UPDATE_TEAM_FAILURE:
            return {
                teams: [...state.teams.filter((tp) => tp.id !== action.id)]
            };
        case DESTROY_TEAM:
            return {
                teams: [...state.teams.filter((tp, i) => i !== action.index)]
            };
        case INCREMENT_SCORE:
            return {
                teams: [...state.teams.map((tp) => {
                    if(tp.id === action.id) {
                        return Object.assign({}, tp, {
                            score: state.score + 1
                        });
                    }
                })]
            }
        case DECREMENT_SCORE:
            return {
                teams: [...state.teams.map((tp) => {
                    if(tp.id === action.id) {
                        return Object.assign({}, tp, {
                            score: state.score - 1
                        });
                    }
                })]
            }
      default:
            return state;
    }
}
