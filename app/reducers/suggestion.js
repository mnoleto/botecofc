import { combineReducers } from 'redux';
import * as types from 'types';

const isFetching = (
	state = false,
	action
) => {
	switch (action.type) {
		case types.GET_PLACESSUGGESTIONS_REQUEST:
			return true;
		case types.GET_PLACESSUGGESTIONS_SUCCESS:
		case types.GET_PLACESSUGGESTIONS_FAILURE:
			return false;
		default:
			return state;
	}
};

const message = (
	state = '',
	action
) => {
  switch (action.type) {
	case types.CREATE_PLACESUGGESTION_FAILURE:
	case types.CREATE_PLACESUGGESTION_SUCCESS:
		return action.message;
	case types.DISMISS_MESSAGE:
		return '';
	default:
		return state;
	}
};

const placeSuggestion = (
	state = {},
	action
) => {
	switch (action.type) {
		case types.CREATE_PLACESUGGESTION_REQUEST:
			return {
				id: action.id,
				place: {
					name: action.place.name,
					address: action.place.address,
					reference: action.place.reference,
					cityId: action.place.cityId,
					stateId: action.place.stateId
				},
				user: {
					id: action.user.id,
					name: action.user.name,
					email: action.user.email
				}
			};
		default:
			return state;
	}
};

const placeSuggestions = (
	state = [],
	action
) => {
	switch (action.type) {
		case types.GET_PLACESSUGGESTIONS_SUCCESS:
			return action.req.data;
		case types.CREATE_PLACESUGGESTION_REQUEST:
			return [...state, placeSuggestion(undefined, action)];
		case types.CREATE_PLACESUGGESTION_FAILURE:
			return state.filter(t => t.id !== action.id);
		case types.DESTROY_PLACESUGGESTION:
			return state.filter(t => t.id !== action.id);
		default:
			return state;
	}
};

const suggestionReducer = combineReducers({
	placeSuggestions,
	message,
	isFetching
});

export default suggestionReducer;
