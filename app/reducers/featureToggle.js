import { combineReducers } from 'redux';
import * as types from 'types';

const isFetching = (
  state = false,
  action
) => {
  switch (action.type) {
    case types.GET_FEATURE_TOGGLES_REQUEST:
      return true;
    case types.GET_FEATURE_TOGGLES_SUCCESS:
    case types.GET_FEATURE_TOGGLES_FAILURE:
      return false;
    default:
      return state;
  }
};

const message = (
  state = '',
  action
) => {
  switch (action.type) {
    case types.CREATE_FEATURE_TOGGLE_FAILURE:
    case types.CREATE_FEATURE_TOGGLE_SUCCESS:
      return action.message;
    case types.DISMISS_MESSAGE:
      return '';
    default:
      return state;
  }
};

const featureToggle = (
  state = {},
  action
) => {
  switch (action.type) {
    case types.CREATE_FEATURE_TOGGLE_REQUEST:
      return {
        id: action.id,
        place_id: action.place_id,
        feature: action.feature,
      };
    default:
      return state;
  }
};

const featureToggles = (
  state = [],
  action
) => {
  switch (action.type) {
    case types.GET_FEATURE_TOGGLES_SUCCESS:
      return action.req.data;
    case types.CREATE_FEATURE_TOGGLE_REQUEST:
      return [...state, featureToggle(undefined, action)];
    case types.CREATE_FEATURE_TOGGLE_FAILURE:
      return state.filter(t => t.id !== action.id);;
    case types.DESTROY_FEATURE_TOGGLE:
      return state.filter(t => t.id !== action.id);
    default:
      return state;
  }
}

const featureToggleReducer = combineReducers({
  featureToggles,
  message,
  isFetching
});

export default featureToggleReducer;