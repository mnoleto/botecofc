import * as types from 'types';
import { combineReducers } from 'redux';

const termsAccepted = (
  state = false,
  action
) => {
  return state;
};

const isLogin = (
  state = true,
  action
) => {
  switch (action.type) {
    case types.TOGGLE_LOGIN_MODE:
      return !state;
    default:
      return state;
  }
};

const isLoading = (
  state = false,
  action
) => {
  switch (action.type) {
    case types.TOGGLE_LOADING:
      return false;
    default:
      return state;
  }
};

const message = (
  state = '',
  action
) => {
  switch (action.type) {
    case types.TOGGLE_LOGIN_MODE:
    case types.MANUAL_LOGIN_USER:
    case types.SIGNUP_USER:
    case types.LOGOUT_USER:
    case types.LOGIN_SUCCESS_USER:
    case types.SIGNUP_SUCCESS_USER:
      return '';
    case types.LOGIN_ERROR_USER:
    case types.SIGNUP_ERROR_USER:
    case types.UPDATE_SIGNUP_SUCCESS:
      return action.message;
    default:
      return state;
  }
};

const isWaiting = (
  state = false,
  action
) => {
  switch (action.type) {
    case types.MANUAL_LOGIN_USER:
    case types.SIGNUP_USER:
    case types.LOGOUT_USER:
      return true;
    case types.LOGIN_SUCCESS_USER:
    case types.SIGNUP_SUCCESS_USER:
    case types.LOGOUT_SUCCESS_USER:
    case types.LOGIN_ERROR_USER:
    case types.SIGNUP_ERROR_USER:
    case types.LOGOUT_ERROR_USER:
      return false;
    default:
      return state;
  }
};

const authenticated = (
  state = false,
  action
) => {
  switch (action.type) {
    case types.LOGIN_SUCCESS_USER:
    case types.SIGNUP_SUCCESS_USER:
    case types.LOGOUT_ERROR_USER:
      return true;
    case types.LOGIN_ERROR_USER:
    case types.SIGNUP_ERROR_USER:
    case types.LOGOUT_SUCCESS_USER:
      return false;
    default:
      return state;
  }
};

const account = (
  state = {},
  action
) => {
  switch (action.type) {
    case types.USER_OWNERSHIP_REQUEST:
      return Object.assign({}, state, {
        ownerships: action.ownerships
      })
    case types.ADD_PLACETOWATCH_REQUEST:
    case types.REMOVE_PLACETOWATCH_REQUEST:
      return Object.assign({}, state, {
        watch: action.watch
      })
    case types.UPDATE_SIGNUP_REQUEST:
      const {type, ...rest} = action
      return {
        ...state,
        ...rest,
      }
    case types.LOGOUT_SUCCESS_USER:
      return {};
    default:
      return state;
  }
};

const selectedCity = (
  state = '',
  action
) => {
  switch (action.type) {
    case types.SELECT_LOCALE_CITY:
      return action.selectedCity
    default:
      return state;
  }
}

const userReducer = combineReducers({
  termsAccepted,
  isLogin,
  isWaiting,
  authenticated,
  message,
  account,
  isLoading,
  selectedCity,
});

export default userReducer;
