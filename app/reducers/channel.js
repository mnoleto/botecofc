import {
    CREATE_CHANNEL_REQUEST,
    CREATE_CHANNEL_FAILURE,
    UPDATE_CHANNEL_REQUEST,
    UPDATE_CHANNEL_FAILURE,
    DESTROY_CHANNEL,
    GET_CHANNELS_REQUEST,
    GET_CHANNELS_SUCCESS,
    GET_CHANNELS_FAILURE
} from 'types';


export default function channel(state = {
    channels: []
}, action) {
    switch (action.type) {
        case GET_CHANNELS_REQUEST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case GET_CHANNELS_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                channels: action.req.data
            });
        case GET_CHANNELS_FAILURE:
            return Object.assign({}, state, {
                isFetching: false
            });
        case CREATE_CHANNEL_REQUEST:
            return {
                channels: [...state.channels, {
                    id: action.id,
                    name: action.name,
                    // logo: action.logo,
                    date: new Date()
                }]
            };
        case CREATE_CHANNEL_FAILURE:
            return {
                channels: [...state.channels.filter((tp) => tp.id !== action.id)]
            };
        case UPDATE_CHANNEL_REQUEST:
            return {
                channels: [
                    ...state.channels.slice(0, action.index),
                    Object.assign({}, state.channels[action.index], {
                        id: action.id,
                        name: action.name,
                        // logo: action.logo,
                    }
                    ),
                    ...state.channels.slice(action.index + 1)
                ]
            };
        case UPDATE_CHANNEL_FAILURE:
            return {
                channels: [...state.channels.filter((tp) => tp.id !== action.id)]
            };
        case DESTROY_CHANNEL:
            return {
                channels: [...state.channels.filter((tp, i) => i !== action.index)]
            };
        default:
            return state;
    }
}
