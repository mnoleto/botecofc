import {
	CREATE_COUNTRY_REQUEST,
	CREATE_COUNTRY_FAILURE,
	UPDATE_COUNTRY_REQUEST,
	UPDATE_COUNTRY_FAILURE,
	DESTROY_COUNTRY,
	GET_COUNTRIES_REQUEST,
	GET_COUNTRIES_SUCCESS,
	GET_COUNTRIES_FAILURE } from 'types';


export default function country(state = {
	countries: []
}, action) {
	switch (action.type) {
		case GET_COUNTRIES_REQUEST:
			return Object.assign({}, state, {
				isFetching: true
			});
		case GET_COUNTRIES_SUCCESS:
			return Object.assign({}, state, {
				isFetching: false,
				countries: action.req.data
			});
		case GET_COUNTRIES_FAILURE:
			return Object.assign({}, state, {
				isFetching: false
			});
		case CREATE_COUNTRY_REQUEST:
			return {
				countries: [...state.countries, { 
					id: action.id, 
					name: action.name,
					slug: action.slug,
					date: new Date()
				}]
			};
		case CREATE_COUNTRY_FAILURE:
			return {
				countries: [...state.countries.filter((tp) => tp.id !== action.id)]
			};
		case UPDATE_COUNTRY_REQUEST:
			return {
				countries: [
					...state.countries.slice(0, action.index),
						Object.assign({}, state.countries[action.index], {
							id: action.id,
							name: action.name,
							slug: action.slug
						}
					),
					...state.countries.slice(action.index + 1)
				]
			};
		case UPDATE_COUNTRY_FAILURE:
			return {
				countries: [...state.countries.filter((tp) => tp.id !== action.id)]
			};
		case DESTROY_COUNTRY:
			return {
				countries: [...state.countries.filter((tp, i) => i !== action.index)]
			};
		default:
			return state;
	}
}
