import {
  ACTIVATE_STATE,
  CREATE_STATE_FAILURE,
  CREATE_STATE_REQUEST,
  DESTROY_STATE,
  GET_ACTIVE_STATES_REQUEST,
  GET_ACTIVE_STATES_FAILURE,
  GET_ACTIVE_STATES_SUCCESS,
  GET_STATES_FAILURE,
  GET_STATES_REQUEST,
  GET_STATES_SUCCESS,
  UPDATE_STATE_FAILURE,
  UPDATE_STATE_REQUEST,
} from 'types';

export default (state = {
  active: [],
  states: []
}, action) => {
  switch (action.type) {
    case GET_ACTIVE_STATES_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        active: action.req.data
      });
    case GET_STATES_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        states: action.req.data
      });

    case GET_STATES_REQUEST:
    case GET_ACTIVE_STATES_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    case GET_ACTIVE_STATES_FAILURE:
    case GET_STATES_FAILURE:
      return Object.assign({}, state, {
        isFetching: false
      });
    case CREATE_STATE_REQUEST:
      return Object.assign({}, state, {
        states: [...state.states, {
          ...action,
        }]
      });

    case CREATE_STATE_FAILURE:
      return Object.assign({}, state, {
        states: [...state.states.filter((tp) => tp.id !== action.id)]
      });

    case ACTIVATE_STATE:
    case UPDATE_STATE_REQUEST:
      return Object.assign({}, state, {
        states: [
          ...state.states.map(item => {
            if (item.id === action.id) {
              return {
                ...action,
              }
            }
            return item
          })
        ]
      });

    case UPDATE_STATE_FAILURE:
      return Object.assign({}, state, {
        states: [...state.states.filter((tp) => tp.id !== action.id)]
      });

    case DESTROY_STATE:
      return Object.assign({}, state, {
        states: [...state.states.filter((tp) => tp.id !== action.id)]
      });

    default:
      return state;
  }
}
