import {
	CREATE_GAME_REQUEST,
	CREATE_GAME_FAILURE,
	UPDATE_GAME_REQUEST,
	UPDATE_GAME_FAILURE,
	DESTROY_GAME,
	GET_GAMES_REQUEST,
	GET_GAMES_SUCCESS,
	GET_GAMES_FAILURE } from 'types';


export default function game(state = {
	games: []
}, action) {
	switch (action.type) {
		case GET_GAMES_REQUEST:
			return Object.assign({}, state, {
				isFetching: true
			});
		case GET_GAMES_SUCCESS:
			return Object.assign({}, state, {
				isFetching: false,
				games: action.req.data
			});
		case GET_GAMES_FAILURE:
			return Object.assign({}, state, {
				isFetching: false
			});
		case CREATE_GAME_REQUEST:
			return {
				games: [...state.games, { 
					id: action.id, 
					city: action.city,
					stadium: action.stadium,
					championship: action.championship,
					homeTeam: action.homeTeam,
					visitingTeam: action.visitingTeam,
					gameDate: action.gameDate,
					channels: action.channels,
					date: new Date()
				}]
			};
		case UPDATE_GAME_REQUEST:
			return {
				games: [
					...state.games.slice(0, action.index),
						Object.assign({}, state.games[action.index], {
							id: action.id,
							city: action.city,
							stadium: action.stadium,
							championship: action.championship,
							homeTeam: action.homeTeam,
							visitingTeam: action.visitingTeam,
							gameDate: action.gameDate,
							channels: action.channels,
						}
					),
					...state.games.slice(action.index + 1)
				]
			};
		case UPDATE_GAME_FAILURE:
			return {
				games: [...state.games.filter((tp) => tp.id !== action.id)]
			};
		case CREATE_GAME_FAILURE:
			return {
				games: [...state.games.filter((tp) => tp.id !== action.id)]
			};
		case DESTROY_GAME:
			return {
				games: [...state.games.filter((tp, i) => i !== action.index)]
			};
		default:
			return state;
	}
}
