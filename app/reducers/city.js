import {
  ACTIVATE_CITY,
  CREATE_CITY_FAILURE,
  CREATE_CITY_REQUEST,
  DESTROY_CITY,
  GET_ACTIVE_CITIES_FAILURE,
  GET_ACTIVE_CITIES_REQUEST,
  GET_ACTIVE_CITIES_SUCCESS,
  GET_CITIES_FAILURE,
  GET_CITIES_REQUEST,
  GET_CITIES_SUCCESS,
  UPDATE_CITY_FAILURE,
  UPDATE_CITY_REQUEST,
} from 'types';

export default (state = {
  active: [],
  cities: []
}, action) => {
  switch (action.type) {
    case GET_ACTIVE_CITIES_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        active: action.req.data.map(({
          state_id,
          ...rest,
        }) => ({
          ...rest,
          stateId: state_id,
        }))
      });
    case GET_CITIES_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        cities: action.req.data.map(({
          state_id,
          ...rest,
        }) => ({
          ...rest,
          stateId: state_id,
        }))
      });
    case GET_CITIES_FAILURE:
    case GET_ACTIVE_CITIES_FAILURE:
      return Object.assign({}, state, {
        isFetching: false
      });
    case GET_ACTIVE_CITIES_REQUEST:
    case GET_CITIES_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    case CREATE_CITY_REQUEST:
      return Object.assign({}, state, {
        cities: [...state.cities, {
          id: action.id,
          isDefault: action.isDefault,
          isActive: action.isActive,
          name: action.name,
          state_id: action.stateId,
        }]
      });

    case CREATE_CITY_FAILURE:
      return Object.assign({}, state, {
        cities: [...state.cities.filter((tp) => tp.id !== action.id)]
      });

    case UPDATE_CITY_REQUEST:
      return Object.assign({}, state, {
        cities: [
          ...state.cities.map(item => {
            if (item.id === action.id) {
              return {
                ...action,
              }
            }
            return item
          })
        ]
      });

    case ACTIVATE_CITY:
      return Object.assign({}, state, {
        cities: [
          ...state.cities.map(item => {
            if (item.id === action.id) {
              return {
                id: action.id,
                isActive: action.isActive,
              }
            }
            return item
          })
        ]
      });

    case UPDATE_CITY_FAILURE:
      return Object.assign({}, state, {
        cities: [...state.cities.filter((tp) => tp.id !== action.id)]
      });

    case DESTROY_CITY:
      return Object.assign({}, state, {
        cities: [...state.cities.filter((tp) => tp.id !== action.id)]
      });

    default:
      return state;
  }
}

