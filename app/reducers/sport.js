import {
	CREATE_SPORT_REQUEST,
	CREATE_SPORT_FAILURE,
	UPDATE_SPORT_REQUEST,
	UPDATE_SPORT_FAILURE,
	DESTROY_SPORT,
	GET_SPORTS_REQUEST,
	GET_SPORTS_SUCCESS,
	GET_SPORTS_FAILURE } from 'types';


export default function sport(state = {
	sports: []
}, action) {
	switch (action.type) {
		case GET_SPORTS_REQUEST:
			return Object.assign({}, state, {
				isFetching: true
			});
		case GET_SPORTS_SUCCESS:
			return Object.assign({}, state, {
				isFetching: false,
				sports: action.req.data
			});
		case GET_SPORTS_FAILURE:
			return Object.assign({}, state, {
				isFetching: false
			});
		case CREATE_SPORT_REQUEST:
			return {
				sports: [...state.sports, { 
					id: action.id, 
					name: action.name,
					slug: action.slug,
					date: new Date()
				}]
			};
		case CREATE_SPORT_FAILURE:
			return {
				sports: [...state.sports.filter((tp) => tp.id !== action.id)]
			};
		case UPDATE_SPORT_REQUEST:
			return {
				sports: [
				...state.sports.slice(0, action.index),
					Object.assign({}, state.sports[action.index], {
						id: action.id,
						name: action.name,
						slug: action.slug
					}
				),
				...state.sports.slice(action.index + 1)
				]
			};
		case UPDATE_SPORT_FAILURE:
			return {
				sports: [...state.sports.filter((tp) => tp.id !== action.id)]
			};
		case DESTROY_SPORT:
			return {
				sports: [...state.sports.filter((tp, i) => i !== action.index)]
			};
		default:
			return state;
	}
}
