import * as types from 'types';

export default function user(state = {
	users: [],
	user: {}
}, action) {
	switch (action.type) {
		case types.GET_USERS_REQUEST:
		    return Object.assign({}, state, {
		        isFetching: true
		    });
		case types.GET_USERS_SUCCESS:
		    return Object.assign({}, state, {
		        isFetching: false,
		        users: action.req.data
		    });
		case types.GET_USERS_FAILURE:
		    return Object.assign({}, state, {
		        isFetching: false
		    });
		case types.CREATE_USER_REQUEST:
		    return {
		        users: [
		            ...state.users, { 
		                _id: action._id, 
		                name: action.name,
		                email: action.email,
		                date: new Date()
		            }
		        ]
		    };
		case types.CREATE_USER_FAILURE:
		    return {
		        users: [...state.users.filter((tp) => tp.id !== action.id)]
		    };
		case types.UPDATE_SIGNUP_REQUEST:
		    return {
		        users: [
		            ...state.users.slice(0, action.index),
		            Object.assign({}, state.users[action.index], {
		                _id: action._id,
		                name: action.name,
		                email: action.email
		            }),
		            ...state.users.slice(action.index + 1)
		        ]
		    };
		case types.UPDATE_ROLE_REQUEST:
		    return {
		        users: state.users.map((user) => {
		        	if(user._id === action._id) {
		        		return {
									...user,
									role: action.role,
								}
		        	}
							return user
		        })
		    };
		case types.UPDATE_SIGNUP_FAILURE:
		    return {
		        users: [...state.users.filter((tp) => tp.id !== action._id)]
		    };
		case types.DESTROY_USER:
		    return {
		        users: [...state.users.filter((tp, i) => i !== action.index)]
		    };
		default:
			return state;
	}
};
