import {
  CREATE_BANNER_REQUEST,
  CREATE_BANNER_FAILURE,
  UPDATE_BANNER_REQUEST,
  UPDATE_BANNER_FAILURE,
  DESTROY_BANNER,
  GET_BANNERS_REQUEST,
  GET_BANNERS_SUCCESS,
  GET_BANNERS_FAILURE
} from 'types';


export default function banner(state = {
  banners: []
}, action) {
  switch (action.type) {
    case GET_BANNERS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    case GET_BANNERS_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        banners: action.req.data
      });
    case GET_BANNERS_FAILURE:
      return Object.assign({}, state, {
        isFetching: false
      });
    case CREATE_BANNER_REQUEST:
      return {
        banners: [...state.banners, {
          date: new Date(),
          ...action,
        }]
      };
    case CREATE_BANNER_FAILURE:
      return {
        banners: [...state.banners.filter((tp) => tp.id !== action.id)]
      };
    case UPDATE_BANNER_REQUEST:
      return {
        banners: state.banners.map(banner => {
          if (banner.id === action.id) {
            return Object.assign({}, banner, {
              ...action,
            });
          }
          return banner;
        }),
      };
    case UPDATE_BANNER_FAILURE:
      return {
        banners: [...state.banners.filter((tp) => tp.id !== action.id)]
      };
    case DESTROY_BANNER:
      return {
        banners: [...state.banners.filter(tp => tp.id !== action.id)]
      };
    default:
      return state;
  }
}
