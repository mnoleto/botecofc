import { combineReducers } from 'redux';
import * as types from 'types';

const isFetching = (
	state = false,
	action
) => {
	switch (action.type) {
		case types.GET_OWNERSHIPS_REQUEST:
			return true;
		case types.GET_OWNERSHIPS_SUCCESS:
		case types.GET_OWNERSHIPS_FAILURE:
			return false;
		default:
			return state;
	}
};

const message = (
	state = '',
	action
) => {
  switch (action.type) {
	case types.CREATE_OWNERSHIPS_FAILURE:
	case types.CREATE_OWNERSHIPS_SUCCESS:
		return action.message;
	case types.DISMISS_MESSAGE:
		return '';
	default:
		return state;
	}
};

const ownership = (
	state = {},
	action
) => {
	switch (action.type) {
		case types.CREATE_OWNERSHIPS_REQUEST:
			return {
				id: action.id,
				place: {
					id: action.place.id,
					name: action.place.name
				},
				user: {
					name: action.user.name,
					email: action.user.email,
					role: action.user.role
				}
			};
		default:
			return state;
	}
};

const  ownerships = (
	state = [],
	action
) => {
	switch (action.type) {
		case types.GET_OWNERSHIPS_SUCCESS:
			return action.req.data;
		case types.CREATE_OWNERSHIP_REQUEST:
			return [...state, ownership(undefined, action)];
		case types.CREATE_OWNERSHIP_FAILURE:
			return state.filter(t => t.id !== action.id);;
		case types.DESTROY_OWNERSHIP:
			return state.filter(t => t.id !== action.id);
		default:
			return state;
	}
}

const ownershipReducer = combineReducers({
	ownerships,
	message,
	isFetching
});

export default ownershipReducer;