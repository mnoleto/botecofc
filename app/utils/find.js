const findById = (id, list) => {
  if (list.length === 0 || !id) return null;
  return list.find(item => item.id === id);
}

export default findById;
