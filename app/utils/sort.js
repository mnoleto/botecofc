export const sortByGameDate = (data) =>
  data.sort((a, b) =>
    new Date(a.gameDate).getTime() - new Date(b.gameDate).getTime(),
  );

export const sortByFavoriteTeam = (data, favoriteTeam) =>
  data.sort((a, b) => {
    const id = favoriteTeam.id;
    const isAfavorite = a.homeTeam.id === id || a.visitingTeam.id === id;
    const isBfavorite = b.homeTeam.id === id || b.visitingTeam.id === id;

    if (!isAfavorite && isBfavorite) {
      return 1;
    }
    if (isAfavorite && !isBfavorite) {
      return -1;
    }
    return 0;
  });

export const sortGames = (data, favoriteTeam) => {
  const sortedByDate = sortByGameDate(data);
  if (!favoriteTeam) { 
    return sortedByDate;
  }
  return sortByFavoriteTeam(sortedByDate, favoriteTeam);
}

export const sortByDate = (a, b) => {
  const aDate = a.date ? new Date(a.date) : Date.now();
  const bDate = b.date ? new Date(b.date) : Date.now();

  return bDate - aDate;
}