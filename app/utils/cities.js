const cities = [
	'Águas Claras',
	'Asa Norte',
	'Asa Sul',
	'Brazlândia',
	'Candangolândia',
	'Ceilândia',
	'Cruzeiro',
	'Fercal',
	'Gama',
	'Guará',
	'Guará I',
	'Guará II',
	'Itapoã',
	'Jardim Botânico',
	'Lago Norte',
	'Lago Sul',
	'Noroeste',
	'Núcleo Bandeirante',
	'Octogonal',
	'Paranoá',
	'ParkWay',
	'Planaltina',
	'Recanto das Emas',
	'Riacho Fundo I',
	'Riacho Fundo II',
	'Samambaia',
	'Santa Maria',
	'SCIA',
	'SIA',
	'SIG',
	'Sobradinho',
	'Sudoeste',
	'Taguatinga',
	'Varjão',
	'Vicente Pires',
	'Vila Planalto'
];

export const findCityById = (cityId, cities) => {
	if (
	  cityId == null ||
	  cityId === '' ||
	  cities == null ||
	  cities.length === 0
	) {
	  return null
	}
	return cities.find(item => item.id === cityId)
}

export const findDefaultCity = (cities) => {
  if (
    cities == null ||
    cities.length === 0
  ) {
    return null
  }
  return cities.find(item => item.isDefault)
}

export const findDefaultLocale = (cities, states) => {
  if (
    cities == null ||
    cities.length === 0
  ) {
    return null
  }
	const city = cities.find(item => item.isDefault)
	if (city == null) {
	  return null
	}
	const state = findStateById(city.stateId, states)
	return {
	  city,
	  state,
	}

}

export const findStateById = (stateId, states) => {
  if (
    stateId == null ||
    stateId === '' ||
    states == null ||
    states.length === 0
  ) {
    return null
  }
  return states.find(item => item.id === stateId)
}

export const findLocaleByCity = (cityId, cities, states) => {
	const city = findCityById(cityId, cities)
	if (city == null) {
		return null
	}
	const state = findStateById(city.stateId, states)
	return {
		city,
		state,
	}
}

export default cities;
