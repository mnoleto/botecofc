import _ from 'lodash';

export function getPlacesByGame(places, game) {
    if (game) {
        const { confirmed, recomended } = places.reduce((acc, place) => {
            if (
                _.has(place, 'broadcastGames') &&
                place.broadcastGames.some(broadcast => broadcast._id === game._id)
            ) {
                acc.confirmed.push(Object.assign({}, place, { confirmed: true }));
            } else if (
                _.has(place, 'recomendations') &&
                place.recomendations.some(recomendation =>
                    recomendation.team === game.visitingTeam.id ||
                    recomendation.team === game.homeTeam.id
                )
            ) {
                acc.recomended.push(Object.assign({}, place, { confirmed: false }));
            }
            return acc;
        }, { confirmed: [], recomended: [] });

        confirmed.sort((a, b) => (a.name > b.name));
        recomended.sort((a, b) => (b.score - a.score));
        return confirmed.concat(recomended);
    }
    return [];
}