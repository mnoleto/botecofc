const dynamicSort = function(property) {
  var sortOrder = 1;
  if(property[0] === "-") {
    sortOrder = -1;
    property = property.substr(1);
  }
  return function (a,b) {
    return a[property].localeCompare(b[property]);
  }
}
export default dynamicSort;
