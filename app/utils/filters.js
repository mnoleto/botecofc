import moment from 'moment';
import { findCityById } from './cities'

export const filterRecentGames = (games) => {
	const filterDate = moment().subtract(2, 'hours')
	if (!games || games.length === 0) {
		return []
	}
	return games.filter(game => {
		return moment(game.gameDate) > filterDate
	});
}

const checkGameBroadcast = (game, broadcast) => {
    if (!broadcast && !game) return false;
    if (!broadcast.length === 0) return false;
    const head = broadcast[0];
    const tail = broadcast.slice(1);
    if (!head) return false;
    if (head.id === game.id) {
        return true;
    } else if (tail.length > 0) {
        return checkGameBroadcast(game, tail);
    } else {
        return false;
    }
};

export const filterConfirmedGames = (games, places) => {
    if (!games || !places) return [];
    const placesWithBroadcast = places
        .filter(place => place.broadcastGames && place.broadcastGames.length > 0)
        .map((place) => {
            return place.broadcastGames;
        });
    const placesMerged = [].concat.apply([], placesWithBroadcast).filter(place => place);
    const filteredGames = games.filter((game) => {
        if (!game.id) return false;
        return checkGameBroadcast(game, placesMerged)
    });
    return filteredGames;
}

export const filterGames = (games, filters) => {
    if (filters.show === 'SHOW_ALL') {
        return games;
    } else {
        return filterGamesByChampionship(games, filters);
    }
}

export const filterGamesByChampionship = (games, filters) => {
	if (filters.championship === 'TODOS') {
		return games;
	} else {
		return games.filter((game) => {
			return game.championship.id === filters.championship;
		});
	}
}

export const filterHomePlacesByCity = (places, city, cities) => {
	if (
        city === '' ||
        city == null ||
        cities == null ||
        cities.length === 0
    ) {
		return places;
	} else {
        const selectedCity = findCityById(city, cities)
        if (selectedCity == null) {
            return places
        }
		return places.filter(function(place) {
			if (place && place.cityId != null && place.cityId !== '') {
				return (place.cityId === selectedCity.id);
            }
            return false;
		});
	}
}

export const filterPlacesByCity = (places, city) => {
    if (city == null || city === '') {
        return places
    }
    return places.filter(function (place) {
        return place.cityId === city;
    });
}

export const filterChampionships = (championships, filters) => {
	if (filters.show === 'SHOW_ALL') {
		return championships;
	} else {
		return championships.filter(function(championship) {
			return championship.countries.some(function(country, index) {
				return country.id === filters.countries.id;
			});
		});
	}
}

export const filterChampionshipsByTeam = (championships, filters) => {
    if (filters.team === '') {
        return championships;
    } else {
        let filteredChampionships = [];
        championships.forEach(function(championship, index) {
            championship.teams.forEach(function(team, index) {
                if (team.id === filters.team) {
                    filteredChampionships.push(championship);
                }
            });
        });
        return filteredChampionships;
    }
}

export const filterPlacesByTeam = (places, filters) => {
    if (filters.team === '') {
        return places;
    } else {
        let filteredPlaces = [];
        places.forEach(function(place, index) {
            if (place.teamOficial) {
                if (place.teamOficial.id === filters.team) {
                    filteredPlaces.push(place);
                }
            }
        });
        return filteredPlaces;
    }
}

export const filterGamesByTeam = (games, filters) => {
    if (filters.team === '') {
        return games;
    } else {
        let filteredGames = [];
        games.forEach(function(game, index) {
            if (game.homeTeam.id === filters.team || game.visitingTeam.id === filters.team) {
                filteredGames.push(game);
            }
        });
        return filteredGames;
    }
}

export const filterTeamsByCountry = (teams, filters) => {
	if (filters.countries.length === 0) {
		return teams;
	} else {
		let countries = [];
		teams.forEach(function(team, index) {
			if (team.country.id === filters.countries.id) countries.push(team);
		});
		return countries;
	}
}

export function filterPlaces(places, ownerships) {
	if (!ownerships) {
		return [];
	} else {
		let placesSelected = [];
		places.forEach(function(place, index) {
			ownerships.forEach(function(owner, i) {
				if (place.id === owner.id) {
					placesSelected.push(place);
				}
			});
		});
		return placesSelected;
	}
}
