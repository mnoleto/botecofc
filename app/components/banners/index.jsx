import React, { Component } from 'react';
import Slider from 'react-slick';
import classNames from 'classnames/bind';
import styles from '../../css/components/banners';

const cx = classNames.bind(styles);

const settings = {
  autoplay: true,
  autoplaySpeed: 6000,
  arrows: false,
  dots: true,
  dotsClass: cx('dots', 'dotslist'),
  infinite: true,
  speed: 500,
  slidesToShow: 1,
};

class Banners extends Component {
  render() {
    const { banners, type, ga } = this.props;
    const bannersSlides = banners.slice(0, 3).map(item => {
      const bannerUrl =
        (type === 'mobile' && item.imageMobile && item.imageMobile.responseText)
          ? item.imageMobile.responseText
          : item.image.responseText;
      return (
        <React.Fragment key={item.id}>
          {item.link === ''
            ? <img src={item.image.responseText} alt={item.name} />
            : (
              <a href={item.link} target="_blank" onClick={() =>
                ga.send('send', 'event', 'Home Banners', 'click', item.name)
              }>
                <img src={bannerUrl} alt={item.name} />
              </a>
            )
          }
        </React.Fragment>
      );
    });
    if (banners && banners.length > 0) {
      return (
        <div className={cx('banners', 'home-banners', 'initialized')} ref={c => (this._slider_container = c)}>
          <Slider {...settings} className={cx('banners-carousel')}>
            {bannersSlides}
          </Slider>
        </div>
      );
    }
    return <span />
  }
}

export default Banners;
