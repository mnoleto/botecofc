import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import registerImage from '../../images/bg/register@2x.jpg';
import MessageModal from '../modais/MessageModal';
import RegisterCompleteForm from '../register/RegisterCompleteForm';
import { fetchCities } from '../../actions/cities';
import { fetchStates } from '../../actions/states';
import { fetchTeams } from '../../actions/teams';
import styles from '../../css/components/loginOrRegister';

const cx = classNames.bind(styles);

class Register extends Component {
	static need = [
		fetchCities,
		fetchTeams,
		fetchStates,
	];

	constructor(props) {
		super(props);
		this.state = {
			type: '',
			message: ''
		};
		this.updateUserProfile = this.updateUserProfile.bind(this);
	}

	componentDidMount() {
		document.querySelector('.site-footer').classList.add('hide');
	}

	updateUserProfile(data) {
		const { updateRegister } = this.props;
		const _ = this;
		updateRegister(data, type => {
			if (type === 'success') {
				_.setState({
					message: {
							title: "Cadastro Atualizado!",
							body: "Obrigado por atualizar os seus dados no Qual Bar."
					},
					type: 'success'
				});
				document.querySelector('#messagemodal').style.display = 'block';
			} else if (type === 'error') {
				_.setState({
					message: {
						title: "Ooops!",
						body: "Algo errado aconteceu e não foi possível atualizar o seu cadastro. Tente novamente mais tarde."
					},
					type: 'error'
				});
				document.querySelector('#messagemodal').style.display = 'block';
			}
		});
	}

	render() {
		const { cities, states, user, teams } = this.props;
		
		return (
			<div className={cx('register')}>
				<figure className={cx('header-image')}>
					<img src={registerImage} alt="" />
				</figure>

				<RegisterCompleteForm
					cities={cities}
					states={states}
					user={user}
					teams={teams}
					updateRegister={this.updateUserProfile}
				/>

				<MessageModal
					id="messagemodal"
					type={(this.state.type) ? this.state.type : ''}
					title={(this.state.message) ? this.state.message.title : ''}
					body={(this.state.message) ? this.state.message.body : ''}
				/> 
			</div>
		);
	}
}

Register.propTypes = {
	cities: PropTypes.array,
	states: PropTypes.array,
	teams: PropTypes.array.isRequired,
	updateRegister: PropTypes.func.isRequired,
	user: PropTypes.object,
};

export default Register;
