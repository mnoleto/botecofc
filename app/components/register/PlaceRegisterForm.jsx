import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import InputElement from 'react-input-mask';
import Slugify from '../../utils/slugify';
import CheckboxWithLabel from '../CheckboxWithLabel';
import UploadImage from '../admin/UploadImage';
import { CitiesSelect, StatesSelect } from '../selects'
import styles from '../../css/admin/form';

const cx = classNames.bind(styles);

class PlaceRegisterForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			about: '',
			address: '',
			cards: [],
			city: '',
			cityId: '',
			clearImages: false,
			cnpj: '',
			dayend: 'domingo',
			daystart: 'domingo',
			email: '',
			facebook: '',
			gallery: [],
			googleplus: '',
			hourend: '22:00',
			hourstart: '08:00',
			id: '',
			instagram: '',
			logo: {},
			message: '',
			name: '',
			opening: [],
			ownerName: '',
			ownerships: {},
			phone: '',
			services: [],
			slug: '',
			stateId: '',
			twitter: '',
			website: '',
			zipcode: '',
		};

		this.eventHandlers = {};

		this.isEditing = false;
		this.clearImages = false;
		this.isSubmiting = false;
		this.renderMessage = this.renderMessage.bind(this);
		this.renderContent = this.renderContent.bind(this);

		this.getGalleryObject = this.getGalleryObject.bind(this);
		this.handleCardsChange = this.handleCardsChange.bind(this);
		this.handleFormChange = this.handleFormChange.bind(this);
		this.handleOpeningClick = this.handleOpeningClick.bind(this);
		this.handleServicesChange = this.handleServicesChange.bind(this);
		this.handleTitleChange = this.handleTitleChange.bind(this);
		this.onClearImages = this.onClearImages.bind(this);
		this.onEntryUpdate = this.onEntryUpdate.bind(this);
		this.onLogoUploadComplete = this.onLogoUploadComplete.bind(this);
		this.onLogoRemoveImage = this.onLogoRemoveImage.bind(this);
		this.onGalleryRemoveImage = this.onGalleryRemoveImage.bind(this);
		this.onGalleryUploadComplete = this.onGalleryUploadComplete.bind(this);
		this.removeOpeningItem = this.removeOpeningItem.bind(this);
		this.handleStateChange = this.handleStateChange.bind(this);
		this.handleCityChange = this.handleCityChange.bind(this);
	};

	componentWillReceiveProps(nextProps) {
		if (nextProps.place) {
			const { user } = this.props;
			this.isEditing = true;
			this.setState({
				about: nextProps.place.about,
				address: nextProps.place.address,
				cards: nextProps.place.cards,
				city: nextProps.place.city,
				cityId: nextProps.place.cityId,
				cnpj: nextProps.place.cnpj,
				email: nextProps.place.email,
				facebook: nextProps.place.facebook,
				gallery: nextProps.place.gallery,
				googleplus: nextProps.place.googleplus,
				id: nextProps.place.id,
				instagram: nextProps.place.instagram,
				logo: nextProps.place.logo,
				name: nextProps.place.name,
				opening: nextProps.place.opening,
				ownerName: nextProps.place.ownerName,
				ownerships: {
					email: user.email,
					id: user._id,
					name: user.name,
				},
				phone: nextProps.place.phone,
				services: nextProps.place.services,
				slug: nextProps.place.slug,
				stateId: nextProps.place.stateId,
				twitter: nextProps.place.twitter,
				website: nextProps.place.website,
				zipcode: nextProps.place.zipcode,
			});
		}
	}

	getGalleryObject(index) {
		if (this.state.gallery && this.state.gallery[index]) {
			return this.state.gallery[index];
		} else {
			return [];
		}
	}

	handleCardsChange(val) {
		let checked = this.state.cards.length > 0 ? this.state.cards.slice() : [];
	    if (checked.includes(val)) {
	      checked.splice(checked.indexOf(val), 1);
	    } else {
	      checked.push(val);
	    }
	    this.setState({ cards: checked })
	    this.forceUpdate();
	};

	handleFormChange(event) {
		this.setState({ [event.target.name]: event.target.value });
	};

	handleOpeningClick(event) {
		const daystart = this.state.daystart;
		const dayend = this.state.dayend;
		const hourstart = this.state.hourstart;
		const hourend = this.state.hourend;

		this.state.opening.push({
			daystart,
			dayend,
			hourstart,
			hourend
		});
		this.forceUpdate();
	};

	handleServicesChange(val) {
		let checked = this.state.services.length > 0 ? this.state.services.slice() : [];
	    if (checked.includes(val)) {
			checked.splice(checked.indexOf(val), 1);
	    } else {
			checked.push(val);
	    }
	    this.setState({ services: checked })
	    this.forceUpdate();
	};

	handleTitleChange(event) {
		const title = event.target.value;
		this.setState({ [event.target.name]: title });
		this.state.slug = Slugify (title);
	}

	handleStateChange(event) {
		this.setState({
			stateId: event.target.value,
		})
	}

	handleCityChange(event) {
		this.setState({
			cityId: event.target.value,
		})
	}

	onClearImages() {
		this.state.clearImages = false;
	}

	onEntryUpdate(e) {
		e.preventDefault();
		const { entryUpdate, user } = this.props;

		if(!this.state.logo || !this.state.logo.responseText || this.state.logo.responseText === '') {
			this.setState({ message: 'Faço upload da logo do seu estabelecimento.' });
			return false;
		}

		if(this.state.name === '') {
			this.setState({ message: 'Preencha o nome do seu estabelecimento.' });
			return false;
		}

		if(this.state.address === '') {
			this.setState({ message: 'Preencha o endereço do seu estabelecimento.' });
			return false;
		}

		if (this.state.city === '') {
			this.setState({ message: 'Preencha o campo bairro.' });
			return false;
		}

		if (this.state.stateId === '') {
			this.setState({ message: 'Preencha o campo estado.' });
			return false;
		}

		if (this.state.cityId === '') {
			this.setState({ message: 'Preencha o campo cidade.' });
			return false;
		}

		if(this.state.phone === '') {
			this.setState({ message: 'Preencha o telefone do seu estabelecimento.' });
			return false;
		}

		let phoneReplace;
		if (this.state.phone && this.state.phone !== '' && this.state.phone.length > 0) {
			phoneReplace = this.state.phone;
			phoneReplace = phoneReplace.split('_').join('');
			this.setState({ phone: phoneReplace });
		}

		entryUpdate({
			about: this.state.about,
			address: this.state.address,
			cards: this.state.cards,
			cityId: this.state.cityId,
			cnpj: this.state.cnpj,
			email: this.state.email,
			facebook: this.state.facebook,
			gallery: this.state.gallery,
			googleplus: this.state.googleplus,
			id: this.state.id,
			instagram: this.state.instagram,
			logo: this.state.logo,
			name: this.state.name,
			opening: this.state.opening,
			ownerName: this.state.ownerName,
			ownerships: {
				email: user.email,
				id: user._id,
				name: user.name,
			},
			phone: phoneReplace,
			services: this.state.services,
			slug: this.state.slug,
			stateId: this.state.stateId,
			twitter: this.state.twitter,
			website: this.state.website,
			zipcode: this.state.zipcode,
		});
	};

	onLogoRemoveImage(file) {
		this.setState({ logo: {} });
	}

	onLogoUploadComplete(file) {
		this.state.logo = file;
	};

	onGalleryUploadComplete(file) {
		let gallery = (this.state.gallery.length > 0) ? this.state.gallery : [];
		gallery.push(file);
		this.setState({gallery: gallery});
	};

	onGalleryRemoveImage(file) {
		const index = this.state.gallery.findIndex(element =>
			element && element.responseText === file.responseText,
		);
		this.setState({
			gallery: this.state.gallery.map((v, i) => {
				if(i === index) {
					return {};
				} else {
					return v;
				}
			}),
		});
	}

	renderMessage() {
		if (this.state.message !== '') {
			return (
				<div className={cx('message')}>
					<i className="zmdi zmdi-block"></i> {this.state.message}
				</div>
			);
		}
	}

	removeOpeningItem(index) {
		this.state.opening.splice(index, 1);
		this.forceUpdate();
	}

	renderContent() {
		const { cities, dismissMessage, message, states } = this.props;

		if (message && message.length > 0) {
			window.scrollTo(0, 0);

			if (message === 'success') {
				return (
					<div className={cx('message', 'success')} onClick={dismissMessage}>
						<i className="zmdi zmdi-check-circle" />
						<h1>Seu estabelecimento foi enviado com sucesso!</h1>
						<p>Vamos confirmar os dados e entramos em contato com você em breve por e-mail.</p>
						<Link to="/cadastrar-bar" onClick={dismissMessage}>Cadastrar outro bar</Link>
					</div>
				);
			} else {
				return (
					<div className={cx('message', 'error')} onClick={dismissMessage}>
						<i className="zmdi zmdi-block" />
						<h1>Ooops!</h1>
						<p>Desculpe, mas aconteceu algo inesperado e o seu estabelecimento não foi cadastrado.</p>
						<button type="button" onClick={dismissMessage}>Tente novamente</button>
					</div>
				);
			}
		} else {
			// const cities = citiesArray.map((city, index) => {
			// 	return <option key={index} value={city}>{city}</option>
			// });
			const filteredCities = cities.filter(city => city.stateId === this.state.stateId);

			return (
				<form action="/uploadPlace" ref="adminForm" className={cx('clearfix', 'form-inline')} method="post" encType="multipart/form-data" onSubmit={this.onEntryUpdate} role="form">
					<h2 className={cx('form-title')}>Dados do Estabelecimento</h2>
					{this.renderMessage()}

					<div className={cx('row')}>
						<div className={cx('upload-field', 'col-md-12')}>
							<UploadImage
								id={'logo'}
								classNames={'upload-logo'}
								clearImages={this.clearImages}
								image={this.state.logo}
								isEditing={this.isEditing}
								postUrl={'/uploadPlace'}
								onClearImages={this.onClearImages}
								onRemoveImage={this.onLogoRemoveImage}
								onUploadComplete={this.onLogoUploadComplete}
								text={'INCLUA A LOGO DO ESTABELECIMENTO'}
								uploadMultiple={false}
								/>
							<p className={cx('text-center', 'recomendation')}>Recomendamos incluir a logo da sua empresa com 350x350 pixels pra ter uma melhor visibilidade.</p>
	          			</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-12')}>
							<div className={cx('field-group')}>
								<label htmlFor="name" className={cx('sr-only')}>Nome *</label>
								<input type="text" name="name" placeholder="Nome do Bar *" value={this.state.name || ''} onChange={this.handleTitleChange} />
							</div>
						</div>
						<div className={cx('col-md-6')}>
							<div className={cx('field-group')}>
								<label htmlFor="name" className={cx('sr-only')}>Nome do responsável</label>
								<input type="text" name="ownerName" placeholder="Nome do responsável" value={this.state.ownerName || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-6')}>
							<div className={cx('field-group')}>
								<label htmlFor="website" className={cx('sr-only')}>Site</label>
								<input type="text" name="website" placeholder="Site" value={this.state.website || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-6')}>
							<div className={cx('field-group')}>
								<label htmlFor="address" className={cx('sr-only')}>Endereço *</label>
								<input type="text" name="address" placeholder="Endereço *" value={this.state.address || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-6')}>
							<div className={cx('field-group')}>
								<label htmlFor="address" className={cx('sr-only')}>Bairro *</label>
								<input type="text" name="city" placeholder="Bairro *" value={this.state.city || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-6')} style={{ zIndex: 5 }}>
							<StatesSelect stateId={this.state.stateId} states={states} onChange={this.handleStateChange} />
						</div>
						<div className={cx('col-md-6')} style={{ zIndex: 4 }}>
							<CitiesSelect
								cities={filteredCities}
								cityId={this.state.cityId}
								onChange={this.handleCityChange}
							/>
						</div>
						<div className={cx('col-md-6')}>
							<div className={cx('field-group')}>
								<label htmlFor="zipcode" className={cx('sr-only')}>CEP</label>
								<InputElement name="zipcode" placeholder="CEP" mask="99.999-999" value={this.state.zipcode || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-6')}>
							<div className={cx('field-group')}>
								<label htmlFor="cnpj" className={cx('sr-only')}>CNPJ</label>
								<InputElement name="cnpj" placeholder="CNPJ" mask="999.999.999/9999-99" value={this.state.cnpj || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-6')}>
							<div className={cx('field-group')}>
								<label htmlFor="email" className={cx('sr-only')}>E-mail</label>
								<input type="text" name="email" placeholder="E-mail" value={this.state.email || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-6')}>
							<div className={cx('field-group')}>
								<label htmlFor="phone" className={cx('sr-only')}>Telefone *</label>
								<InputElement name="phone" placeholder="Telefone *" mask="(99) 999999999" value={this.state.phone || ''} onChange={this.handleFormChange} />
							</div>
						</div>
					</div>

					<div className={cx('row', 'gallery')}>
						<div className={cx('col-md-12')}>
							<p>Nesta versão do site só é possível incluir imagens de até 1Mb.</p>
							<div className={cx('col-md')}>
								<UploadImage
									id={'gallery0'}
									classNames={'dropzone-gallery'}
									clearImages={this.clearImages}
									image={this.getGalleryObject(0)}
									isEditing={this.isEditing}
									postUrl={'/uploadPlace'}
									onUploadComplete={this.onGalleryUploadComplete}
									onClearImages={this.onClearImages}
									onRemoveImage={this.onGalleryRemoveImage}
									text={''}
									uploadMultiple={false}
									/>
        			</div>
        			<div className={cx('col-md')}>
								<UploadImage
									id={'gallery1'}
									classNames={'dropzone-gallery'}
									clearImages={this.clearImages}
									image={this.getGalleryObject(1)}
									isEditing={this.isEditing}
									postUrl={'/uploadPlace'}
									onUploadComplete={this.onGalleryUploadComplete}
									onClearImages={this.onClearImages}
									onRemoveImage={this.onGalleryRemoveImage}
									text={''}
									uploadMultiple={false}
									/>
        			</div>
        			<div className={cx('col-md')}>
								<UploadImage
									id={'gallery2'}
									classNames={'dropzone-gallery'}
									clearImages={this.clearImages}
									image={this.getGalleryObject(2)}
									isEditing={this.isEditing}
									postUrl={'/uploadPlace'}
									onUploadComplete={this.onGalleryUploadComplete}
									onClearImages={this.onClearImages}
									onRemoveImage={this.onGalleryRemoveImage}
									text={''}
									uploadMultiple={false}
									/>
        			</div>
        			<div className={cx('col-md')}>
								<UploadImage
									id={'gallery3'}
									classNames={'dropzone-gallery'}
									clearImages={this.clearImages}
									image={this.getGalleryObject(3)}
									isEditing={this.isEditing}
									postUrl={'/uploadPlace'}
									onUploadComplete={this.onGalleryUploadComplete}
									onClearImages={this.onClearImages}
									onRemoveImage={this.onGalleryRemoveImage}
									text={''}
									uploadMultiple={false}
									/>
        			</div>
        			<div className={cx('col-md')}>
								<UploadImage
									id={'gallery4'}
									classNames={'dropzone-gallery'}
									clearImages={this.clearImages}
									image={this.getGalleryObject(4)}
									isEditing={this.isEditing}
									postUrl={'/uploadPlace'}
									onUploadComplete={this.onGalleryUploadComplete}
									onClearImages={this.onClearImages}
									onRemoveImage={this.onGalleryRemoveImage}
									text={''}
									uploadMultiple={false}
									/>
        			</div>
        			<div className={cx('col-md')}>
								<UploadImage
									id={'gallery5'}
									classNames={'dropzone-gallery'}
									clearImages={this.clearImages}
									image={this.getGalleryObject(5)}
									isEditing={this.isEditing}
									postUrl={'/uploadPlace'}
									onUploadComplete={this.onGalleryUploadComplete}
									onClearImages={this.onClearImages}
									onRemoveImage={this.onGalleryRemoveImage}
									text={''}
									uploadMultiple={false}
									/>
        			</div>
        		</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-12')}>
							<div className={cx('field-group')}>
								<label htmlFor="about">Sobre o Bar <small>máx 400 caracteres</small></label>
								<textarea name="about" ref="about" value={this.state.about || ''} onChange={this.handleFormChange}></textarea>
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-6')}>
							<div className={cx('field-group')}>
								<legend>Selecione os serviços</legend>
								<CheckboxWithLabel data={this.state.services} name="services" value="Ar Condicionado" onChange={this.handleServicesChange} />
								<CheckboxWithLabel data={this.state.services} name="services" value="Brinquedoteca" onChange={this.handleServicesChange} />
								<CheckboxWithLabel data={this.state.services} name="services" value="Comanda Individual" onChange={this.handleServicesChange} />
								<CheckboxWithLabel data={this.state.services} name="services" value="Estacionamento Privativo" onChange={this.handleServicesChange} />
								<CheckboxWithLabel data={this.state.services} name="services" value="Manobrista" onChange={this.handleServicesChange} />
								<CheckboxWithLabel data={this.state.services} name="services" value="Mesas ao ar livre" onChange={this.handleServicesChange} />
								<CheckboxWithLabel data={this.state.services} name="services" value="Wifi" onChange={this.handleServicesChange} />
								{/*<CheckboxWithLabel data={this.state.services} name="services" value="Vallet" onChange={this.handleServicesChange} />*/}
							</div>
						</div>
						<div className={cx('col-md-6')}>
							<div className={cx('field-group')}>
								<legend>Selecione os cartões</legend>
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Visa" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Mastercard" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Maestro" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Elo" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="American Express" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Diners Club" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Vale - Alelo Refeição / Visa Vale" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Vale - Sodexo" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Vale - Ticket Restaurante" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Vale - Vale Card" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Vale - VR Smart" onChange={this.handleCardsChange} />
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-6')}>
							<div className={cx('field-group', 'facebook')}>
								<input type="text" name="facebook" placeholder="url facebook" value={this.state.facebook || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-6')}>
							<div className={cx('field-group', 'instagram')}>
								<input type="text" name="instagram" placeholder="url instagram" value={this.state.instagram || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-6')}>
							<div className={cx('field-group', 'twitter')}>
								<input type="text" name="twitter" placeholder="url twitter" value={this.state.twitter || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-6')}>
							<div className={cx('field-group', 'googleplus')}>
								<input type="text" name="googleplus" placeholder="url g+" value={this.state.googleplus || ''} onChange={this.handleFormChange} />
							</div>
						</div>
					</div>

					{this.renderMessage()}

					<div className={cx('row')}>
						<div className={cx('col-md-12', 'required')}><p className={cx('pull-right')}><small>* campos obrigatórios</small></p></div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-12')}><input className={cx('submit-btn', 'pull-right')} type="submit" value='Enviar' /></div>
					</div>
				</form>
			);
		}
	}

	render() {
		return (
			<div className={cx('register-header-form-col', 'container')}>
				{this.renderContent()}
			</div>
		);
	};
}

PlaceRegisterForm.propTypes = {
	cities: PropTypes.array,
	dismissMessage: PropTypes.func.isRequired,
	entryUpdate: PropTypes.func.isRequired,
	message: PropTypes.string,
	place: PropTypes.object,
	states: PropTypes.array,
	user: PropTypes.object.isRequired
};

export default PlaceRegisterForm;
