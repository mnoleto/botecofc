import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import classNames from 'classnames/bind';
import InputElement from 'react-input-mask';
import styles from '../../css/components/loginOrRegister';
import sortByName from '../../utils/sortByName';
import PolicyModal from '../modais/PolicyModal';
import TermsModal from '../modais/TermsModal';

const cx = classNames.bind(styles);

class RegisterCompleteForm extends Component {
  constructor(props) {
    super(props);
    const { account } = props.user;
    const team = (account.profile.team && account.profile.team.id) ? account.profile.team : {};

    this.state = {
      id: account._id,
      name: account.name,
      gender: account.profile.gender,
      birthday: account.profile.birthday,
      phone: account.profile.phone,
      cityId: account.profile.cityId,
      stateId: account.profile.stateId,
      team: team.id ? team : {},
      selectedCity: account.profile.cityId
        ? {
            value: account.profile.cityId,
            label: this.getCityById(account.profile.cityId).name
          }
        : '',
      selectedState: account.profile.stateId
        ? {
            value: account.profile.stateId,
            label: this.getStateById(account.profile.stateId).name
          }
        : '',
      selectedTeam: team.id ? { value: team.id, label: team.name } : '',
      terms: false,
      message: '',
    };
    this.handleCheckChange = this.handleCheckChange.bind(this);
    this.handleFormChange = this.handleFormChange.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleTeamsChange = this.handleTeamsChange.bind(this);
    this.renderMessage = this.renderMessage.bind(this);
    this.handleStateChange = this.handleStateChange.bind(this);
    this.handleCityChange = this.handleCityChange.bind(this);
    this.getCityById = this.getCityById.bind(this);
    this.getStateById = this.getStateById.bind(this);
  }

  componentDidMount() {
    document.querySelector('.site-footer').style.display = 'none';
  }

  componentDidUpdate(prevProps) {
    if (prevProps.user == null) {
      const { account } = this.pros.user;
      const team = (account.profile.team && account.profile.team.id) ? account.profile.team : {};
      this.setState({
        id: account._id,
        name: account.name,
        gender: account.profile.gender,
        birthday: account.profile.birthday,
        phone: account.profile.phone,
        cityId: account.profile.cityId,
        stateId: account.profile.stateId,
        team: team.id ? team : {},
        selectedCity: team.id ? { value: account.profile.cityId, label: this.getCityById(account.profile.cityId) } : '',
        selectedTeam: team.id ? { value: account.profile.stateId, label: this.getStateById(account.profile.stateId) } : '',
        selectedTeam: team.id ? { value: team.id, label: team.name } : '',
      });
    }
  }

  // componentWillReceiveProps(nextProps) {
  //   if (nextProps.user) {
  //     const { account } = nextProps.user;
  //     const team = (account.profile.team && account.profile.team.id) ? account.profile.team : {};
  //     this.setState({
  //       id: account._id,
  //       name: account.name,
  //       gender: account.profile.gender,
  //       birthday: account.profile.birthday,
  //       phone: account.profile.phone,
  //       cityId: account.profile.cityId,
  //       stateId: account.profile.stateId,
  //       team: team.id ? team : {},
  //       selectedCity: team.id ? { value: account.profile.cityId, label: this.getCityById(account.profile.cityId) } : '',
  //       selectedTeam: team.id ? { value: account.profile.stateId, label: this.getStateById(account.profile.stateId) } : '',
  //       selectedTeam: team.id ? { value: team.id, label: team.name } : '',
  //     });
  //   }
  // }

  getCityById(id) {
    return this.props.cities.find(item => {
      return item.id === id
    })
  }

  getStateById(id) {
    return this.props.states.find(item => {
      return item.id === id
    })
  }

  handleFormChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleCheckChange(event) {
    this.setState({ [event.target.name]: event.target.checked });
  };

  handleOnSubmit(event) {
    event.preventDefault();
    
    const { updateRegister } = this.props;
    const { account } = this.props.user;

    if (this.state.name == '') {
      this.state.message = 'Preencha o campo nome.';
      return false;
    }

    if (this.state.stateId === '') {
      this.setState({ message: 'Preencha o campo estado.' });
      return false;
    }

    if (this.state.cityId === '') {
      this.setState({ message: 'Preencha o campo cidade.' });
      return false;
    }

    if (!this.refs.terms.checked) {
      this.state.message = 'Aceite os termos de uso antes de prosseguir.';
      this.forceUpdate();
      return false;
    }

    this.state.message = '';

    updateRegister(Object.assign({}, account, {
      _id: this.state.id,
      name: this.state.name,
      profile: Object.assign({}, account.profile, {
        birthday: this.state.birthday,
        cityId: this.state.cityId,
        stateId: this.state.stateId,
        gender: this.state.gender,
        phone: this.state.phone,
        team: (this.state.team) ? this.state.team : {},
        terms: true
      })
    }));
  }

  handleTeamsChange(selectedTeam) {
    this.setState({
      selectedTeam,
      team: {
        id: selectedTeam.value,
        name: selectedTeam.label,
      },
    });
  };

  handleStateChange(selectedState) {
    this.setState({
      selectedState,
      stateId: selectedState.value,
    })
  }

  handleCityChange(selectedCity) {
    this.setState({
      selectedCity,
      cityId: selectedCity.value,
    })
  }

  renderMessage() {
    if (this.state.message != '') {
      return (
        <div className={cx('message')}>
          {this.state.message}
        </div>
      );
    }
  }

  render() {
    const { cities, states, teams } = this.props;
    const { selectedCity, selectedTeam, selectedState, stateId } = this.state;

    const filteredCities = cities.filter(city => city.stateId === this.state.stateId);

    const alphabeticalTeams = teams;
    alphabeticalTeams.sort(sortByName('name'));

    const listItem = alphabeticalTeams.map(item => {
      return { value: item.id,
        label: item.name,
      };
    });
    return (
      <div className={cx('container')}>
        <h2 className={cx('heading')}>Complete o seu cadastro</h2>

        <form className={cx('form-register')} onSubmit={this.handleOnSubmit}>
          {this.renderMessage()}

          <div className={cx('form-group')}>
            <label htmlFor="name" className={cx('sr-only')}>Nome *</label>
            <input type="text" name="name" placeholder="Nome *" value={this.state.name} onChange={this.handleFormChange} />
          </div>

          <div className={cx('form-radio')}>
            <input type="radio" name="gender" value="masculino" checked={this.state.gender === 'masculino'} onChange={this.handleFormChange} /> <label>Masculino</label>
            <input type="radio" name="gender" value="feminino" checked={this.state.gender === 'feminino'} onChange={this.handleFormChange} /> <label>Feminino</label>
          </div>

          <div className={cx('form-group')}>
            <label htmlFor="name" className={cx('sr-only')}>Nasc:</label>
            <InputElement name="birthday" placeholder="Nascimento" mask="99/99/9999" value={this.state.birthday || ''} className={cx('input')} onChange={this.handleFormChange} />
          </div>

          <div className={cx('form-group')}>
            <label htmlFor="name" className={cx('sr-only')}>Telefone:</label>
            <InputElement name="phone" placeholder="Telefone" mask="(99) 999999999" value={this.state.phone || ''} className={cx('input')} onChange={this.handleFormChange} />
          </div>

          <div className={cx('form-group')} style={{ zIndex: 5 }}>
            {states.length > 0 && <Select
              name="state"
              multi={false}
              isSearchable={false}
              value={selectedState}
              onChange={this.handleStateChange}
              options={states.map(item => ({
                value: item.id,
                label: item.name
              }))}
              placeholder="Selecione seu estado"
              style={{zIndex: 1000}}
            />}
          </div>
          <div className={cx('form-group')} style={{ zIndex: 4 }}>
            {filteredCities.length > 0 && <Select
              name="city"
              isDisabled={!stateId || stateId === ''}
              isSearchable={false}
              multi={false}
              value={selectedCity}
              onChange={this.handleCityChange}
              options={filteredCities.map(item => ({
                value: item.id,
                label: item.name
              }))}
              placeholder="Selecione sua cidade"
              style={{ zIndex: 1000 }}
            />}
          </div>

          <div className={cx('form-group')} style={{ zIndex: 3 }}>
            <label htmlFor="name" className={cx('sr-only')}>Qual o seu time favorito?</label>
            <Select
              name="team"
              multi={false}
              value={selectedTeam}
              onChange={this.handleTeamsChange}
              options={listItem}
              noResultsText="Time não encontrado"
              placeholder="Qual o seu time?"
              style={{ zIndex: 1000 }}
            />
          </div>

          <div className={cx('form-group', 'terms-and-policy')}>
            <input type="checkbox" ref="terms" name="terms" checked={this.state.terms} onChange={this.handleCheckChange} /> <label htmlFor="terms">Li e aceito os <a data-toggle="modal" data-target="#termsModal">termos de uso</a> e <a data-toggle="modal" data-target="#policyModal">politica de privacidade</a></label>
          </div>

          {this.renderMessage()}

          <input className={cx('btn-submit')} type="submit" value="Salvar" />
        </form>

        <TermsModal
          id={'termsModal'}
          onAcceptTerms={this.handleCheckChange} />

        <PolicyModal
          id={'policyModal'}
          onAcceptTerms={this.handleCheckChange} />
      </div>
    );
  }
}

RegisterCompleteForm.propTypes = {
  cities: PropTypes.array,
  states: PropTypes.array,
  user: PropTypes.object.isRequired,
  teams: PropTypes.array.isRequired,
  updateRegister: PropTypes.func.isRequired
};

export default RegisterCompleteForm;
