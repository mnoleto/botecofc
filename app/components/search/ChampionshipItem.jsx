import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ChampionshipItem from '../championship/ChampionshipItem';
import ChampionshipGamesModal from '../modais/ChampionshipGamesModal';

class SearchChampionshipItem extends Component {
  constructor(props) {
    super(props)
    this.handleChampionshipSelected = this.handleChampionshipSelected.bind(this)
  }

  handleChampionshipSelected = (championship) => {
    const { onClick } = this.props;
    onClick();
    document.querySelector('#championshipGamesModal').style.display = 'block';
  }

  render() {
    const { championship, games, teams } = this.props;
    return (
      <div>
        <ChampionshipItem
          championship={championship}
          onSelectChampionship={this.handleChampionshipSelected}
        />

        <ChampionshipGamesModal
          id={"championshipGamesModal"}
          championship={championship}
          teams={teams}
          games={games}
        />
      </div>
    );
  };
};

SearchChampionshipItem.propTypes = {
  championship: PropTypes.object.isRequired,
  teams: PropTypes.array.isRequired,
  games: PropTypes.array.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default SearchChampionshipItem;
