import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import TeamItem from '../search/TeamItem';
import ChampionshipItem from '../search/ChampionshipItem';
import PlaceItem from '../search/PlaceItem';
import styles from '../../css/components/list';

const cx = classNames.bind(styles);

class SearchResult extends Component {
	findGameByTeam = (teamID) => {
		const { games } = this.props;
		const dateGames = games;
		dateGames.sort((a, b) => 
			new Date(a.gameDate).getTime() - new Date(b.gameDate).getTime(),
		);
		return dateGames.find((game, index) =>
			teamID === game.visitingTeam.id || teamID === game.homeTeam.id,
		);
	}

	render() {
		const { result, games, teams, onClose } = this.props;
		let teamsList = [];
		let placesList = [];
		let championshipsList = [];
		
		if (result.teams && result.teams.length > 0) {
			teamsList = result.teams.map(team => {
				const game = this.findGameByTeam(team.id);
				return(
					<li key={team.id}>
						<TeamItem
							team={team}
							game={game}
							onClick={onClose}
						/>
					</li>
				);
			});
		}

		if (result.championships && result.championships.length > 0) {
			championshipsList = result.championships.map(championship => {
				return (
					<li key={championship.id}>
						<ChampionshipItem
							teams={teams}
							games={games}
							championship={championship}
							onClick={onClose}
						/>
					</li>
				);
			});
		}

		if (result.places && result.places.length > 0) {
			placesList = result.places.map(place => {
				return (
					<li key={place.id}>
						<PlaceItem
							place={place}
							games={games}
							teams={teams}
							onClick={onClose}
						/>
					</li>
				);
			});
		}
		
		return (
			<div className={cx('list-container')}>
				<div className={cx('list-content')}>
					{(teamsList.length > 0 || championshipsList.length > 0 || placesList.length > 0) &&
						<ul className={cx('search-list')}>
							{teamsList}
							{championshipsList}
							{placesList}
						</ul>
					}
				</div>
			</div>
		);
	}
};

SearchResult.propTypes = {
	id: PropTypes.string,
	result: PropTypes.object,
	games: PropTypes.array.isRequired,
	teams: PropTypes.array.isRequired,
	onClose: PropTypes.func.isRequired,
};

export default SearchResult;
