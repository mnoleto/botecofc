import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PlaceItem from '../place/PlaceItem';

import GamesListModal from 'components/modais/GamesListModal';

class SearchPlaceItem extends Component {
  onSelectPlace = (place) => {
    if (document) document.querySelector('#gamesList').style.display = 'block';
  }

  render() {
    const { games, teams, place } = this.props;

    const dateGames = games;
    dateGames.sort((a, b) =>
      new Date(a.gameDate).getTime() - new Date(b.gameDate).getTime(),
    );

    let placeGames = [];
    dateGames.forEach((game, index) => {
      if (place.broadcastGames) {
        var willBroadcast = false;
        var wasRecomended = false;
        var recomendationCounter = 0;
        // Game broadcast was confirmed by the place
        place.broadcastGames.forEach((broadcast, i) => {
          if (broadcast && broadcast !== null && game.id === broadcast.id) {
            willBroadcast = true;
            recomendationCounter = 1;
            return;
          }
        });

        // The Place is a oficial place
        if (place.teamOficial && (place.teamOficial.id === game.homeTeam.id || place.teamOficial.id === game.visitingTeam.id)) {
          willBroadcast = true;
          recomendationCounter = 1;
        }

        // Users said the the place usually broadcast games from the teams of the match
        place.recomendations.forEach((recomend, i) => {
          if (recomend.team === game.homeTeam.id || recomend.team === game.visitingTeam.id) {
            if (recomend.counter > 0) {
              recomendationCounter = recomend.counter;
              wasRecomended = true;
            }

          }
        });

        if (willBroadcast) {
          game.confirmed = true;
          game.recomendations = recomendationCounter;
          placeGames.push(game);
        } else if (wasRecomended) {
          game.confirmed = false;
          game.recomendations = recomendationCounter;
          placeGames.push(game);
        }
      }
    }, this);

    return (
      <div>
        <PlaceItem
          place={place}
          showConfirmed={false}
          onSelectPlace={this.onSelectPlace}
          onClick={this.props.onClick}
        />

        {(place) &&
          <GamesListModal
            id={'gamesList'}
            games={placeGames}
            place={place}
            teams={teams}
          />
        }
      </div>
    );
  }
};

SearchPlaceItem.propTypes = {
  games: PropTypes.array.isRequired,
  place: PropTypes.object.isRequired,
  teams: PropTypes.array.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default SearchPlaceItem;
