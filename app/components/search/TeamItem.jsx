import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from '../../css/components/list';
import iconTeam from '../../images/icons/team-teams-list.png';

const cx = classNames.bind(styles);

class SearchResultItem extends Component {
	constructor(props) {
		super(props);
		this.renderDate = this.renderDate.bind(this);
		this.renderHour = this.renderHour.bind(this);
	}

	renderDate(date) {
		const gameDate = new Date(date);
		const month = gameDate.getMonth() + 1;
		return ( (gameDate.getDate() < 10) ? '0' : '' ) + gameDate.getDate() + '/' +  ( (month < 9) ? '0' : '' ) + month;
	}

	renderHour(date) {
		const gameDate = new Date(date);
		return (gameDate.getHours()<10?'0':'') + gameDate.getHours() + 'H' + (gameDate.getMinutes()<10?'0':'') + gameDate.getMinutes();
	}

	render() {
		const { team, game, onClick } = this.props;
		
		return (
			<div className={cx('list-item', 'team-item', 'search-list')}>
				<div className={cx('list-col-1')}>
					<div className={cx('col-container')}>
						<h3 className={cx('title')}><Link to={'/times/' + team.slug} onClick={onClick}>{team.name}</Link></h3>
						{game &&
							<div className={cx('next-game-container')}>
								<h4 className={cx('subtitle')}>Próximo jogo</h4>
								<div className={cx('gameday')}>
									<div className={cx('game-day')}>{this.renderDate(game.gameDate)}</div>
									<div className={cx('game-hour')}>{this.renderHour(game.gameDate)}</div>
								</div>
							</div>
						}
					</div>
				</div>

				<div className={cx('list-middle')}>
					<div className={cx('list-middle-container')}>
						<Link to={'/times/' + team.slug} className={cx('logo-container')} style={{backgroundColor: team.color}} onClick={onClick}><img src={team.logo.responseText} alt={team.name} className={cx('logo')} /></Link>
					</div>
				</div>

				<div className={cx('list-col-2')}>
					<div className={cx('col-container')}>
						<Link to={'/times/' + team.slug} role="button" className={cx('btn-team-list')} onClick={onClick}>
							<i className={cx('list-icon', 'listteam-icon-team')}><img src={iconTeam} alt="Ver o time" /></i>
							<small>ver o</small><br /><strong>time</strong>
						</Link>
					</div>
				</div>
			</div>
		);
	}
}

SearchResultItem.propTypes = {
	onClick: PropTypes.func.isRequired,
	team: PropTypes.object.isRequired,
	game: PropTypes.object,
};

export default SearchResultItem;
