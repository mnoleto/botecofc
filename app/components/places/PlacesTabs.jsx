import React from 'react';
import PropTypes from 'prop-types';
import { Tabs, TabList, Tab, TabPanel } from 'react-tabs';
import classNames from 'classnames/bind';
import ListGames from '../games/ListGames';
import ListPlaces from '../places/ListPlaces';
import styles from '../../css/components/tabs';

const cx = classNames.bind(styles);

const PlacesTabs = ({ places, games, teams, onSelectPlace }) => {
	return (
		<Tabs className={cx('tabs')} defaultIndex={0}>
			<TabList className={cx('tabs-list', 'two-tabs')}>
				<Tab className={cx('tab', 'tablist-places')}>Bares</Tab>
				<Tab className={cx('tab', 'tablist-teams')}>Próximos Jogos</Tab>
			</TabList>

			<TabPanel className={cx('tabpanel')}>
				<ListPlaces
					title={['Lista de Bares']}
					places={places}
					showAll={true}
					onSelectPlace={onSelectPlace}
					showConfirmed={false}
				/>
			</TabPanel>

			<TabPanel className={cx('tabpanel')}>
				<ListGames
					title={['Jogos para os', <br />, <strong>próximos dias</strong>]}
					games={games}
					teams={teams}
					showAll={true}
				/>
			</TabPanel>
		</Tabs>
	);
};

PlacesTabs.propTypes = {
	places: PropTypes.array.isRequired,
	games: PropTypes.array.isRequired,
	teams: PropTypes.array.isRequired,
	onSelectPlace: PropTypes.func.isRequired
};

export default PlacesTabs;
