import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import sortByName from '../../utils/sortByName';
import PlaceItem from '../place/PlaceItem';
import styles from '../../css/components/list';

const cx = classNames.bind(styles);

import GamesListModal from 'components/modais/GamesListModal';

class ListPlaces extends Component {
	constructor(props) {
		super(props);
		this.state = {
			place: {},
		};
		this.onSelectPlace = this.onSelectPlace.bind(this);
		this.renderTitle = this.renderTitle.bind(this);
	}

	onSelectPlace(place) {
		this.setState({
			place: place,
		});
		if (document) document.querySelector('#gamesList').style.display = 'block';
	}

	renderTitle() {
		const { title } = this.props;

		if (title && title.length > 0) {
			const text = title.map((textOrHTML, index) => {
				return <span key={index}>{textOrHTML}</span>;
			});
			return (
				<header>
					<h2 className={cx('title-list')}>{text}</h2>
				</header>
			);
		}
	}

	render() {
		const { places, games, teams, showAll, onSelectPlace, keepOrder, showConfirmed } = this.props;

		const alphabeticalPlaces = places;
		if (!keepOrder) {
			alphabeticalPlaces.sort(sortByName('name'));
		}

		let listItems = [];
		if (alphabeticalPlaces && alphabeticalPlaces.length > 0) {
			listItems = alphabeticalPlaces.map((place, index) => {
				if (!showAll && index > 9) {
					return;
				} else {
					return(
						<li key={index}>
							<PlaceItem
								place={place}
								showConfirmed={showConfirmed}
								onSelectPlace={this.onSelectPlace}
							/>
						</li>
					);
				}
			}, this);
		} else {
			listItems.push(<li key="empty" className={cx('empty-message')}>Nenhum bar encontrado.</li> );
		}

		let button;
		if (!showAll && places.length >= 9) {
			button = <footer>
					<Link to="/bares" className={cx('btn-more')}>mais bares</Link>
				</footer>
		}


		const dateGames = games;
		dateGames.sort((a, b) =>
		    new Date(a.gameDate).getTime() - new Date(b.gameDate).getTime(),
		);

		let placeGames = [];
		dateGames.forEach((game, index) => {
		    if (this.state.place.broadcastGames) {
		        var willBroadcast = false;
		        var wasRecomended = false;
		        var recomendationCounter = 0;
		        // Game broadcast was confirmed by the place
		        this.state.place.broadcastGames.forEach((broadcast, i) => {
		            if (broadcast && broadcast !== null && game.id === broadcast.id) {
		                willBroadcast = true;
		                recomendationCounter = 1;
		                return;
		            }
		        });

		        // The Place is a oficial place
		        if (this.state.place.teamOficial && (this.state.place.teamOficial.id === game.homeTeam.id || this.state.place.teamOficial.id === game.visitingTeam.id) ) {
		            willBroadcast = true;
		            recomendationCounter = 1;
		        }

		        // Users said the the place usually broadcast games from the teams of the match
		        this.state.place.recomendations.forEach((recomend, i) => {
		            if (recomend.team === game.homeTeam.id || recomend.team === game.visitingTeam.id) {
		                if (recomend.counter > 0) {
		                    recomendationCounter = recomend.counter;
		                    wasRecomended = true;
		                }

		            }
		        });

		        if (willBroadcast) {
		            game.confirmed = true;
		            game.recomendations = recomendationCounter;
		            placeGames.push(game);
		        } else if (wasRecomended) {
		            game.confirmed = false;
		            game.recomendations = recomendationCounter;
		            placeGames.push(game);
		        }
		    }
		}, this);

		return (
			<div className={cx('list-container')}>
				{this.renderTitle()}

				<div className={cx('list-content')}>
					<ul className={cx('list')}>
						{listItems}
					</ul>
				</div>

				{button}

				{(this.state.place) &&
				<GamesListModal
					id={'gamesList'}
					games={placeGames}
					place={this.state.place}
					teams={teams}
				/>
				}
			</div>
		);
	}
};

ListPlaces.propTypes = {
	places: PropTypes.array.isRequired,
	games: PropTypes.array.isRequired,
	teams: PropTypes.array.isRequired,
	title: PropTypes.array,
	showAll: PropTypes.bool,
	keepOrder: PropTypes.bool,
	showConfirmed: PropTypes.bool
};

export default ListPlaces;
