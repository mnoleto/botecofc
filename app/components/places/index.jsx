import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cookie from 'react-cookie';
import { browserHistory } from 'react-router';
import classNames from 'classnames/bind';
import { fetchCities } from '../../actions/cities'
import { fetchStates } from '../../actions/states'
import { fetchChampionships } from '../../actions/championships';
import { fetchGames } from '../../actions/games';
import { fetchPlaces } from '../../actions/places';
import { fetchPlacesSuggestions } from '../../actions/placesSuggestions';
import { fetchTeams } from '../../actions/teams';
import ListPlaces from '../places/ListPlaces';
import SuggestPlaceModal from '../modais/SuggestPlaceModal';
import LoginModal from '../modais/LoginModal';
import styles from '../../css/components/places';

const cx = classNames.bind(styles);

class Places extends Component {
    static need = [
        fetchChampionships,
        fetchGames,
        fetchPlaces,
        fetchPlacesSuggestions,
        fetchTeams,
        fetchCities,
        fetchStates
    ];

    constructor(props) {
        super(props);
        this.state = {
            place: {},
            city: '',
        }
        this.createPlace = this.createPlace.bind(this);
        this.getPlaceById = this.getPlaceById.bind(this);
        this.suggestPlace = this.suggestPlace.bind(this);
    }

    componentDidMount() {
        if (!this.props.location.query || !this.props.location.query.openModal) return false;
        cookie.remove('returnTo', { path: '/' });
        if (this.props.location.query.openModal == 'sugerirBar') {
            document.querySelector('#suggestPlace').style.display = 'block';
        } else if (this.props.location.query.openModal == 'cadastrarBar') {
            browserHistory.push('cadastrar-bar');
        }
    }

    createPlace() {
        const { authenticated } = this.props.user;
        if (authenticated) {
            browserHistory.push('cadastrar-bar');
        } else {
            cookie.save('returnTo', window.location.pathname + '?openModal=cadastrarBar', { path: '/', sameSite: 'Strict' });
            document.querySelector('#loginmodal').style.display = 'block';
        }
    }

    getPlaceById(id) {
        const { places } = this.props;
        return places.find(place => place.id === id);
    }

    suggestPlace() {
        const { authenticated } = this.props.user;
        if (authenticated) {
            document.querySelector('#suggestPlace').style.display = 'block';
        } else {    
            cookie.save('returnTo', window.location.pathname + '?openModal=sugerirBar', { path: '/', sameSite: 'Strict' });
            document.querySelector('#loginmodal').style.display = 'block';
        }
    }

    render() {
        const { cities, games, teams, user, createPlaceSuggestion, dismissMessage, states } = this.props;
        const { places } = this.props;
        const { message } = this.props.suggestions;

        return (
            <div className={cx('places')}>
                <div className={cx('group-buttons', 'clearfix')}>
                    <button type="button" className={cx('top-button')} onClick={this.suggestPlace}><strong>Quero sugerir</strong><br />um bar</button>
                    <button type="button" className={cx('top-button')} onClick={this.createPlace}>Quero cadastrar<br /><strong>meu estabelecimento</strong></button>
                </div>

                <div className={cx('place-container')}>
                    <div className={cx('filters')}></div>
                    <div className={cx('place-list')}>
                        <ListPlaces
                            title={['Bares']}
                            places={places}
                            games={games}
                            teams={teams}
                            showAll={true}
                            showConfirmed={false}
                        />
                    </div>
                </div>

                {user.authenticated &&
                    <SuggestPlaceModal
                        id={'suggestPlace'}
                        onEntrySaveSuggestion={createPlaceSuggestion}
                        user={user}
                        message={message}
                        dismissMessage={dismissMessage}
                        cities={cities}
                        states={states}
                    />
                }

                {!user.authenticated &&
                    <LoginModal
                        id={'loginmodal'}
                    />
                }
            </div>
        );
    };
};

Places.propTypes = {
    user: PropTypes.object.isRequired,
    championships: PropTypes.array.isRequired,
    games: PropTypes.array.isRequired,
    places: PropTypes.array.isRequired,
    suggestions: PropTypes.object.isRequired,
    teams: PropTypes.array.isRequired,
    createPlaceSuggestion: PropTypes.func.isRequired,
    dismissMessage: PropTypes.func.isRequired,
    setVisibilityFilter: PropTypes.func.isRequired
};

export default Places;
