import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import Slider from 'react-slick';
import classNames from 'classnames/bind';
import FilterCity from '../filters/FilterCity';
import PlaceCarouselItem from '../place/PlaceCarouselItem';
import styles from '../../css/components/home';

const cx = classNames.bind(styles);

class PlacesCarousel extends Component {
	constructor(props) {
		super(props);
		this.carousel = null;
	}

	render () {
		const { allplaces, game, places, updateCityFilter, gameVisibilityFilter } = this.props;

		if (typeof window === 'undefined') return false;

		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		const ismobile = width && width < 962;
		
		const placesToRender = ismobile ? places.slice(0, 9) : places.slice(0, 18);
		const placeSlide = placesToRender.map((place, index) => {
			const confirmedClass = place.confirmed ? 'place-confirmed' : '';
			return (
				<div key={index} className={cx('place-item')}>
					<PlaceCarouselItem
						image={(place.logo) ? place.logo.responseText : ''}
						name={place.name}
						slug={(place.slug) ? place.slug : ''}
						confirmed={confirmedClass}
					/>
				</div>
			);
		});
		
		if (places || places.length > 1) {
			this.settings = {
				arrows: false,
				dots: true,
				dotsClass: cx('dots', 'dotslist'),
				infinite: true,
				speed: 500,
				slidesToShow: (places.length >= 6) ? 6 : places.length,
				slidesToScroll: (places.length >= 6) ? 6 : places.length,
				responsive: [
					{
						breakpoint: 480,
						settings: {
							slidesToShow: (places.length >= 3) ? 3 : places.length,
							slidesToScroll: (places.length >= 3) ? 3 : places.length,
							dots: true,
						}
					},
					{
						breakpoint: 600,
						settings: {
							slidesToShow: (places.length >= 3) ? 3 : places.length,
							slidesToScroll: (places.length >= 3) ? 3 : places.length,
							dots: true,
						}
					},
					{
						breakpoint: 992,
						settings: {
							slidesToShow: (places.length >= 5) ? 5 : places.length,
							slidesToScroll: (places.length >= 5) ? 5 : places.length,
							dots: true,
						}
					},
				],
				reinit: () => {
					if( document) {
						const el = document.querySelector('.' + cx('places-carousel'));
						if (!el || !el.querySelector('li.slick-active')) return;
						setTimeout(() => {
							el.querySelector('li.slick-active').classList.add(cx('active'));
						});
					}
				},
				beforeChange: () => {
					let el = document.querySelector('.' + cx('places-carousel'));
					if (!el || !el.querySelector('li') || !el.querySelector('li.slick-active')) return;
					setTimeout(() => {
						el.querySelector('li').classList.remove(cx('active'));
						el.querySelector('li.slick-active').classList.add(cx('active'));
					});
				},
			};
		}
		return (
			<div className={cx('places')}>
				<header>
					<h2>Bares que poderão transmitir esse jogo</h2>
					<div className={cx('filter-nav')}>
						{(places && places.length > 0 && (places.length >= 18) ) && <Link to={'/jogo/' + game} className={cx('see-all-button-desktop')}>ver todos</Link>}
					</div>
				</header>

				{(places && places.length > 0 && (places.length >= 9) ) &&
					<Link to={'/jogo/' + game} className={cx('see-all-button-mobile')}>ver todos</Link>
				}
				<div className={cx('places-carousel')} ref={(node) => this.carousel = node}>
					{(places && places.length > 0 && this.settings) &&
					<Slider {...this.settings} className={cx('carousel', 'places-carousel-slick')}>
						{placeSlide}
					</Slider>
					}
					{(places && places.length == 0) &&
						<div className={cx('empty-place')}>Nenhum bar da cidade selecionada confirmou a transmissão desse jogo.</div>
					}
				</div>
			</div>
			
		);
	}
};

PlacesCarousel.propTypes = {
	updateCityFilter: PropTypes.func.isRequired,
	gameVisibilityFilter: PropTypes.object.isRequired,
	game: PropTypes.string,
	places: PropTypes.array,
	allplaces: PropTypes.array,
};

export default PlacesCarousel;
