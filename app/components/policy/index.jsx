import React from 'react';
import classNames from 'classnames/bind';
import PolicyContent from '../policy/PolicyContent';
import registerImage from '../../images/bg/register@2x.jpg';
import styles from '../../css/components/loginOrRegister';

const cx = classNames.bind(styles);

const Policy = () => {
    return (
        <div className={cx('login')}>
            <figure className={cx('header-image')}>
                <img src={registerImage} alt="" />
            </figure>

            <div className={cx('container')}>
                <h1 className={cx('heading')}>Politica de privacidade para <a href='http://botecofc.com'>Qual Bar</a></h1>
                <PolicyContent />
            </div>
        </div>
    );
}

export default Policy;
