import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from '../css/admin/form';

const cx = classNames.bind(styles);

class Checkbox extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isChecked: false,
        };

        this.onHandleChange = this.onHandleChange.bind(this);
    }

    UNSAFE_componentWillUpdate(nextProps, prevState) {
        if (nextProps.isChecked !== this.props.isChecked) {
            this.setState({
                isChecked: nextProps.isChecked,
            });
        }
    }

    onHandleChange() {
        const { onChange, id } = this.props;
        this.setState({ isChecked: !this.state.isChecked });
        onChange(id);
    }

    render() {
        const { value, name, id } = this.props;
        return (
            <div className={cx('check-row')}>
                <input
                    type="checkbox"
                    name={name}
                    value={id}
                    checked={this.state.isChecked}
                    onChange={this.onHandleChange}
                    className={cx('check-inline')}
                />
                {value && (<label>{value}</label>)}
            </div>
        );
    }
}

Checkbox.propTypes = {
    isChecked: PropTypes.bool,
    value: PropTypes.string,
    id: PropTypes.string.isRequired,
    name: PropTypes.string,
    onChange: PropTypes.func.isRequired
};

export default Checkbox;
