import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from '../css/admin/form';

const cx = classNames.bind(styles);

class CheckboxWithLabel extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isChecked: false,
		};

		this.onHandleChange = this.onHandleChange.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		const { value } = this.props;
		if (nextProps.data && nextProps.data.length > 0) {
			const query = value.toLowerCase();
			let index = -1;
			const checked = nextProps.data.some((element, i) => {
				if (query === element.toLowerCase()) {
					index = i;
					return true;
				}
			});
			if (checked) {
				this.setState({ isChecked: true });
			}
		}
	}

	onHandleChange() {
		const { onChange, value } = this.props;
		this.setState({ isChecked: !this.state.isChecked });
		onChange(value);
	}

	render() {
		const { value, name } = this.props;
		return (
			<div className={cx('check-row')}>
				<input
					type="checkbox"
					name={name}
					value={value}
					checked={this.state.isChecked}
					onChange={this.onHandleChange}
					className={cx('check-inline')}
				/>
				<label>{value}</label>
			</div>
		);
	}
}

CheckboxWithLabel.propTypes = {
	data: PropTypes.array,
	value: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired
};

export default CheckboxWithLabel;
