import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import Icon from '../Icon';
import styles from '../../css/components/filter';

const cx = classNames.bind(styles);

class CountryListItem extends Component {
	constructor(props) {
		super(props);
		this.onHandleClick = this.onHandleClick.bind(this);
	}

	onHandleClick(e) {
		const { id, name, onClickCountry } = this.props;
		e.preventDefault();
		onClickCountry(id, name);
	}

	render() {
		const { name } = this.props;

		return (
			<li><a onClick={this.onHandleClick}>{name}</a></li>
		);
	}
}

CountryListItem.propTypes = {
	id: PropTypes.string,
	name: PropTypes.string,
	onClickCountry: PropTypes.func
};

class FilterCountry extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
			id: '',
			name: ''
		}

		this.changeActiveCountry = this.changeActiveCountry.bind(this);
		this.selectCountry = this.selectCountry.bind(this);
		this.toggleFilter = this.toggleFilter.bind(this);
	}

	componentDidUpdate(prevProps, prevState) {
		if(prevProps.countries !== this.props.countries) {
			if(this.props.countries.length > 0) {
				let index;
				this.props.countries.some((country, i) => {
					if(country.slug === 'brasil') {
						index = i;
						return;
					}
				});
				if(this.props.countries[index].id !== this.state.id) {
					this.changeActiveCountry(this.props.countries[index].id, this.props.countries[index].name);
				}
			}
		}
	}

	changeActiveCountry(id, name) {
		const { onFilter } = this.props;

		$('li', '#contryFilter').removeClass(cx('active'));
		$(' li:eq(' + id + ')', '#contryFilter').addClass(cx('active'));

		this.setState({
			id,
			name
		});

		onFilter({id, name});
	}

	selectCountry(id, name) {
		this.changeActiveCountry(id, name);
		this.toggleFilter();
	}

	toggleFilter(event) {
		$('#contryFilter').toggleClass(cx('open'));
	}

	render() {
		const { classnames, countries } = this.props;
		const { selectCountry } = this;
		
		const filterItem = countries.map((country, index) => {
			return (
				<CountryListItem 
					key={index}
					id={country.id}
					name={country.name}
					onClickCountry={selectCountry} />
			);
		});
		return (
			<div id="contryFilter" className={cx('filter', 'country-filter', 'clearfix')}>
				<button type="button" onClick={this.toggleFilter}>
					<Icon size={'lg'} name={'place'}></Icon>
					<span className={cx('active')}>{this.state.name}</span>
				</button>

				<div className={cx('filter-menu')}>
					<ul>
						{filterItem}
					</ul>
				</div>
			</div>
		);
	}
};

FilterCountry.propTypes = {
	countries: PropTypes.array.isRequired,
	classnames: PropTypes.string,
	onFilter: PropTypes.func.isRequired
};

export default FilterCountry;
