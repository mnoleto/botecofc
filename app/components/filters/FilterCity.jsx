import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import Icon from '../Icon';
import citiesArray from '../../utils/cities';
import styles from '../../css/components/filter';

const cx = classNames.bind(styles);

class CityListItem extends Component {
	constructor(props) {
		super(props);
		this.onHandleClick = this.onHandleClick.bind(this);
	}

	onHandleClick(e) {
		const { id, name, onClickCity } = this.props;
		e.preventDefault();
		onClickCity(id);
	}

	render() {
		const { name, counter, showCounter } = this.props;
		const content = (!showCounter) ? '' : (counter === 0 ? [' (', counter, ')'] : <strong>{' ('}{counter}{')'}</strong> );

		return (
			<li><a onClick={this.onHandleClick}>{name}{content}</a></li>
		);
	}
}

CityListItem.propTypes = {
	id: PropTypes.number,
	name: PropTypes.string,
	showCounter: PropTypes.bool,
	counter: PropTypes.number,
	onClickCity: PropTypes.func
};

class FilterCity extends Component {
	constructor(props) {
		super(props);

		const { city, showAll } = props;

		this.state = {
			city: (city) ? city : ''
		}

		if(showAll) {
			if(citiesArray.indexOf('TODO DF') === -1) citiesArray.unshift('TODO DF');
		} else {
			const index = citiesArray.indexOf('TODOS');
			if(index !== -1) citiesArray.splice(index, 1);
		}

		this.changeActiveCity = this.changeActiveCity.bind(this);
		this.getCityCouter = this.getCityCouter.bind(this);
		this.selectCity = this.selectCity.bind(this);
		this.toggleFilter = this.toggleFilter.bind(this);
	}

	componentDidMount() {
		let index = 0;
		const _ = this;

		if(this.state.city !== '') {
			index = citiesArray.findIndex(value => {
				return (value === _.state.city)
			});
		}

		this.changeActiveCity(index);
	}

	changeActiveCity(id) {
		const { onFilter } = this.props;

		$('li', '#filterCity').removeClass(cx('active'));
		$(' li:eq(' + id + ')', '#filterCity').addClass(cx('active'));

		this.setState({city: citiesArray[id]});

		// update container state
		onFilter(citiesArray[id]);
	}

	getCityCouter(city) {
		const { places } = this.props;
		let counter = 0;
		if(city === 'TODO DF') {
			counter = places.length
		} else {
			places.forEach((value) => {
				if(value.city === city) {
					counter ++;
				}
			});
		}
		return counter;
	}

	selectCity(id) {
		this.changeActiveCity(id);
		this.toggleFilter();
	}

	toggleFilter() {
		$('#cityFilter').toggleClass(cx('open'));
	}

	render() {
		const { classname, showCounter, places } = this.props;
		const { selectCity } = this;
		
		const filterItem = citiesArray.map((city, index) => {
			
			return (
				<CityListItem 
					key={index}
					id={index}
					name={city}
					counter={this.getCityCouter(city)}
					showCounter={showCounter}
					onClickCity={selectCity} />
			);
		}, this);
		
		return (
			<div id="cityFilter" className={cx('filter', 'city-filter', classname)}>
				<button type="button" onClick={this.toggleFilter}>
					<Icon size={'lg'} name={'place'}></Icon>
					<span className={cx('active')}>{this.state.city} <strong>({this.getCityCouter(this.state.city)})</strong></span>
				</button>

				<div className={cx('filter-menu')}>
					<ul>
						{filterItem}
					</ul>
				</div>
			</div>
		);
	}
};

FilterCity.propTypes = {
	city: PropTypes.string.isRequired,
	places: PropTypes.array,
	classname: PropTypes.string,
	onFilter: PropTypes.func,
	showAll: PropTypes.bool.isRequired,
	showCounter: PropTypes.bool
};

export default FilterCity;
