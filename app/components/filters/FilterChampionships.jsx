import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import Icon from '../Icon';
import sortByName from '../../utils/sortByName';
import styles from '../../css/components/filter';

const cx = classNames.bind(styles);

class ChampionshipListItem extends Component {
	constructor(props) {
		super(props);
		this.onHandleClick = this.onHandleClick.bind(this);
	}

	onHandleClick(e) {
		const { id, name, onClickChampionship } = this.props;
		e.preventDefault();
		onClickChampionship(id, name);
	}

	render() {
		const { name } = this.props;

		return (
			<li><a onClick={this.onHandleClick}>{name}</a></li>
		);
	}
}

ChampionshipListItem.propTypes = {
	id: PropTypes.string,
	name: PropTypes.string,
	onClickChampionship: PropTypes.func
};

class FilterChampionships extends Component {
	constructor(props) {
		super(props);

		const { championship } = props;

		this.state = {
			id: (championship) ? championship : ''
		}

		this.getChampionshipName = this.getChampionshipName.bind(this);
		this.changeActiveChampionship = this.changeActiveChampionship.bind(this);
		this.selectChampionship = this.selectChampionship.bind(this);
		this.selectAll = this.selectAll.bind(this);
		this.toggleFilter = this.toggleFilter.bind(this);
	}

	componentDidUpdate(prevProps, prevState) {
		const { championship } = this.props;
		if(!championship && !prevProps.championship) {
			this.changeActiveChampionship('');
		} else if(prevProps.championship !== championship) {
			this.changeActiveChampionship(this.state.id);
		}
	}

	changeActiveChampionship(id) {
		const { onFilter, championships } = this.props;
		if(id === '') id = 'TODOS';

		this.setState({
			id
		});

		onFilter(id);
	}

	getChampionshipName(id) {
		const { championships } = this.props;
		if(!championships || championships.length < 1 || id === '') return false;
		if(id === 'TODOS') return 'TODOS OS CAMPEONATOS';

		let championship = championships.find(value => {
			return (value.id === id);
		});

		return (championship) ? championship.name : '';
	}

	selectChampionship(id, name) {
		this.changeActiveChampionship(id);
		this.toggleFilter();
	}

	selectAll() {
		this.selectChampionship('');
	}

	toggleFilter(event) {
		$('#championshipFilter').toggleClass(cx('open'));
	}

	render() {
		const { classnames, championships } = this.props;
		const { selectChampionship } = this;

		const alphabeticalChampionships = championships;
		alphabeticalChampionships.sort(sortByName('name'));

		const filterItem = alphabeticalChampionships.map((championship, index) => {
			return (
				<ChampionshipListItem
					key={index}
					id={championship.id}
					name={championship.name}
					onClickChampionship={selectChampionship} />
			);
		});

		return (
			<div id="championshipFilter" className={cx('filter', 'championship-filter', classnames)}>
				<button type="button" onClick={this.toggleFilter}>
					<Icon size={'lg'} name={'championship'}></Icon>
					<span className={cx('active')}>{this.getChampionshipName(this.state.id)}</span>
				</button>

				<div className={cx('filter-menu')}>
					<ul>
						<li><a onClick={this.selectAll}>TODOS</a></li>
						{filterItem}
					</ul>
				</div>
			</div>
		);
	}

}

FilterChampionships.propTypes = {
	championship: PropTypes.string.isRequired,
	championships: PropTypes.array.isRequired,
	classnames: PropTypes.string,
	onFilter: PropTypes.func.isRequired
};

export default FilterChampionships;
