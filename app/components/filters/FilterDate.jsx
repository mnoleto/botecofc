import React, { Component } from 'react';
import PropTypes from 'prop-types';
import 'moment/locale/pt-br';
import DatePicker from 'react-datepicker';
import classNames from 'classnames/bind';
import styles from '../../css/components/filter';

const cx = classNames.bind(styles);

class CustomInput  extends Component {
	render() {
		const {onClick, value} = this.props
		return(
			<button type="button" onClick={onClick}>
				<span className={cx('active')}>{value}</span>
			</button>
		);
	}
}

class FilterDate extends Component {
	constructor(props) {
		super(props);

		const date  = (props.date) ? new Date(props.date) : new Date();
		this.state = {
			startDate: new Date(date)
		}
		this.handleChange = this.handleChange.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.startDate !== this.props.startDate) {
			this.setState({
				startDate: new Date(nextProps.startDate),
			});
		}
	}

	componentDidMount() {
		const { onFilter } = this.props;
		onFilter(this.state.startDate);
	}

	handleChange(date) {
		const { onFilter } = this.props;
		this.setState({
			startDate: date
		});
		onFilter(date);
	}

	render() {
		const { date, showTimeSelect, dateFormat } = this.props;

		return (
			<div className={cx('filter-date')}>
				<DatePicker
					customInput={<CustomInput />}
					dateFormat={dateFormat}
					locale='pt-br'
					showTimeSelect={showTimeSelect}
					timeIntervals={15}
					timeFormat="HH:mm"
					onChange={this.handleChange}
					selected={new Date(date)}
					withPortal
				/>
			</div>
		);
	}
  
}

FilterDate.propTypes = {
	date: PropTypes.object.isRequired,
	dateFormat: PropTypes.string,
	showTimeSelect: PropTypes.bool,
	onFilter: PropTypes.func.isRequired
};

export default FilterDate;
