import React from 'react';
import classNames from 'classnames/bind';
import { Link } from 'react-router';
import styles from '../../../css/components/owner';

const cx = classNames.bind(styles);

const Blocked = ({ hasRequestedPosts, handleRequestPost }) => {
  if (hasRequestedPosts) {
    return (
      <div className={cx('blocked')}>
        <div className={cx('wrapper')}>
          <h2 className={cx('blocked-title')}>Obrigado!</h2>
          <p>Faremos contato em breve para liberar esta funcionalidade.</p>
          <p><strong>Mantenha o número do seu celular atualizado.</strong></p>
          <div className={cx('actions')}>
            <Link to="/gerenciar-jogos/confirmados" className={cx('btn-back')}>Voltar</Link>
            <Link to="/cadastro" className={cx('btn-new')}>Editar Dados</Link>
          </div>
        </div>
      </div>
    );
  }
  return (
    <div className={cx('blocked')}>
      <div className={cx('wrapper')}>
        <h2 className={cx('blocked-title')}>OOPS!</h2>
        <p>Você não está com acesso a criar Posts Dinâmicos para seu estabelecimento.</p>
        <p><strong>Solicite liberação para o nosso administrador.</strong></p>
        <div className={cx('actions')}>
          <Link to="/gerenciar-jogos/confirmados" className={cx('btn-back')}>Voltar</Link>
          <button type="button" className={cx('btn-post')} onClick={handleRequestPost}>Solicitar</button>
        </div>
      </div>
    </div>
  );
}

export default Blocked;