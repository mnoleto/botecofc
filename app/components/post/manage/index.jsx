import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classNames from 'classnames/bind';
import { fetchPlaces } from '../../../actions/places';
import { fetchChampionships } from '../../../actions/championships';
import { fetchGames } from '../../../actions/games';
import { fetchTeams } from '../../../actions/teams';
import { fetchChannels } from '../../../actions/channels';
import { fetchFeatureToggles } from '../../../actions/featureToggle';
import { fetchPostsBgs } from '../../../actions/postsBg';
import { fetchPostsFigures } from '../../../actions/postsFigures';
import ManageHeader from '../../header/ManageHeader';
import Blocked from './Blocked';
import Editor from './Editor';
import Posts from './Posts';
import BotecoFcLogo from '../../../images/post/botecofc.png';
import styles from '../../../css/components/post';
import LoadingImage from '../../../images/loading.gif';
import { titles } from './images';

const cx = classNames.bind(styles);

class BroadcastPost extends Component {
  static need = [
    fetchChampionships,
    fetchFeatureToggles,
    fetchPostsBgs,
    fetchPostsFigures,
    fetchGames,
    fetchPlaces,
    fetchTeams,
    fetchChannels,
  ];

  constructor(props) {
    super(props);

    const { ownerManageFilter, places } = this.props;

    const firstPlace = places && places.length > 0 ? places[0] : {};
    const selectedPlace = ownerManageFilter.place && ownerManageFilter.place.id ? ownerManageFilter.place : null;

    const activePlace = selectedPlace ? selectedPlace : firstPlace;

    this.state = {
      figure: null,
      place: this.getPlaceById(activePlace.id),
      selectedGames: [],
      selectedImage: {},
      droppedImage: {},
      uploadedImage: {},
      typeOfImage: 'default',
      postText: 'transmissao',
      isRequesting: false,
      isUploading: false,
      uploadError: false,
      screenState: 'selecting', // selecting, editing, processing or success
    };

    this.handleAddGame = this.handleAddGame.bind(this);
    this.handleRemoveGame = this.handleRemoveGame.bind(this);
    this.handlePlaceChange = this.handlePlaceChange.bind(this);
    this.handleRequestPost = this.handleRequestPost.bind(this);
    this.handleUpdateDroppedImage = this.handleUpdateDroppedImage.bind(this);
    this.handleUpdateSelectedImage = this.handleUpdateSelectedImage.bind(this);
    this.handleUpdateText = this.handleUpdateText.bind(this);
    this.handleSavePost = this.handleSavePost.bind(this);
    this.handleFigureChange = this.handleFigureChange.bind(this);
    this.hasRequested = this.hasRequested.bind(this);
    this.setScreenState = this.setScreenState.bind(this);
    this.setVisibilityFilter = this.setVisibilityFilter.bind(this);
    this.clearText = this.clearText.bind(this);
    this.getTeamById = this.getTeamById.bind(this);
    this.getPlaceById = this.getPlaceById.bind(this);
    this.getChampionshipById = this.getChampionshipById.bind(this);
  }

  componentDidMount() {
    document.querySelector('.register-ad').style.display = 'none';
    document.querySelector('.site-footer').style.paddingTop = 0;
  }

  handleFigureChange(index) {
    const figure = index == null ? null : index;
    this.setState({
      figure,
    })
  }

  getChampionshipById(championshipId) {
    const { championships } = this.props;
    return championships.find(championship => championship.id === championshipId);
  }

  getPlaceById(placeId) {
    const { places } = this.props;
    return places.find(place => place.id === placeId);
  }

  getTeamById(teamId) {
    const { teams } = this.props;
    return teams.find(team => team.id === teamId);
  }

  hasRequested(placeId) {
    const { featureToggles } = this.props;
    return featureToggles.some(feature => feature.place_id === placeId);
  }

  handlePlaceChange(event) {
    const { places } = this.props;
    const place = places.find((place) => place.id == event.target.value);

    this.setState({
      place,
      isRequesting: false,
    });

    this.setVisibilityFilter(place);
  }

  handleAddGame(game) {
    if (this.state.selectedGames.length === 3) return false;
    this.setState(prevState => ({
      selectedGames: [...prevState.selectedGames, game],
    }));
  }

  handleRemoveGame(game) {
    this.setState({
      selectedGames: this.state.selectedGames.filter(selected => selected.id !== game.id),
    });
  }

  handleRequestPost() {
    const { createFeatureToggle } = this.props;
    if (!_.has(this.state.place, 'id') || this.state.isRequesting) return false;

    this.setState({
      isRequesting: true,
    });
    
    createFeatureToggle(this.state.place.id)
      .then(() => {
        this.setState({
          isRequesting: false,
        });
      });
  }

  setVisibilityFilter(place) {
    const { ownerManageFilter, setVisibilityFilter } = this.props;
    setVisibilityFilter('SHOW_ONLY', ownerManageFilter.championship, ownerManageFilter.initialDate, ownerManageFilter.endDate, place);
  }

  setScreenState(screenState) {
    window && window.scrollTo(0, 0)
    this.setState({
      screenState,
    })
  }

  handleUpdateDroppedImage(droppedImage) {
    const size = droppedImage.size / 1024 / 1024;
    if (size > 1) {
      this.setState({
        uploadError: true,
      });
    } else {
      const { uploadFile } = this.props;
      this.setState({
        droppedImage,
        typeOfImage: 'custom',
        selectedImage: {},
        isUploading: true,
        uploadError: false,
      });

      const fd = new FormData();
      fd.append('file', droppedImage);
      const _this = this;
      uploadFile(fd, '/uploadPost', (feedback) => {
        if (feedback.status === 200) {
          const data = JSON.parse(feedback.response)
          _this.setState({
            uploadedImage: data.responseText,
          });
        }
        _this.setState({
          isUploading: false,
        });
      });
    }
  }

  handleUpdateSelectedImage(selectedImage) {
    this.setState({
      droppedImage: {},
      typeOfImage: 'default',
      selectedImage,
    });
  }

  handleUpdateText(event) {
    this.setState({
      postText: event.target.value,
    });
  }

  clearText() {
    this.setState({
      postText: 'transmissao',
    });
  }

  handleSavePost() {
    const { createPost, incrementPosts, figures } = this.props;
    const {
      typeOfImage,
      selectedImage,
      uploadedImage,
      postText,
      place,
      selectedGames,
    } = this.state;

    const normalizeNumber = (num) => {
      return num > 9 ? num.toString() : '0' + num;
    }

    const normalizedGames = selectedGames.map(game => {
      const home = this.getTeamById(game.homeTeam.id);
      const visiting = this.getTeamById(game.visitingTeam.id);
      const championship = this.getChampionshipById(game.championship.id);
      const gameDate = new Date(game.gameDate);
      const day = normalizeNumber(gameDate.getDate());
      const month = normalizeNumber(gameDate.getMonth() + 1);
      const hours = normalizeNumber(gameDate.getHours());
      const minutes = normalizeNumber(gameDate.getMinutes());
      
      return Object.assign({}, game, {
        day: `${day}-${month}`,
        hour: `${hours}H${minutes}`,
        homeTeam: Object.assign({}, game.homeTeam, {
          logo: home.logo.responseText,
        }),
        visitingTeam: Object.assign({}, game.visitingTeam, {
          logo: visiting.logo.responseText,
        }),
        championship: Object.assign({}, game.championship, {
          slug: championship.slug,
        }),
      });
    });

    this.setScreenState('processing');

    let text;
    switch (postText) {
      case 'transmissao-audio':
        text = titles[0];
        break;
      case 'transmissao-telao':
        text = titles[1];
        break;
      case 'transmissao':
      default:
        text = titles[2];
        break;
    }

    const versus = '/app/images/post/versus.png';
    const { figure } = this.state;

    createPost({
      background: typeOfImage === 'custom' ? uploadedImage : selectedImage.large,
      rect: selectedImage.medium,
      portrait: selectedImage.large,
      figure: figure == null ? null : figures[figure],
      typeOfImage,
      text,
      logo: place.logo.responseText,
      logoBoteco: BotecoFcLogo,
      games: normalizedGames,
      versus
    }).then((res) => {
      if (res.error) {
        return this.setScreenState('error');
      }
      incrementPosts(place.id);
      this.setScreenState('finishing');
    }, this);
  }

  renderPosts() {
    const { selectedGames, place } = this.state;
    const { allGames, bgs, teams, channels, figures, post } = this.props;
    
    if (this.state.screenState === 'selecting') {
      return (
        <Posts
          allGames={allGames}
          teams={teams}
          channels={channels}
          place={place}
          selectedGames={selectedGames}
          handleAddGame={this.handleAddGame}
          handleRemoveGame={this.handleRemoveGame}
          handleCreatePost={this.setScreenState}
        />
      );
    } else if (this.state.screenState === 'editing') {
      return (
        <Editor
          bgs={bgs}
          figures={figures}
          droppedImage={this.state.droppedImage}
          figure={this.state.figure}
          place={place}
          postText={this.state.postText}
          selectedGames={selectedGames}
          selectedImage={this.state.selectedImage}
          teams={teams}
          typeOfImage={this.state.typeOfImage}
          handleBack={this.setScreenState}
          updateFigure={this.handleFigureChange}
          updateDroppedImage={this.handleUpdateDroppedImage}
          updateSelectedImage={this.handleUpdateSelectedImage}
          updateText={this.handleUpdateText}
          savePost={this.handleSavePost}
          clearText={this.clearText}
          isUploading={this.state.isUploading}
          uploadError={this.state.uploadError}
        />
      );
    } else if (this.state.screenState === 'processing') {
      return (
        <div className={cx('post-success')}>
          <div className={cx('post-loading')}><img src={LoadingImage} alt="" /></div>
          <h2>Seu post está sendo criado!</h2>
          <p>Os arquivos estarão disponíveis em alguns segundos.</p>
        </div>
      );
    } else if (this.state.screenState === 'error') {
      return (
        <div className={cx('post-success')}>
          <h2>Erro!</h2>
          <p>Aconteceu algo inesperado e não criamos o seu post, tente novamente mais tarde ou contacte <a href="mailto:contato@botecofc.com.br">nossos administradores</a>.</p>
          <div className={cx('actions')}>
            <button type="button" className={cx('btn-secondary')} onClick={() => this.setScreenState('selecting')}>Voltar</button>
          </div>
        </div>
      );
    }
    return (
      <div className={cx('post-success')}>
        <h2>Seu post foi criado com sucesso!</h2>
        <div className={cx('post-image')}>
          <a href={post.retangulo} download="feed"><img src={post.retangulo} alt="" width="100" /></a>
          <p><a href={post.retangulo} download="feed">Clique aqui</a> para baixar a imagem do FEED.</p>
        </div>
        <div className={cx('post-image')}>
          <a href={post.horizontal} download="stories"><img src={post.horizontal} alt="" width="100" /></a>
          <p><a href={post.horizontal} download="stories">Clique aqui</a> para baixar a imagem do STORIES.</p>
        </div>
        <div className={cx('actions')}>
          <button type="button" className={cx('btn-secondary')} onClick={() => this.setScreenState('editing')}>Voltar</button>
          <button type="button" className={cx('btn-primary')} onClick={() => this.setScreenState('selecting')}>Criar outros</button>
        </div>
      </div>
    );
  }

  render() {
    const { places } = this.props;
    const { canGeneratePosts = false } = this.state.place ? this.state.place : {};
    const hasRequestedPosts = this.hasRequested(this.state.place.id);
    return (
      <div className={cx('owner-manage-games')}>
        <ManageHeader
          places={places}
          place={this.state.place}
          changePlace={this.handlePlaceChange}
          isDisabled={this.state.screenState !== 'selecting'}
        />
        {canGeneratePosts
          ? this.renderPosts()
          : <Blocked hasRequestedPosts={hasRequestedPosts} handleRequestPost={this.handleRequestPost} />
        }
      </div>
    );
  }
}

BroadcastPost.propTypes = {
  user: PropTypes.object.isRequired,
  allGames: PropTypes.array.isRequired,
  bgs: PropTypes.array.isRequired,
  figures: PropTypes.array.isRequired,
  places: PropTypes.array.isRequired,
  teams: PropTypes.array.isRequired,
  channels: PropTypes.array.isRequired,
  post: PropTypes.object,
  ownerManageFilter: PropTypes.object.isRequired,
  featureToggles: PropTypes.array.isRequired,
  createFeatureToggle: PropTypes.func.isRequired,
  createPost: PropTypes.func.isRequired,
  incrementPosts: PropTypes.func.isRequired,
  uploadFile: PropTypes.func.isRequired,
  championships: PropTypes.array.isRequired,
};

export default BroadcastPost;