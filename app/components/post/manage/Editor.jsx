import React, { Component } from 'react';
import _ from 'lodash';
import classNames from 'classnames/bind';
import Dropzone from 'react-dropzone';
import Game from '../../game/Game';
import BotecoFcLogo from '../../../images/post/botecofc.png';
import styles from '../../../css/components/post';

const cx = classNames.bind(styles);

const ImageButton = ({ image, selectedImage, typeOfImage, handleSelectedImage }) => {
  const selectedClass = (image === selectedImage && typeOfImage === 'default') ? 'selected' : '';
  const selectImage = (event) => {
    event.preventDefault();
    event.stopPropagation();
    handleSelectedImage(image);
  };
  return (
    <button className={cx('image-button', selectedClass)} onClick={selectImage}>
      <img src={image.large} alt="" className={cx('image-thumb')} />
    </button>
  );
};

class Editor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showDropzone: false,
    }

    this.onClickBack = this.onClickBack.bind(this);
    this.handleDroppedImage = this.handleDroppedImage.bind(this);
    this.handleSelectedImage = this.handleSelectedImage.bind(this);
    this.getTeamById = this.getTeamById.bind(this);
    this.toggleDropzone = this.toggleDropzone.bind(this);
  }

  componentDidMount() {
    this.handleSelectedImage(this.props.bgs[0]);
  }

  toggleDropzone() {
    this.setState({
      showDropzone: !this.state.showDropzone,
    })
  }

  onClickBack() {
    const { handleBack } = this.props;
    handleBack('selecting');
  }

  getTeamById(teamId) {
    const { teams } = this.props;
    return teams.find(team => team.id === teamId);
  }

  handleDroppedImage(acceptedFile) {
    const { updateDroppedImage } = this.props;
    if (_.has(this.dropzone, 'fileInputEl')) {
      updateDroppedImage(this.dropzone.fileInputEl.files[0]);
    }
  }

  handleSelectedImage(image) {
    const { updateSelectedImage } = this.props;
    updateSelectedImage(image);
    return false;
  }

  render() {
    const {
      bgs,
      droppedImage,
      figure,
      figures,
      place,
      postText,
      selectedGames,
      selectedImage,
      typeOfImage,
      updateText,
      savePost,
      isUploading,
      uploadError,
      updateFigure,
    } = this.props;

    const { showDropzone } = this.state;

    let optionClass;
    switch (selectedGames.length) {
      case 1:
        optionClass = 'template-one';
        break;
      case 2:
        optionClass = 'template-two';
        break;
      case 3:
        optionClass = 'template-three';
        break;
      case 4:
        optionClass = 'template-four';
        break;
      default:
        break;
    }
    return (
      <div className={cx('post-edit', 'edit')}>
        <div className={cx('preview-post', optionClass)}>
          <div className={cx('selected-games')}>
            {selectedGames.map(({
              visitingTeam,
              homeTeam,
              id,
              championship,
              gameDate,
            }) => {
              const normalizeNumber = (num) => {
                return num > 9 ? num.toString() : '0' + num;
              }
              const gDate = new Date(gameDate);
              const day = normalizeNumber(gDate.getDate());
              const month = normalizeNumber(gDate.getMonth() + 1);
              const hours = normalizeNumber(gDate.getHours());
              const minutes = normalizeNumber(gDate.getMinutes());
              return (
                <div key={id} className={cx('game-item')}>
                  <Game
                    visitingTeam={this.getTeamById(visitingTeam.id)}
                    homeTeam={this.getTeamById(homeTeam.id)}
                    className={optionClass}
                  />
                  <div className={cx('game-championship')}>{championship.name}</div>
                  <div className={cx('game-date-time')}>
                    <div className={cx('game-date')}>{day}/{month}</div>
                    <div className={cx('game-time')}>{hours}H{minutes}</div>
                  </div>
                </div>
              );
            })}
          </div>
          {postText !== '' && <div className={cx('post-text', `post-text-${postText}`)} />}
          <div className={cx('boteco-logo')}><img src={BotecoFcLogo} alt="Boteco FC" /></div>
          <div className={cx('place-logo')}><img src={place.logo.responseText} alt={place.name} /></div>
          {figure != null && <div className={cx('figure')}><img src={figures[figure]} alt="" /></div>}
          {(_.has(selectedImage, 'large') || _.has(droppedImage, 'preview')) &&
          <div style={{
            backgroundImage: "url(" + (selectedImage.large || droppedImage.preview) + ")",
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
          }} className={cx('bg')} alt="" />
          }
        </div>

        <div className={cx('custom-post')}>
          <ol>
            <li className={cx('post-step')}>
              <h2><span className={cx('bullet')}>1</span> Selecione ou insira uma imagem de fundo:</h2>
              <div className={cx('images-container')}>
                {/* <h3 className={cx('tab-title')}>Selecione uma imagem</h3> */}
                {bgs.map((image, index) => (
                  <ImageButton
                    key={`image-${index}`}
                    image={image}
                    selectedImage={selectedImage}
                    typeOfImage={typeOfImage}
                    handleSelectedImage={this.handleSelectedImage}
                  />
                ))}

                {showDropzone &&
                  <Dropzone
                    accept="image/*"
                    name="post-image"
                    className={cx('dropzone')}
                    multiple={false}
                    ref={(node) => { this.dropzone = node; }}
                    onDrop={this.handleDroppedImage}
                  >
                    {(typeOfImage === 'custom' && _.has(droppedImage, 'preview')) ? (
                      <div
                        className={cx('dropzone-preview')}
                      >
                        {isUploading && <div className={cx('upload-wait')}>Aguarde</div>}
                        {uploadError && <div className={cx('upload-error')}>A imagem excede o limite de peso de 1MB.</div>}
                        <img src={droppedImage.preview} alt="" />
                      </div>
                    ) : (
                        <div className={cx('empty-dropzone')}>Inserir foto</div>
                      )
                    }
                  </Dropzone>
                }

                <button className={cx('post-inserir')} onClick={this.toggleDropzone}>
                  <i className={cx('post-inserir-icon')} />
                  Inserir outra imagem
                </button>
              </div>
            </li>

            <li className={cx('post-step')}>
              <h2><span className={cx('bullet')}>2</span> Escolha o tipo de sua transmissão:</h2>
              <div className={cx('post-transmissao')}>
                <div className={cx('post-radio')}>
                  <input
                    type="radio"
                    name="broadcast"
                    value="transmissao"
                    checked={postText === 'transmissao'}
                    onChange={updateText}
                  />
                  <label>Transmissão ao vivo</label>
                </div>
                <div className={cx('post-radio')}>
                  <input
                    type="radio"
                    name="broadcast"
                    value="transmissao-audio"
                    checked={postText === 'transmissao-audio'}
                    onChange={updateText}
                  />
                  <label>Transmissão ao vivo com áudio</label>
                </div>
                <div className={cx('post-radio')}>
                  <input
                    type="radio"
                    name="broadcast"
                    value="transmissao-telao"
                    checked={postText === 'transmissao-telao'}
                    onChange={updateText}
                  />
                  <label>Transmissão ao vivo no telão</label>
                </div>
              </div>
            </li>
            <li className={cx('post-step')}>
              <h2><span className={cx('bullet')}>3</span> Escolha uma figurinha:</h2>
              <p className={cx('note')}>(Ficará visível no formato STORIES)</p>

              <div className={cx('figures-container')}>
                <div className={cx('figures-container-item', 'figures-container-item-empty', figure === null ? 'checked' : '')} onClick={() => updateFigure(null)}>Nenhuma</div>
                {figures.map((img, index) => {
                  const handleUpdate = () => {
                    updateFigure(index)
                  }
                  return (
                    <div key={img} className={cx('figures-container-item', figure === index ? 'checked' : '')} onClick={handleUpdate}><img src={img} alt="" /></div>
                  );
                })}
              </div>
            </li>
          </ol>
          <div className={cx('actions')}>
            <button type="button" className={cx('btn-secondary')} onClick={this.onClickBack}>Voltar</button>
            <button type="button" className={cx('btn-primary')} disabled={isUploading} onClick={savePost}>Salvar</button>
          </div>
        </div>
      </div>
    );
  }
}

export default Editor;
