import React from 'react';
import classNames from 'classnames/bind';
import Game from '../../game/Game';
import styles from '../../../css/components/templates.css';

const cx = classNames.bind(styles);

const Template = ({ selectedGames, option, teams }) => {
  if (!selectedGames) return false;

  const getTeamById = teamId => teams.find(team => team.id === teamId);

  let optionClass;
  let activeClass = '';
  switch (option) {
    case 1:
      optionClass = 'template-one';
      if (selectedGames.length === 1) activeClass = 'active-template';
      break;
    case 2:
      optionClass = 'template-two';
      if (selectedGames.length === 2) activeClass = 'active-template';
      break;
    case 3:
      optionClass = 'template-three';
      if (selectedGames.length === 3) activeClass = 'active-template';
      break;
    case 4:
      optionClass = 'template-four';
      if (selectedGames.length === 4) activeClass = 'active-template';
      break;
    default:
      break;
  }
  const selectedGamesRange = selectedGames.slice(0, option);

  return (
    <div className={cx('template', optionClass, activeClass)}>
      <div className={cx('template-container')}>
        {selectedGamesRange.map(({ visitingTeam, homeTeam, id, gameDate, championship }) => {
          const normalizeNumber = (num) => {
            return num > 9 ? num.toString() : '0' + num;
          }
          const gDate = new Date(gameDate);
          const day = normalizeNumber(gDate.getDate());
          const month = normalizeNumber(gDate.getMonth() + 1);
          const hours = normalizeNumber(gDate.getHours());
          const minutes = normalizeNumber(gDate.getMinutes());
          return (
            <div key={id} className={cx('game-item')}>
              <Game
                visitingTeam={getTeamById(visitingTeam.id)}
                homeTeam={getTeamById(homeTeam.id)}
                className={optionClass}
                isMini
              />
              <div className={cx('game-championship')}>{championship.name}</div>
              <div className={cx('game-date-time')}>
                <div className={cx('game-date')}>{day}/{month}</div>
                <div className={cx('game-time')}>{hours}H{minutes}</div>
              </div>
            </div>
          );
      })}
      </div>
    </div>
  );
};

export default Template;
