import React, { Component } from 'react';
import classNames from 'classnames/bind';
import { Link } from 'react-router';
import GameSelectItem from '../../game/GameSelectItem';
import Template from './Template';
import styles from '../../../css/components/owner';

const cx = classNames.bind(styles);

class Posts extends Component {
  render() {
    const { allGames, teams, channels, place, selectedGames, handleAddGame, handleRemoveGame, handleCreatePost } = this.props;
    if (!teams || !channels) return null;

    // order games by date
    const sortedGames = allGames;
    sortedGames.sort((a, b) => {
      return new Date(a.gameDate).getTime() - new Date(b.gameDate).getTime();
    });
    const gamesList = sortedGames.map(game => {
      const { broadcastGames } = place;
      const willBroadcast = broadcastGames && broadcastGames.length ? broadcastGames.some(item => {
        return item.id === game.id;
      }) : false;
      if (!willBroadcast) return null;
      const isChecked = selectedGames.some(selected => selected.id === game.id);
      return (
        <GameSelectItem
          key={game.id}
          game={game}
          teams={teams}
          channels={channels}
          isChecked={isChecked}
          addGame={handleAddGame}
          removeGame={handleRemoveGame}
          showConfirmed
        />
      );
    }, this);

    const createPost = () => {
      handleCreatePost('editing');
    };

    return (
      <div className={cx('post')}>
        <div className={cx('title')}>
          Selecione até <strong>3 jogos</strong> para inserir no post.
        </div>

        <div className={cx('games-list')}>
          {gamesList}
        </div>

        <div className={cx('post-example')}>
          <div className={cx('post-example-container')}>
            <Template selectedGames={selectedGames} teams={teams} option={1} />
            <Template selectedGames={selectedGames} teams={teams} option={2} />
            <Template selectedGames={selectedGames} teams={teams} option={3} />
            {/* <Template selectedGames={selectedGames} teams={teams} option={4} /> */}
          </div>
        </div>

        <div className={cx('actions')}>
          <Link to="/gerenciar-jogos" className={cx('btn-back')}>Editar</Link>
          <button className={cx('btn-post')} onClick={createPost}>Criar Post</button>
        </div>
      </div>
    );
  }
}

export default Posts;
