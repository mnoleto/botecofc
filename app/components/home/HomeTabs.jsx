import React from 'react';
import PropTypes from 'prop-types';
import { Tabs, TabList, Tab, TabPanel } from 'react-tabs';
import classNames from 'classnames/bind';
import { filterConfirmedGames } from '../../utils/filters';
import ListChampionships from '../championships/ListChampionships';
import ListGames from '../games/ListGames';
import ListPlaces from '../places/ListPlaces';
import ListTeams from '../teams/ListTeams';
import styles from '../../css/components/tabs';

const cx = classNames.bind(styles);

const HomeTabs = ({championships, places, games, teams, selectPlace}) => {
	return (
		<Tabs className={cx('tabs', 'home-tabs')} defaultIndex={0}>
			<TabList className={cx('tabs-list', 'four-tabs')}>
				<Tab className={cx('tab', 'tablist-games')}>Jogos</Tab>
				<Tab className={cx('tab', 'tablist-places')}>Bares</Tab>
				<Tab className={cx('tab', 'tablist-teams')}>Times</Tab>
				<Tab className={cx('tab', 'tablist-championships')}>Campeonatos</Tab>
			</TabList>

			<TabPanel className={cx('tabpanel')}>
				<ListGames
					title={['Jogos para os', <br />, <strong>próximos dias</strong>]}
					games={filterConfirmedGames(games, places)}
					teams={teams}
				/>
			</TabPanel>

			<TabPanel className={cx('tabpanel')}>
				<ListPlaces
					title={['Bares']}
					places={places}
					games={games}
					teams={teams}
					showConfirmed={false}
				/>
			</TabPanel>

			<TabPanel className={cx('tabpanel')}>
				<ListTeams
					title={['Times']}
					teams={teams}
					games={games}
					showAll={false}
				/>
			</TabPanel>

			<TabPanel className={cx('tabpanel')}>
				<ListChampionships
					title={['Principais', <br />, <strong>campeonatos</strong>]}
					championships={championships}
					games={games}
					teams={teams}
				/>
			</TabPanel>
		</Tabs>
	);
};

HomeTabs.propTypes = {
	championships: PropTypes.array.isRequired,
	games: PropTypes.array.isRequired,
	places: PropTypes.array.isRequired,
	teams: PropTypes.array.isRequired
};

export default HomeTabs;
