import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import { fetchChampionships } from '../../actions/championships';
import { fetchBanners } from '../../actions/banners';
import { fetchGames } from '../../actions/games';
import { fetchPlaces } from '../../actions/places';
import { fetchTeams } from '../../actions/teams';
import { fetchCities } from '../../actions/cities';
import styles from '../../css/components/home';
import { getPlacesByGame } from '../../utils/get';
import Banners from '../banners';
import HomeChampionship from '../home/HomeChampionship';
import HomeTabs from '../home/HomeTabs';
import ShirtPromo from '../modais/ShirtPromo';

const cx = classNames.bind(styles);

class Home extends Component {
	static need = [
		fetchChampionships,
		fetchTeams,
		fetchGames,
		fetchPlaces,
		fetchBanners,
		fetchCities,
	];

	constructor(props) {
		super(props);
		this.state = {
			city: '',
			date: '',
			championship: '',
			game: '',
			games: [],
			isLoaded: false,
		}
		this.getGame = this.getGame.bind(this);
		this.updateChampionshipFilter = this.updateChampionshipFilter.bind(this);
		this.updateCityFilter = this.updateCityFilter.bind(this);
		this.updateDateFilter = this.updateDateFilter.bind(this);
		this.updateGameFilter = this.updateGameFilter.bind(this);
	}

	getGame(id) {
		const { allgames } = this.props;
		if (!id) return null;
		return allgames.find((g) => g.id === id);
	}

	updateChampionshipFilter(value) {
		const { setVisibilityFilter } = this.props;
		if (value === 'TODOS') {
			let show = (this.state.city === '') ? 'SHOW_ALL' : 'SHOW_ONLY';
			this.setState({ championship: 'TODOS' });
			setVisibilityFilter(show, value, this.state.city, '');
		} else {
			this.setState({ championship: value });
			setVisibilityFilter('SHOW_ONLY', value, this.state.city, '');
		}
	}

	updateCityFilter(value) {
		const { setVisibilityFilter } = this.props;

		if (value === 'TODO DF') {
			this.setState({ city: '' });
			let show = (this.state.championship === 'TODOS') ? 'SHOW_ALL' : 'SHOW_ONLY';
			setVisibilityFilter(show, this.state.championship, '', '');
		} else {
			this.setState({ city: value });
			setVisibilityFilter('SHOW_ONLY', this.state.championship, value, '');
		}
	}

	updateDateFilter(value) {
		const { setVisibilityFilter } = this.props;
		this.state.date = value;
		setVisibilityFilter('SHOW_ONLY', this.state.championship, this.state.city, value);
	}

	updateGameFilter(value) {
		this.setState({ game: value });
	}

	render() {
		const {
			account,
			allgames,
			allplaces,
			banners,
			championships,
			ga,
			games,
			gameVisibilityFilter,
			isLoading,
			places,
			teams,
			termsAccepted,
			toggleLoading,
		} = this.props;
		const { isLoaded } = this.state
		const game = this.getGame(this.state.game);

		if (
			championships.length > -1 &&
			teams.length > 0 &&
			allplaces.length > 0 &&
			!isLoaded
		) {
			setTimeout(() => {
				this.setState({
					isLoaded: true
				})
				toggleLoading();
			}, 3000);
		}
		return (
			<div className={cx('home')}>
				{isLoading && (<div className={cx('loading')}></div>)}
				{!termsAccepted && <ShirtPromo />}
				<div className={cx('banner-desktop')}><Banners banners={banners} type="desktop" ga={ga} /></div>
				<div className={cx('championship')}>
					<HomeChampionship
						allplaces={allplaces}
						allplacesByGame={getPlacesByGame(allplaces, game)}
						championships={championships}
						game={this.state.game}
						games={games}
						gameVisibilityFilter={gameVisibilityFilter}
						teams={teams}
						places={getPlacesByGame(places, game)}
						updateChampionshipFilter={this.updateChampionshipFilter}
						updateCityFilter={this.updateCityFilter}
						updateDateFilter={this.updateDateFilter}
						updateGameFilter={this.updateGameFilter}
						account={account}
					/>
				</div>
				<div className={cx('banner-mobile')}><Banners banners={banners} type="mobile" ga={ga} /></div>
				<div className={cx('tabs')}>
					<HomeTabs
						championships={championships}
						games={allgames}
						places={allplaces}
						teams={teams}
					/>
				</div>
			</div>
		);
	}
}

Home.propTypes = {
	account: PropTypes.object,
	allgames: PropTypes.array.isRequired,
	authenticated: PropTypes.bool.isRequired,
	banners: PropTypes.array.isRequired,
	championships: PropTypes.array.isRequired,
	games: PropTypes.array.isRequired,
	gameVisibilityFilter: PropTypes.object.isRequired,
	isLoading: PropTypes.bool.isRequired,
	places: PropTypes.array.isRequired,
	setVisibilityFilter: PropTypes.func.isRequired,
	teams: PropTypes.array.isRequired,
	termsAccepted: PropTypes.bool.isRequired,
	toggleLoading: PropTypes.func.isRequired,
};

export default Home;