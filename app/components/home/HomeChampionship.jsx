import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import GamesCarousel from '../../components/games/GamesCarousel';
import PlacesCarousel from '../../components/places/PlacesCarousel';
import { filterConfirmedGames } from '../../utils/filters';
import styles from '../../css/components/home';

const cx = classNames.bind(styles);

class HomeChampionship extends Component {
	constructor(props) {
		super(props);
		this.state = {
			city: '',
			date: '',
			championship: '',
		}
	}

	render() {
		const {
			allplaces,
			allplacesByGame,
			championships,
			game,
			games,
			gameVisibilityFilter,
			teams,
			places,
			updateChampionshipFilter,
			updateCityFilter,
			updateGameFilter,
			account,
		} = this.props;
		
		return (
			<div className={cx('home-championships')}>
				<div className={cx('home-championships-box')}>
					<div className={cx('championship')}>
						<GamesCarousel
							gameVisibilityFilter={gameVisibilityFilter}
							championships={championships}
							updateChampionshipFilter={updateChampionshipFilter}
							games={filterConfirmedGames(games, allplaces)}
							places={places}
							teams={teams}
							selectGame={updateGameFilter}
							account={account}
						/>
						
						<PlacesCarousel
							allplaces={allplacesByGame}
							game={game}
							places={places}
							updateCityFilter={updateCityFilter}
							gameVisibilityFilter={gameVisibilityFilter}
						/>
					</div>
				</div>
			</div>
		);
	}
};

HomeChampionship.propTypes = {
	allplaces: PropTypes.array,
	allplacesByGame: PropTypes.array,
	championships: PropTypes.array.isRequired,
	game: PropTypes.string,
	games: PropTypes.array,
	gameVisibilityFilter: PropTypes.object.isRequired,
	teams: PropTypes.array.isRequired,
	places: PropTypes.array,
	updateChampionshipFilter: PropTypes.func.isRequired,
	updateCityFilter: PropTypes.func.isRequired,
	updateDateFilter: PropTypes.func.isRequired,
	updateGameFilter: PropTypes.func.isRequired,
	account: PropTypes.object.isRequired,
};

export default HomeChampionship;
