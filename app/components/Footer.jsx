import React from 'react';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import FooterRegisterMobile from '../images/bg/footer-register@2x.png';
import FooterRegister from '../images/bg/footer-register.png';
import LogoFooter from '../images/logo-footer.png';
import styles from '../css/components/footer';

const cx = classNames.bind(styles);

const Footer = ({}) => {
  return (
    <div className={cx('footer', 'site-footer')}>
      <div className={cx('register-ad')}>
        <div className={cx('register-ad-title')}>
          <h4>
            Receba informações sobre<br />
            <strong>os Bares que transmitem os<br />
            jogos do seu time.</strong>
          </h4>
        </div>
        <img
          src={FooterRegisterMobile}
          className={cx('footer-register-image', 'hidden-md', 'hidden-lg')}
          alt=""
        />
        <img
          src={FooterRegister}
          className={cx('footer-register-image', 'hidden-xs', 'hidden-sm')}
          alt=""
        />
        <Link
          to="/login"
          className={cx('btn-ad-register')}>
          Cadastre-se <strong>agora!</strong>
        </Link>
      </div>

      <footer className={cx('main-footer')}>
        <div className={cx('footer-wrapper')}>
          <Link
            to="/"
            className={cx('logo-footer')}>
            <img src={LogoFooter} alt="Boteco FC" />
          </Link>
          <ul className={cx('legal')}>
            <li>
              <a href="/termos" alt="Termos de uso">Termos de uso</a>
            </li>
            <li>
              <a href="/politica-de-privacidade" alt="Política de privacidade">Política de privacidade</a>
            </li>
          </ul>
          <p className={cx('copyright')}>Todos os Direitos Reservados ® - 2022</p>
        </div>
      </footer>
    </div>
  );
};

export default Footer;
