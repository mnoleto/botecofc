import React from 'react';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import logo from '../../images/logo_qualbar.png';
import styles from '../../css/components/navigation';

const cx = classNames.bind(styles);

const Logo = () => 
	<Link to="/" className={cx('logo')}>
		<img src={logo} alt="Onde passa o jogo?" />
	</Link>;

export default Logo;
