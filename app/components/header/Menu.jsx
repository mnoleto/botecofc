import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { browserHistory, Link } from 'react-router';
import classNames from 'classnames/bind';
import logo from '../../images/logo_qualbar.png';
import Icon from '../Icon';
import styles from '../../css/components/navigation';

const cx = classNames.bind(styles);

class Menu extends Component {
  constructor(props) {
    super(props);
    this.getUserContent = this.getUserContent.bind(this);
    this.onHandleLogout = this.onHandleLogout.bind(this);
    this.toggleMenu = this.toggleMenu.bind(this);
  };

  getUserContent() {
    const { authenticated, account } = this.props.user;

    if(authenticated) {
      return (
        <div className={cx('user-info')}>
          {(account.profile.picture) &&
            <figure>
              <img src={account.profile.picture} className={cx('user-image')} alt={account.name} />
            </figure>
          }
          <span className={cx('welcome')}><strong>Olá</strong><br />{account.name}</span>
          <ul id="user-info-menu">
            <li>
              <Link to="/cadastro" className={cx('info-link', 'edit')} onClick={this.toggleMenu}>Editar dados</Link>
            </li>
            {(account.role === 'owner' || account.role === 'admin') &&
            <li>
              <Link to="/editar-bar" className={cx('info-link', 'places')} onClick={this.toggleMenu}>Editar bar</Link>
            </li>
            }
            {(account.role === 'owner' || account.role === 'admin') &&
            <li>
              <Link to="/gerenciar-jogos" className={cx('info-link', 'insert')} onClick={this.toggleMenu}>Gerenciar Transmissões</Link>
            </li>
            }
            <li>
              <Link to="/" className={cx('info-link', 'logout')} onClick={this.onHandleLogout}>Sair</Link>
            </li>
          </ul>
        </div>
      );
    } else {
      return(
        <div className={cx('user-register')}>
          <Link to="/login" className={cx('btn-register')} onClick={this.toggleMenu}>
            <Icon size={'2x'} name={'register'}></Icon>
            Logar<br/>
            <span>Faça o login e receba informações do seu time e tenha acesso a Promoções.</span>
          </Link>
        </div>
      );
    }
  }

  hasBackButton() {
    var backButton = '';
    if(browserHistory) {
      backButton =
        <button type="button" className={cx('btn-back', 'hidden-md', 'hidden-lg')} onClick={browserHistory.goBack}>
          <span className={cx('sr-only')}>Voltar</span>
        </button>
    }
    return backButton;
  }

  onHandleLogout() {
    const { onLogOut } = this.props;
    onLogOut();
    this.toggleMenu();
  }

  toggleMenu(event) {
    $('.' + cx('btn-toggle')).toggleClass(cx('open'));
    $('#menu').toggleClass(cx('open'));
  }

  render() {
    return (
      <div id="menu" className={cx('main-nav')}>
        <nav className={cx('main-menu')}>
          <div className={cx('menu-header')}>
            <div className={cx('menu-logo')}><img src={logo} alt="BotecoFC" /></div>
            <p>
              <span>O bar transmite,</span><br />
              <span>a gente divulga,</span><br />
              <span>você comemora!</span>
            </p>
          </div>

          <Link className={cx('nav-cadastrar-bar')} to="/cadastrar-bar" onClick={this.toggleMenu}>
            Cadastrar bar
          </Link>

          <div className={cx('menu')}>
            <ul className={cx('sidebar-menu')}>
              <li className={cx('treeview')}>
                <Link to="/" onClick={this.toggleMenu}>
                  <Icon size={'2x'} name={'games'}></Icon>&nbsp;
                  <span>Home</span>
                </Link>
              </li>

              <li className={cx('treeview')}>
                <Link to="/bares" onClick={this.toggleMenu}>
                  <Icon size={'2x'} name={'botecos'}></Icon>&nbsp;
                  <span>Ver Bares</span>
                </Link>
              </li>

              <li className={cx('treeview')}>
                <Link to="/times" onClick={this.toggleMenu}>
                  <Icon size={'2x'} name={'teams'}></Icon>&nbsp;
                  <span>Times</span>
                </Link>
              </li>

              <li className={cx('treeview')}>
                <Link to="/campeonatos" onClick={this.toggleMenu}>
                  <Icon size={'2x'} name={'championships'}></Icon>&nbsp;
                  <span>Campeonatos</span>
                </Link>
              </li>

              <li className={cx('treeview')}>
                <a href="mailto:contato@qualbar.com.br" onClick={this.toggleMenu}>
                  <Icon size={'2x'} name={'mail'}></Icon>&nbsp;
                  <span>Fale Conosco</span>
                </a>
              </li>
            </ul>

            <div className={cx('user-nav')}>
              {this.getUserContent()}
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

Menu.propTypes = {
  user: PropTypes.object,
  onLogOut: PropTypes.func
};

export default Menu;
