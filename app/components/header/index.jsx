import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cookie from 'react-cookie';
import { browserHistory } from 'react-router';
import classNames from 'classnames/bind';
import { fetchChampionships } from '../../actions/championships';
import { fetchGames } from '../../actions/games';
import { fetchPlaces } from '../../actions/places';
import { fetchTeams } from '../../actions/teams';
import styles from '../../css/components/navigation';
import Locale from '../../containers/Locale'
import Brasao from '../../images/icons/brasao.png';
import findById from '../../utils/find';
import Logo from '../header/Logo';
import Menu from '../header/Menu';
import FavoriteTeamModal from '../modais/FavoriteTeamModal';
import LoginModal from '../modais/LoginModal';
import SearchModal from '../modais/SearchModal';

const cx = classNames.bind(styles);

class Header extends Component {
  static need = [
    fetchChampionships,
    fetchGames,
    fetchPlaces,
    fetchTeams,
  ];

  constructor(props) {
    super(props);
    this.openFavoriteTeam = this.openFavoriteTeam.bind(this);
  }

  componentDidMount() {
    cookie.remove('returnTo', { path: '/' });
    if (!this.props.location || !this.props.location.query || !this.props.location.query.openModal) return;
    document.querySelector('#favoriteTeamModal').style.display = 'block';
  }

  hasBackButton() {
    var backButton = '';
    if (browserHistory) {
      backButton =
        <button type="button" className={cx('btn-back', 'hidden-md', 'hidden-lg')} onClick={browserHistory.goBack}>
          <span className={cx('sr-only')}>Voltar</span>
        </button>
    }
    return backButton;
  }

  toggleMenu() {
    document.querySelector('.' + cx('btn-toggle')).classList.toggle(cx('open'));
    document.querySelector('#menu').classList.toggle(cx('open'));
  }

  openSearch() {
    document.querySelector("body").classList.add(cx('modal-open'));
    document.querySelector('#searchModal').style.display = 'block';
  }

  openFavoriteTeam() {
    const { authenticated } = this.props.user;
    if (authenticated) {
      document.querySelector('#favoriteTeamModal').style.display = 'block';
    } else {
      cookie.save('returnTo', window.location.pathname + '?openModal=favorito', { path: '/', sameSite: 'Strict' });
      document.querySelector('#loginmodal').style.display = 'block';
    }
  }

  closeSearch = () => {
    document.querySelector("body").classList.remove(cx('modal-open'));
    document.querySelector('#searchModal').style.display = 'none';
  }

  openLoginModal() {
    document.querySelector('#favoriteTeamModal').style.display = 'none';
    document.querySelector('#loginmodal').style.display = 'block';
  }

  render() {
    const { user, teams, games, places, championships, location, logOut } = this.props;
    let favoriteTeam = null;
    if (user.account.profile && user.account.profile.team && user.account.profile.team.id ) {
      favoriteTeam = findById(user.account.profile.team.id, teams);
    }

    const hasLocation =
      (
        location.pathname === '/' ||
        location.pathname.includes('bares') ||
        location.pathname.includes('jogo')
      ) && !location.pathname.includes('bfc-admin') && !location.pathname.includes('gerenciar-jogos')
    return (
      <React.Fragment>
        <header className={cx('main-header', 'boteco-header')}>
          <Logo />
          {this.hasBackButton()}

          <button type="button" className={cx('btn-search')} onClick={this.openSearch}>
            <span className={cx('sr-only')}>Search</span>
          </button>

          <button type="button" className={cx('btn-toggle')} onClick={this.toggleMenu}>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </button>

          
          <button
            onClick={this.openFavoriteTeam}
            className={cx('favorite-team', favoriteTeam ? '' : 'empty')}
          >
            {favoriteTeam
              ? <img src={favoriteTeam.logo.responseText} alt={favoriteTeam.name} />
              : <img src={Brasao} alt="" />
            }
          </button>

          <Menu user={user} onLogOut={logOut} />
        </header>

        {hasLocation && <div className={cx('locale')}><Locale /></div>}

        <SearchModal
          id={'searchModal'}
          teams={teams}
          games={games}
          places={places}
          championships={championships}
          onClose={this.closeSearch}
        />

        <FavoriteTeamModal
          id={'favoriteTeamModal'}
          team={favoriteTeam}
          authenticated={user.authenticated}
          openLoginModal={this.openLoginModal}
        />

        {!user.authenticated &&
          <LoginModal
            id={'loginmodal'}
          />
        }
      </React.Fragment>
    );
  }
}

Header.propTypes = {
  championships: PropTypes.array.isRequired,
  dispatch: PropTypes.func,
  games: PropTypes.array.isRequired,
  logOut: PropTypes.func,
  places: PropTypes.array.isRequired,
  teams: PropTypes.array.isRequired,
  user: PropTypes.object,
};

export default Header;
