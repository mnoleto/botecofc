import React, { Component } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from '../../css/admin/main';

const cx = classNames.bind(styles);

class MenuAdmin extends Component {
  render() {
    return (
      <nav className={cx('admin-nav')}>
        <ul className={cx('sidebar-menu')}>
          <li className={cx('treeview')}>
            <Link activeClassName={cx('active')} to="/bfc-admin/post-bg">
              Marketing
            </Link>
            <ul className={cx('treeview-menu')}>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/banners">
                  Banners
                </Link>
              </li>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/bgs">
                  Backgrounds
                </Link>
              </li>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/figurinhas">
                  Figurinhas
                </Link>
              </li>
            </ul>
          </li>
          <li className={cx('treeview')}>
            <Link activeClassName={cx('active')} to="/bfc-admin/bares">
              Bares
            </Link>
            <ul className={cx('treeview-menu')}>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/bares/aprovados">
                  Aprovados
                </Link>
              </li>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/bares/pendentes">
                  Pendentes
                </Link>
              </li>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/bares/sugestoes">
                  Sugestões
                </Link>
              </li>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/bares/transmissoes">
                  Transmissão de Jogos
                </Link>
              </li>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/bares/post">
                  Liberar Posts
                </Link>
              </li>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/usuarios/solicitacoes">
                  Solicitações de propriedade
                </Link>
              </li>
            </ul>
          </li>
          <li className={cx('treeview')}>
            <Link activeClassName={cx('active')} to="/bfc-admin/campeonatos">
              Conteúdo
            </Link>
            <ul className={cx('treeview-menu')}>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/campeonatos">
                  Campeonatos
                </Link>
              </li>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/jogos">
                  Jogos
                </Link>
              </li>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/times">
                  Times
                </Link>
              </li>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/esportes">
                  Modalidades esportivas
                </Link>
              </li>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/paises">
                  Paises
                </Link>
              </li>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/estados">
                  Estados
                </Link>
              </li>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/cidades">
                  Cidades
                </Link>
              </li>
              <li>
                <Link activeClassName={cx('active')} to="/bfc-admin/canais">
                  Canais
                </Link>
              </li>
            </ul>
          </li>
          <li className={cx('treeview')}>
            <Link activeClassName={cx('active')} to="/bfc-admin/usuarios">
              Gerenciar usuários
            </Link>
          </li>
        </ul>
      </nav>
    );
  }
}

export default MenuAdmin;
