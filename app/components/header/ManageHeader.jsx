import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import sortByName from '../../utils/sortByName';
import styles from '../../css/components/owner';

const cx = classNames.bind(styles);

const OwnerManageHeader = ({ place, places, changePlace, isDisabled }) => {
  const alphabeticalPlaces = places;
  let placesList = [];
  if (places) {
    alphabeticalPlaces.sort(sortByName('name'));
    placesList = alphabeticalPlaces.map((item) => {
      return (<option key={item.id} value={item.id}>{item.name}</option>);
    });
  }

  return (
    <header className={cx('owner-manage-header')}>
      <div className={cx('owner-manage-container')}>
        <div className={cx('row')}>
          <div className={cx('col-md-6', 'col-sm-12', 'col-xs-12')}>
            <h1 className={cx('title')}>
              <span className={cx('title-small')}>Gerenciador de</span><br />
              <span className={cx('title-medium')}>Transmissões e</span><br />
              <span className={cx('title-large')}>Posts</span>
            </h1>
          </div>
          <div className={cx('col-md-6', 'col-sm-12', 'col-xs-12', 'select-place')}>
            <label htmlFor="place">Seu Estabelecimento</label>
            <div className={cx('field-group')}>
              <select
                name="place"
                className={cx('full')}
                value={place && place.id ? place.id : ''}
                onChange={changePlace}
                disabled={isDisabled}
                >
                {placesList}
              </select>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

OwnerManageHeader.propTypes = {
  places: PropTypes.array.isRequired,
  place: PropTypes.object,
  changePlace: PropTypes.func.isRequired,
  isDisabled: PropTypes.bool.isRequired,
};

export default OwnerManageHeader;
