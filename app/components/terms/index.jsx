import React from 'react';
import classNames from 'classnames/bind';
import TermsContent from '../terms/TermsContent';
import registerImage from '../../images/bg/register@2x.jpg';
import styles from '../../css/components/loginOrRegister';

const cx = classNames.bind(styles);

const Terms = () => {
    return (
        <div className={cx('login')}>
            <figure className={cx('header-image')}>
                <img src={registerImage} alt="" />
            </figure>

            <div className={cx('container')}>
                <h1 className={cx('heading')}>Termos, condições e avisos do site Qual Bar</h1>
                <TermsContent />
            </div>
        </div>
    );
}

export default Terms;
