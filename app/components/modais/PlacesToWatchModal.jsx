import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import sortByName from '../../utils/sortByName';
import PlaceToWatchItem from '../../components/place/PlaceToWatchItem';
import styles from '../../css/components/modal';

const cx = classNames.bind(styles);

class PlacesToWatchModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			places: [],
			message: ''
		};
		this.addPlace = this.addPlace.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.getPlaceById = this.getPlaceById.bind(this);
		this.removePlace = this.removePlace.bind(this);
		this.renderSelectedPlaces = this.renderSelectedPlaces.bind(this);
	}

	addPlace() {
		const { addPlaceToWatch } = this.props;

		const placeEl = this.refs.places;
		if(placeEl.value === '0') {
			this.setState({'message': 'Selecione um boteco.'});
			return false;
		}

		this.setState({
			'message': ''
		});

		addPlaceToWatch(placeEl.value);

		placeEl.value = '0';
		this.forceUpdate();
	}

	closeModal() {
		const { id } = this.props;
		document.querySelector( '#' + id ).style.display = 'none';
	}

	getPlaceById(id) {
		const { places } = this.props;
		let selectedPlace;
		places.forEach((value, index) => {
			if(value.id === id) selectedPlace = value;
		});
		return selectedPlace;
	}

	removePlace(id) {
		const { removePlaceToWatch } = this.props;

		this.setState({
			'message': ''
		});

		removePlaceToWatch(id);
	}

	renderMessage() {
		if(this.state.message != '') {
			return (
				<div className={cx('message')}>
					<i className="zmdi zmdi-block"></i> {this.state.message}
				</div>
			);
		}
	}

	renderSelectedPlaces() {
		const { placesToWatch } = this.props;
		if(placesToWatch && placesToWatch.length > 0) {
			const _ = this;
			const placesItems = placesToWatch.map((value, index) => {
				const place = _.getPlaceById(value);
				return (
					<PlaceToWatchItem
						key={'place_' + place.id}
						id={place.id}
						name={place.name}
						slug={place.slug}
						image={place.logo.responseText}
						removePlace={_.removePlace}
					/>
				);
			});
			return <div className={cx('places-list')}>{placesItems}</div>
		} else {
			return <div className={cx('no-result')}>Nenhum bar adicionado. =/</div>
		}
	}

	render() {
		const { id, places } = this.props;

		const alphabeticalPlaces = places;
		alphabeticalPlaces.sort(sortByName('name'));
		const listItems = alphabeticalPlaces.map((place, index) => {
			return(
				<option key={'place_' + place.id + '_' + index} value={place.id}>
					{place.name}
				</option>
			);
		});

		return (
			<div id={id} className={cx('modal', 'places-modal')} data-backdrop="false">
				<header className={cx('modal-header', 'gray-header')}>
					<button type="button" className={cx('close')} data-dismiss="modal" aria-label="Close" onClick={this.closeModal}><span aria-hidden="true">&times;</span></button>
					<div className={cx('header-content')}>
						<h3 className={cx('modal-title')}><span>Aonde você costuma<br /><strong>assistir aos jogos</strong></span></h3>

						<form className={cx('clearfix', 'form-inline', 'modal-form')} method="post" encType="multipart/form-data" role="form">
							<div className={cx('row')}>
								{this.renderMessage()}
								<div className={cx('places-field')}>
									<label htmlFor="search" className={cx('sr-only')}>Selecione um bar</label>
									<select ref="places">
										<option value="0">Selecione um bar</option>
										{listItems}
									</select>
								</div>
								<input type="button" className={cx('btn-add')} value="Adicionar" onClick={this.addPlace} />
							</div>
						</form>
					</div>
				</header>
				<div className={cx('modal-body')}>
					{this.renderSelectedPlaces()}
		        </div>
	        </div>
		);
	}
}

PlacesToWatchModal.propTypes = {
  id: PropTypes.string,
  places: PropTypes.array,
  placesToWatch: PropTypes.array.isRequired,
  team: PropTypes.object.isRequired,
  addPlaceToWatch: PropTypes.func.isRequired,
  removePlaceToWatch: PropTypes.func.isRequired
};

export default PlacesToWatchModal;
