import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from '../../css/components/modal';

const cx = classNames.bind(styles);

class RegisterModal extends Component {
	constructor(props) {
		super(props);
		this.closeModal = this.closeModal.bind(this);
	}

	closeModal() {
		const { id } = this.props;
		document.querySelector( '#' + id ).style.display = 'none';
	}

	render() {
		const { id } = this.props;
		
		return (
			<div id={id} className={cx('modal')} data-backdrop="false">
				<div className={cx('modal-content')}>
					<header>
						<button type="button" className={cx('close')} data-dismiss="modal" aria-label="Close" onClick={this.closeModal}><span aria-hidden="true">&times;</span></button>
						<figure className={cx('sad-face')}>=(</figure>
					</header>
					<div className={cx('register-modal')}>
						<h3 className={cx('modal-title')}><span><strong>Pena que você não quer se cadastrar agora!</strong></span></h3>
						<p>Você poderá navegar e ver vários jogos e bares, mas não terá acesso as promoções e algumas funcionalidades ficarão limitadas. Se mudar de ideia, será um prazer tê-lo como nosso cliente. E lembre-se: <strong>é gratuito</strong>.</p>
			        </div>
			        <footer>
			        	<Link className={cx('btn-account', 'btn-modal')} to="login-ou-cadastro" onClick={this.closeModal}>Mudei de ideia, <strong>criar conta</strong></Link>
			        	<Link className={cx('btn-later', 'btn-modal')} to="home" onClick={this.closeModal}>Continuar sem criar conta</Link>
			        </footer>
		        </div>
	        </div>
		);
	}
}

RegisterModal.propTypes = {
  id: PropTypes.string
};

export default RegisterModal;