import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { browserHistory } from 'react-router';
import cookie from 'react-cookie';
import classNames from 'classnames/bind';
import styles from '../../css/components/modal';

const cx = classNames.bind(styles);

class MessageModal extends Component {
	constructor(props) {
		super(props);
		this.closeModal = this.closeModal.bind(this);
	}

	closeModal() {
		const { id } = this.props;
		document.querySelector('#' + id).style.display = 'none';
		if (cookie.load('returnTo')) {
			browserHistory.push(cookie.load('returnTo'));
		} else {
			browserHistory.push('/');
		}
	}

	render() {
		const { id, title, body, type } = this.props;
		
		return (
			<div id={id} className={cx('modal')} data-backdrop="false">
				<div className={cx('modal-content')}>
					<header>
						<button className={cx('close')} data-dismiss="modal" aria-label="Close" onClick={this.closeModal}><span aria-hidden="true">&times;</span></button>
					</header>
					<div className={cx('message', type)}>
						{ (type == 'success') && <i className="zmdi zmdi-check-circle"></i> }
						<h3 className={cx('modal-title')}>{title}</h3>
						<p>{body}</p>
					</div>
		    </div>
	    </div>
		);
	}
}

MessageModal.propTypes = {
	id: PropTypes.string,
	type: PropTypes.string,
	title: PropTypes.string,
	body: PropTypes.string
};

export default MessageModal;
