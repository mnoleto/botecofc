import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cookie from 'react-cookie';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from '../../css/components/modal';

const cx = classNames.bind(styles);

class LoginModal extends Component {
	constructor(props) {
		super(props);
		this.closeModal = this.closeModal.bind(this);
	}

	closeModal() {
		const { id } = this.props;
		cookie.remove('returnTo', { path: '/' });
		document.querySelector( '#' + id ).style.display = 'none';
	}

	render() {
		const { id } = this.props;
		
		return (
			<div id={id} className={cx('modal')} data-backdrop="false">
				<div className={cx('modal-content')}>
					<header>
						<button type="button" className={cx('close')} data-dismiss="modal" aria-label="Close" onClick={this.closeModal}><span aria-hidden="true">&times;</span></button>
					</header>
					<div className={cx('login-modal')}>
						<h3 className={cx('modal-title')}>Que bom te receber aqui!<br />Se conecte com a gente e reforce o time do Qual Bar. :)</h3>

						<div className={cx('login-button-group')}>
							<a href="/auth/facebook" className={cx('btn-more-register', 'btn-facebook')}>Conectar com <strong>Facebook</strong></a>
							<a href="/auth/google" className={cx('btn-more-register', 'btn-google')}>Conectar com <strong>Google</strong></a>
						</div>
					</div>
		        </div>
	        </div>
		);
	}
}

LoginModal.propTypes = {
  id: PropTypes.string
};

export default LoginModal;
