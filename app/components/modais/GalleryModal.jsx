import React, { Component } from 'react';
import PropTypes from 'prop-types';
import createReactClass from 'create-react-class';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import Slider from 'react-slick';
import styles from '../../css/components/modal';

const cx = classNames.bind(styles);

const NextArrow = createReactClass({
	render() {
		return (
			<button type="button" {...this.props} className={cx('btn-next')}>Próxima</button>
		);
	}
});

const PrevArrow = createReactClass({
	render() {
		return (
			<button type="button" {...this.props} className={cx('btn-prev')}>Anterior</button>
		);
	}
});

class GalleryModal extends Component {
	constructor(props) {
		super(props);
		this.closeModal = this.closeModal.bind(this);
	}

	closeModal() {
		const { id } = this.props;
		document.querySelector( '#' + id ).style.display = 'none';
	}

	render() {
		const { id, place, open } = this.props;
		var settings = {
			arrows: true,
			dots: false,
			infinite: true,
			nextArrow: <NextArrow />,
			prevArrow: <PrevArrow />,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1
		};

		let galleryList;
		if(place.gallery && place.gallery.length > 0) {
			galleryList = place.gallery.map((value, index) => {
				return (value && value.responseText) ? <div className={cx('cover')} key={index}><img src={value.responseText} alt="" /></div> : ''
			});
		}
		
		return (
			<div id={id} className={cx('modal', 'gallery')} data-backdrop="false">
				<div className={cx('modal-content-full')}>
					<header>
						<button type="button" className={cx('close', 'close-white')} data-dismiss="modal" aria-label="Close" onClick={this.closeModal}><span aria-hidden="true">&times;</span></button>
						<h3 className={cx('modal-title')}><span>Galeria do<br /><strong>Boteco</strong></span></h3>
					</header>

					<div className={cx('modal-body')}>
						{(open) &&
						<Slider {...settings} className={cx('carousel')}>
							{galleryList}
						</Slider>
						}
						
						<div className={cx('place-description')}>
							<div className={cx('logo-container')}>
								<figure><img src={(place.logo && place.logo.responseText) ? place.logo.responseText : ''} alt="" /></figure>
							</div>
							<h2 className={cx('name')}>{place.name}</h2>
						</div>
					</div>
		        </div>
	        </div>
		);
	}
}

GalleryModal.propTypes = {
	id: PropTypes.string,
	place: PropTypes.object,
	open: PropTypes.bool
};

export default GalleryModal;
