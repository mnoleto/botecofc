import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cookie from 'react-cookie';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import Brasao from '../../images/icons/brasao-big.png';
import styles from '../../css/components/modal';

const cx = classNames.bind(styles);

class FavoriteTeamModal extends Component {
  constructor(props) {
    super(props);
    this.closeModal = this.closeModal.bind(this);
  }

  closeModal() {
    const { id } = this.props;
    cookie.remove('returnTo', { path: '/' });
    document.querySelector('#' + id).style.display = 'none';
  }

  render() {
    const { id, team, authenticated, openLoginModal } = this.props;

    return (
      <div id={id} className={cx('modal')} data-backdrop="false">
        <div className={cx('modal-content')}>
          <header>
            <button type="button" className={cx('close')} data-dismiss="modal" aria-label="Close" onClick={this.closeModal}><span aria-hidden="true">&times;</span></button>
          </header>
          {team ?
            <div className={cx('favorite-team-modal')}>
              <img src={team.logo.responseText} alt={team.name}/>
              <h1>{team.name}</h1>
              <p>é o seu time favorito.</p>
              <p>Os jogos do seu time aparecem<br />primeiro na página inicial.</p>

              <div className={cx('actions')}>
                <a href="/cadastro" className={cx('btn-edit')}>editar</a>
                <button onClick={this.closeModal} className={cx('btn-ok')}>ok, entendi</button>
              </div>
            </div>
            :
            <div className={cx('favorite-team-modal')}>
              <img src={Brasao} alt="" />
              <h1 className={cx('title-no-team')}>Escolha seu time Favorito.</h1>
              <p>Os jogos do seu time aparecem<br />primeiro na página inicial.</p>

              <div className={cx('actions')}>
                <button onClick={this.closeModal}  className={cx('btn-edit')}>depois</button>
                {authenticated
                  ? <a href="/cadastro" className={cx('btn-ok')}>escolher</a>
                  : <button onClick={openLoginModal} className={cx('btn-ok')}>escolher</button>
                }
              </div>
            </div>
          }
        </div>
      </div>
    );
  }
}

FavoriteTeamModal.propTypes = {
  id: PropTypes.string,
  team: PropTypes.object,
  authenticated: PropTypes.bool.isRequired,
  openLoginModal: PropTypes.func.isRequired,
};

export default FavoriteTeamModal;
