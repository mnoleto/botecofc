import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import { CitiesSelect, StatesSelect } from '../selects'
import styles from '../../css/components/modal';

const cx = classNames.bind(styles);

class SuggestPlaceModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			cityId: '',
			message: '',
			stateId: '',
		}
		this.closeModal = this.closeModal.bind(this);
		this.onEntrySave = this.onEntrySave.bind(this);
		this.renderMessage = this.renderMessage.bind(this);
		this.renderContent = this.renderContent.bind(this);
		this.handleStateChange = this.handleStateChange.bind(this)
		this.handleCityChange = this.handleCityChange.bind(this)
	}

	handleStateChange(event) {
		this.setState({
			stateId: event.target.value,
		})
	}

	handleCityChange(event) {
		this.setState({
			cityId: event.target.value,
		})
	}

	closeModal() {
		const { id } = this.props;
		document.querySelector( '#' + id ).style.display = 'none';
	}

	onEntrySave(e) {
		e.preventDefault();
		const { onEntrySaveSuggestion } = this.props;
		const { account } = this.props.user;
		const { cityId, message, stateId } = this.state

		const name = this.refs.name.value,
			address = this.refs.address.value,
			reference = this.refs.reference.value

		if(name === '') {
			this.setState({
				message: 'Preencha o nome do bar'
			})
			return false;
		}
		if(cityId === '') {
			this.setState({
				message: 'Preencha uma cidade'
			})
			return false;
		}
		if(stateId === '') {
			this.setState({
				message: 'Preencha uma ESTADO'
			})
			return false;
		}

		this.setState({
			message: ''
		})
		
		onEntrySaveSuggestion({
			place: {
				name,
				address,
				reference,
				cityId: cityId,
				stateId: stateId
			},
			user: {
				id: account._id,
				name: account.name,
				email: account.email
			}
		})
	}

	renderMessage() {
		if(this.state.message != '') {
			return (
				<div className={cx('message')}>
					<i className="zmdi zmdi-block"></i> {this.state.message}
				</div>
			);
		}
	}

	renderContent() {
		const { message, dismissMessage, cities, states } = this.props;
		if(message && message.length > 0) {
			if(message === 'success') {
				return(
					<div className={cx('modal-content')}>
						<header className={cx('modal-header')}>
							<button type="button" className={cx('close')} data-dismiss="modal" aria-label="Close" onClick={this.closeModal}><span aria-hidden="true">&times;</span></button>
						</header>
						<div className={cx('message', 'success')}>
							<i className="zmdi zmdi-check-circle"></i>
							<p>
								<strong>Sua sugestão foi enviada com sucesso!</strong><br />
								Vamos confirmar os dados para atualizar os Botecos.
							</p>
							<button type="button" className={cx('btn-back', 'btn-modal')} data-dismiss="modal" aria-label="Close" onClick={dismissMessage}>Voltar</button>
						</div>
					</div>
				);
			}
		} else {
			const filteredCities = cities.filter(city => city.stateId === this.state.stateId);

			return(
				<div className={cx('modal-content')}>
					<header className={cx('modal-header')}>
						<button type="button" className={cx('close')} data-dismiss="modal" aria-label="Close" onClick={this.closeModal}><span aria-hidden="true">&times;</span></button>
						<h3 className={cx('modal-title')}><span>Gostaria de sugerir<br /><strong>um bar</strong></span></h3>
					</header>
					<div className={cx('modal-body')}>
						<p className={cx('suggest-text')}>Caso não encontre o Bar da sua preferência, fique à vontade pra sugerir.</p>
						<form className={cx('clearfix', 'form-inline', 'modal-form')} method="post" encType="multipart/form-data" role="form" onSubmit={this.onEntrySave}>
							{this.renderMessage()}

							<div className={cx('field-group')}>
								<label htmlFor="name" className={cx('sr-only')}>Nome do bar:</label>
								<input type="text" name="name" ref="name" placeholder="Nome do Bar *" />
							</div>
							<div className={cx('field-group')}>
								<label htmlFor="address" className={cx('sr-only')}>Endereço:</label>
								<input type="text" name="address" ref="address" placeholder="Endereço" />
							</div>
							<div className={cx('field-group')}>
								<label htmlFor="reference" className={cx('sr-only')}>Referência:</label>
								<textarea name="reference" ref="reference" placeholder="Referência"></textarea>
							</div>
							<div className={cx('field-group')}>
								<div className={cx('select')}>
									<label htmlFor="name" className={cx('sr-only')}>Estado *</label>
									<select name="estado" className={cx('full')} value={this.state.stateId} onChange={this.handleStateChange}>
										<option value={0}>Selecione um estado...</option>
										{states.map(item => {
											return <option key={item.id} value={item.id}>{item.name}</option>
										})}
									</select>
								</div>
							</div>
							<div className={cx('field-group')}>
								<div className={cx('select')}>
									<label htmlFor="name" className={cx('sr-only')}>Cidade *</label>
									<select name="cidade" className={cx('full')} value={this.state.cityId} onChange={this.handleCityChange}>
										<option value={0}>Selecione uma cidade...</option>
										{filteredCities.map(item => {
											return (
												<option key={item.id} value={item.id}>{item.name}</option>
											)
										})}
									</select>
								</div>
							</div>
							<div className={cx('form-button')}>
								<button type="submit" className={cx('btn-modal', 'btn-submit')}><strong>enviar</strong></button>
							</div>
						</form>
					</div>
				</div>
			);
		}
	}

	render() {
		const { id } = this.props;
		return (
			<div id={id} className={cx('modal', 'place-suggest')} data-backdrop="false">
				{this.renderContent()}
	    </div>
		);
	}
}

SuggestPlaceModal.propTypes = {
	id: PropTypes.string,
	onEntrySaveSuggestion: PropTypes.func.isRequired,
	user: PropTypes.object.isRequired,
	message: PropTypes.string,
	dismissMessage: PropTypes.func.isRequired,
	cities: PropTypes.array.isRequired,
	states: PropTypes.array.isRequired,
};

export default SuggestPlaceModal;
