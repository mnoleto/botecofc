import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import offerCreditcard from '../../images/icons/creditcard.png';
import offerWifi from '../../images/icons/wifi.png';
import offerComanda from '../../images/icons/comanda.png';
import offerAir from '../../images/icons/air.png';
import offerGarage from '../../images/icons/garage.png';
import offerMesas from '../../images/icons/mesas.png';
import offerVallet from '../../images/icons/vallet.png';
import offerBrinquedoteca from '../../images/icons/brinquedoteca.png';
import offer24h from '../../images/icons/24h.png';
import offerDelivery from '../../images/icons/delivery.png';
import styles from '../../css/components/modal';

const cx = classNames.bind(styles);

class ServicesModal extends Component {
	constructor(props) {
		super(props);
		this.closeModal = this.closeModal.bind(this);
	}

	closeModal() {
		const { id } = this.props;
		document.querySelector( '#' + id ).style.display = 'none';
	}

	renderServices() {
		const { services } = this.props;
    	if(!services) return false;

    	let servicesList = [];
    	services.forEach((value, index) => {
    		switch(value) {
    			case 'Ar Condicionado': {
    				servicesList.push(<li key={index}><img src={offerAir} alt="Ar condicionado" /> Ar condicionado</li>);
    				break;
    			}
    			case 'Brinquedoteca': {
    				servicesList.push(<li key={index}><img src={offerBrinquedoteca} alt="Brinquedoteca" /> Brinquedoteca</li>);
    				break;
    			}
    			case 'Comanda Individual': {
    				servicesList.push(<li key={index}><img src={offerComanda} alt="Comanda Individual" /> Comanda Individual</li>);
    				break;
    			}
    			case 'Estacionamento Privativo': {
    				servicesList.push(<li key={index}><img src={offerGarage} alt="Estacionamento Privativo" /> Estacionamento Privativo</li>);
    				break;
    			}
    			case 'Manobrista': {
    				servicesList.push(<li key={index}><img src={offerVallet} alt="Manobrista" /> Manobrista</li>);
    				break;
    			}
                case 'Mesas ao ar livre': {
                    servicesList.push(<li key={index}><img src={offerMesas} alt="Mesas ao ar livre" /> Mesas ao ar livre</li>);
                    break;
                }
    			case 'Wifi': {
    				servicesList.push(<li key={index}><img src={offerWifi} alt="Wifi" /> Wifi</li>);
    				break;
    			}
    			case 'Vallet': {
    				servicesList.push(<li key={index}><img src={offerVallet} alt="Manobrista" /> Manobrista</li>);
    				break;
    			}
    		}
    	});
    	return servicesList;
    }

	render() {
		const { id } = this.props;
		
		return (
			<div id={id} className={cx('modal', 'services')} data-backdrop="false">
				<div className={cx('modal-content')}>
					<header className={cx('modal-header')}>
						<button type="button" className={cx('close')} data-dismiss="modal" aria-label="Close" onClick={this.closeModal}><span aria-hidden="true">&times;</span></button>
						<h3 className={cx('modal-title')}><span>Entenda as<br /><strong>Legendas</strong></span></h3>
					</header>
					<div className={cx('modal-body')}>
						<ul>
							{this.renderServices()}
						</ul>
			        </div>
		        </div>
	        </div>
		);
	}
}

ServicesModal.propTypes = {
	id: PropTypes.string,
	services: PropTypes.array.isRequired
};

export default ServicesModal;
