import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import { findCityById, findStateById } from '../../utils/cities'
import styles from '../../css/components/modal';

const cx = classNames.bind(styles);

const getPlaceName = (cityId, cities, states) => {
  const selected = findCityById(cityId, cities)
  if (selected == null) {
    return ''
  }
  const state = findStateById(selected.stateId, states)
  if (state == null) {
    return ''
  }
  return `${selected.name}, ${state.abbr}`
}

const LocaleItem = ({
  id,
  isSelected,
  name,
  onSelect
}) => {
  const handleOnClick = () => {
    onSelect(id)
  }
  return (
    <li className={cx('locale-item')} onClick={handleOnClick}>{name}</li>
  )
}

const LocaleModal = ({
  cities,
  closeModal,
  id,
  onSelect,
  selectedCity,
  states
}) => {
  const [place, changePlace] = useState(getPlaceName(selectedCity, cities, states))
  const [search, changeSearch] = useState('')
  const [showList, changeShowList] = useState(false)

  useEffect(() => {
    if (place !== selectedCity) {
      changePlace(getPlaceName(selectedCity, cities, states))
    }
  }, [selectedCity])
  
  if (!cities || cities.length === 0) {
    return null
  }

  const handleSearchChange = (event) => {
    changeSearch(event.target.value)
  }

  const handleSearchClick = () => {
    changeShowList(!showList)
  }

  const filteredList = cities.filter(item => {
      const value = getPlaceName(item.id, cities, states)
    if (search !== '') {
      if (value.search(new RegExp(search, "i")) !== -1) {
        return item
      }
    } else {
      if (value.search(new RegExp(place, "i")) !== -1) {
        return item
      }
    }
  }).slice(0, 9)

  return (
    <div id={id} className={cx('modal', 'modal-locale')} data-backdrop="false">
      <div className={cx('modal-content', 'locale-content')}>
        <header className={cx('modal-header', 'locale-header')}>
          <button
            type="button"
            className={cx('close')}
            data-dismiss="modal"
            aria-label="Close"
            onClick={closeModal}
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <h3 className={cx('modal-title', 'locale-title')}><span><strong>Localização</strong></span></h3>
        </header>
        <div className={cx('modal-body')}>
          <div className={cx('locale-input-wrapper')}>
            <input
              className={cx('locale-input')}
              type="text"
              value={search}
              placeholder={place}
              onChange={handleSearchChange}
              onClick={handleSearchClick}
            />
          </div>
          {filteredList.length > 0 ?
            <ul className={cx('locale-list')}>
              {filteredList.map(item => {
                const name = getPlaceName(item.id, cities, states)
                return (
                  <LocaleItem
                    key={item.id}
                    id={item.id}
                    name={name}
                    onSelect={onSelect}
                    isSelected={item.id === selectedCity}
                  />
                )
              })}
            </ul>
            : <p>Ops! Sem bares para esta cidade.</p>
          }
        </div>
      </div>
    </div>
  );
}

LocaleModal.propTypes = {
  cities: PropTypes.array.isRequired,
  closeModal: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  onSelect: PropTypes.func.isRequired,
  selectedCity: PropTypes.string,
  states: PropTypes.array.isRequired,
};

export default LocaleModal;
