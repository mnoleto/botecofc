import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import ListGamesByDay from '../games/ListGamesByDay';
import iconChampionship from '../../images/icons/championships-tab.png';
import styles from '../../css/components/modal';

const cx = classNames.bind(styles);

class ChampionshipGamesModal extends Component {
	constructor(props) {
		super(props);
		this.closeModal = this.closeModal.bind(this);
	}

	closeModal() {
		const { id } = this.props;
		document.querySelector( '#' + id ).style.display = 'none';
	}

	render() {
		const { id, championship, games, teams } = this.props;

		const filteredGames = filterGamesByChampionship(games, championship.id);
		
		return (
			<div id={id} className={cx('modal', 'place-games-list')} data-backdrop="false">
				<header className={cx('modal-header', 'gray-header')}>
					<button type="button" className={cx('close')} data-dismiss="modal" aria-label="Close" onClick={this.closeModal}><span aria-hidden="true">&times;</span></button>
					<h3 className={cx('modal-title')}><span>Lista de jogos<br /><strong>deste campeonato</strong></span></h3>
					
					<div className={cx('col-1')}>
						<div className={cx('col-container')}>
							<h4>{championship.name}</h4>
						</div>
					</div>
					<div className={cx('col-middle')}>
        				<div className={cx('col-middle-container')}>
							<figure><img src={(championship.logo) ? championship.logo.responseText : ''} /></figure>
						</div>
					</div>
					<div className={cx('col-2')}>
						<div className={cx('col-container')}>
							<Link to={'/campeonatos/' + championship.slug} role="button" className={cx('btn-championship')}>
								<i className={cx('button-icon', 'icon-championship')}><img src={iconChampionship} alt="Ver o campeonato" /></i>
								<small>Ver o</small><br /><strong>Campeonato</strong>
							</Link>
						</div>
					</div>
				</header>
				
				<div className={cx('modal-body')}>
					<ListGamesByDay
						games={filteredGames}
						teams={teams}
					/>
		        </div>
	        </div>
		);
	}
}

function filterGamesByChampionship(games, filter) {
	if (filter === '') {
		return games;
	} else {
		return games.filter((game, index) => {
			return game.championship.id === filter;
		});
	}
}

ChampionshipGamesModal.propTypes = {
	id: PropTypes.string.isRequired,
	championship: PropTypes.object.isRequired,
	teams: PropTypes.array.isRequired,
	games: PropTypes.array.isRequired
};

export default ChampionshipGamesModal;
