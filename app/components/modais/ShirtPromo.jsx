import React, { Component } from 'react';
import { Link } from 'react-router';
import Cookies from 'react-cookie';
import classNames from 'classnames/bind';
import styles from '../../css/components/promo';

const cx = classNames.bind(styles);

class ShirtPromo extends Component {
  constructor(props) {
    super(props);
    this.closeModal = this.closeModal.bind(this);
  }

  cookieSave() {
    Cookies.save('notFirst', true);
  }

  closeModal() {
    document.querySelector( '#promo' ).style.display = 'none';
  }

  render() {
    // const notFirst = Cookies.load('notFirst');
    // if (notFirst) return <span />;
    // this.cookieSave();
    return (
      <div id="promo" className={cx('modal-promo')} data-backdrop="false">
        <div className={cx('promo-content')}>
          <header className={cx('promo-header')}>
            <button type="button" className={cx('close')} data-dismiss="modal" aria-label="Close" onClick={this.closeModal}></button>
            <h3 className={cx('promo-title')}><span><strong>Olá. :)</strong><br />VOCÊ TEM UM BAR?</span></h3>
            {/*<h3 className={cx('promo-title')}><span>CONCORRA A <strong>R$ 500,00</strong> EM CONSUMAÇÃO!</span></h3>*/}
          </header>
          <div className={cx('promo-body', 'term-text')}>
            {/*<ol>
              <li><strong>1. Cadastre-se no site</strong></li>
              <li>2. VEJA as REGRAS NO post DO SORTEIO EM instagram <a href="https://www.instagram.com/botecofc/" target="_blank">@botecofc</a></li>
            </ol>*/}
            <p><strong>CADASTRE SEU ESTABELECIMENTO</strong> E DIVULGUE SUAS TRANSMISSÕES DE <strong>FUTEBOL</strong>.</p>
          </div>
          <footer>
            <a href="/cadastrar-bar" className={cx('cadastrar-button')} onClick={this.closeModal}>Cadastrar</a>
          </footer>
        </div>
      </div>
    );
  }
}

export default ShirtPromo;
