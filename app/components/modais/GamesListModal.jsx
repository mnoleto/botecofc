import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import ListGamesByDay from '../games/ListGamesByDay';
import iconPlaces from '../../images/icons/bar.png';
import styles from '../../css/components/modal';

const cx = classNames.bind(styles);

class GamesListModal extends Component {
	constructor(props) {
		super(props);
		this.closeModal = this.closeModal.bind(this);
	}

	closeModal() {
		const { id } = this.props;
		document.querySelector( '#' + id ).style.display = 'none';
	}

	render() {
		const { id, games, place, teams } = this.props;
		return (
			<div id={id} className={cx('modal', 'place-games-list')} data-backdrop="false">
				<header className={cx('modal-header', 'gray-header')}>
					<button type="button" className={cx('close')} data-dismiss="modal" aria-label="Close" onClick={this.closeModal}><span aria-hidden="true">&times;</span></button>
					<h3 className={cx('modal-title')}><span>Lista de jogos<br /><strong>deste bar</strong></span></h3>
					
					<div className={cx('col-1')}>
						<div className={cx('col-container')}>
							<h4>{place.name}</h4>
						</div>
					</div>
					<div className={cx('col-middle')}>
        			<div className={cx('col-middle-container')}>
							<div><img src={(place.logo) ? place.logo.responseText : ''} /></div>
						</div>
					</div>
					<div className={cx('col-2')}>
						<div className={cx('col-container')}>
							<Link to={'/bares/' + place.slug} className={cx('btn-place')} onClick={this.closeModal}>
								<i className={cx('button-icon', 'icon-place')}><img src={iconPlaces} alt="Ver o bar" /></i>
								<small>Ver o</small><br /><strong>bar</strong>
							</Link>
						</div>
					</div>

					<p className={cx('icon-explanation')}><span>Jogos com este ícone possuem transmissão confirmada pelo estabelecimento</span></p>
				</header>
				
				<div className={cx('modal-body')}>
					<ListGamesByDay
						games={games}
						teams={teams}
					/>
		        </div>
	        </div>
		);
	}
}

GamesListModal.propTypes = {
	id: PropTypes.string,
	place: PropTypes.object.isRequired,
	games: PropTypes.array.isRequired,
	teams: PropTypes.array.isRequired
};

export default GamesListModal;
