import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import SearchResult from '../search/SearchResult';
import styles from '../../css/components/modal';
import normalizeString from '../../utils/normalizeString';
import sortByName from '../../utils/sortByName';

const cx = classNames.bind(styles);

class SearchModal extends Component {
	constructor(props) {
		super(props);

		this.state = {
			result: {},
			typingTimeout: 0,
		};
	}

	handleChange = (e) => {
		e.preventDefault();
		const { typingTimeout } = this.state;
		const fieldText = this.refs.search.value;

		if (fieldText.length < 3) {
			return
		}

		if (typingTimeout) {
      clearTimeout(typingTimeout);
    }

    const timer = window.setTimeout(() => {
			this.onSearch(fieldText)
		}, 300);
		
		this.setState({
      typingTimeout: timer,
    });
	}

	onSearch = (searchText) => {
		const { teams, places, championships } = this.props;

		const alphabeticalPlaces = places;
		alphabeticalPlaces.sort(sortByName('name'));

		if (searchText === '') {
			this.setState({
				result: {},
			});
		} else {
			const search = normalizeString(searchText);
			
			const filteredTeams = teams ? teams.filter(team => {
				if (!team.name) return false;
				const name = normalizeString(team.name);
				return (name.includes(search));
			}) : [];

			const filteredPlaces = alphabeticalPlaces ? alphabeticalPlaces.filter(place => {
				const name = normalizeString(place.name);
				const about = normalizeString(place.about);
				const hasService = place.services.some(service => {
					const normalizedService = normalizeString(service);
					return normalizedService.includes(search)
				});
				const hasName = name !== '' && name.includes(search);
				const hasAbout = about !== '' && about.includes(search);
				return (hasName || hasAbout || hasService);
			}) : [];

			const filteredChampionships = championships ? championships.filter(championship => {
				if (!championship.name) return false;
				const name = normalizeString(championship.name);
				return (name.includes(search));
			}) : [];

			this.setState({
				result: {
					teams: filteredTeams,
					places: filteredPlaces,
					championships: filteredChampionships,
				},
			});
		}
		
		this.forceUpdate();
	}

	render() {
		const { id, games, teams, onClose } = this.props;
		
		return (
			<div id={id} className={cx('modal', 'search-modal')} data-backdrop="false">
				<header className={cx('modal-header', 'gray-header')}>
					<button type="button" className={cx('close')} data-dismiss="modal" aria-label="Close" onClick={onClose}><span aria-hidden="true">&times;</span></button>
					<div className={cx('header-content')}>
						<h3 className={cx('modal-title')}><span>Realize<br /><strong>sua busca</strong></span></h3>

						<form className={cx('clearfix', 'form-inline', 'modal-form')} method="post" encType="multipart/form-data" role="form">
							<div className={cx('row')}>
								<label htmlFor="search">O que você procura?</label>
								<div className={cx('search-field')}>
									<input type="text" ref="search" name="search" placeholder="Time, estabelecimento, campeonato, bebidas, brinquedoteca..." onChange={this.handleChange} />
									<input type="submit" value="Ok" onClick={this.handleChange} />
								</div>
								{this.refs.search && this.refs.search.value !== '' && <div className={cx('search-word')}>Encontramos "<span>{this.refs.search.value}</span>" em:</div>}
							</div>
						</form>
					</div>
				</header>
				<div className={cx('modal-body')}>
					<SearchResult
						id={id}
						result={this.state.result}
						games={games}
						teams={teams}
						onClose={onClose}
					/>
				</div>
			</div>
		);
	}
}

SearchModal.propTypes = {
  id: PropTypes.string,
  teams: PropTypes.array.isRequired,
	games: PropTypes.array.isRequired,
	places: PropTypes.array.isRequired,
	championships: PropTypes.array.isRequired,
	onClose: PropTypes.func.isRequired
};

export default SearchModal;