import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import TermsContent from '../terms/TermsContent';
import styles from '../../css/components/modal';

const cx = classNames.bind(styles);

class TermsModal extends Component {
	constructor(props) {
		super(props);
		this.closeModal = this.closeModal.bind(this);
		this.handleAcceptTerms = this.handleAcceptTerms.bind(this);
	}

	handleAcceptTerms() {
		const { onAcceptTerms } = this.props;
		this.closeModal();
		onAcceptTerms({target: {name: 'terms', checked: true}});
	}

	closeModal() {
		const { id } = this.props;
		document.querySelector( '#' + id ).style.display = 'none';
		
	}

	render() {
		const { id } = this.props;
		
		return (
			<div id={id} className={cx('modal')} data-backdrop="false">
				<div className={cx('modal-content')}>
					<header className={cx('modal-header')}>
						<button type="button" className={cx('close')} data-dismiss="modal" aria-label="Close" onClick={this.closeModal}><span aria-hidden="true">&times;</span></button>
						<h3 className={cx('modal-title')}><span><strong>Termos, condições e avisos do site Qual Bar</strong></span></h3>
					</header>
					<div className={cx('modal-body', 'term-text')}>
						<TermsContent />
					</div>
					{/* <footer>
						<button type="button" className={cx('btn-modal', 'btn-white')} onClick={this.handleAcceptTerms}>Aceito os termos de uso</button>
					</footer> */}
				</div>
			</div>
		);
	}
}

TermsModal.propTypes = {
  id: PropTypes.string,
  onAcceptTerms: PropTypes.func.isRequired
};

export default TermsModal;