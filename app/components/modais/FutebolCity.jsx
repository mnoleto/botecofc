import React, { Component } from 'react';
import { Link } from 'react-router';
import Cookies from 'react-cookie';
import classNames from 'classnames/bind';
import styles from '../../css/components/promo';
import logo from '../../images/futebolCity.png';

const cx = classNames.bind(styles);

class FutebolCity extends Component {
  constructor(props) {
    super(props);

    this.closeModal = this.closeModal.bind(this);
  }

  cookieSave() {
    Cookies.save('futebolCity', true);
  }

  closeModal() {
    document.querySelector( '#promo' ).style.display = 'none';
  }

  render() {
    const futebolCity = Cookies.load('futebolCity');
    this.cookieSave();
    if (futebolCity) return <span />;
    return (
      <div id="promo" className={cx('modal-promo')} data-backdrop="false">
        <div className={cx('promo-content')}>
          <header className={cx('promo-header')}>
            <button type="button" className={cx('close')} data-dismiss="modal" aria-label="Close" onClick={this.closeModal}></button>
          </header>
          <div className={cx('promo-body', 'term-text')}>
            <a href="http://futebolcity.com.br" target="_blank" onClick={this.closeModal}><img src={logo} alt="Futebol City" /></a>
            <p>02 a 29 de Outubro | Parque da Cidade | Brasília/DF</p>
            <p className={cx('promo-title-white')}>PRIMEIRO <strong>PARQUE DE FUTEBOL</strong><br />ITINERANTE DO MUNDO</p>
          </div>
          <footer>
            <a href="http://futebolcity.com.br" target="_blank" className={cx('ingressos-button')} onClick={this.closeModal}>Ingressos</a>
          </footer>
        </div>
      </div>
    );
  }
}

export default FutebolCity;
