import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import citiesArray from '../../utils/cities';
import styles from '../../css/components/placeRegister';

const cx = classNames.bind(styles);

class GamesSelectedListItem extends Component {
	constructor(props) {
		super(props);
		this.removeItem = this.removeItem.bind(this);
	}

	removeItem() {
		const { id, onRemoveItem } = this.props;
		onRemoveItem(id);
	}

	render() {
		const { homeTeam, visitingTeam, gameDate } = this.props;
		let dateObj = new Date(gameDate);
		let date = dateObj.getDate() + '/' + ( dateObj.getMonth() + 1 );

		return (
			<li>
				<a onClick={this.removeItem}>
					<i className={cx('fa', 'fa-close', 'fa-fw')} aria-hidden={true}></i>&nbsp;{date} - <strong>{homeTeam.name} X {visitingTeam.name}</strong>
				</a>
			</li>
		);
	}
}

GamesSelectedListItem.propTypes = {
	id: PropTypes.string,
	homeTeam: PropTypes.object,
	visitingTeam: PropTypes.object,
	gameDate: PropTypes.string,
	onRemoveItem: PropTypes.func
};

class GamesSelectedList extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { data, onRemoveItem } = this.props;
		let list;
		if(data.length > 0) {
			list = data.filter(value => {
				return (value && value !== null)
			}).map((value, index) => {
				return(
					<GamesSelectedListItem
						key={index}
						id={value.id}
						homeTeam={value.homeTeam}
						visitingTeam={value.visitingTeam}
						gameDate={value.gameDate}
						onRemoveItem={onRemoveItem}
					/>
				);
			});
		} else {
			list = <li className={cx('text-center')}>Adicione o primeiro jogo!</li>
		}
		
		return (
			<ul className={cx('group-list')}>
				{list}
			</ul>
		);
	}
}

GamesSelectedList.propTypes = {
	data: PropTypes.array,
	onRemoveItem: PropTypes.func.isRequired
};

export default GamesSelectedList;
