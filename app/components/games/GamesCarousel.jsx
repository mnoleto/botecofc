import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import classNames from 'classnames/bind';
import FilterChampionships from '../filters/FilterChampionships';
import GameCarouselItem from '../game/GameCarouselItem';
import { sortGames } from '../../utils/sort';
import styles from '../../css/components/home';

const cx = classNames.bind(styles);

class GamesCarousel extends Component {
	constructor(props) {
		super(props);

		this.state = {
			game: null,
		};

		this.getTeamScore = this.getTeamScore.bind(this);
		this.next = this.next.bind(this);
		this.previous = this.previous.bind(this);
	}

	componentDidUpdate() {
		const { games, selectGame } = this.props;
		if (games.length > 0 && this.state.game !== games[0].id) {
			this.setState({
				game: games[0].id,
			});
			selectGame(games[0].id);
			this.settings = {
				adaptiveHeight: false,
				arrows: false,
				dots: false,
				infinite: true,
				speed: 500,
				slidesToShow: 1,
				slidesToScroll: 1,
				afterChange: (index) => {
					const gameid = games[index].id;
					selectGame(gameid);
				},
			};
		} else if (games.length === 0 && this.state.game !== null) {
			this.setState({
				game: null,
			});
			selectGame(null);
		}
	}

	next() {
		this.slider.slickNext();
	}

	previous() {
		this.slider.slickPrev();
	}

	getTeamScore(teamId) {
		const { teams } = this.props;
		let selectedTeam = teams.filter((team) => (team.id === teamId));
		return (selectedTeam.score) ? selectedTeam.score : 0;
	}

	render () {
		const {
			championships,
			updateChampionshipFilter,
			gameVisibilityFilter,
			games,
			teams,
			account,
		} = this.props;
		
		const favoriteTeam = (account.profile && account.profile.team && account.profile.team.id) ? account.profile.team : undefined;
		const sortedGames = sortGames(games, favoriteTeam);

		let gameDisplay;
		if(games && games.length > 0) {
			gameDisplay = sortedGames.map((game, index) => {
				return (
					<div key={game.id} data-gameid={game.id} className={cx('game-item')}>
						<GameCarouselItem
							game={game}
							teams={teams}
						/>
					</div>
				);
			});
		}

		return (
			<div className={cx('games-carousel')} ref={this.carousel}>
				<FilterChampionships
					classnames={'home'}
					championship={gameVisibilityFilter.championship}
					championships={championships}
					onFilter={updateChampionshipFilter}
					/>
				{(games.length > 0 && this.settings) &&
				<div>
					<Slider {...this.settings} className={cx('carousel')} ref={c => (this.slider = c)}>
						{gameDisplay}
					</Slider>
					<button className="button" className={cx('btn-prev')} onClick={this.previous}>
							Anterior
					</button>
					<button className="button" className={cx('btn-next')} onClick={this.next}>
						Próxima
					</button>
				</div>
				}
				{(games.length === 0) &&
				<div className={cx('empty-game')}>Nenhum jogo encontrado com os filtros selecionados. <strong>Você também pode buscar direto pelo seu time na lupa acima.</strong></div>
				}
			</div>
		);
	}
};

GamesCarousel.propTypes = {
	championships: PropTypes.array.isRequired,
	updateChampionshipFilter: PropTypes.func.isRequired,
	gameVisibilityFilter: PropTypes.object.isRequired,
	games: PropTypes.array,
	places: PropTypes.array,
	teams: PropTypes.array,
	selectGame: PropTypes.func.isRequired,
	account: PropTypes.object.isRequired,
};

export default GamesCarousel;
