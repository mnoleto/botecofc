import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import GameItem from '../game/GameItem';
import styles from '../../css/components/list';

const cx = classNames.bind(styles);

class ListGamesByDay extends Component {
	constructor(props) {
		super(props);

		this.date;

		this.renderDate = this.renderDate.bind(this);
	}

	renderDate() {
		const { games, teams } = this.props;
		const _ = this;
		const monthNames = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];

		if(!games || games.length === 0) return false;

		let byday = {};
		function groupday(value, index) {
		    let d = new Date(value['gameDate']);
		    d = parseInt(
		    	d.getFullYear().toString() +
		    	(d.getMonth() < 10 ? '0' + d.getMonth().toString() : d.getMonth().toString()) +
		    	(d.getDate() < 10 ? '0' + d.getDate().toString() : d.getDate().toString())
		    );
		    byday[d]=byday[d]||[];
		    byday[d].push(value);
		}
		games.map(groupday);

		var group = [];
		for(var groupDay in byday) {
			if( byday.hasOwnProperty(groupDay) ) {
				group.push({
					date: byday[groupDay][0].gameDate,
					games: byday[groupDay]
				});
			}
		}
		
		var html = new Array();
		group.forEach((value, index) => {
			let gameDate = new Date(value.date);
			html.push( <div className={cx('game-day-title')} key={index + value.date}><strong>{gameDate.getDate()}</strong> { monthNames[ gameDate.getMonth() ] }</div> );
			
			const list = value.games.map((game, i) => {
				return (
					<li key={game.id}>
					    <GameItem
					      game={game}
					      teams={teams}
					    />
					</li>
				)
			});
			html.push(<ol key={index}>
				{list}
			</ol>);
			
		});
		return html;
	}

	render() {
		return (
			<div className={cx('list-container')}>
				{this.renderDate()}
			</div>
		);
	}
};

ListGamesByDay.propTypes = {
	games: PropTypes.array.isRequired,
	teams: PropTypes.array.isRequired
};

export default ListGamesByDay;
