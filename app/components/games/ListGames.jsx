import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import GameItem from '../game/GameItem';
import styles from '../../css/components/list';

const cx = classNames.bind(styles);

class ListGames extends Component {
	constructor(props) {
		super(props);
		this.renderTitle = this.renderTitle.bind(this);
	}

	renderTitle() {
		const { title } = this.props;
		
		if(title && title.length > 0) {
			const text = title.map((textOrHTML, index) => {
				return <span key={index}>{textOrHTML}</span>;
			});
			return (
				<header>
					<h2 className={cx('title-list')}>{text}</h2>
				</header>
			);
		}
	}

	render() {
		const { games, teams, showAll } = this.props;
		const _this = this;

		const dateGames = games;
		dateGames.sort((a, b) => {
			return new Date(a.gameDate).getTime() - new Date(b.gameDate).getTime();
		});
		const listItems = games.map((game, index) => {
			// if(!showAll && index > 9) {
			// 	return;
			// } else {
				return (
					<li key={index}>
						<GameItem
							game={game}
							teams={teams}
						/>
					</li>
				);
			// }
		});

		let button;
		if(!showAll && games.length >= 9) {
			button = <footer>
					<Link to="/" className={cx('btn-more')}>mais jogos</Link>
				</footer>
		}
		return (
			<div className={cx('list-container')}>
				{this.renderTitle()}

				<div className={cx('list-content')}>
					<ul className={cx('list')}>
						{listItems}
					</ul>
				</div>

				{/*button*/}
			</div>
		);
	}
};

ListGames.propTypes = {
	title: PropTypes.array,
	games: PropTypes.array.isRequired,
	teams: PropTypes.array.isRequired,
	showAll: PropTypes.bool
};

export default ListGames;
