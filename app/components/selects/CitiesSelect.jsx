import classNames from 'classnames/bind';
import PropTypes from 'prop-types'
import React from 'react'
import styles from '../../css/admin/form';

const cx = classNames.bind(styles);

const CitiesSelect = ({
  disabled = false,
  onChange,
  selectedCity,
  cities,
}) => {
  // if (cities == null || cities.length === 0) {
  //   return null
  // }
  return (
    <div className={cx('field-group', 'col-md-full')}>
      <label htmlFor="name" className={cx('sr-only')}>Cidade *</label>
      <select name="cidade" className={cx('full')} value={selectedCity} onChange={onChange} disabled={disabled}>
        <option value={0}>Selecione uma cidade...</option>
        {cities.map(item => {
          return (
            <option key={item.id} value={item.id}>{item.name}</option>
          )
        })}
      </select>
    </div>
  )
}

CitiesSelect.propTypes = {
  onChange: PropTypes.func.isRequired,
  selectedCity: PropTypes.string,
  cities: PropTypes.array.isRequired,
}

export default CitiesSelect
