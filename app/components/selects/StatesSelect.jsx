import classNames from 'classnames/bind';
import PropTypes from 'prop-types'
import React from 'react'
import styles from '../../css/admin/form';

const cx = classNames.bind(styles);

const StatesSelect = ({
  disabled = false,
  onChange,
  selectedState,
  states,
}) => {
  if (states == null || states.length === 0) {
    return null
  }
  return (
    <div className={cx('field-group', 'col-md-full')}>
      <label htmlFor="name" className={cx('sr-only')}>Estado *</label>
      <select name="estado" className={cx('full')} value={selectedState} onChange={onChange} disabled={disabled}>
        <option value={0}>Selecione um estado...</option>
        {states.map(item => {
          return <option key={item.id} value={item.id}>{item.name}</option>
        })}
      </select>
    </div>
  )
}

StatesSelect.propTypes = {
  onChange: PropTypes.func.isRequired,
  selectedState: PropTypes.string,
  states: PropTypes.array.isRequired,
}

export default StatesSelect
