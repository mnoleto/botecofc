import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from '../css/common/icons';

const cx = classNames.bind(styles);

const Icon = ({name, size}) => {
	const sizeClass = 'icon-' + size;
	return (
		<i
			className={cx('icon', name, sizeClass)}
			aria-hidden={true}
		/>
	);
}

Icon.propTypes = {
  name: PropTypes.string,
  size: PropTypes.string
};

export default Icon;
