import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import { fetchTeams } from '../../actions/teams';
import { fetchCountries } from '../../actions/countries';
import { fetchGames } from '../../actions/games';
import FilterCountry from '..//filters/FilterCountry';
import ListTeams from '../teams/ListTeams';
import styles from '../../css/components/teams';

const cx = classNames.bind(styles);

class Teams extends Component {
    static need = [
        fetchTeams,
        fetchGames,
        fetchCountries
    ];

    constructor(props) {
        super(props);
        this.state = {
            country: '',
        }
        this.updateCountryFilter = this.updateCountryFilter.bind(this);
    }

    updateCountryFilter(value) {
        const { setTeamsVisibilityFilter } = this.props;
        this.state.country = value;
        setTeamsVisibilityFilter('SHOW_ONLY', value);
    }

    render() {
        const { teams, countries, games } = this.props;
        return (
            <div className={cx('teams')}>
                <FilterCountry
                    countries={countries}
                    onFilter={this.updateCountryFilter}
                />

                <ListTeams
                    title={['Times']}
                    teams={teams}
                    games={games}
                    showAll={true}
                />
            </div>
        );
    }

};

Teams.propTypes = {
    countries: PropTypes.array.isRequired,
    teams: PropTypes.array.isRequired,
    games: PropTypes.array.isRequired,
    setTeamsVisibilityFilter: PropTypes.func.isRequired
};

export default Teams;
