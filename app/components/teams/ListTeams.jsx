import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import sortByName from '../../utils/sortByName';
import TeamItem from '../team/TeamItem';
import styles from '../../css/components/list';

const cx = classNames.bind(styles);

class ListTeams extends Component {
	constructor(props) {
		super(props);
		this.findGameByTeam = this.findGameByTeam.bind(this);
		this.renderTitle = this.renderTitle.bind(this);
	}

	findGameByTeam(teamID) {
		const { games } = this.props;
		const dateGames = games;
		dateGames.sort((a, b) => {
			return new Date(a.gameDate).getTime() - new Date(b.gameDate).getTime();
		});
		return dateGames.find((game, index) => {
			return (teamID === game.visitingTeam.id || teamID === game.homeTeam.id);
		});
	}

	renderTitle() {
		const { title } = this.props;
		if (title && title.length > 0) {
			const text = title.map((textOrHTML, index) => {
				return <span key={index}>{textOrHTML}</span>;
			});
			return (
				<header>
					<h2 className={cx('title-list')}>{text}</h2>
				</header>
			);
		}
	}

	render() {
		const { teams, games, showAll } = this.props;
		const _ = this;
		const alphabeticalTeams = teams;
		alphabeticalTeams.sort(sortByName('name'));
		const listItem = alphabeticalTeams.map((team, index) => {
			if (!showAll && index > 9) {
				return;
			} else {
				return(
					<li key={index} style={{backgroundColor: team.color}}>
						<TeamItem
							team={team}
							game={_.findGameByTeam(team.id)}
						/>
					</li>
				);
			}
		});

		let button;
		if (!showAll && teams.length >= 9) {
			button = (
				<footer>
					<Link to="/times" className={cx('btn-more')}>mais times</Link>
				</footer>
			);
		}
		return (
			<div className={cx('list-container')}>
				{this.renderTitle()}
				<div className={cx('list-content')}>
					<ul className={cx('team-list')}>
						{listItem}
					</ul>
				</div>
				{button}
			</div>
		);
	}
};

ListTeams.propTypes = {
	title: PropTypes.array,
	teams: PropTypes.array.isRequired,
	games: PropTypes.array.isRequired,
	showAll: PropTypes.bool
};

export default ListTeams;
