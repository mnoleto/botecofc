import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import { fetchChampionships } from '../../actions/championships';
import { fetchGames } from '../../actions/games';
import { fetchTeams } from '../../actions/teams';
import ListGames from '../games/ListGames';
import styles from '../../css/components/championships';

const cx = classNames.bind(styles);

class Championship extends Component {
    static need = [
        fetchChampionships,
        fetchGames,
        fetchTeams,
    ];

    constructor(props) {
        super(props);

        this.state = {
            slug: '',
            championship: {}
        };
    }

    componentDidMount() {
        const { championships, params } = this.props;
        if (params.slug) {
            if (params.slug !== this.state.slug && championships.length > 0) {
                this.setState({ slug: params.slug });
                this.getChampionshipBySlug(params.slug);
            }
        }
    }

    getChampionshipBySlug(slug) {
        const { championships } = this.props;
        let championshipSelected;
        championships.some((item, index) => {
            if (item.slug === slug) {
                championshipSelected = item;
                return;
            }
        });
        this.setState({ championship: championshipSelected });
        this.forceUpdate();
    }

    getMonth(value) {
        switch (value) {
            case 0: return 'JAN';
            case 1: return 'FEV';
            case 2: return 'MAR';
            case 3: return 'ABR';
            case 4: return 'MAI';
            case 5: return 'JUN';
            case 6: return 'JUL';
            case 7: return 'AGO';
            case 8: return 'SET';
            case 9: return 'OUT';
            case 10: return 'NOV';
            case 11: return 'DEZ';
        }
    }

    render() {
        const { games, teams } = this.props;
        let imageURL = (this.state.championship.logo) ? this.state.championship.logo.responseText : '';
        let dateBegin = (this.state.championship && this.state.championship.dateBegin) ? new Date(this.state.championship.dateBegin) : undefined;
        let dateEnd = (this.state.championship && this.state.championship.dateEnd) ? new Date(this.state.championship.dateEnd) : undefined;

        const filteredGames = games.filter((game) => {
            if (game.championship.id === this.state.championship.id) {
                return game;
            }
        });
        return (
            <div className={cx('championship')}>
                <div className={cx('championship-content')}>
                    <figure className={cx('championship-logo-lg')}><img src={imageURL} alt={this.state.championship.name} /></figure>

                    <header className={cx('championship-name')}>
                        <h1>{this.state.championship.name}</h1>
                    </header>

                    <div className={cx('championship-box')}>
                        <h3>Período</h3>
                        <div className={cx('championship-date')}>
                            <div className={cx('initial-date')}>
                                {(dateBegin) && <span>{dateBegin.getDate()} <small>{this.getMonth(dateBegin.getMonth())}</small></span>}
                            </div>
                            <div className={cx('date-between')}>até</div>
                            <div className={cx('final-date')}>
                                {(dateEnd) && <span>{dateEnd.getDate()} <small>{this.getMonth(dateEnd.getMonth())}</small></span>}
                            </div>
                        </div>

                        <div className={cx('championship-status')}>
                            <div className={cx('active')}>Em Andamento</div>
                        </div>
                    </div>

                    <ListGames
                        title={['Jogos para os próximos dias']}
                        games={filteredGames}
                        teams={teams}
                        showAll={true}
                    />
                </div>
            </div>
        );
    }
};

Championship.propTypes = {
    params: PropTypes.object.isRequired,
    championships: PropTypes.array.isRequired,
    games: PropTypes.array.isRequired,
    teams: PropTypes.array.isRequired,
};

export default Championship;