import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from '../../css/components/list';
import iconGame from '../../images/icons/games-place-list.png';

const cx = classNames.bind(styles);

class ChampionshipItem extends Component {
	constructor(props) {
		super(props);
		this.handleSelectChampionship = this.handleSelectChampionship.bind(this);
	}

	handleSelectChampionship() {
		const { championship, onSelectChampionship } = this.props;
		onSelectChampionship(championship);
	}

	render() {
		const { championship } = this.props;

		return (
			<div className={cx('list-item', 'championship-item')}>
				<div className={cx('list-col-1')}>
					<div className={cx('col-container')}>
						<h3 className={cx('title-champ')}>
							<Link to={'/campeonatos/' + championship.slug}>{championship.name}</Link>
						</h3>
					</div>
				</div>

				<div className={cx('list-middle')}>
					<div className={cx('list-middle-container')}>
						<Link to={'/campeonatos/' + championship.slug} className={cx('logo-container')}><img src={(championship.logo) ? championship.logo.responseText : ''} alt="" className={cx('logo-championship')} /></Link>
					</div>
				</div>

				<div className={cx('list-col-2')}>
					<div className={cx('col-container')}>
						<button type="button" className={cx('btn-game-list')} onClick={this.handleSelectChampionship}>
							<i className={cx('list-icon', 'listplace-icon-game')}><img src={iconGame} alt="Lista de jogos" /></i>
							<small>lista de</small><br /><strong>jogos</strong>
						</button>
					</div>
				</div>
			</div>
		);
	}
};

ChampionshipItem.propTypes = {
	championship: PropTypes.object.isRequired,
	onSelectChampionship: PropTypes.func.isRequired
};

export default ChampionshipItem;
