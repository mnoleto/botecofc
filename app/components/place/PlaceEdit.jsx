import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import sortByName from '../../utils/sortByName';
import PlaceEditForm from '../place/PlaceEditForm';
import { fetchCities } from '../../actions/cities'
import { fetchStates } from '../../actions/states'
import { fetchPlaces } from '../../actions/places';
import styles from '../../css/components/placeRegister';

const cx = classNames.bind(styles);

class PlaceEdit extends Component {
    static need = [
        fetchPlaces,
        fetchCities,
        fetchStates
    ];

    constructor(props) {
        super(props);
        this.state = {
            place: {},
        };

        this.getPlaceById = this.getPlaceById.bind(this);
        this.handlePlaceChange = this.handlePlaceChange.bind(this);
        this.renderSelector = this.renderSelector.bind(this);
    }

    getPlaceById(id) {
        const { places } = this.props;
        return places.find(place => place.id === id);
    }

    handlePlaceChange() {
        const place = this.refs.places.value;
        if (place === '0') {
            this.setState({ place: {} });
            document.querySelector('#place-form').style.display = 'none';
        } else {
            this.setState({ place: this.getPlaceById(place) });
            document.querySelector('#place-form').style.display = 'block';
        }
    }

    renderSelector() {
        const { message, places } = this.props;

        const alphabeticalPlaces = places;
        alphabeticalPlaces.sort(sortByName('name'));
        const placesList = alphabeticalPlaces.map(place => {
            return <option key={place.id} value={place.id}>{place.name}</option>
        });

        if (!message) {
            return <div className={cx('row', 'place-register-container')}>
                <div className={cx('col-md-6', 'col-md-offset-3', 'col-sm-12', 'col-xs-12')}>
                    <div className={cx('field-group')}>
                        <select ref="places" className={cx('full')} onChange={this.handlePlaceChange}>
                            <option value="0">Selecione o estabelecimento</option>
                            {placesList}
                        </select>
                    </div>
                </div>
            </div>
        }
        return;
    }

    render() {
        const { updatePlace, cities, message, states, dismissMessage } = this.props;
        const { account } = this.props.user;

        return (
            <div className={cx('place-register')}>
                <header className={cx('register-header')}>
                    <h1 className={cx('title')}>Editar Bares</h1>
                </header>

                {this.renderSelector()}


                <div id="place-form" className={cx('place-form-container')}>
                    <PlaceEditForm
                        cities={cities}
                        states={states}
                        place={this.state.place}
                        message={message}
                        dismissMessage={dismissMessage}
                        entryUpdate={updatePlace}
                        user={account}
                    />
                </div>
            </div>
        );
    }
}

PlaceEdit.propTypes = {
    cities: PropTypes.array.isRequired,
    states: PropTypes.array.isRequired,
    message: PropTypes.string,
    places: PropTypes.array.isRequired,
    updatePlace: PropTypes.func.isRequired,
    dismissMessage: PropTypes.func.isRequired
};

export default PlaceEdit;
