import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from '../../css/components/places';

const cx = classNames.bind(styles);

const PlaceCarouselItem = ({ image, name, slug, confirmed }) => {
  return (
    <div className={cx('place', confirmed)}>
      <Link to={'bares/' + slug}>
        <figure className={cx('rounded')}><img src={image} alt={name} /></figure>
        <span>{name}</span>
      </Link>
    </div>
  );
};

PlaceCarouselItem.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  confirmed: PropTypes.string
};

export default PlaceCarouselItem;
