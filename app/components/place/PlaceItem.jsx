import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from '../../css/components/list';
import iconGame from '../../images/icons/games-place-list.png';

const cx = classNames.bind(styles);

class PlaceItem extends Component {
	constructor(props) {
		super(props);
		this.showList = this.showList.bind(this);
	}

	showList() {
		const { place, onSelectPlace } = this.props;
		onSelectPlace(place);
	}

	onClick = () => {
		if (this.props.onClick) {
			this.props.onClick();
		}
	}

	render() {
		const { place, showConfirmed } = this.props;
		const confirmedClass = (place.confirmed && showConfirmed) ? 'place-confirmed' : '';
		return (
		    <div className={cx('list-item', 'place-item', confirmedClass)}>
		    	<div className={cx('list-col-1')}>
		    		<div className={cx('col-container')}>
						<Link to={"/bares/" + place.slug} className={cx('name')} onClick={this.onClick}>{place.name}</Link>
						<div className={cx('distance')}>{place.city}</div>
					</div>
				</div>
				
				<div className={cx('list-middle')}>
					<div className={cx('list-middle-container')}>
						<Link to={"/bares/" + place.slug} className={cx('logo-container')} onClick={this.onClick}><img src={(place.logo) ? place.logo.responseText : ''} alt="" className={cx('logo-place')} /></Link>
					</div>
				</div>
				
				<div className={cx('list-col-2')}>
					<div className={cx('col-container')}>
						<button type="button" className={cx('btn-game-list')} data-toggle="modal" onClick={this.showList}>
							<i className={cx('list-icon', 'listplace-icon-game')}><img src={iconGame} alt="Lista de jogos" /></i>
							<small>lista de</small><br /><strong>jogos</strong>
						</button>
					</div>
				</div>
		    </div>
		);
	}
};

PlaceItem.propTypes = {
	place: PropTypes.object.isRequired,
	onSelectPlace: PropTypes.func.isRequired,
	showConfirmed: PropTypes.bool,
	onClick: PropTypes.func,
};

export default PlaceItem;
