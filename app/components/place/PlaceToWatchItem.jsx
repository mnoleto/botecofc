import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from '../../css/components/list';
import iconGame from '../../images/icons/places-list.png';

const cx = classNames.bind(styles);

class PlaceToWatchItem extends Component {
	constructor(props) {
		super(props);
		this.handleRemoveClick = this.handleRemoveClick.bind(this);
	}

	handleRemoveClick() {
		const { id, removePlace } = this.props;
		removePlace(id);
	}

	render() {
		const { id, slug, name, image } = this.props;
		return (
		    <div className={cx('list-item', 'place-item')}>
		    	<div className={cx('list-col-1')}>
		    		<div className={cx('col-container')}>
						<Link to={"bares/" + slug} className={cx('name')}>{name}</Link>
						{/*<div className={cx('distance')}>1,6 km</div>*/}
					</div>
				</div>

				<div className={cx('list-middle')}>
					<div className={cx('list-middle-container')}>
						<Link to={"bares/" + slug} className={cx('logo-container')}><img src={image} alt="" className={cx('logo-place')} /></Link>
					</div>
				</div>

				<div className={cx('list-col-2')}>
					<div className={cx('col-container')}>
						<button type="button" className={cx('btn-game-list')} onClick={this.handleRemoveClick}>
							<i className={cx('list-icon', 'listplace-icon-game')}><img src={iconGame} alt="Ver o Bar" /></i>
							<small>remover</small><br /><strong>o bar</strong>
						</button>
					</div>
				</div>
		    </div>
		);
	}
};

PlaceToWatchItem.propTypes = {
	id: PropTypes.string.isRequired,
	image: PropTypes.string.isRequired,
	slug: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	removePlace: PropTypes.func.isRequired
};

export default PlaceToWatchItem;
