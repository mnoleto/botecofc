import React, { Component } from 'react';
import cookie from 'react-cookie'
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import PlaceNextGame from '../place/PlaceNextGame';
import LoginModal from '../modais/LoginModal';
import MessageModal from '../modais/MessageModal';
import ServicesModal from '../modais/ServicesModal';
import GalleryModal from '../modais/GalleryModal';
import GamesListModal from '../modais/GamesListModal';
import { fetchCities } from '../../actions/cities';
import { fetchStates } from '../../actions/states';
import { fetchPlaces } from '../../actions/places';
import { fetchGames } from '../../actions/games';
import { fetchTeams } from '../../actions/teams';

import iconMap from '../../images/icons/map.png';
import iconOwner from '../../images/icons/owner.png';
import iconPhone from '../../images/icons/phone.png';
import iconWebsite from '../../images/icons/website.png';
import offerWifi from '../../images/icons/wifi.png';
import offerComanda from '../../images/icons/comanda.png';
import offerAir from '../../images/icons/air.png';
import offerGarage from '../../images/icons/garage.png';
import offerVallet from '../../images/icons/vallet.png';
import offerMesas from '../../images/icons/mesas.png';
import offerBrinquedoteca from '../../images/icons/brinquedoteca.png';
import payVisa from '../../images/flags/visa.png';
import payMaster from '../../images/flags/mastercard.png';
import payMaestro from '../../images/flags/maestro.png';
import payElo from '../../images/flags/elo.png';
import payAmericanexpress from '../../images/flags/americanexpress.png';
import payDiners from '../../images/flags/diners.png';
import payAleloRefeicao from '../../images/flags/alelo-refeicao.png';
import payValeSodexo from '../../images/flags/sodexo.png';
import payValeRestaurante from '../../images/flags/ticket-restaurante.png';
import payValeCard from '../../images/flags/vale-card.png';
import payVRSmart from '../../images/flags/vr.png';
import FacebookDark from '../../images/icons/facebook-dark.png';
import TwitterDark from '../../images/icons/twitter-dark.png';
import InstagramDark from '../../images/icons/instagram-dark.png';
import GooglePlusDark from '../../images/icons/google-plus-dark.png';

import styles from '../../css/components/places';

const cx = classNames.bind(styles);

class Place extends Component {
    static need = [
        fetchCities,
        fetchStates,
        fetchPlaces,
        fetchGames,
        fetchTeams
    ];

    constructor(props) {
        super(props);

        this.state = {
            slug: '',
            message: {
                title: '',
                body: '',
            },
            place: {},
        };

        this.galleryOpen = false;
        this.formatPhone = this.formatPhone.bind(this);
        this.getPlaceBySlug = this.getPlaceBySlug.bind(this);
        this.getTeamById = this.getTeamById.bind(this);
        this.onClickOwner = this.onClickOwner.bind(this);
        this.openGalleryModal = this.openGalleryModal.bind(this);
        this.openServiceModal = this.openServiceModal.bind(this);
        this.renderGallery = this.renderGallery.bind(this);
        this.renderServices = this.renderServices.bind(this);
        this.renderTeamOficial = this.renderTeamOficial.bind(this);
        this.handleLocaleSelection = this.handleLocaleSelection.bind(this);
    }

    componentDidMount() {
        const { places, params } = this.props;
        if (params.slug) {
            if (params.slug !== this.state.slug && places.length > 0) {
                this.setState({ slug: params.slug });
                this.getPlaceBySlug(params.slug);
            }
        }
    }

    getPlaceBySlug(slug) {
        const { places } = this.props;
        const placeSelected = places.find(item => {
            return (item.slug === slug)
        });
        if (!placeSelected) {
            return
        }
        this.handleLocaleSelection(placeSelected.cityId)
        this.setState({ place: placeSelected });
        this.forceUpdate();
    }

    getTeamById(id) {
        const { teams } = this.props;
        return teams.find(value => value.id === id);
    }

    onClickOwner(e) {
        e.preventDefault();
        const { account } = this.props.user;
        const { createOwnership } = this.props;

        // requisitar boteco pro usuário
        const data = {
            place: {
                id: this.state.place.id,
                name: this.state.place.name,
            },
            user: {
                name: account.name,
                email: account.email,
                role: account.role,
            },
        };
        createOwnership(data);

        this.setState({
            message: {
                title: "Solicitação enviada!",
                body: "Nossa equipe irá analisar sua solicitação e em breve você poderá gerenciar o seu estabelecimento.",
            },
        });
        document.querySelector('#messagemodal').style.display = 'block';
    }

    openServiceModal(e) {
        e.preventDefault();
        document.querySelector('#servicesmodal').style.display = 'block';
    }

    openGalleryModal(e) {
        e.preventDefault();
        if (!this.state.place.gallery || this.state.place.gallery.length === 0) return false;

        this.galleryOpen = true;
        document.querySelector('#galleryModal').style.display = 'block';
        this.forceUpdate();
    }

    renderCards() {
        let cards = [];
        if (!this.state.place || !this.state.place.cards) return false;
        this.state.place.cards.forEach((value, index) => {
            switch (value) {
                case 'Visa': {
                    cards.push(<li key={index}><img src={payVisa} alt="Visa" /></li>);
                    break;
                }
                case 'Mastercard': {
                    cards.push(<li key={index}><img src={payMaster} alt="Master Card" /></li>);
                    break;
                }
                case 'Maestro': {
                    cards.push(<li key={index}><img src={payMaestro} alt="Maestro" /></li>);
                    break;
                }
                case 'Elo': {
                    cards.push(<li key={index}><img src={payElo} alt="Elo" /></li>);
                    break;
                }
                case 'American Express': {
                    cards.push(<li key={index}><img src={payAmericanexpress} alt="American Express" /></li>);
                    break;
                }
                case 'Diners Club': {
                    cards.push(<li key={index}><img src={payDiners} alt="Diners Club" /></li>);
                    break;
                }
                case 'Vale - Alelo Refeição / Visa Vale': {
                    cards.push(<li key={index}><img src={payAleloRefeicao} alt="Vale - Alelo Refeição / Visa Vale" /></li>);
                    break;
                }
                case 'Vale - Sodexo': {
                    cards.push(<li key={index}><img src={payValeSodexo} alt="Vale - Sodexo" /></li>);
                    break;
                }
                case 'Vale - Ticket Restaurante': {
                    cards.push(<li key={index}><img src={payValeRestaurante} alt="Vale - Ticket Restaurante" /></li>);
                    break;
                }
                case 'Vale - Vale Card': {
                    cards.push(<li key={index}><img src={payValeCard} alt="Vale - Vale Card" /></li>);
                    break;
                }
                case 'Vale - VR Smart': {
                    cards.push(<li key={index}><img src={payVRSmart} alt="Vale - VR Smart" /></li>);
                    break;
                }
            }
        });
        return cards;
    }

    renderGallery() {
        const { place } = this.state;
        if (place && place.gallery && place.gallery[0]) {
            const bgStyle = (place && place.gallery && place.gallery[0] && place.gallery[0].responseText)
                ? {
                    'backgroundImage': `url(${place.gallery[0].responseText.replace(/ /g, '%20')})`,
                    'backgroundSize': 'cover',
                }
                : {};
            return (
                <a className={cx('cover')} onClick={this.openGalleryModal} style={bgStyle}>
                    <img src={(place && place.gallery && place.gallery[0]) ? place.gallery[0].responseText : ''} alt="" />
                </a>
            );
        } else {
            return (
                <div className={cx('cover')}></div>
            );
        }
    }

    renderOpening() {
        if (!this.state.place || !this.state.place.opening) return false;
        let opening = [];

        this.state.place.opening.forEach((value, index) => {
            if (value.dayend != '') {
                opening.push(<li key={index}>{value.daystart + ' - ' + value.dayend + ' - ' + value.hourstart + ' - ' + value.hourend}</li>);
            } else {
                opening.push(<li key={index}>{value.daystart + ' - ' + value.hourstart + ' - ' + value.hourend}</li>);
            }
        });
        return opening;
    }

    renderServices() {
        if (!this.state.place || !this.state.place.services) return false;

        let services = [];
        const openServiceModal = this.openServiceModal;

        this.state.place.services.forEach((value, index) => {
            switch (value) {
                case 'Ar Condicionado': {
                    services.push(<li key={index}><a onClick={openServiceModal}><img src={offerAir} alt="Ar condicionado" /></a></li>);
                    break;
                }
                case 'Brinquedoteca': {
                    services.push(<li key={index}><a onClick={openServiceModal}><img src={offerBrinquedoteca} alt="Brinquedoteca" /></a></li>);
                    break;
                }
                case 'Comanda Individual': {
                    services.push(<li key={index}><a onClick={openServiceModal}><img src={offerComanda} alt="Comanda Individual" /></a></li>);
                    break;
                }
                case 'Estacionamento Privativo': {
                    services.push(<li key={index}><a onClick={openServiceModal}><img src={offerGarage} alt="Estacionamento Privativo" /></a></li>);
                    break;
                }
                case 'Manobrista': {
                    services.push(<li key={index}><a onClick={openServiceModal}><img src={offerVallet} alt="Manobrista" /></a></li>);
                    break;
                }
                case 'Mesas ao ar livre': {
                    services.push(<li key={index}><a onClick={openServiceModal}><img src={offerMesas} alt="Mesas ao ar livre" /></a></li>);
                    break;
                }
                case 'Wifi': {
                    services.push(<li key={index}><a onClick={openServiceModal}><img src={offerWifi} alt="Wifi" /></a></li>);
                    break;
                }
            }
        });
        return services;
    }

    renderTeamOficial() {
        if (!this.state.place || !this.state.place.teamOficial || !this.state.place.teamOficial.id) return false;
        const team = this.getTeamById(this.state.place.teamOficial.id);
        if (!team) return false;
        return (
            <div className={cx('oficial')}>
                <div className={cx('oficial-logo')} style={{ backgroundColor: team.color }}><figure><img src={(team.logo && team.logo.responseText) ? team.logo.responseText : ''} alt="" /></figure></div>
                <div className={cx('oficial-text')} style={{ backgroundColor: team.color }}>Torcida</div>
            </div>
        );
    }

    formatPhone(phone) {
        if (phone && phone !== '') {
            return '+55-' + phone.replace(/[()]/g, '').replace(/[ ]/g, '-');
        }
    }

    handleLocaleSelection(city) {
        const { selectedCity, selectPreferredCity } = this.props
        const storedCity = cookie.load('city')
        if (storedCity !== city) {
            cookie.save('city', city, { sameSite: 'Strict' })
        }
        if (selectedCity !== city) {
            selectPreferredCity({
                selectedCity: city,
            })
        }
    }

    render() {
        const { cities, games, states, teams } = this.props;
        const { authenticated } = this.props.user;
        const _ = this;

        if (!games) return false;

        const dateGames = games;
        dateGames.sort((a, b) => {
            return new Date(a.gameDate).getTime() - new Date(b.gameDate).getTime();
        });

        let placeGames = [];
        if (dateGames.length > 0) {
            dateGames.forEach((game, index) => {
                var willBroadcast = false;
                var wasRecomended = false;
                var recomendationCounter = 0;
                // Game broadcast was confirmed by the place
                if (_.state.place.broadcastGames && _.state.place.broadcastGames.length > 0) {
                    _.state.place.broadcastGames.forEach((broadcast, i) => {
                        if (broadcast && broadcast !== null && game._id === broadcast._id) {
                            willBroadcast = true;
                            recomendationCounter = 1;
                            return;
                        }
                    });
                }

                // The Place is a oficial place
                if (_.state.place.teamOficial && (_.state.place.teamOficial.id === game.homeTeam.id || _.state.place.teamOficial.id === game.visitingTeam.id)) {
                    willBroadcast = true;
                    recomendationCounter = 1;
                }

                // Users said the the place usually broadcast games from the teams of the match
                if (_.state.place.recomendations && _.state.place.recomendations.length > 0) {
                    _.state.place.recomendations.forEach((recomend, i) => {
                        if (recomend.team === game.homeTeam.id || recomend.team === game.visitingTeam.id) {
                            if (recomend.counter > 0) {
                                recomendationCounter = recomend.counter;
                                wasRecomended = true;
                            }
                        }
                    });
                }


                if (willBroadcast) {
                    game.confirmed = true;
                    game.recomendations = recomendationCounter;
                    placeGames.push(game);
                } else if (wasRecomended) {
                    game.confirmed = false;
                    game.recomendations = recomendationCounter;
                    placeGames.push(game);
                }
            });
        }
        const placeState = this.state.place && this.state.place.stateId
            ? states.find(item => item.id === this.state.place.stateId)
            : ''
        const placeCity = this.state.place && this.state.place.cityId
            ? cities.find(item => item.id === this.state.place.cityId)
            : ''

        return (
            <div className={cx('interna')}>
                <div className={cx('place-detail', 'clearfix')}>
                    <header className={cx('place-header')}>
                        {this.renderTeamOficial()}

                        {this.renderGallery()}

                        <div className={cx('place-description')}>
                            <figure className={cx('logo-container')}><img src={(this.state.place && this.state.place.logo) ? this.state.place.logo.responseText : ''} alt="" /></figure>
                            <h2 className={cx('name')}>{this.state.place.name}</h2>
                            {(this.state.place && this.state.place.gallery && this.state.place.gallery[0]) &&
                                <a className={cx('gallery-btn')} onClick={this.openGalleryModal}>Galeria de imagens</a>
                            }
                        </div>
                    </header>

                    <div className={cx('place-content')}>
                        {(placeGames.length > 0) &&
                            <PlaceNextGame
                                game={placeGames[0]}
                                teams={teams}
                            />
                        }

                        <div className={cx('place-container')}>
                            <div className={cx('place-info')}>
                                <i className={cx('icon')}><img src={iconMap} alt="Mapa" /></i>
                                <address>
                                    {(this.state.place.address !== '') && <span className={cx('address')}>{this.state.place.address}<br /></span>}
                                    <span className={cx('city')}>{this.state.place.city}, {placeCity.name}</span>-<span className={cx('state')}>{placeState.abbr}</span> <span className={cx('zipcode')}>{this.state.place.zipcode}</span>
                                </address>
                            </div>

                            {(this.state.place && (!this.state.place.ownerships || (this.state.place.ownerships && !this.state.place.ownerships.name))) &&
                                <div className={cx('ownership')}>
                                    {(authenticated) &&
                                        <a onClick={this.onClickOwner}>
                                            <i className={cx('icon')}><img src={iconOwner} alt="Dono" /></i>
                                            <span>É dono deste estabelecimento?</span>
                                        </a>
                                    }

                                    {(!authenticated) &&
                                        <a data-toggle="modal" data-target='#loginmodal'>
                                            <i className={cx('icon')}><img src={iconOwner} alt="Dono" /></i>
                                            <span>É dono deste estabelecimento?</span>
                                        </a>
                                    }
                                </div>
                            }

                            <ul className={cx('contact')}>
                                {(this.state.place.phone !== '') &&
                                    <li className={cx('phone')}>
                                        <i className={cx('icon')}><img src={iconPhone} alt="Telefone" /></i>
                                        <a href={"tel:" + this.formatPhone(this.state.place.phone)}>{this.state.place.phone}</a>
                                    </li>
                                }
                                {(this.state.place.website !== '') &&
                                    <li className={cx('website')}>
                                        <i className={cx('icon')}><img src={iconWebsite} alt="Website" /></i>
                                        <a href={"http://" + this.state.place.website} className={cx('website')} target="_blank">{this.state.place.website}</a>
                                    </li>
                                }
                            </ul>

                            <div className={cx('about')}>
                                {(this.state.place.about !== '') &&
                                    <div>
                                        <h3>Sobre o <strong>bar</strong></h3>
                                        <p>{this.state.place.about}</p>
                                    </div>
                                }

                                {(this.state.place.facebook !== '' || this.state.place.googleplus !== '' || this.state.place.instagram !== '' || this.state.place.twitter !== '') &&
                                    <ul className={cx('networks')}>
                                        {this.state.place.facebook !== '' && <li><a href={this.state.place.facebook} target="_blank"><img src={FacebookDark} alt="Facebook" /></a></li>}
                                        {this.state.place.twitter !== '' && <li><a href={this.state.place.twitter} target="_blank"><img src={TwitterDark} alt="Twitter" /></a></li>}
                                        {this.state.place.instagram !== '' && <li><a href={this.state.place.instagram} target="_blank"><img src={InstagramDark} alt="Instagram" /></a></li>}
                                        {this.state.place.googleplus !== '' && <li><a href={this.state.place.googleplus} target="_blank"><img src={GooglePlusDark} alt="Google+" /></a></li>}
                                    </ul>
                                }
                            </div>

                            {(this.state.place.services && this.state.place.services.length > 0) &&
                                <div className={cx('services')}>
                                    <h3>O que <strong>oferecemos?</strong></h3>
                                    <ul>
                                        {this.renderServices()}
                                    </ul>
                                </div>
                            }

                            {(this.state.place.cards && this.state.place.cards.length > 0) &&
                                <div className={cx('payment')}>
                                    <h3>Aceitamos</h3>
                                    <ul>
                                        {this.renderCards()}
                                    </ul>
                                </div>
                            }
                        </div>
                    </div>
                </div>

                <GalleryModal
                    id="galleryModal"
                    place={this.state.place}
                    open={this.galleryOpen}
                />

                <ServicesModal
                    id="servicesmodal"
                    services={(this.state.place.services) ? this.state.place.services : []}
                />

                <LoginModal
                    id="loginmodal"
                />

                <MessageModal
                    id="messagemodal"
                    type={'success'}
                    title={(this.state.message) ? this.state.message.title : ''}
                    body={(this.state.message) ? this.state.message.body : ''}
                />

                <GamesListModal
                    id="gameslistmodal"
                    place={this.state.place}
                    games={placeGames}
                    teams={teams}
                />
            </div>
        );
    }
};

Place.propTypes = {
    user: PropTypes.object,
    params: PropTypes.object.isRequired,
    places: PropTypes.array.isRequired,
    games: PropTypes.array.isRequired,
    teams: PropTypes.array.isRequired,
    createOwnership: PropTypes.func.isRequired,
    selectedCity: PropTypes.string,
    selectPreferredCity: PropTypes.func.isRequired,
};

export default Place;
