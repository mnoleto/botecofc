import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import GameTeamItem from '../game/GameTeamItem';
import styles from '../../css/components/list';
import iconDate from '../../images/icons/calendar-list.png';
import iconOtherGame from '../../images/icons/outros-jogos.png';
import iconConfirmed from '../../images/icons/confirmed-green.png';

const cx = classNames.bind(styles);

class PlaceNextGame extends Component {
	constructor(props) {
		super(props);
		this.renderDate = this.renderDate.bind(this);
		this.renderHour = this.renderHour.bind(this);
		this.renderTeamItem = this.renderTeamItem.bind(this);
	}

	openGamesModal() {
		document.querySelector('#gameslistmodal').style.display = 'block';
	}

	renderDate(date) {
		const gameDate = new Date(date);
		return gameDate.getDate() + '/' + (gameDate.getMonth()+1);
	}

	renderHour(date) {
		const gameDate = new Date(date);
		return (gameDate.getHours()<10?'0':'') + gameDate.getHours() + 'H' + (gameDate.getMinutes()<10?'0':'') + gameDate.getMinutes();
	}

	renderTeamItem(obj) {
		const { teams } = this.props;
		let selected = {};
		if (teams) {
			teams.forEach(team => {
				if (team.id === obj.id) selected = team;
			});
			return (
				<GameTeamItem
					id={selected.id}
					image={(selected.logo) ? selected.logo.responseText : ''}
					name={selected.name}
					slug={selected.slug}
				/>
			)
		}
	}

	render() {
		const { game, teams } = this.props;
		const confirmedClass = game.confirmed ? 'game-confirmed' : '';
		return (
			<div className={cx('next-game-item', 'game-item', confirmedClass)}>
				<div className={cx('list-col-1')}>
					<div className={cx('col-container')}>
						<i className={cx('list-icon', 'list-icon-date')}><img src={iconDate} alt="Data" /></i>
						<div className={cx('game-day')}>{this.renderDate(game.gameDate)}</div>
						<div className={cx('game-hour')}>{this.renderHour(game.gameDate)}</div>
					</div>
				</div>

				<div className={cx('list-middle')}>
					<div className={cx('list-middle-container')}>
						{this.renderTeamItem(game.homeTeam)}
						<div className={cx('team-versus')}>x</div>
						{this.renderTeamItem(game.visitingTeam)}
					</div>

					{ (game.confirmed) &&
					<div className={cx('confirmed')}><img src={iconConfirmed} alt="Confirmado" /></div>
					}
				</div>

				<div className={cx('list-col-2', 'other-games-col')}>
					<div className={cx('col-container')}>
						<button type="button" className={cx('btn-other-games')} onClick={this.openGamesModal}>
							<i className={cx('list-icon', 'list-icon-other-games')}><img src={iconOtherGame} alt="Outros Jogos" /></i>
							<small>outros</small><br /><strong>jogos</strong>
						</button>
					</div>
				</div>
			</div>
		);
	}
	
};

PlaceNextGame.propTypes = {
	game: PropTypes.object.isRequired,
	teams: PropTypes.array.isRequired
};

export default PlaceNextGame;
