import React, { Component } from 'react';
import cookie from 'react-cookie'
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from '../../css/components/locale';
import { findDefaultCity, findDefaultLocale, findLocaleByCity } from '../../utils/cities'
// import Icon from '../Icon';
import LocaleModal from '../modais/LocaleModal'

const cx = classNames.bind(styles);

class Locale extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalOpen: false
    }
    this.closeModal = this.closeModal.bind(this)
    this.handleLocaleClick = this.handleLocaleClick.bind(this)
    this.handleLocaleSelection = this.handleLocaleSelection.bind(this)
    this.getLocaleName = this.getLocaleName.bind(this)
  }

  componentDidMount() {
    const { activeCities, selectedCity, selectPreferredCity } = this.props
    const storedCity = cookie.load('city')
    if (storedCity != null && storedCity !== '' && storedCity !== selectedCity) {
      selectPreferredCity({
        selectedCity: storedCity,
      })
    } else if (selectedCity === '' && activeCities.length > 0) {
      const defaultCity = findDefaultCity(activeCities)
      if (!defaultCity) {
        return
      }
      selectPreferredCity({
        selectedCity: defaultCity.id,
      })
    }
  }

  handleLocaleClick(event) {
    event.stopPropagation();
    this.setState({
      modalOpen: true
    })
    document.querySelector('#locale').style.display = 'block';
  }

  closeModal() {
    this.setState({
      modalOpen: false
    })
    document.querySelector('#locale').style.display = 'none';
  }

  handleLocaleSelection(city) {
    const { selectedCity, selectPreferredCity } = this.props
    const storedCity = cookie.load('city')
    if (storedCity !== city) {
      cookie.save('city', city, { sameSite: 'Strict' })
    }
    if (selectedCity !== city) {
      selectPreferredCity({
        selectedCity: city,
      })
    }
    this.closeModal()
  }

  getLocaleName() {
    const { activeCities, activeStates, selectedCity } = this.props;
    if (
      activeCities == null ||
      activeStates == null ||
      activeCities.length === 0 ||
      activeStates.length === 0
    ) {
      return 'Selecione uma cidade'
    }
    const defaultCity = findDefaultLocale(activeCities, activeStates)
    let locale;
    if (
      selectedCity != null &&
      selectedCity !== ''
    ) {
      locale = findLocaleByCity(selectedCity, activeCities, activeStates)
      if (!locale || !locale.city || !locale.state) {
        if (defaultCity != null) {
          locale = {
            ...defaultCity
          }
        } else {
          return 'Selecione uma cidade'
        }
      }
      return `${locale.city.name}, ${locale.state.abbr}`
    }
    if (defaultCity != null) {
      locale = {
        ...defaultCity
      }
      return `${locale.city.name}, ${locale.state.abbr}`
    }
    return 'Selecione uma cidade'
  }

  render() {
    const { activeCities, selectedCity, activeStates } = this.props;
    const { modalOpen } = this.state
    return (
      <React.Fragment>
        <div className={cx('locale', {
          modalOpen
        })}>
          <button className={cx('locale-button')} onClick={this.handleLocaleClick}>
            {/* <Icon size={'lg'} name={'place'}></Icon> */}
            <span>{this.getLocaleName()}</span>
          </button>
        </div>
        {activeCities != null && activeCities.length > 0 && activeStates != null && activeStates.length > 0 &&
          <LocaleModal
            cities={activeCities}
            closeModal={this.closeModal}
            id="locale"
            selectedCity={selectedCity}
            states={activeStates}
            onSelect={this.handleLocaleSelection}
          />
        }
      </React.Fragment>
    );
  }
}

Locale.propTypes = {
  activeCities: PropTypes.array.isRequired,
  activeStates: PropTypes.array.isRequired,
  selectedCity: PropTypes.string,
  selectPreferredCity: PropTypes.func.isRequired,
  user: PropTypes.object,
};

export default Locale;
