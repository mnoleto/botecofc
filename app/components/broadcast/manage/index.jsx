import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import moment from 'moment';
import classNames from 'classnames/bind';
import { fetchPlaces } from '../../../actions/places';
import { fetchChampionships } from '../../../actions/championships';
import { fetchGames } from '../../../actions/games';
import { fetchTeams } from '../../../actions/teams';
import { fetchChannels } from '../../../actions/channels';
import ManageFilters from '../ManageFilters';
import ManageHeader from '../../header/ManageHeader';
import GameSelectItem from '../../game/GameSelectItem';
import styles from '../../../css/components/owner';

const cx = classNames.bind(styles);

class BroadcastManage extends Component {
    static need = [
        fetchChampionships,
        fetchGames,
        fetchPlaces,
        fetchTeams,
        fetchChannels,
    ];

    constructor(props) {
        super(props);

        const { ownerManageFilter, places } = this.props;

        const firstPlace = places && places.length > 0 ? places[0] : {};
        const selectedPlace = ownerManageFilter.place && ownerManageFilter.place.id ? ownerManageFilter.place : null;

        const activePlace = selectedPlace ? selectedPlace : firstPlace;

        this.state = {
            championship: '',
            place: this.getPlaceById(activePlace.id),
            initialDate: ownerManageFilter.initialDate === '' ? moment().subtract(2, 'hours') : ownerManageFilter.initialDate,
            endDate: ownerManageFilter.endDate === '' ? moment().add(30, 'days') : ownerManageFilter.endDate,
            broadcastGames: this.getBroadcast(activePlace.id),
        };
        
        this.getBroadcast = this.getBroadcast.bind(this);
        this.getPlaceById = this.getPlaceById.bind(this);
        this.handleChangeEndDate = this.handleChangeEndDate.bind(this);
        this.handleChangeInitialDate = this.handleChangeInitialDate.bind(this);
        this.handleChampionshipChange = this.handleChampionshipChange.bind(this);
        this.handleAddGame = this.handleAddGame.bind(this);
        this.handleRemoveGame = this.handleRemoveGame.bind(this);
        this.handlePlaceChange = this.handlePlaceChange.bind(this);
        this.renderGamesList = this.renderGamesList.bind(this);
        this.renderContent = this.renderContent.bind(this);
        this.renderContentTitle = this.renderContentTitle.bind(this);
        this.setVisibilityFilter = this.setVisibilityFilter.bind(this);
        this.updateBroadcast = this.updateBroadcast.bind(this);
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.place !== this.state.place) {
            this.setState({
                broadcastGames: this.state.place.broadcastGames,
            });
        }
    }

    componentDidMount() {
        document.querySelector('.register-ad').style.display = 'none';
        document.querySelector('.site-footer').style.paddingTop = 0;
        
        this.setVisibilityFilter('', this.state.initialDate, this.state.endDate, this.state.place);
    }

    getPlaceById(placeId) {
        const { places } = this.props;
        return places.find(place => place.id === placeId);
    }

    getBroadcast(placeId) {
        const place = this.getPlaceById(placeId);
        return place && place.broadcastGames ? place.broadcastGames : [];
    }

    handleChangeEndDate(date) {
        this.setState({
            endDate: moment(date),
        });
        this.setVisibilityFilter(this.state.championship, this.state.initialDate, moment(date), this.state.place);
    }

    handleChangeInitialDate(date) {
        this.setState({
            initialDate: moment(date),
        });
        this.setVisibilityFilter(this.state.championship, moment(date), this.state.endDate, this.state.place);
    }

    handleChampionshipChange(event) {
        this.setState({
            championship: event.target.value,
        });
        this.setVisibilityFilter(event.target.value, this.state.initialDate, this.state.endDate, this.state.place);
    }

    handlePlaceChange(event) {
        const { places } = this.props;
        const place = places.find((place) => place.id == event.target.value);
        this.setState({
            place,
        });
        
        this.setVisibilityFilter(this.state.championship, this.state.initialDate, this.state.endDate, place);
    }

    handleAddGame(game) {
        const broadcastGames = [...this.state.broadcastGames, game];

        this.setState({
            broadcastGames,
        });
    }

    handleRemoveGame(game) {
        const broadcastGames = this.state.broadcastGames.filter(item => {
            return game.id !== item.id
        });
        this.setState({
            broadcastGames,
        });
    }

    updateBroadcast() {
        const { updateBroadcastGame } = this.props;
        
        updateBroadcastGame({
            id: this.state.place.id,
            broadcastGames: this.state.broadcastGames,
        }).then(() => {
            browserHistory.push('/gerenciar-jogos/confirmados');
        }, this);
    }

    setVisibilityFilter(championship, initialDate, endDate, place) {
        const { setVisibilityFilter } = this.props;
        setVisibilityFilter('SHOW_ONLY', championship, initialDate, endDate, place);
    }

    renderGamesList() {
        const { games, teams, channels } = this.props;
        if(!teams || !channels) return null;

        if (!games || games.length === 0) {
            return "Oops. Não encontramos nenhum evento na data sugerida. Tente alterar o período. :)"
        }
        const sortedGames = games;
        sortedGames.sort((a, b) => {
            return new Date(a.gameDate).getTime() - new Date(b.gameDate).getTime();
        });
        return sortedGames.map(game => {
            const broadcast = this.state.broadcastGames.some(item => {
                return item._id === game._id;
            });

            return (
                <GameSelectItem
                    key={game.id}
                    game={game}
                    teams={teams}
                    channels={channels}
                    isChecked={broadcast}
                    addGame={this.handleAddGame}
                    removeGame={this.handleRemoveGame}
                />
            );
        });
    }

    renderContentTitle() {
        if (this.state.screenState === 'ready') {
            const singularOrPlural = this.state.broadcastGames.length > 1 ? 'jogos' : 'jogo';

            return (
                <div className={cx('confirmation')}>
                    <div className={cx('confirmation-description')}>
                        <i className={cx('confirmation-icon')}></i>
                        <span>Você irá<br />transmitir</span>
                    </div>
                    <div className={cx('confirmation-counter')}><strong>{this.state.broadcastGames.length}</strong><br />{singularOrPlural}</div>
                </div>
            );
        } else {
            return (
                <h2 className={cx('games-title')}><span>Selecione os jogos que</span><br />você quer transmitir</h2>
            );
        }
    }

    renderContent() {
        const { games, championships } = this.props;
        return (
            <div>
                <ManageFilters
                    championships={championships}
                    championship={this.state.championship}
                    initialDate={this.state.initialDate}
                    endDate={this.state.endDate}
                    changeInitialDate={this.handleChangeInitialDate}
                    changeEndDate={this.handleChangeEndDate}
                    changeChampionship={this.handleChampionshipChange}
                />

                <div className={cx('games')}>
                    <h2 className={cx('games-title')}><span>Selecione os jogos que</span><br />você quer transmitir</h2>
                    <div className={cx('games-list')}>
                        {this.renderGamesList()}
                    </div>
                    {(games
                        && games.length > 0
                    ) && (
                        <button
                            type="button"
                            className={cx('btn-next')}
                            onClick={this.updateBroadcast}
                        >
                            Confirmar
                        </button>
                    )}
                </div>
            </div>
        );
    }

    render() {
        const { places } = this.props;

        return (
            <div className={cx('owner-manage-games')}>
                <ManageHeader
                    places={places}
                    place={this.state.place}
                    changePlace={this.handlePlaceChange}
                    isDisabled={false}
                />
                {this.renderContent()}
            </div>
        );
    }
}

BroadcastManage.propTypes = {
    user: PropTypes.object.isRequired,
    championships: PropTypes.array.isRequired,
    allGames: PropTypes.array.isRequired,
    games: PropTypes.array.isRequired,
    places: PropTypes.array.isRequired,
    teams: PropTypes.array.isRequired,
    channels: PropTypes.array.isRequired,
    setVisibilityFilter: PropTypes.func.isRequired,
    updateBroadcastGame: PropTypes.func.isRequired,
    ownerManageFilter: PropTypes.object.isRequired,
};

export default BroadcastManage;