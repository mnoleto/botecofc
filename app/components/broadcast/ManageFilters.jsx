import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import sortByName from '../../utils/sortByName';
import FilterDate from '../../components/filters/FilterDate';
import styles from '../../css/components/owner';

const cx = classNames.bind(styles);

class ManageFilters extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showFilter: true,
        };
    }

    render() {
        const { championships, initialDate, endDate, changeInitialDate, changeEndDate, changeChampionship } = this.props;

        const alphabeticalChampionships = championships;
        let championshipsList = [];
        if (championships) {
            alphabeticalChampionships.sort(sortByName('name'));
            championshipsList = alphabeticalChampionships.map((championship, index) => {
                return <option key={championship.id} value={championship.id}>{championship.name}</option>
            });
        }

        return (
            <div className={cx('owner-manage-filters')}>
                <div className={cx('owner-manage-container')}>
                    <div className={cx('owner-manage-selected-filters')}>
                        <span className={cx('selected-date')}>{initialDate.format('DD/MM')}</span> até <span className={cx('selected-date')}>{endDate.format('DD/MM')}</span>
                    </div>
                </div>

                <div className={cx('owner-manage-form-filters')}>
                    <div className={cx('owner-manage-container')}>
                        {this.state.showFilter &&
                            (<div className={cx('owner-manage-filter-content')}>
                                <div className={cx('row')}>
                                    <div className={cx('col-md-3', 'col-sm-6', 'col-xs-6')}>
                                        <div className={cx('field-group', 'date-filter')}>
                                            <label>DE:</label>
                                            <span>
                                                <FilterDate
                                                    date={initialDate}
                                                    dateFormat="dd/MM"
                                                    showTimeSelect={false}
                                                    onFilter={changeInitialDate}
                                                />
                                            </span>
                                        </div>
                                    </div>
                                    <div className={cx('col-md-3', 'col-sm-6', 'col-xs-6')}>
                                        <div className={cx('field-group', 'date-filter')}>
                                            <label>ATÉ:</label>
                                            <span>
                                                <FilterDate
                                                    date={endDate}
                                                    dateFormat="dd/MM"
                                                    showTimeSelect={false}
                                                    onFilter={changeEndDate}
                                                />
                                            </span>
                                        </div>
                                    </div>
                                    <div className={cx('col-md-6', 'col-sm-12', 'col-xs-12')}>
                                        <div className={cx('field-group')}>
                                            <select className={cx('full')} onChange={changeChampionship}>
                                                <option value="">TODOS OS CAMPEONATOS</option>
                                                {championshipsList}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>)
                        }
                    </div>
                </div>
            </div>
        );
    }
}

ManageFilters.propTypes = {
    championships: PropTypes.array.isRequired,
    championship: PropTypes.string.isRequired,
    initialDate: PropTypes.object.isRequired,
    endDate: PropTypes.object.isRequired,
    changeInitialDate: PropTypes.func.isRequired,
    changeEndDate: PropTypes.func.isRequired,
    changeChampionship: PropTypes.func.isRequired,
};

export default ManageFilters;
