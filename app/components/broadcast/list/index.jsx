import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import { fetchPlaces } from '../../../actions/places';
import { fetchGames } from '../../../actions/games';
import { fetchTeams } from '../../../actions/teams';
import { fetchChannels } from '../../../actions/channels';
import ManageHeader from '../../header/ManageHeader';
import GameShowItem from '../../game/GameShowItem';
import styles from '../../../css/components/owner';

const cx = classNames.bind(styles);

class ConfirmedGames extends Component {
    static need = [
        fetchGames,
        fetchPlaces,
        fetchTeams,
        fetchChannels,
    ];

    constructor(props) {
        super(props);

        const { ownerManageFilter, places } = this.props;
        const firstPlace = places && places.length > 0 ? places[0] : {};
        const filteredPlace = ownerManageFilter.place && ownerManageFilter.place.id ? ownerManageFilter.place : null;
        const selectedPlace = filteredPlace ? filteredPlace : firstPlace;

        const activePlace = selectedPlace ? selectedPlace : firstPlace;
        
        this.state = {
            place: this.getPlaceById(activePlace.id),
        };
        
        this.handlePlaceChange = this.handlePlaceChange.bind(this);
        this.renderGamesList = this.renderGamesList.bind(this);
        this.setVisibilityFilter = this.setVisibilityFilter.bind(this);
    }

    componentDidMount() {
        document.querySelector('.register-ad').style.display = 'none';
        document.querySelector('.site-footer').style.paddingTop = 0;
    }

    getPlaceById(placeId) {
        const { places } = this.props;
        return places.find(place => place.id === placeId);
    }

    handlePlaceChange(event) {
        const { places } = this.props;
        const place = places.find((place) => place.id == event.target.value);
        
        this.setState({
            place,
        });

        this.setVisibilityFilter(place);
    }

    setVisibilityFilter(place) {
        const { ownerManageFilter, setVisibilityFilter } = this.props;
        setVisibilityFilter('SHOW_ONLY', ownerManageFilter.championship, ownerManageFilter.initialDate, ownerManageFilter.endDate, place);
    }

    renderGamesList() {
        const { allGames, teams, channels } = this.props;
        if (!teams || !channels) return null;

        // order games by date
        const sortedGames = allGames;
        sortedGames.sort((a, b) => {
            return new Date(a.gameDate).getTime() - new Date(b.gameDate).getTime();
        });
        return sortedGames.map(game => {
            const { broadcastGames } = this.state.place;
            const willBroadcast = broadcastGames && broadcastGames.length ? broadcastGames.some(item => {
                return item.id === game.id;
            }) : false;
            if (!willBroadcast) {
                return null;
            }
            return (
                <GameShowItem
                    key={game.id}
                    game={game}
                    teams={teams}
                    channels={channels}
                />
            );
        });
    }

    render() {
        const { places } = this.props;
        const { broadcastGames } = this.state.place ? this.state.place : [];
        const singularOrPlural = broadcastGames.length > 1 ? 'jogos' : 'jogo';

        return (
            <div className={cx('owner-manage-games')}>
                <ManageHeader
                    places={places}
                    place={this.state.place}
                    changePlace={this.handlePlaceChange}
                    isDisabled={false}
                />
                <div>
                    <div className={cx('games')}>
                        <div className={cx('confirmation')}>
                            <div className={cx('confirmation-description')}>
                                <i className={cx('confirmation-icon')}></i>
                                <span>Você irá<br />transmitir</span>
                            </div>
                            <div className={cx('confirmation-counter')}>
                                <strong>{broadcastGames && broadcastGames.length ? broadcastGames.length : 0}</strong>
                                <br />
                                {singularOrPlural}
                            </div>
                            <div className={cx('post-action')}></div>
                        </div>

                        <div className={cx('games-list')}>
                            {this.renderGamesList()}
                        </div>

                        <div className={cx('actions')}>
                            <Link to="/gerenciar-jogos" className={cx('btn-back')}>Editar</Link>
                            <Link to="/gerenciar-post" className={cx('btn-post', 'btn-novo')}>Criar Post</Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ConfirmedGames.propTypes = {
    user: PropTypes.object.isRequired,
    allGames: PropTypes.array.isRequired,
    places: PropTypes.array.isRequired,
    teams: PropTypes.array.isRequired,
    channels: PropTypes.array.isRequired,
    ownerManageFilter: PropTypes.object.isRequired,
};

export default ConfirmedGames;