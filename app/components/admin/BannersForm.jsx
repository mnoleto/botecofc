import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from '../../css/admin/form';
import { CitiesSelect, StatesSelect } from '../selects'
import UploadImage from './UploadImage';

const cx = classNames.bind(styles);

/*
 * TEAMS FORM
 */
class BannersForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cityId: '',
      id: '',
      name: '',
      image: {},
      imageMobile: {},
      link: '',
      stateId: '',
      tracking: '',

      message: '',
    };

    this.eventHandlers = {};

    this.isEditing = false;
    this.clearImages = false;
    this.isSubmiting = false;

    this.clearForm = this.clearForm.bind(this);
    this.onClearImages = this.onClearImages.bind(this);
    this.onClearImagesMobile = this.onClearImagesMobile.bind(this);
    this.handleFormChange = this.handleFormChange.bind(this);
    this.onRemoveImage = this.onRemoveImage.bind(this);
    this.onRemoveImageMobile = this.onRemoveImageMobile.bind(this);
    this.onImageUploadComplete = this.onImageUploadComplete.bind(this)
    this.onImageMobileUploadComplete = this.onImageMobileUploadComplete.bind(this)
    this.onEntrySave = this.onEntrySave.bind(this);
		this.handleStateChange = this.handleStateChange.bind(this);
		this.handleCityChange = this.handleCityChange.bind(this);

    this.renderContent = this.renderContent.bind(this);
    this.renderMessage = this.renderMessage.bind(this);
  };

  componentDidUpdate(prevProps) {
    const { banner } = this.props;
    if (banner.name !== prevProps.banner.name) {
      this.setState({
        id: banner.id,
        name: banner.name,
        image: banner.image,
        imageMobile: banner.imageMobile,
        link: banner.link,
        tracking: banner.tracking,
      });
    }
    this.isEditing = true;
    this.isSubmiting = false;
  }

  clearForm() {
    const { onClear } = this.props;
    this.setState({
      id: '',
      name: '',
      image: {},
      imageMobile: {},
      link: '',
      tracking: '',
      startDate: '',
      endDate: '',
    });
    onClear();
    this.Mobile = true;
    window.scrollTo(0, 0);
    this.forceUpdate();
  };

  onClearImages() {
    this.Mobile = false;
  };

  onClearImagesMobile() {
    this.clearImagesMobile = false;
  };

  handleFormChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  };

	handleStateChange(event) {
		this.setState({
			stateId: event.target.value,
		})
	}

	handleCityChange(event) {
		this.setState({
			cityId: event.target.value,
		})
	}

  onEntrySave(e) {
    e.preventDefault();
    const { banner, onEntrySave, onEntryChange } = this.props;

    if (!this.state.image.responseText) {
      this.setState({ message: 'Faça o upload do banner.' });
      return false;
    }

    if (!this.state.imageMobile.responseText) {
      this.setState({ message: 'Faça o upload do banner mobile.' });
      return false;
    }

    if (this.state.name === '') {
      this.setState({ message: 'Preencha o campo nome.' });
      return false;
    }

    this.setState({ message: '' });

    this.isSubmiting = true;

    const data = {
      cityId: this.state.cityId,
      name: this.state.name,
      image: this.state.image,
      imageMobile: this.state.imageMobile,
      link: this.state.link,
      stateId: this.state.stateId,
      tracking: this.state.tracking,
    };
    const _ = this;
    if (banner.name) {
      onEntryChange({
        id: this.state.id,
        ...data,
      });
    } else {
      onEntrySave({
        ...data
      }).then(() => _.isSubmiting = false);
    }

    this.clearForm();
  };

  onRemoveImage(file) {
    this.setState({ image: {} });
  }

  onRemoveImageMobile(file) {
    this.setState({ imageMobile: {} });
  }

  onImageUploadComplete(file) {
    this.setState({
      image: file,
    });
  }

  onImageMobileUploadComplete(file) {
    this.setState({
      imageMobile: file,
    });
  }

  renderContent() {
    const { cities, states } = this.props;
    const { cityId, stateId } = this.state;

		const filteredCities = cities.filter(city => city.stateId === stateId);

    return (
      <form className={cx('clearfix', 'form-inline', 'container-fluid')} method="post" encType="multipart/form-data" onSubmit={this.onEntrySave} role="form">
        {this.renderMessage()}

        <div className={cx('row')}>
          <div className={cx('upload-field', 'col-md-full')}>
            <UploadImage
              id={'image'}
              classNames={'upload-banner'}
              clearImages={this.clearImages}
              image={this.state.image}
              isEditing={this.isEditing}
              postUrl={'/uploadBanner'}
              onClearImages={this.onClearImages}
              onRemoveImage={this.onRemoveImage}
              onUploadComplete={this.onImageUploadComplete}
              text={'INCLUA O BANNER - 970x150'}
              uploadMultiple={false}
            />
          </div>
        </div>

        <div className={cx('row')}>
          <div className={cx('upload-field', 'col-md-full')}>
            <UploadImage
              id={'image-mobile'}
              classNames={'upload-bannerMobile'}
              clearImages={this.clearImagesMobile}
              image={this.state.imageMobile}
              isEditing={this.isEditing}
              postUrl={'/uploadBanner'}
              onClearImages={this.onClearImagesMobile}
              onRemoveImage={this.onRemoveImageMobile}
              onUploadComplete={this.onImageMobileUploadComplete}
              text={'INCLUA O BANNER MOBILE - 1420x440'}
              uploadMultiple={false}
            />
          </div>
        </div>

        <div className={cx('row')}>
          <div className={cx('col-md-full')}>
            <div className={cx('field-group')}>
              <label htmlFor="name" className={cx('sr-only')}>Nome *</label>
              <input type="text" name="name" ref="name" placeholder="Nome *" value={this.state.name || ''} onChange={this.handleFormChange} />
            </div>
          </div>
          <div className={cx('col-md-full')}>
            <div className={cx('field-group')}>
              <label htmlFor="link" className={cx('sr-only')}>Link</label>
              <input type="text" name="link" ref="link" placeholder="Link" value={this.state.link || ''} onChange={this.handleFormChange} />
            </div>
          </div>
          <div className={cx('col-md-full')}>
            <div className={cx('field-group')}>
              <label htmlFor="tracking" className={cx('sr-only')}>Tracking</label>
              <input type="text" name="tracking" ref="tracking" placeholder="Tracking" value={this.state.tracking || ''} onChange={this.handleFormChange} />
            </div>
          </div>
        </div>

        <div className={cx('row')}>
          <StatesSelect selectedState={stateId} states={states} onChange={this.handleStateChange} />
        </div>

        {stateId !== '' && (
          <div className={cx('row')}>
            <CitiesSelect
              cities={filteredCities}
              selectedCity={cityId}
              onChange={this.handleCityChange}
            />
          </div>
        )}

        <div className={cx('row')}>
          <div className={cx('col-md-full', 'required')}><p className={cx('pull-right')}><small>* campos obrigatórios</small></p></div>
        </div>

        {this.renderMessage()}

        <div className={cx('row')}>
          <input className={cx('submit-btn', 'pull-right')} type="submit" value='Enviar' />
          <input className={cx('clear-btn', 'pull-right')} type="button" value="Limpar" onClick={this.clearForm} />
        </div>
      </form>
    );
  }

  renderMessage() {
    if (this.state.message !== '') {
      return (
        <div className={cx('message')}>
          <i className="zmdi zmdi-block"></i> {this.state.message}
        </div>
      );
    }
    return <span />;
  }

  render() {
    return (
      <div className={cx('admin-form-col')}>
        <h2 className={cx('form-title')}>Dados do banner</h2>
        {this.renderContent()}
      </div>
    );
  };
}

BannersForm.propTypes = {
  banner: PropTypes.object,
  cities: PropTypes.array.isRequired,
  states: PropTypes.array.isRequired,
  onEntryChange: PropTypes.func.isRequired,
  onEntrySave: PropTypes.func.isRequired,
  onClear: PropTypes.func.isRequired,
};

export default BannersForm;
