import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from 'css/admin/table';
const cx = classNames.bind(styles);

class UserRequestLine extends Component {
	constructor(props) {
		super(props);
		this.onHandleApprove = this.onHandleApprove.bind(this);
		this.onHandleDisapprove = this.onHandleDisapprove.bind(this);
	}

	onHandleApprove() {
		const { request, onApprove } = this.props;
		onApprove(request.place, request.user, request.id);
	}

	onHandleDisapprove() {
		const { id, onReprove } = this.props;
		onReprove(id);
	}

	render() {
		const { request } = this.props;
		return (
			<tr>
				<td><strong>{request.place.name}</strong></td>
				<td>{request.user.email}</td>
				<td className={cx('text-center')}>
					<button type="button" className={cx('btn-approve')} onClick={this.onHandleApprove}>
						<i className={cx('zmdi', 'zmdi-check-circle')}></i>
					</button>

					<button type="button" className={cx('btn-disapprove')} onClick={this.onHandleDisapprove}>
						<i className={cx('zmdi', 'zmdi-close-circle')}></i>
					</button>
				</td>
			</tr>
		);
	}
}

UserRequestLine.propTypes = {
	id: PropTypes.string.isRequired,
	index: PropTypes.number.isRequired,
	request: PropTypes.object.isRequired,
	onApprove: PropTypes.func.isRequired,
	onReprove: PropTypes.func.isRequired
};

class OwnershipsRequestsTable extends Component {
	constructor(props) {
		super(props);
	};

	render() {
		const { ownerships, onApprove, onReprove } = this.props;
		const _ = this;
		let lines
		if(ownerships && ownerships.length > 0) {
			lines = ownerships.map((value, index) => {
				if(!value.place || !value.user) {
					onReprove(value.id);
					return;
				}
				return(
					<UserRequestLine
						key={'ownerships' + index}
						id={value.id}
						index={index}
						request={value}
						onApprove={onApprove}
						onReprove={onReprove}
					/>
				);
			});
		} else {
			lines = <tr><td colSpan="4" className={cx('text-center')}>Nenhuma solicitação de propriedade encontrada.</td></tr>
		}

		return (
			<div className={cx('container-fluid')}>
				<div className={cx('col-md-12')}>
					<div className={cx('box')}>
						<table className={cx('table', 'table-bordered')}>
							<thead>
								<tr>
									<th>Bar</th>
									<th>Solicitante</th>
									<th width="8%" className={cx('text-center')}>Ação</th>
								</tr>
							</thead>
							<tbody>
								{lines}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		);
	}
}

OwnershipsRequestsTable.propTypes = {
	ownerships: PropTypes.array.isRequired,
	onApprove: PropTypes.func.isRequired,
	onReprove: PropTypes.func.isRequired
};

export default OwnershipsRequestsTable;

