import classNames from 'classnames/bind';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from '../../css/admin/form';
import { CitiesSelect, StatesSelect } from '../selects'
import LoadingMessage from './LoadingMessage';

const cx = classNames.bind(styles);

/*
 * COUNTRIES FORM
 */
export default class UsersForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			id: '',
			name: '',
			email: '',
			team: '',
			city: '',
			cityId: '',
			stateId: '',
			phone: '',
			gender: '',
			date: '',
			role: '',
			ownerships: null
		};

		this.eventHandlers = {};

		this.isEditing = false;
		this.isSubmiting = false;
		this.renderContent = this.renderContent.bind(this);

		this.clearForm = this.clearForm.bind(this);
		this.handleFormChange = this.handleFormChange.bind(this);
		this.handleOnSubmit = this.handleOnSubmit.bind(this);
		this.handleCityChange = this.handleCityChange.bind(this)
		this.handleStateChange = this.handleStateChange.bind(this)
	};

	componentDidUpdate(prevProps){
		if(prevProps.user !== this.props.user) {
			this.setState({
				id: this.props.user._id,
				name: this.props.user.name,
				email: this.props.user.email,
				team: (this.props.user.profile && this.props.user.profile.team && this.props.user.profile.team.name) ? this.props.user.profile.team.name : '',
				city: (this.props.user.profile && this.props.user.profile.city) ? this.props.user.profile.city : '',
				cityId: (this.props.user.profile && this.props.user.profile.cityId) ? this.props.user.profile.cityId : '',
				stateId: (this.props.user.profile && this.props.user.profile.stateId) ? this.props.user.profile.stateId : '',
				phone: (this.props.user.profile && this.props.user.profile.phone) ? this.props.user.profile.phone : '',
				gender: (this.props.user.profile && this.props.user.profile.gender) ? this.props.user.profile.gender : '',
				date: this.props.user.date,
				role: this.props.user.role,
				ownerships: this.props.user.ownerships
			});
			this.isSubmiting = false;
		}
	}

	clearForm() {
		this.setState({
			id: '',
			name: '',
			email: '',
			team: '',
			city: '',
			cityId: '',
			stateId: '',
			phone: '',
			gender: '',
			date: '',
			role: '',
			ownerships: null
		});
		this.clearImages = true;
		window.scrollTo(0,0);
		this.forceUpdate();
	}

	handleStateChange(event) {
		this.setState({
			stateId: event.target.value,
		})
	}

	handleCityChange(event) {
		this.setState({
			cityId: event.target.value,
		})
	}

	handleFormChange(event) {
		this.setState({[event.target.name]: event.target.value});
	};

	handleOnSubmit(event) {
		event.preventDefault();
		const { onEntryChange } = this.props;

		this.isSubmiting = true;

		onEntryChange({
			id: this.props.user._id,
			role: this.refs.role.value
		});

		this.clearForm();
	};

	renderContent() {
		if(!this.isSubmiting) {
			let placesList;
			const { cityId, stateId } = this.state
			const { cities, states } = this.props

			const filteredCities = cities.filter(city => city.stateId === stateId);

			if(this.state.ownerships && this.state.ownerships.length > 0) {
				placesList = this.state.ownerships.map((place, index) => {
					return (<li key={place.id}>{place.name}</li>);
				});
			}
			let date = new Date(this.state.date);
			let month = date.getMonth() + 1;
			let formattedDate;
			let day = date.getDate() < 10 ? ('0' + date.getDate()) : date.getDate();
			let monthFormatted = month < 10 ? '0' + month : month;
			
			formattedDate = day + '/' + monthFormatted + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes();
				
			return (
				<form className={cx('clearfix', 'form-inline', 'container-fluid')} onSubmit={this.handleOnSubmit} role="form">
					<div className={cx('row')}>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="name" className={cx('sr-only')}>Nome *</label>
								<input type="text" name="name" placeholder="Nome" value={this.state.name || ''} disabled={true} />
							</div>
						</div>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="email" className={cx('sr-only')}>E-mail *</label>
								<input type="email" name="email" placeholder="E-mail" value={this.state.email || ''} disabled={true} />
							</div>
						</div>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="email" className={cx('sr-only')}>Time</label>
								<input type="text" name="team" placeholder="Time" value={this.state.team || ''} disabled={true} />
							</div>
						</div>
						<div className={cx('col-md-full')}>
							<StatesSelect
								disabled
								selectedState={stateId}
								states={states}
								onChange={this.handleStateChange}
								
							/>
						</div>
						{stateId !== '' && (
							<div className={cx('col-md-full')}>
								<CitiesSelect
									disabled
									cities={filteredCities}
									selectedCity={cityId}
									onChange={this.handleCityChange}
								/>
							</div>
						)}
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="email" className={cx('sr-only')}>Região</label>
								<input type="text" name="region" placeholder="Região" value={this.state.city || ''} disabled={true} />
							</div>
						</div>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="email" className={cx('sr-only')}>Telefone</label>
								<input type="text" name="phone" placeholder="Telefone" value={this.state.phone || ''} disabled={true} />
							</div>
						</div>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="email" className={cx('sr-only')}>Sexo</label>
								<input type="text" name="gender" placeholder="Sexo" value={this.state.gender || ''} disabled={true} />
							</div>
						</div>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="email" className={cx('sr-only')}>Data da inscrição</label>
								<input type="text" name="date" placeholder="Data da inscrição" value={formattedDate || ''} disabled={true} />
							</div>
						</div>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<select name="role" ref="role" className={cx('full')} value={this.state.role || '0'} onChange={this.handleFormChange}>
									<option value="0">Selecione o tipo de usuário *</option>
									<option value="subscriber">Usuário</option>
									<option value="owner">Dono de bar</option>
									<option value="admin">Admin</option>
								</select>
							</div>
						</div>
					</div>

					{(this.state.ownerships && this.state.ownerships.length > 0) &&
					<div className={cx('row')}>
						<div className={cx('place-owner')}>
							<h4>DONO DOS BOTECOS:</h4>
							<ul>
								{placesList}
							</ul>
						</div>
					</div>
					}

					{(this.props.user.email) &&
						<div className={cx('row')}>
							<input className={cx('submit-btn', 'pull-right')} type="submit" value='Alterar' />
						</div>
					}
				</form>
			);
		} else {
			return <LoadingMessage />;
		}
	}

	render() {
		return (
			<div className={cx('admin-form-col')}>
				<h2 className={cx('form-title')}>Dados do Usuário</h2>
				{this.renderContent()}
			</div>
		);
	};
}

UsersForm.propTypes = {
	cities: PropTypes.array.isRequired,
	states: PropTypes.array.isRequired,
	user: PropTypes.object,
	onEntrySave: PropTypes.func.isRequired,
	onEntryChange: PropTypes.func.isRequired
};
