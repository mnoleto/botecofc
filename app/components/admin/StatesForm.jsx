import classNames from 'classnames/bind';
import PropTypes from 'prop-types';
import React from 'react';
import { useEffect, useState } from 'react';
import styles from '../../css/admin/form';
import LoadingMessage from './LoadingMessage';

const cx = classNames.bind(styles);

export const StatesForm = ({
  state,
  onEntryChange,
  onEntrySave,
}) => {
  const [newState, changeState] = useState({
    abbr: '',
    id: '',
    isActive: false,
    name: '',
  })

  const [message, changeMessage] = useState('')
  const [isSubmiting, changeIsSubmiting] = useState(false)

  useEffect(() => {
    if (state && state.id) {
      changeState({
        ...state,
      })
    }
    changeIsSubmiting(false)
  }, [state])

  const clearForm = () => {
    changeState({
      id: '',
      isActive: false,
      name: ''
    });
    changeMessage('')
    changeIsSubmiting(false)
    window.scrollTo(0, 0);
  };

  const handleNameChange = (event) => {
    changeState({
      ...newState,
      name: event.target.value,
    })
  };

  const handleIsActive = (event) => {
    changeState({
      ...newState,
      isActive: event.target.value === '1',
    })
  }

  const onSave = (e) => {
    e.preventDefault();

    if (newState.name === '') {
      changeMessage('Preencha o campo nome.');
      return false;
    }
    changeMessage('');
    changeIsSubmiting(true)
    if (state.name) {
      onEntryChange({
        ...newState,
        name: newState.name,
      })
    } else {
      onEntrySave({
        name: newState.name,
      })
    }
    clearForm()
  };

  const renderContent = () => {
    if (!isSubmiting) {
      return (
        <form className={cx('clearfix', 'form-inline', 'container-fluid')} method="post" encType="multipart/form-data" onSubmit={onSave} role="form">
          {renderMessage()}

          <div className={cx('row')}>
            <div className={cx('field-group', 'col-md-full')}>
              <label htmlFor="name" className={cx('sr-only')}>Nome *</label>
              <input type="text" name="name" placeholder="Nome *" value={newState.name || ''} onChange={handleNameChange} />
            </div>
          </div>

          <div className={cx('row')}>
            <label>Ativado para filtro?</label>
            <br />
            <input type="radio" name="active" value="0" checked={!newState.isActive} onChange={handleIsActive} /> Não
            <br />
            <input type="radio" name="active" value="1" checked={newState.isActive} onChange={handleIsActive} /> Sim
            <br/>
          </div>

          <div className={cx('row')}>
            <div className={cx('col-md-full', 'required')}><p className={cx('pull-right')}><small>* campos obrigatórios</small></p></div>
          </div>

          {renderMessage()}

          {newState && newState.id != '' && (
            <div className={cx('row')}>
              <input className={cx('submit-btn', 'pull-right')} type="submit" value='Enviar' />
              <input className={cx('clear-btn', 'pull-right')} type="button" value="Limpar" onClick={clearForm} />
            </div>
          )}
        </form>
      );
    } else {
      return <LoadingMessage />;
    }
  }

  const renderMessage = () => {
    if (message !== '') {
      return (
        <div className={cx('message')}>
          <i className="zmdi zmdi-block"></i> {message}
        </div>
      );
    }
  }

  return (
    <div className={cx('admin-form-col')}>
      <h2 className={cx('form-title')}>Dados do estado</h2>
      {renderContent()}
    </div>
  );
}

StatesForm.propTypes = {
  state: PropTypes.object,
  onEntryChange: PropTypes.func.isRequired,
  onEntrySave: PropTypes.func.isRequired
};

export default StatesForm
