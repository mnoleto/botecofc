import React from 'react';
import { createRef } from 'react';
import Dropzone from 'react-dropzone';
import classNames from 'classnames/bind';
import styles from '../../../css/components/bgs';

const cx = classNames.bind(styles);

const Figures = ({
  images,
  onUploadComplete,
  uploadFile,
  removeFigure,
}) => {
  const dropzoneRef = createRef();

	const uploadComplete = (data) => {
    onUploadComplete(JSON.parse(data))
  }
	
	const onDrop = (acceptedFiles) => {
		const fd = new FormData();

    acceptedFiles.forEach(file => {
      fd.append('files[]', file, file.name);
    });

		uploadFile(fd, '/uploadPostFigures', (feedback) => {
			if (feedback.status === 200) {
				uploadComplete(feedback.response);
			}
		});
	}

  const renderContent = () => {
    if (images.length === 0) {
      return (
        <div className={cx('drop-text')}>Adicionar figurinhas</div>
      )
    }
    
    return (
      <div className={cx('dropzone-preview')}>
        {images.map(({ _id, image }, index) => {
          return (
            <div key={index} className={cx('dropzone-preview-item')}>
              <div className={cx('dropzone-buttons')}>
                <button type="button" className={cx('btn-remove')} onClick={(e) => {
                  e.stopPropagation();
                  removeFigure(_id)
                }}>
                  <i className={cx('zmdi', 'zmdi-close')} />
                </button>
              </div>
              <img src={image.responseText} alt="" />
            </div>
          )
        })}
      </div>
    )
  }

  return (
    <div className={cx('bg-wrapper')}>
      <Dropzone
        accept="image/*"
        className={cx('bg-dropzone')}
        name="postBg"
        multiple
        onDrop={onDrop}
        ref={dropzoneRef}
      >
        {renderContent()}
      </Dropzone>
    </div>
  )
}

export default Figures;
