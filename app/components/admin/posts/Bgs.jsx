import React, { useState } from 'react';
import { createRef } from 'react';
import Dropzone from 'react-dropzone';
import classNames from 'classnames/bind';
import styles from '../../../css/components/bgs';

const cx = classNames.bind(styles);

const BgsList = ({ bgs, removeBg }) => {
  if (bgs.length === 0) {
    return (
      <div>Nenhum background adicionado.</div>
    )
  }

  return (
    <div>
      <h3>Adicionados:</h3>
      <table width="100%">
        <thead>
          <tr>
            <th className={cx('bg-table-th')}>Índice</th>
            <th className={cx('bg-table-th')}>Retrato</th>
            <th className={cx('bg-table-th')}>Retangulo</th>
            <th className={cx('bg-table-th')}></th>
          </tr>
        </thead>
        <tbody>
          {bgs.map((item, index) => {
            return (
              <tr key={item.id || index}>
                <td className={cx('bg-number', 'bg-table-td')}>
                  {index + 1}
                </td>
                <td className={cx('bg-table-td')}>
                  <div className={cx('bg-thumb')}>
                    <img src={item.rect.responseText} alt=""/>
                  </div>
                </td>
                <td className={cx('bg-table-td')}>
                  <div className={cx('bg-thumb')}>
                    <img src={item.portrait.responseText} alt=""/>
                  </div>
                </td>
                <td className={cx('bg-table-td')}>
                  {item._id && (
                    <button type="button" className={cx('btn-remove')} onClick={() => removeBg(item._id)}>
                      <i className={cx('zmdi', 'zmdi-close')} />
                    </button>
                  )}
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

const Bgs = ({
  bgs,
  onAdd,
  uploadFile,
  removeBg
}) => {
  const [rect, setRect] = useState(null);
  const [portrait, setPortrait] = useState(null);

  const [rectUpload, setRectUpload] = useState(null);
  const [portraitUpload, setPortraitUpload] = useState(null);

  const dropzoneRectRef = createRef();
  const dropzonePortraitRef = createRef();

	const uploadComplete = (data, type) => {
    const parse = JSON.parse(data)
    if (type === 'rect') {
      setRectUpload(parse);
    }
    setPortraitUpload(parse);
  }

  const onDropRect = (acceptedFile) => {
    const file = acceptedFile[0]
    setRect(file);
    upload(file, 'rect');
  }

  const onDropPortrait = (acceptedFile) => {
    const file = acceptedFile[0]
    setPortrait(file);
    upload(file, 'portrait');
  }

  const onCleanDropzone = (type) => {
    if (type === 'rect') {
      setRect(null)
    }
    setPortrait(null)
  }

  const clear = () => {
    setRect(null)
    setPortrait(null)
    setRectUpload(null)
    setPortraitUpload(null)
  }
	
	const upload = (file, type) => {
		const fd = new FormData();

    fd.append('file', file, file.name);

		uploadFile(fd, '/uploadPostBgs', (feedback) => {
			if (feedback.status === 200) {
				uploadComplete(feedback.response, type);
			}
		});
	}

  const add = () => {
    if (!portraitUpload || !rectUpload) {
      return;
    }

    onAdd({
      portrait: portraitUpload,
      rect: rectUpload,
    }).then(() => {
      clear();
    });

    
  }

  const renderContent = (name, state, size) => {
    if (!state) {
      return (
        <div className={cx('drop-text')}>Adicionar {name} - {size}</div>
      )
    }
    
    return (
      <div className={cx('dropzone-preview')}>
        <div className={cx('dropzone-preview-item')}>
          <div className={cx('dropzone-buttons')}>
            <button type="button" className={cx('btn-remove')} onClick={(e) => {
              e.stopPropagation();
              onCleanDropzone(name)
            }}>
              <i className={cx('zmdi', 'zmdi-close')} />
            </button>
          </div>
          <img src={state.preview} alt="" />
        </div>
      </div>
    )
  }

  return (
    <div className={cx('bg-container')}>
      <div className={cx('bg-new-container')}>
        <div className={cx('bg-wrapper')}>
          <Dropzone
            accept="image/*"
            className={cx('bg-dropzone')}
            name="postBg"
            onDrop={onDropPortrait}
            ref={dropzonePortraitRef}
          >
            {renderContent('retrato', portrait, '750 x 1024 pixels')}
          </Dropzone>
        </div>

        <div className={cx('bg-wrapper')}>
          <Dropzone
            accept="image/*"
            className={cx('bg-dropzone')}
            name="postBg"
            onDrop={onDropRect}
            ref={dropzoneRectRef}
          >
            {renderContent('rect', rect, '1024 x 1024 pixels')}
          </Dropzone>
        </div>

        <div className={cx('add-container')}>
          <button type="button" className={cx('submit-btn')} onClick={add}>Adicionar</button>
        </div>
      </div>

      <BgsList bgs={bgs} removeBg={removeBg} />
    </div>
  )
}

export default Bgs;
