import React, { Component } from 'react';
import CsvDownloader from 'react-csv-downloader';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import AdminTitle from '../AdminTitle';
import { fetchPlaces } from '../../../actions/places';
import { fetchFeatureToggles } from '../../../actions/featureToggle';
import sortByName from '../../../utils/sortByName';
import styles from '../../../css/admin/main';

const cx = classNames.bind(styles);

const PlaceBlockOrRelease = ({ place, request, updateGeneratePosts, destroyFeatureToggle, showCounter }) => {
  function handleBlockFeature () {
    updateGeneratePosts(place.id, false);
    if (request) {
      destroyFeatureToggle(request.id);
    }
  }

  function handleReleaseFeature () {
    updateGeneratePosts(place.id, true)
    .then(() => {
      if (request) {
        destroyFeatureToggle(request.id);
      }
    });
  }
  return (
    <li key={place.id}>
      {place.canGeneratePosts ?
        <button className={cx('feature-button', 'block-button')} onClick={handleBlockFeature}>Bloquear</button> :
        <button className={cx('feature-button', 'release-button')} onClick={handleReleaseFeature}>Liberar</button>
      }
      <span>{place.name}</span>
      {(showCounter && place.numberOfPosts) ? place.numberOfPosts : 0}
    </li>
  );
};

class FeatureToggle extends Component {
  static need = [
    fetchFeatureToggles,
    fetchPlaces,
  ];

  render() {
    const { places, featureToggles, updateGeneratePosts, destroyFeatureToggle } = this.props;

    const alphabeticalPlaces = places.sort(sortByName('name'));
    const listPlaces = alphabeticalPlaces.map(place => (
      <PlaceBlockOrRelease key={place.id}
        place={place}
        updateGeneratePosts={updateGeneratePosts}
        showCounter
      />
    ));

    let requestList;
    if (featureToggles.length > 0) {
      requestList = featureToggles.map(request => {
        const place = alphabeticalPlaces.find(place => place.id === request.place_id);
        return <PlaceBlockOrRelease key={request.id}
          place={place}
          updateGeneratePosts={updateGeneratePosts}
          request={request}
          destroyFeatureToggle={destroyFeatureToggle}
        />
      });
    } else {
      requestList = (<span>Nenhuma solicitação enviada.</span>);
    }

    const placesToExport = places.reduce((accPlaces, place) => {
      if (!place.canGeneratePosts) {
        return [...accPlaces]
      }
      let posts = []
      if (place.posts) {
        posts = place.posts.reduce((acc, cur) => {
          const date = new Date(cur.date);
          const key = `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`

          if (acc[key]) {
            return {
              [key]: acc[key] + 1
            }
          }
          return {
            ...acc,
            [key]: 1
          }
        }, {})
      }

      return [...accPlaces, {
        name: place.name,
        posts,
        numberOfPosts: place.numberOfPosts,
      }]
    }, [])
    console.log(placesToExport)
    const dataCsv = placesToExport.reduce((acc, place) => {
      if (Object.keys(place.posts).length === 0) {
        return [...acc, {
          name: place.name,
          numberOfPosts: place.numberOfPosts
        }]
      }
      const value = Object.keys(place.posts).map((key, index) => {
        const posts = place.posts[key]
        if (index === 0) {
          return {
            name: place.name,
            date: key,
            posts,
            numberOfPosts: place.numberOfPosts
          }
        }
        return {
          date: key,
          posts,
        }
      })
      return [...acc, ...value]
    }, []);

    const columns = [
      {
        id: 'name',
        displayName: 'Bar',
      },
      {
        id: 'date',
        displayName: 'Data',
      },
      {
        id: 'posts',
        displayName: 'Número de Posts',
      },
      {
        id: 'numberOfPosts',
        displayName: 'Total',
      }
    ];
		const fileName = 'posts';

    return (
      <div className={cx('admin-content')}>
        <AdminTitle main_title={"Liberação de post"} main_description={""} />
        <div className={cx('actions')}>
					<CsvDownloader
						filename={fileName}
						columns={columns}
						datas={dataCsv}
						suffix="true"
					>
						<button className={cx('btn-export')}>Exportar CSV</button>
					</CsvDownloader>
				</div>
        <div className={cx('admin-row')}>
          <div className={cx('admin-col-6')}>
            <h2 className={cx('col-title')}>Lista de Bares</h2>
            <ul className={cx('admin-list')}>{listPlaces}</ul>
          </div>
          <div className={cx('admin-col-6')}>
            <h2 className={cx('col-title')}>Solicitações</h2>
            <ul>{requestList}</ul>
          </div>
        </div>
      </div>
    );
    
  }
}

FeatureToggle.propTypes = {
  places: PropTypes.array.isRequired,
  featureToggles: PropTypes.array.isRequired,
  updateGeneratePosts: PropTypes.func.isRequired,
  destroyFeatureToggle: PropTypes.func.isRequired,
};

export default FeatureToggle;
