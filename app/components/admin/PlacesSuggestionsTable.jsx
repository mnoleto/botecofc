import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from 'css/admin/table';
const cx = classNames.bind(styles);


class PlaceSuggestionItem extends Component {
	constructor(props) {
		super(props);
		this.onHandleApprove = this.onHandleApprove.bind(this);
		this.onHandleDisapprove = this.onHandleDisapprove.bind(this);
	};

	onHandleApprove() {
		const { suggestion, onApprove } = this.props;
		const { place } = this.props.suggestion;
		
		onApprove({
			name: place.name,
			address: place.address,
			city: place.city
		}, suggestion.id);
	}

	onHandleDisapprove() {
		const { suggestion, onDisapprove } = this.props;
		onDisapprove(suggestion.id);
	}

	render() {
		const { cities, states, suggestion } = this.props;
		const dateFormated = new Date(suggestion.date);
		const month = dateFormated.getMonth() + 1;
		const date = dateFormated.getDate() + '/' + month + '/' + dateFormated.getFullYear();
		const city = cities.find(item => item.id === suggestion.place.cityId)
		const state = states.find(item => item.id === suggestion.place.stateId)
		return (
			<tr>
				<td>
					<button type="button" className={cx('btn-approve')} onClick={this.onHandleApprove}>
						<i className={cx('zmdi', 'zmdi-check-circle')}></i>
					</button>

					<button type="button" className={cx('btn-disapprove')} onClick={this.onHandleDisapprove}>
						<i className={cx('zmdi', 'zmdi-close-circle')}></i>
					</button>

					<strong>{suggestion.place.name}</strong>
				</td>
				<td>
					{(suggestion.place.address) ? suggestion.place.address : '--'}
				</td>
				<td>
					{(suggestion.place.reference) ? suggestion.place.reference : '--'}
				</td>
				<td>
					{city && city.name}-{state && state.name}
				</td>
				<td>
					{(suggestion.user.email) ? suggestion.user.email : '--'}
				</td>
				<td>
					{date}
				</td>
			</tr>
		);
	};
};

PlaceSuggestionItem.propTypes = {
	cities: PropTypes.array.isRequired,
	states: PropTypes.array.isRequired,
	suggestion: PropTypes.object.isRequired,
	index: PropTypes.number.isRequired,
	onApprove: PropTypes.func.isRequired,
	onDisapprove: PropTypes.func.isRequired
};



export default class PlacesSuggestionsTable extends Component {
	constructor(props) {
		super(props);
	};

	render() {
		const { cities, states, placesSuggestions, onDisapprove, onApprove } = this.props;

		let suggestionsItens;
		if(placesSuggestions.length > 0) {
			suggestionsItens = placesSuggestions.map((item, index) => {
				return (
					<PlaceSuggestionItem
						cities={cities}
						states={states}
						key={index}
						index={index}
						suggestion={item}
						onApprove={onApprove}
						onDisapprove={onDisapprove}
					/>
				);
			});
		} else {
			suggestionsItens = <tr>
			  <td colSpan="2">Nenhum registro encontrado.</td>
			</tr>
		}
		
		return (
			<div className={cx('container-fluid')}>
				<div className={cx('col-md-12')}>
					<div className={cx('box')}>
						<table className={cx('table', 'table-bordered', 'table-suggestion')}>
							<thead>
								<tr>
									<th>Nome Boteco</th>
									<th>Endereço</th>
									<th>Referência</th>
									<th>Localidade</th>
									<th>Enviado por</th>
									<th>Data</th>
								</tr>
							</thead>
							<tbody>
								{suggestionsItens}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		);
	}
};

PlacesSuggestionsTable.propTypes = {
	cities: PropTypes.array.isRequired,
	states: PropTypes.array.isRequired,
	placesSuggestions: PropTypes.array.isRequired,
	onApprove: PropTypes.func.isRequired,
	onDisapprove: PropTypes.func.isRequired
};

