import React, { Component } from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames/bind';
import styles from 'css/admin/pagination';
const cx = classNames.bind(styles);

class Dot extends Component {
	constructor(props) {
		super(props);

		this.onHandleClickDot = this.onHandleClickDot.bind(this);
	};

	onHandleClickDot() {
		const { goto, index } = this.props;
		goto(index);
	}

	render() {
		const { index, className } = this.props;
		return (
			<li><a onClick={this.onHandleClickDot} className={cx(className)}>{index+1}</a></li>
		);
	};
};

Dot.propTypes = {
	className: PropTypes.string,
	index: PropTypes.number.isRequired,
	goto: PropTypes.func
};

export default class Pagination extends Component {
	constructor(props) {
		super(props);
	};

	render() {
		const { page, length, goto } = this.props;

		let dataDots = [];
		for(var i = 0; i < length; i ++) {
			let index = i;

			dataDots.push(
				<Dot
					key={index}
					index={index}
					className={(page === i) ? 'active' : ''}
					goto={goto}
				/>);
		}
		return (
			<div className={cx('pagination')}>
				<ul>
					{dataDots}
				</ul>
			</div>
		)
	};
}

Pagination.propTypes = {
	page: PropTypes.number.isRequired,
	length: PropTypes.number.isRequired,
	goto: PropTypes.func.isRequired
};
