import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import { StatesSelect } from '../selects'
import AdminTableLinks from '../admin/AdminTableLinks';
import Pagination from '../admin/Pagination';
import sortByName from '../../utils/sortByName';
import styles from '../../css/admin/table';

const cx = classNames.bind(styles);

/*
 * ADMIN LIST
 */
export default class CityList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      itensPerPage: 10,
      page: 0,
      stateId: ''
    }

    this.changePage = this.changePage.bind(this);
    this.changeItensPerPage = this.changeItensPerPage.bind(this);
    this.handleStateIdChange = this.handleStateIdChange.bind(this)
  };
  
  handleStateIdChange(event) {
    this.setState({
      stateId: event.target.value
    })
  }

  changePage(page) {
    this.setState({ page });
  }

  changeItensPerPage() {
    this.setState({ itensPerPage: this.refs.itensPerPage.value });
  }

  render() {
    const { cities, headerTitle, onDestroy, onEdit, states } = this.props;
    if (!cities || cities.length === 0 || !states || states.length === 0) {
      return
    }
    const { itensPerPage, page, stateId } = this.state
    const filteredCity = cities.filter(item => item.stateId === stateId)
    let citiesItens = [];

    const initialItem = page * itensPerPage;
    const endItem = initialItem + (itensPerPage - 1);
    const numberOfPages = Math.ceil(filteredCity.length / itensPerPage);

    if (filteredCity && filteredCity.length > 0 && stateId != '') {
      filteredCity.sort(sortByName('name'));

      citiesItens = filteredCity.map((item, index) => {
        if (index >= initialItem && index <= endItem) {
          return (
            <tr key={index}>
              <td className={cx('table-link')}>
                <div className={cx('table-first-column')}>
                  <div className={cx('main-info')}>{item.name}</div>
                  <AdminTableLinks
                    id={item.id}
                    index={index}
                    data={item}
                    onDestroy={onDestroy}
                    onEdit={onEdit} />
                </div>
              </td>
            </tr>
          );
        }

      });
    } else {
      citiesItens = <tr>
        <td colSpan="2">Nenhum registro encontrado.</td>
      </tr>
    }

    return (
      <div className={cx('admin-table-col')}>
        <h2 className={cx('table-title')}>{headerTitle}</h2>
        <div className={cx('box', 'box-success')}>
          <div className={cx('box-body')}>
            <div className={cx('table-header')}>
              <div className={cx('show-entries', 'pull-right', 'form-inline')}>
                <label htmlFor="perpage">Mostrar</label>
                <select ref="itensPerPage" value={this.state.itensPerPage} className={cx('form-control', 'input-sm')} onChange={this.changeItensPerPage}>
                  <option value="10">10</option>
                  <option value="25">25</option>
                  <option value="50">50</option>
                </select>
              </div>
            </div>

            <div>
              <StatesSelect
                onChange={this.handleStateIdChange}
                selectedState={this.state.stateId}
                states={states}
              />
            </div>

            <table className={cx('table', 'table-bordered')}>
              <thead>
                <tr>
                  <th>Nome</th>
                </tr>
              </thead>

              <tbody>
                {citiesItens}
              </tbody>
            </table>

            <Pagination
              page={this.state.page}
              length={numberOfPages}
              goto={this.changePage}
            />
          </div>
        </div>
      </div>
    );
  }
}

CityList.propTypes = {
  cities: PropTypes.array.isRequired,
  states: PropTypes.array.isRequired,
  folder: PropTypes.string,
  headerTitle: PropTypes.string.isRequired,
  onDestroy: PropTypes.func,
  onEdit: PropTypes.func.isRequired,
};
