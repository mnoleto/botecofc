import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { fetchChannels } from 'actions/channels';
import AdminList from 'components/admin/AdminList';
import AdminTitle from 'components/admin/AdminTitle';
import ChannelsForm from 'components/admin/channels/ChannelsForm';

import classNames from 'classnames/bind';
import styles from 'css/admin/main';
const cx = classNames.bind(styles);

/*
 * CHANNELS
 */
class Channels extends Component {
    static need = [
        fetchChannels,
    ];

    constructor(props) {
        super(props);

        this.state = {
            index: null,
            channel: {}
        };

        this.clearChange = this.clearChange.bind(this);
        this.onDestroy = this.onDestroy.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.onEntryChange = this.onEntryChange.bind(this);
    };

    clearChange() {
        this.setState({
            index: null,
            channels: {}
        });
    };

    onDestroy(id, index) {
        const { destroyChannel } = this.props;
        destroyChannel(id, index);
    };

    onEdit(data, index) {
        this.setState({
            index: index,
            channel: data
        });
    };

    onEntryChange(data) {
        const { updateChannel } = this.props;
        updateChannel(this.state.index, data);
        this.clearChange();
    };

    render() {
        const { channels, createChannel } = this.props;

        return (
            <div className={cx('row', 'admin-content')}>
                <AdminTitle
                    main_title={"Canais"}
                    main_description={"Responsável pelo gerenciamento de canais de tv."}
                />

                <ChannelsForm
                    channel={this.state.channel}
                    onEntrySave={createChannel}
                    onEntryChange={this.onEntryChange}
                />

                <AdminList
                    data={channels}
                    folder={'channels'}
                    headerTitle={"Canais cadastrados"}
                    onDestroy={this.onDestroy}
                    onEdit={this.onEdit}
                />
            </div>
        );
    }

};

Channels.propTypes = {
    channels: PropTypes.array.isRequired,
    createChannel: PropTypes.func.isRequired,
    updateChannel: PropTypes.func.isRequired,
    destroyChannel: PropTypes.func.isRequired,
};

export default Channels;
