import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LoadingMessage from 'components/admin/LoadingMessage';
import classNames from 'classnames/bind';
import styles from 'css/admin/form';
const cx = classNames.bind(styles);


/*
 * CHANNELS FORM
 */
export default class ChannelsForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            logo: {},
            message: '',
        };

        this.eventHandlers = {};

        this.isEditing = false;
        this.clearImages = false;
        this.isSubmiting = false;

        this.clearForm = this.clearForm.bind(this);
        this.handleFormChange = this.handleFormChange.bind(this);
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.onClearImages = this.onClearImages.bind(this);
        this.onEntrySave = this.onEntrySave.bind(this);
        this.onLogoRemoveImage = this.onLogoRemoveImage.bind(this);
        this.onLogoUploadComplete = this.onLogoUploadComplete.bind(this);

        this.renderContent = this.renderContent.bind(this);
        this.renderMessage = this.renderMessage.bind(this);
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.channel && nextProps.channel.id) {
            this.setState({
                id: nextProps.channel.id,
                name: nextProps.channel.name,
                logo: nextProps.channel.logo,
            });
        }
        this.isEditing = true;
        this.isSubmiting = false;
    }

    clearForm() {
        this.setState({
            name: '',
            logo: {},
        });

        this.clearImages = true;
    };

    handleFormChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    };

    handleTitleChange(event) {
        const title = event.target.value;
        this.setState({ [event.target.name]: title });
    }

    onClearImages() {
        this.clearImages = false;
    };

    onEntrySave(e) {
        e.preventDefault();
        const { onEntrySave, onEntryChange, channel } = this.props;

        if (this.state.name === '') {
            this.setState({ message: 'Preencha o campo nome.' });
            return false;
        }

        this.setState({ message: '' });

        this.isSubmiting = true;

        if (channel.name) {
            onEntryChange({
                'id': this.state.id,
                'name': this.state.name,
                // 'logo': this.state.logo,
            });
        } else {
            onEntrySave({
                'name': this.state.name,
                // 'logo': this.state.logo,
            });
        }
        this.clearForm();
    };

    onLogoRemoveImage(file) {
        this.setState({ logo: {} });
    }

    onLogoUploadComplete(file) {
        this.state.logo = file;
    };

    renderContent() {
        if (!this.isSubmiting) {
            return (
                <form className={cx('clearfix', 'form-inline', 'container-fluid')} method="post" encType="multipart/form-data" onSubmit={this.onEntrySave} role="form">
                    {this.renderMessage()}
                    {/* <div className={cx('row')}>
                        <div className={cx('upload-field', 'col-md-full')}>
                            <UploadImage
                                id={'logo'}
                                classNames={'upload-logo'}
                                clearImages={this.clearImages}
                                image={this.state.logo}
                                isEditing={this.isEditing}
                                postUrl={'/uploadChannel'}
                                onClearImages={this.onClearImages}
                                onRemoveImage={this.onLogoRemoveImage}
                                onUploadComplete={this.onLogoUploadComplete}
                                text={'INCLUA A LOGO DO CANAL'}
                                uploadMultiple={false}
                            />
                        </div>
                    </div> */}

                    <div className={cx('row')}>
                        <div className={cx('col-md-full')}>
                            <div className={cx('field-group')}>
                                <label htmlFor="name" className={cx('sr-only')}>Nome *</label>
                                <input type="text" name="name" placeholder="Nome *" value={this.state.name || ''} onChange={this.handleTitleChange} />
                            </div>
                        </div>
                    </div>

                    <div className={cx('row')}>
                        <div className={cx('col-md-full', 'required')}><p className={cx('pull-right')}><small>* campos obrigatórios</small></p></div>
                    </div>

                    {this.renderMessage()}

                    <div className={cx('row')}>
                        <input className={cx('submit-btn', 'pull-right')} type="submit" value='Enviar' />
                        <input className={cx('clear-btn', 'pull-right')} type="button" value="Limpar" onClick={this.clearForm} />
                    </div>
                </form>
            );
        } else {
            return <LoadingMessage />;
        }
    }

    renderMessage() {
        if (this.state.message !== '') {
            return (
                <div className={cx('message')}>
                    <i className="zmdi zmdi-block"></i> {this.state.message}
                </div>
            );
        }
    }

    render() {
        return (
            <div className={cx('admin-form-col')}>
                <h2 className={cx('form-title')}>Dados do canal</h2>
                {this.renderContent()}
            </div>
        );
    };
}

ChannelsForm.propTypes = {
    channel: PropTypes.object,
    onEntryChange: PropTypes.func.isRequired,
    onEntrySave: PropTypes.func.isRequired,
};

