import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Dropzone from 'react-dropzone';
import classNames from 'classnames/bind';

import { uploadFile } from '../../actions/files';
import styles from '../../css/admin/dropzone';

const cx = classNames.bind(styles);

class UploadImage extends Component {
	constructor(props) {
		super(props);

		this.state = {
			files: []
		};
		this.error = false;
		this.uploading = false;
		this.uploadComplete = false;
		this.clearImages = this.clearImages.bind(this);
		this.onDrop = this.onDrop.bind(this);
		this.onRemove = this.onRemove.bind(this);
		this.onUploadComplete = this.onUploadComplete.bind(this);
		this.onUploadFile = this.onUploadFile.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.clearImages) {
			this.uploadComplete = false;
			this.clearImages();
		}
		if (nextProps.isEditing) {
			this.setState({
				files: nextProps.image
			});
			this.forceUpdate();
		}
	}

	clearImages() {
		this.setState({
			files: []
		});
		this.uploadComplete = false;
		this.forceUpdate();
	}

	onDrop(acceptedFiles) {
		const size = acceptedFiles[0].size/1024/1024;
		if (size > 1) {
			this.error = true;
		} else {
			this.error = false;
		}
		this.setState({
			files: acceptedFiles
		});
	}

	onRemove() {
		const { onRemoveImage } = this.props;
		if (this.uploading) return false;
		this.error = false;
		onRemoveImage(this.state.files);
		this.clearImages();
		return true;
	}

	onUploadComplete(data) {
		const { index, onUploadComplete } = this.props;
		this.uploading = false;
		this.uploadComplete = true;

		onUploadComplete(JSON.parse(data), index);
		this.forceUpdate();
	}

	onUploadFile() {
		const { postUrl, uploadFile } = this.props;
		const _this = this;

		if (this.uploading || this.error) return false;

		this.uploading = true;
		this.forceUpdate();

		const fd = new FormData();    
		fd.append('file', this.dropzone.fileInputEl.files[0]);

		uploadFile(fd, postUrl, (feedback) => {
			if (feedback.status === 200) {
				_this.onUploadComplete(feedback.response);
			}
		});
	}

	render() {
		const { classNames, uploadMultiple, id, text, isEditing } = this.props;
		const _this = this;

		const classArr = classNames.split(' ');
		const classes = classArr.map((cl, i) => {
			return cx(cl);
		});

		let content;
		if (this.state.files && (Object.keys(this.state.files).length > 0 && this.state.files.constructor === Object)) {
			const image = this.state.files.responseText;
			content = (
				<div className={classes.join(' ')}>
					<div
						className={cx('dropzone-preview')}>
						<img src={image} alt="" />
					</div>
				</div>
			)
		} else if (this.state.files && this.state.files.length > 0) {
			let icon;
			if (this.uploadComplete) {
				icon = (<i className={cx('dropzone-sucess')} />);
			}
			content = <Dropzone
						accept="image/*"
						className={classes.join(' ')}
						name={id}
						multiple={uploadMultiple}
						ref={(node) => { this.dropzone = node; }}
						onDrop={this.onDrop}
					>
						<div
							className={cx('dropzone-preview')}>
							<img src={this.state.files[0].preview} />
							{icon}
						</div>
					</Dropzone>
		} else {
			content = <Dropzone
							accept="image/*"
							className={classes.join(' ')}
							name={id}
							multiple={uploadMultiple}
							ref={(node) => { this.dropzone = node; }}
							onDrop={this.onDrop}
						>
							<div className={cx('drop-button')}>{text}</div>
						</Dropzone>
		}


		let buttons;
		if (this.uploadComplete || (this.state.files && ( Object.keys(this.state.files).length > 0 && this.state.files.constructor === Object))) {
			buttons = <div className={cx('dropzone-buttons')}>
				<button type="button" className={cx('btn-remove')} onClick={this.onRemove}><i className={cx('zmdi', 'zmdi-close')} /></button>
			</div>
		} else if(this.state.files && this.state.files.length > 0) {
			buttons = <div className={cx('dropzone-buttons')}>
					<button type="button" className={cx('btn-remove')} onClick={this.onRemove}><i className={cx('zmdi', 'zmdi-close')} /></button>
					<button type="button" className={cx('btn-upload')} onClick={this.onUploadFile}><i className={cx('zmdi', 'zmdi-upload')} /></button>
				</div>
		}

		let message;
		if (this.uploading) {
			message = <div className={cx('message')}><span>enviando...</span></div>
		} else if (this.error) {
			message = <div className={cx('message')}><span>ERRO!</span></div>
		}

		return (
			<div className={cx('dropzone-custom')}>
				{content}
				{buttons}
				{message}
			</div>
		);
	}
}

UploadImage.propTypes = {
	classNames: PropTypes.string,
	clearImages: PropTypes.bool,
	id: PropTypes.string,
	index: PropTypes.number,
	isEditing: PropTypes.bool,
	image: PropTypes.any,
	onUploadComplete: PropTypes.func.isRequired,
	onRemoveImage: PropTypes.func,
	onClearImages: PropTypes.func,
	postUrl: PropTypes.string.isRequired,
	text: PropTypes.string,
	uploadMultiple: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
	return {};
}

export default connect(mapStateToProps, { uploadFile })(UploadImage);

