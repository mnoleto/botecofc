import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Slugify from 'utils/slugify';

import LoadingMessage from 'components/admin/LoadingMessage';
import UploadImage from 'components/admin/UploadImage';

import classNames from 'classnames/bind';
import styles from 'css/admin/form';
const cx = classNames.bind(styles);

/*
 * TEAMS FORM
 */
class TeamsForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			id: '',
			name: '',
			slug: '',
			sport: {},
			country: {},
			color: '#000000',
			textColor: '#ffffff',
			logo: {},

			message: ''
		};

		this.eventHandlers = {};

		this.isEditing = false;
		this.clearImages = false;
		this.isSubmiting = false;

		this.clearForm = this.clearForm.bind(this);
		this.onClearImages = this.onClearImages.bind(this);
		this.handleFormChange = this.handleFormChange.bind(this);
		this.handleTitleChange = this.handleTitleChange.bind(this);
		this.onLogoRemoveImage = this.onLogoRemoveImage.bind(this);
		this.onLogoUploadComplete = this.onLogoUploadComplete.bind(this)
		this.onEntrySave = this.onEntrySave.bind(this);

		this.renderContent = this.renderContent.bind(this);
		this.renderMessage = this.renderMessage.bind(this);
	};

	componentWillReceiveProps(nextProps) {
		if (nextProps.team.name) {
			this.setState({
				id: nextProps.team.id,
				name: nextProps.team.name,
				slug: nextProps.team.slug,
				sport: nextProps.team.sport,
				country: nextProps.team.country,
				color: nextProps.team.color,
				textColor: nextProps.team.textColor,
				logo: nextProps.team.logo
			});
		}
		this.isEditing = true;
		this.isSubmiting = false;
	}

	clearForm() {
		this.setState({
			id: '',
			name: '',
			slug: '',
			sport: {},
			country: {},
			color: '#000000',
			textColor: '#ffffff',
			logo: {}
		});
		this.refs.sport.value = 0;
		this.refs.country.value = 0;
		this.clearImages = true;
		window.scrollTo(0,0);
		this.forceUpdate();
	};

	onClearImages() {
		this.clearImages = false;
	};

	handleFormChange(event) {
		this.setState({[event.target.name]: event.target.value});
	};

	handleTitleChange(event) {
		const title = event.target.value;
		this.setState({[event.target.name]: title});
		this.state.slug = Slugify(title);
	}

	onEntrySave(e) {
		e.preventDefault();
		const { team, onEntrySave, onEntryChange } = this.props;

		if(!this.state.logo.responseText) {
			this.setState({message: 'Faça o upload da logo do campeonato.'});
			return false;
		}

		if(this.state.name === '') {
			this.setState({message: 'Preencha o campo nome.'});
			return false;
		}

		if(this.state.country === '0') {
			this.setState({message: 'Selecione um país.'});
			return false;
		}

		if(this.state.sport === '0') {
			this.setState({message: 'Selecione um esporte.'});
			return false;
		}

		if(this.state.color === '') {
			this.setState({message: 'Selecione uma cor para o time.'});
			return false;
		}

		if(this.state.textColor === '') {
			this.setState({message: 'Selecione uma cor para o texto do time.'});
			return false;
		}

		this.setState({message: ''});

		this.isSubmiting = true;
		
		if(team.name) {
			onEntryChange({
				'id': this.state.id,
				'name': this.state.name,
				'slug': this.state.slug,
				'sport': {
					'id': this.refs.sport.value,
					'name': this.refs.sport.selectedOptions[0].text
				},
				'country': {
					'id': this.refs.country.value,
					'name': this.refs.country.selectedOptions[0].text
				},
				'color': this.state.color,
				'textColor': this.state.textColor,
				'logo': this.state.logo
			});
		} else {
			onEntrySave({
				'name': this.state.name,
				'slug': this.state.slug,
				'sport': {
					'id': this.state.sport,
					'name': this.refs.sport.selectedOptions[0].text
				},
				'country': {
					'id': this.state.country,
					'name': this.refs.country.selectedOptions[0].text
				},
				'color': this.state.color,
				'textColor': this.state.textColor,
				'logo': this.state.logo
			});
		}
		
		this.clearForm();
	};

	onLogoRemoveImage(file) {
		this.setState({logo: {}});
	}

	onLogoUploadComplete(file) {
		this.state.logo = file;
	};

	renderContent() {
		if(!this.isSubmiting) {
			const { team, countries, sports } = this.props;
			const countriesItens = countries.map((item, index) => {
				return (
					<option key={index} value={item.id}>{item.name}</option>
				);
			});
			const sportsItens = sports.map((item, index) => {
				return (
					<option key={index} value={item.id}>{item.name}</option>
				);
			});
			return (
				<form className={cx('clearfix', 'form-inline', 'container-fluid')} method="post" encType="multipart/form-data" onSubmit={this.onEntrySave} role="form">
					{this.renderMessage()}

					<div className={cx('row')}>
						<div className={cx('upload-field', 'col-md-full')}>
							<UploadImage
								id={'logo'}
								classNames={'upload-logo logo-team'}
								clearImages={this.clearImages}
								image={this.state.logo}
								isEditing={this.isEditing}
								postUrl={'/uploadTeam'}
								onClearImages={this.onClearImages}
								onRemoveImage={this.onLogoRemoveImage}
								onUploadComplete={this.onLogoUploadComplete}
								text={'INCLUA A LOGO DO TIME'}
								uploadMultiple={false}
								/>
	          	</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="name" className={cx('sr-only')}>Nome *</label>
								<input type="text" name="name" ref="name" placeholder="Nome *" value={this.state.name || ''} onChange={this.handleTitleChange} />
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<label htmlFor="slug" className={cx('sr-only')}>Slug *</label>
								<input type="text" name="slug" ref="slug" readOnly placeholder="Slug *" value={this.state.slug || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-quarter')}>
							<div className={cx('field-group')}>
							<label htmlFor="country">Cor *</label>
							<input type="color" name="color" className={cx('inline-field')} value={this.state.color || '#000000'} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-quarter')}>
							<div className={cx('field-group')}>
							<label htmlFor="country">Texto *</label>
							<input type="color" name="textColor" className={cx('inline-field')} value={this.state.textColor || '#ffffff'} onChange={this.handleFormChange} />
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="sport" className={cx('sr-only')}>Esporte *</label>
								<select ref="sport" name="sport" placeholder="Esporte *" value={(this.state.sport) ? this.state.sport.id : '0'} className={cx('full')} onChange={this.handleFormChange}>
									<option value="0">Selecione um esporte</option>
									{sportsItens}
								</select>
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="country" className={cx('sr-only')}>País *</label>
								<select ref="country" name="country" placeholder="País *" value={(this.state.country) ? this.state.country.id : '0'} className={cx('full')} onChange={this.handleFormChange}>
									<option value="0">Selecione um país</option>
									{countriesItens}
								</select>
							</div>
						</div>
					</div>
				  
					<div className={cx('row')}>
						<div className={cx('col-md-full', 'required')}><p className={cx('pull-right')}><small>* campos obrigatórios</small></p></div>
					</div>

					{this.renderMessage()}
				  
					<div className={cx('row')}>
						<input className={cx('submit-btn', 'pull-right')} type="submit" value='Enviar' />
						<input className={cx('clear-btn', 'pull-right')} type="button" value="Limpar" onClick={this.clearForm} />
					</div>
				</form>
			);
		} else {
			return <LoadingMessage />;
		}
	}

	renderMessage() {
		if(this.state.message !== '') {
			return (
				<div className={cx('message')}>
					<i className="zmdi zmdi-block"></i> {this.state.message}
				</div>
			);
		}
	}

	render() {
		return (
			<div className={cx('admin-form-col')}>
				<h2 className={cx('form-title')}>Dados do time</h2>
				{this.renderContent()}
			</div>
		);
	};
}

TeamsForm.propTypes = {
	team: PropTypes.object,
	countries: PropTypes.array.isRequired,
	sports: PropTypes.array.isRequired,
	onEntryChange: PropTypes.func.isRequired,
	onEntrySave: PropTypes.func.isRequired
};

export default TeamsForm;
