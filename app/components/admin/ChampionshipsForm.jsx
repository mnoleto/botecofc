import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Slugify from 'utils/slugify';

import LoadingMessage from 'components/admin/LoadingMessage';
import UploadImage from 'components/admin/UploadImage';
import InputElement from 'react-input-mask';
import ChampionshipsSelectedList from 'components/admin/ChampionshipsSelectedList';

import classNames from 'classnames/bind';
import styles from 'css/admin/form';
const cx = classNames.bind(styles);


/*
 * CHAMPIONSHIP FORM
 */
export default  class ChampionshipsForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			name: '',
			slug: '',
			dateBegin: '',
			dateEnd: '',
			logo: {},
			sport: '',
			country: '',
			teams: '',
			sports: [],
			countries: [],
			teams: [],

			message: ''
		};

		this.eventHandlers = {};

		this.isEditing = false;
		this.clearImages = false;
		this.isSubmiting = false;

		this.clearForm = this.clearForm.bind(this);
		this.handleFormChange = this.handleFormChange.bind(this);
		this.handleFiltersChange = this.handleFiltersChange.bind(this);
		this.handleCountriesChange = this.handleCountriesChange.bind(this);
		this.handleSportsChange = this.handleSportsChange.bind(this);
		this.handleTeamsChange = this.handleTeamsChange.bind(this);
		this.handleTitleChange = this.handleTitleChange.bind(this);
		this.onClearImages = this.onClearImages.bind(this);
		this.onEntrySave = this.onEntrySave.bind(this);
		this.onLogoRemoveImage = this.onLogoRemoveImage.bind(this);
		this.onLogoUploadComplete = this.onLogoUploadComplete.bind(this);

		this.removeSelectedCountry = this.removeSelectedCountry.bind(this);
		this.removeSelectedSport = this.removeSelectedSport.bind(this);
		this.removeSelectedTeam = this.removeSelectedTeam.bind(this);

		this.renderContent = this.renderContent.bind(this);
		this.renderMessage = this.renderMessage.bind(this);
	};

	componentWillReceiveProps(nextProps) {
		if (nextProps.championship.id) {
			this.setState({
				id: nextProps.championship.id,
				name: nextProps.championship.name,
				slug: nextProps.championship.slug,
				dateBegin: this.unconvertDateFormat(nextProps.championship.dateBegin),
				dateEnd: this.unconvertDateFormat(nextProps.championship.dateEnd),
				logo: nextProps.championship.logo,
				countries: nextProps.championship.countries,
				sports: nextProps.championship.sports,
				teams: nextProps.championship.teams
			});
		}
		this.isEditing = true;
		this.isSubmiting = false;
	}

	clearForm() {
		this.setState({
			name: '',
			slug: '',
			dateBegin: '',
			dateEnd: '',
			logo: {},
			sport: '',
			country: '',
			teams: '',
			sports: [],
			countries: [],
			teams: []
		});
		this.refs.country.value = 0;
		this.refs.sport.value = 0;
		this.refs.team.value = 0;

		window.scrollTo(0,0);
		
		this.handleFiltersChange();
		this.clearImages = true;
		this.forceUpdate();
	};

	convertDateFormat(date) {
		const dateArr = date.split('/');
		const dateMonth = parseInt(dateArr[1]);
		const dateStr = (dateMonth) + '/' + dateArr[0] + '/' + dateArr[2];
		return new Date(dateStr);
	};

	unconvertDateFormat(date) {
		const originalDate = new Date(date);
		let day = originalDate.getDate(),
			month = originalDate.getMonth(),
			year = originalDate.getFullYear();
		
		month ++;

		if(day < 10) {
			day = '0' + day;
		}

		if(month < 10) {
			month = '0' + month;
		}
		return (day + '/' + month + '/' + year);
	}

	handleFormChange(event) {
		this.setState({[event.target.name]: event.target.value});
	};

	handleFiltersChange() {
		const { onChangeTeamFilter } = this.props;

		if(this.state.sports.length === 0 && this.state.countries.length === 0) {
			onChangeTeamFilter('SHOW_ALL', '', '');
		} else {
			onChangeTeamFilter('SHOW_ONLY', {
				sports: this.state.sports,
				countries: this.state.countries
			});
		}
	};

	handleCountriesChange() {
		var country = this.refs.country.value;
		if(country === '' || country === '0') return false;
		this.state.countries.push({
			id: country,
			name: this.refs.country.selectedOptions[0].text
		});
		// this.forceUpdate();
		this.handleFiltersChange();
	};

	handleSportsChange() {
		var sport = this.refs.sport.value;
		if(sport === '' || sport === '0') return false;
		this.state.sports.push({
			id: sport,
			name: this.refs.sport.selectedOptions[0].text
		});
		// this.forceUpdate();
		this.handleFiltersChange();
	};

	handleTeamsChange() {
		var team = this.refs.team.value;
		if(team === '' || team === '0') return false;
		this.state.teams.push({
			id: team,
			name: this.refs.team.selectedOptions[0].text
		});
		this.forceUpdate();
	};

	handleTitleChange(event) {
		const title = event.target.value;
		this.setState({[event.target.name]: title});
		this.state.slug = Slugify(title);
	}

	onClearImages() {
		this.clearImages = false;
	};

	onEntrySave(e) {
		e.preventDefault();
		const { onEntrySave, onEntryChange, championship } = this.props;

		if(!this.state.logo.responseText) {
			this.setState({message: 'Faça o upload da logo do campeonato.'});
			return false;
		}

		if(this.state.name === '') {
			this.setState({message: 'Preencha o campo nome.'});
			return false;
		}

		if(this.state.dateBegin === '') {
			this.setState({message: 'Preencha o campo data início.'});
			return false;
		}

		if(this.state.dateEnd === '') {
			this.setState({message: 'Preencha o campo data fim.'});
			return false;
		}

		if(this.state.countries.length === 0) {
			this.setState({message: 'Adicione pelo menos um país.'});
			return false;
		}

		if(this.state.sports.length === 0) {
			this.setState({message: 'Adicione pelo menos um esporte.'});
			return false;
		}

		if(this.state.teams.length === 0) {
			this.setState({message: 'Adicione pelo menos um time.'});
			return false;
		}

		this.setState({message: ''});

		if(this.state.dateBegin !== '') this.state.dateBegin = this.convertDateFormat(this.state.dateBegin);
		if(this.state.dateEnd !== '') this.state.dateEnd = this.convertDateFormat(this.state.dateEnd);
		
		this.isSubmiting = true;
		
		if(championship.name){
			onEntryChange({
				'id': this.state.id,
				'name': this.state.name,
				'slug': this.state.slug,
				'dateBegin': new Date(this.state.dateBegin),
				'dateEnd': new Date(this.state.dateEnd),
				'logo': this.state.logo,
				'countries': this.state.countries,
				'sports': this.state.sports,
				'teams': this.state.teams
			});
		} else {
			onEntrySave({
				'name': this.state.name,
				'slug': this.state.slug,
				'dateBegin': new Date(this.state.dateBegin),
				'dateEnd': new Date(this.state.dateEnd),
				'logo': this.state.logo,
				'countries': this.state.countries,
				'sports': this.state.sports,
				'teams': this.state.teams
			});
		}

		this.clearForm();
	};

	onLogoRemoveImage(file) {
		this.setState({logo: {}});
	}

	onLogoUploadComplete(file) {
		this.state.logo = file;
	};

	removeSelectedCountry(index) {
		this.state.countries.splice(index, 1);
		this.forceUpdate();
	}

	removeSelectedTeam(index) {
		this.state.teams.splice(index, 1);
		this.forceUpdate();
	}

	removeSelectedSport(index) {
		this.state.sports.splice(index, 1);
		this.forceUpdate();
	}

	renderContent() {
		if(!this.isSubmiting) {
			const { countries, sports, teams } = this.props;
			const countriesItens = countries.map((item, index) => {
				return (
					<option key={index} value={item.id}>{item.name}</option>
				);
			});
			const sportsItens = sports.map((item, index) => {
				return (
					<option key={index} value={item.id}>{item.name}</option>
				);
			});
			const teamsItens = teams.map((item, index) => {
				return (
					<option key={index} value={item.id}>{item.name}</option>
				);
			});
			return (
				<form className={cx('clearfix', 'form-inline', 'container-fluid')} method="post" encType="multipart/form-data" onSubmit={this.onEntrySave} role="form">
					{this.renderMessage()}
					<div className={cx('row')}>
						<div className={cx('upload-field', 'col-md-full')}>
							<UploadImage
								id={'logo'}
								classNames={'upload-logo'}
								clearImages={this.clearImages}
								image={this.state.logo}
								isEditing={this.isEditing}
								postUrl={'/uploadChampionship'}
								onClearImages={this.onClearImages}
								onRemoveImage={this.onLogoRemoveImage}
								onUploadComplete={this.onLogoUploadComplete}
								text={'INCLUA A LOGO DO CAMPEONATO'}
								uploadMultiple={false}
								/>
	          			</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="name" className={cx('sr-only')}>Nome *</label>
								<input type="text" name="name" placeholder="Nome *" value={this.state.name || ''} onChange={this.handleTitleChange} />
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="slug" className={cx('sr-only')}>Slug</label>
								<input type="text" name="slug" disabled="disabled" placeholder="Slug" value={this.state.slug || ''} onChange={this.handleFormChange} />
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<label htmlFor="dateBegin" className={cx('sr-only')}>Data Início *</label>
								<InputElement name="dateBegin" placeholder="Data Início *" mask="99/99/9999" value={this.state.dateBegin || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<label htmlFor="dateEnd" className={cx('sr-only')}>Data Fim *</label>
								<InputElement name="dateEnd" placeholder="Data Fim *" mask="99/99/9999" value={this.state.dateEnd || ''} onChange={this.handleFormChange} />
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<h4 className={cx('form-title', 'with-border')}>Paises *</h4>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<select name="country" ref="country" className={cx('full')} onChange={this.handleCountriesChange}>
									<option value="0">selecione os países *</option>
									{countriesItens}
								</select>
							</div>
						</div>
						<div className={cx('col-md-full')}>
							<ChampionshipsSelectedList group={this.state.countries} removeItem={this.removeSelectedCountry} />
						</div>
					</div>

					<div className={cx('row')}>
						<h4 className={cx('form-title', 'with-border')}>Esportes *</h4>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<select name="sport" ref="sport" className={cx('full')} onChange={this.handleSportsChange}>
									<option value="0">selecione os esportes *</option>
									{sportsItens}
								</select>
							</div>
						</div>
						<div className={cx('col-md-full')}>
							<ChampionshipsSelectedList group={this.state.sports} removeItem={this.removeSelectedSport} />
						</div>
					</div>

					<div className={cx('row')}>
						<h4 className={cx('form-title', 'with-border')}>Times *</h4>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<select name="team" ref="team" className={cx('full')} onChange={this.handleTeamsChange}>
									<option value="0">selecione os times *</option>
									{teamsItens}
								</select>
							</div>
						</div>
						<div className={cx('col-md-full')}>
							<ChampionshipsSelectedList group={this.state.teams} removeItem={this.removeSelectedTeam} />
						</div>
					</div>
				  
					<div className={cx('row')}>
						<div className={cx('col-md-full', 'required')}><p className={cx('pull-right')}><small>* campos obrigatórios</small></p></div>
					</div>
				  	
				  	{this.renderMessage()}
					
					<div className={cx('row')}>
						<input className={cx('submit-btn', 'pull-right')} type="submit" value='Enviar' />
						<input className={cx('clear-btn', 'pull-right')} type="button" value="Limpar" onClick={this.clearForm} />
					</div>
				</form>
			);
		} else {
			return <LoadingMessage />;
		}
	}

	renderMessage() {
		if(this.state.message !== '') {
			return (
				<div className={cx('message')}>
					<i className="zmdi zmdi-block"></i> {this.state.message}
				</div>
			);
		}
	}

	render() {
		return (
			<div className={cx('admin-form-col')}>
				<h2 className={cx('form-title')}>Dados do campeonato</h2>
				{this.renderContent()}
			</div>
		);
	};
}

ChampionshipsForm.propTypes = {
	championship: PropTypes.object,
	countries: PropTypes.array.isRequired,
	sports: PropTypes.array.isRequired,
	teams: PropTypes.array.isRequired,
	onEntryChange: PropTypes.func.isRequired,
	onChangeTeamFilter: PropTypes.func.isRequired,
	onEntrySave: PropTypes.func.isRequired
};

