import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import sortByName from '../../utils/sortByName';
import LoadingMessage from '../admin/LoadingMessage';
import GamesSelectedList from '../../components/games/GamesSelectedList';
import styles from '../../css/admin/form';

const cx = classNames.bind(styles);

/*
 * COUNTRIES FORM
 */
export default  class BroadcastsForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			message: ''
		};

		this.eventHandlers = {};

		this.isSubmiting = false;

		this.clearForm = this.clearForm.bind(this);
		this.handleFormChange = this.handleFormChange.bind(this);
		this.handleChampionshipChange = this.handleChampionshipChange.bind(this);
		this.handleGameChange = this.handleGameChange.bind(this);
		this.handlePlaceChange = this.handlePlaceChange.bind(this);

		this.renderContent = this.renderContent.bind(this);
		this.renderMessage = this.renderMessage.bind(this);
	};

	componentDidUpdate(prevProps, prevState) {
		if(prevProps.place !== this.props.place) {
			this.refs.championships.value = '0';
			this.refs.games.value = '0';
		}
	}

	componentDidMount() {
		this.isSubmiting = false;
	};

	clearForm() {
		this.setState({
			message: ''
		});
		window.scrollTo(0,0);
	}

	handleFormChange(event) {
		this.setState({[event.target.name]: event.target.value});
	};

	handleChampionshipChange() {
		const { onSelectChampionship } = this.props;
		const championshipSelected = this.refs.championships.value;
		if(championshipSelected === '0') {
			onSelectChampionship('SHOW_ALL', '');
		} else {
			onSelectChampionship('SHOW_ONLY', championshipSelected);
		}
	}

	handleGameChange() {
		const { games, onSelectGame } = this.props;
		const gameID = this.refs.games.value;

		if(gameID === '0') return false;

		const game = games.find((value) => value.id === gameID);
		onSelectGame(game);
		this.forceUpdate();
	};

	handlePlaceChange() {
		const { onSelectPlace } = this.props;
		const placeSelected = this.refs.places.value;
		if(placeSelected === '0') {
			document.querySelector('#transmissions').style.display = 'none';
			this.setState({place: ''});
		} else {
			document.querySelector('#transmissions').style.display = 'block';
			this.setState({place: placeSelected});
		}
		onSelectPlace(placeSelected);
	}

	renderContent() {
		if(!this.isSubmiting) {
			const { place, championships, games, broadcastGames, onRemoveGame } = this.props;

			if(place && place.name !== '') {
				const alphabeticalChampionships = championships;
				alphabeticalChampionships.sort(sortByName('name'));
				const championshipsList = alphabeticalChampionships.map((championship, index) => (
					<option key={championship.id} value={championship.id}>{championship.name}</option>
				));

				const dateGames = games;
				dateGames.sort((a, b) => {
					return new Date(a.gameDate).getTime() - new Date(b.gameDate).getTime();
				});
				const gamesList = games.map((game) => {
					const date = new Date(game.gameDate);
					const hour = date.getHours() < 10 ? ('0' + date.getHours()) : date.getHours();
					const minutes = date.getMinutes() < 10 ? ('0' + date.getMinutes()) : date.getMinutes();
					return <option key={game.id} value={game.id}>{date.getDate()}/{date.getMonth() + 1} {hour}:{minutes}h - {game.homeTeam.name} x {game.visitingTeam.name}</option>
				});
				return (
					<form className={cx('clearfix', 'form-inline', 'container-fluid')} method="post" encType="multipart/form-data" role="form">
						{this.renderMessage()}

						<div className={cx('row')}>
							<div className={cx('col-md-12')}>
								<h2 className={cx('form-title')}>Adicionar jogos ao boteco: <span className={cx('name')}>{place.name}</span></h2>
							</div>
							<div className={cx('col-md-6')}>
								<div className={cx('field-group')}>
									<select ref="championships" className={cx('full')} onChange={this.handleChampionshipChange}>
										<option value="0">Selecione um campeonato</option>
										{championshipsList}
									</select>
								</div>
							</div>
							<div className={cx('col-md-6')}>
								<div className={cx('field-group')}>
									<select ref="games" className={cx('full')}>
										<option value="0">Próximos jogos a transmitir</option>
										{gamesList}
									</select>
								</div>
							</div>

							<div className={cx('col-md-12')}>
								<div className={cx('button-container')}>
									<input className={cx('submit-btn')} type="button" value='Enviar' onClick={this.handleGameChange} />
								</div>
							</div>

							<div className={cx('col-md-12')}>
								<GamesSelectedList
									data={broadcastGames}
									onRemoveItem={onRemoveGame}
								/>
							</div>

						  	{this.renderMessage()}
						</div>
					</form>
				);
			} else {
				return <div className={cx('message')}>Selecione um boteco na lista ao lado</div>
			}


		} else {
			return <LoadingMessage />;
		}
	}

	renderMessage() {
		if(this.state.message !== '') {
			return (
				<div className={cx('message')}>
					<i className="zmdi zmdi-block"></i> {this.state.message}
				</div>
			);
		}
	}

	render() {
		return (
			<div className={cx('admin-form-col')}>
				{this.renderContent()}
			</div>
		);
	};
}

BroadcastsForm.propTypes = {
	championships: PropTypes.array.isRequired,
	games: PropTypes.array.isRequired,
	place: PropTypes.object,
	broadcastGames: PropTypes.array,
	onRemoveGame: PropTypes.func.isRequired,
	onSelectChampionship: PropTypes.func.isRequired,
	onSelectGame: PropTypes.func.isRequired,
	onSelectPlace: PropTypes.func.isRequired
};
