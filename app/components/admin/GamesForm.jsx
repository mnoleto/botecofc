import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LoadingMessage from 'components/admin/LoadingMessage';
import Checkbox from 'components/Checkbox';
import FilterDate from '../../components/filters/FilterDate';

import classNames from 'classnames/bind';
import styles from 'css/admin/form';

const cx = classNames.bind(styles);

/*
 * COUNTRIES FORM
 */
export default  class GamesForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			championship: {},
			championshipSelected: '',
			channels: [],
			city: '',
			date: new Date(),
			gameDate: '',
			homeTeam: {},
			homeTeamSelected: '',
			id: '',
			message: '',
			places: [],
			stadium: '',
			visitingTeam: {},
			visitingTeamSelected: '',
		};

		this.eventHandlers = {};

		this.isSubmiting = false;

		this.clearForm = this.clearForm.bind(this);
		this.handleFormChange = this.handleFormChange.bind(this);
		this.handleChampionshipChange = this.handleChampionshipChange.bind(this);
		this.handleHomeTeamChange = this.handleHomeTeamChange.bind(this);
		this.handleVisitingTeamChange = this.handleVisitingTeamChange.bind(this);
		this.onEntrySave = this.onEntrySave.bind(this);
		this.handleChannelsChange = this.handleChannelsChange.bind(this);
		this.getChannelId = this.getChannelId.bind(this);
		this.handleDateChange = this.handleDateChange.bind(this);
		this.renderContent = this.renderContent.bind(this);
		this.renderMessage = this.renderMessage.bind(this);
		this.renderCalendar = this.renderCalendar.bind(this);
	};

	clearForm() {
		this.setState({
			championship: {},
			championshipSelected: '',
			channels: [],
			city: '',
			date: new Date(),
			gameDate: '',
			homeTeam: {},
			homeTeamSelected: '',
			id: '',
			places: [],
			stadium: '',
			visitingTeam: {},
			visitingTeamSelected: '',
		});
		window.scrollTo(0,0);
	};

	componentWillReceiveProps(nextProps) {
		if (nextProps.game.id) {
			const championshipSelected = (nextProps.game.championship) ? nextProps.game.championship.id : '0';
			const homeTeamSelected = (nextProps.game.homeTeam) ? nextProps.game.homeTeam.id : '0';
			const visitingTeamSelected = (nextProps.game.visitingTeam) ? nextProps.game.visitingTeam.id : '0';

			this.setState({
				id: nextProps.game.id,
				city: nextProps.game.city,
				stadium: nextProps.game.stadium,
				championship: nextProps.game.championship,
				homeTeam: nextProps.game.homeTeam,
				visitingTeam: nextProps.game.visitingTeam,
				places: nextProps.game.places,
				gameDate: nextProps.game.gameDate,
				date: new Date(nextProps.game.gameDate),
				championshipSelected: championshipSelected,
				homeTeamSelected: homeTeamSelected,
				visitingTeamSelected: visitingTeamSelected,
				channels: nextProps.game.channels || [],
			});
		}
		this.isSubmiting = false;
		this.forceUpdate();
	}

	handleChannelsChange(val) {
		let checked = (this.state.channels.length > 0) ? this.state.channels : [];
		if (checked.includes(val)) {
			checked.splice(checked.indexOf(val), 1);
		} else {
			checked.push(val);
		}
		this.setState({ channels: checked })
		this.forceUpdate();
	};

	getChannelId(name) {
		const { channels } = this.props;
		const channelSelected = channels.find((channel) => channel.name === name)
		return channelSelected ? channelSelected.id : null;
	}

	convertDateFormat(date, hour) {
		const dateArr = date.split('/');
		const hourArr = hour.split(':');
		let dateObj = new Date();
		dateObj.setFullYear(dateArr[2])
		dateObj.setMonth(dateArr[1] - 1)
		dateObj.setDate(dateArr[0])
		dateObj.setHours(hourArr[0])
		dateObj.setMinutes(hourArr[1]);
		return dateObj;
	};

	unconvertDateFormat(date) {
		const originalDate = new Date(date);
		let day = originalDate.getDate(),
			month = originalDate.getMonth(),
			year = originalDate.getFullYear();
		
		month ++;

		if(day < 10) {
			day = '0' + day;
		}

		if(month < 10) {
			month = '0' + month;
		}
		return (day + '/' + month + '/' + year);
	}

	unconvertTimeFormat(date) {
		const originalDate = new Date(date);
		let hour = originalDate.getHours(),
			minute = originalDate.getMinutes();

		if(hour < 10) {
			hour = '0' + hour;
		}

		if(minute < 10) {
			minute = '0' + minute;
		}
		return (hour + ':' + minute);
	}

	handleFormChange(event) {
		this.setState({[event.target.name]: event.target.value});
	};

	handleSelectChange(event) {
		var select = event.target.value;
		if(select === '' || select === '0') return false;
		this.setState({[event.target.name]: {
			id: select,
			name: event.target.selectedOptions[0].text
		}});
		this.forceUpdate();
	}

	handleChampionshipChange(event) {
		const { onChangeChampionshipFilter } = this.props;

		this.handleSelectChange(event);
		this.state.championshipSelected = event.target.value;

		if(event.target.value === 0 ) {
			onChangeChampionshipFilter('SHOW_ALL', '', '');
		} else {
			onChangeChampionshipFilter('SHOW_ONLY', event.target.value);
		}
	};

	handleDateChange(date) {
		this.setState({
			date,
		});
	}

	handleHomeTeamChange(event) {
		this.handleSelectChange(event);
		this.state.homeTeamSelected = event.target.value;
	};

	handleVisitingTeamChange(event) {
		this.handleSelectChange(event);
		this.state.visitingTeamSelected = event.target.value;
	};

	onEntrySave(e) {
		e.preventDefault();
		const { game, onEntrySave, onEntryChange } = this.props;
		
		if(this.state.city === '') {
			this.setState({message: 'Preencha o campo bairro.'});
			return false;
		}

		if(this.state.stadium === '') {
			this.setState({message: 'Preencha o campo estadio.'});
			return false;
		}

		if(this.state.date === '') {
			this.setState({message: 'Preencha o campo data.'});
			return false;
		}

		if(this.state.hour === '') {
			this.setState({message: 'Preencha o campo hora.'});
			return false;
		}

		if(this.state.championship === '0' || this.state.championship === '') {
			this.setState({message: 'Selecione um campeonato.'});
			return false;
		}

		if(!this.state.homeTeam.id) {
			this.setState({message: 'Selecione o time mandante.'});
			return false;
		}

		if(!this.state.visitingTeam.id) {
			this.setState({message: 'Selecione o time visitante.'});
			return false;
		}

		this.setState({message: ''});

		this.isSubmiting = true;
		
		if(game.city){
			onEntryChange({
				'id': this.state.id,
				'city': this.state.city,
				'stadium': this.state.stadium,
				'championship': this.state.championship,
				'homeTeam': this.state.homeTeam,
				'visitingTeam': this.state.visitingTeam,
				'places': this.state.places,
				'gameDate': this.state.date,
				'channels': this.state.channels,
			});
		} else {
			onEntrySave({
				'city': this.state.city,
				'stadium': this.state.stadium,
				'championship': this.state.championship,
				'homeTeam': this.state.homeTeam,
				'visitingTeam': this.state.visitingTeam,
				'places': this.state.places,
				'gameDate': this.state.date,
				'channels': this.state.channels,
			});
		}

		this.clearForm();
	};

	renderCalendar() {
		return (
			<FilterDate
				date={this.state.date}
				dateFormat="dd/MM/yyyy HH:mm"
				showTimeSelect={true}
				onFilter={this.handleDateChange}
			/>
		);
	}

	renderContent() {
		if(!this.isSubmiting) {
			const { championships, teams, channels, game } = this.props;

			const channelsOptions = channels.map((item, index) => {
				const isChecked = (game && game.channels) ? game.channels.includes(item.id) : false;
				return (
					<Checkbox
						key={'channel-' + item.id}
						isChecked={isChecked}
						name="channels"
						id={item.id}
						value={item.name}
						onChange={this.handleChannelsChange}
					/>
				);
			});
			const championshipsItens = championships.map((item, index) => {
				return (
					<option key={'championship' + item.id} value={item.id}>{item.name}</option>
				);
			});
			const teamsItens = teams.map((item, index) => {
				return (
					<option key={'tema' + item.id} value={item.id}>{item.name}</option>
				);
			});
			
			return (
				<form className={cx('clearfix', 'form-inline', 'container-fluid')} method="post" encType="multipart/form-data" onSubmit={this.onEntrySave} role="form">
					{this.renderMessage()}

					<div className={cx('row')}>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<label htmlFor="city" className={cx('sr-only')}>Cidade *</label>
								<input type="text" name="city" placeholder="Cidade *" value={this.state.city || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<label htmlFor="stadium" className={cx('sr-only')}>Estádio *</label>
								<input type="text" name="stadium" placeholder="Estádio *" value={this.state.stadium || ''} onChange={this.handleFormChange} />
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="day" className={cx('sr-only')}>Data *</label>
								{this.renderCalendar()}
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="championship" className={cx('sr-only')}>Campeonato *</label>
								<select name="championship" value={this.state.championshipSelected || '0'} className={cx('full')} onChange={this.handleChampionshipChange}>
									<option value="0">Selecione um campeonato</option>
									{championshipsItens}
								</select>
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<label htmlFor="homeTeam" className={cx('sr-only')}>Time mandante *</label>	
								<select name="homeTeam" value={this.state.homeTeamSelected || '0'} className={cx('full')} onChange={this.handleHomeTeamChange}>
									<option value="0">Selecione o time mandante</option>
									{teamsItens}
								</select>
							</div>
						</div>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<label htmlFor="visitingTeam" className={cx('sr-only')}>Time visitante *</label>
								<select name="visitingTeam" value={this.state.visitingTeamSelected || '0'} className={cx('full')} onChange={this.handleVisitingTeamChange}>
									<option value="0">Selecione o time visitante</option>
									{teamsItens}
								</select>
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('field-group', 'col-md-full')}>
							<legend>Selecione os canais</legend>
							{channelsOptions}
						</div>
					</div>
				  
					<div className={cx('row')}>
						<div className={cx('col-md-full', 'required')}><p className={cx('pull-right')}><small>* campos obrigatórios</small></p></div>
					</div>

					{this.renderMessage()}
				  
					<div className={cx('row')}>
				  		<input className={cx('submit-btn', 'pull-right')} type="submit" value='Enviar' />
						<input className={cx('clear-btn', 'pull-right')} type="button" value="Limpar" onClick={this.clearForm} />
					</div>
				</form>
			);
		} else {
			return <LoadingMessage />;
		}
	}

	renderMessage() {
		if(this.state.message !== '') {
			return (
				<div className={cx('message')}>
					<i className="zmdi zmdi-block"></i> {this.state.message}
				</div>
			);
		}
	}

	render() {
		return (
			<div className={cx('admin-form-col')}>
				<h2 className={cx('form-title')}>Dados do jogo</h2>
				{this.renderContent()}
			</div>
		);
	};
}

GamesForm.propTypes = {
	channels: PropTypes.array.isRequired,
	game: PropTypes.object,
	championships: PropTypes.array.isRequired,
	teams: PropTypes.array.isRequired,
	onEntryChange: PropTypes.func.isRequired,
	onEntrySave: PropTypes.func.isRequired,
	onChangeChampionshipFilter: PropTypes.func.isRequired
};

