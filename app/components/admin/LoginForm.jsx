import React, { Component } from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames/bind';
import styles from 'css/admin/form';
const cx = classNames.bind(styles);

/*
 * COUNTRIES FORM
 */
export default  class LoginForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			email: '',
			password: ''
		};

		this.clearForm = this.clearForm.bind(this);
		this.handleOnSubmit = this.handleOnSubmit.bind(this);
	};

	clearForm() {
		this.setState({
			email: '',
			password: ''
		});
	};

	handleFormChange(event) {
		this.setState({[event.target.name]: event.target.value});
	};

	handleOnSubmit(e) {
		e.preventDefault();
		const { onLogin } = this.props;
		if(this.state.email === '' || this.state.password === '') return false;

		onLogin({
			'email': this.state.email,
			'password': this.state.password
		});
		
		this.clearForm();
	};

	render() {
		const { message } = this.props;
		return (
			<form onSubmit={this.handleOnSubmit} role="form" className={cx('clearfix', 'form-inline')}>
				<p className={cx('message', {'message-show': message && message.length > 0})}>{message}</p>

				<div className={cx('form-group', 'col-sm-8', 'col-sm-offset-2')}>
					<div className={cx('field-group')}>
						<label htmlFor="email" className={cx('sr-only')}>E-mail *</label>
						<input type="email" name="email" placeholder="E-mail *" onChange={this.handleFormChange} />
					</div>
				</div>

				<div className={cx('form-group', 'col-sm-8', 'col-sm-offset-2')}>
					<div className={cx('field-group')}>
						<label htmlFor="password" className={cx('sr-only')}>Senha *</label>
						<input type="password" name="password" placeholder="Senha" onChange={this.handleFormChange} />
					</div>
				</div>

				<div className={cx('col-sm-12', 'required')}><p className={cx('pull-right')}><small>* campos obrigatórios</small></p></div>

				<input className={cx('submit-btn', 'pull-right')} type="submit" value="Login" />
			</form>
		);
	};
}

LoginForm.propTypes = {
	onLogin: PropTypes.func.isRequired
};

