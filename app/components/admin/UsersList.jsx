import classNames from 'classnames/bind';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Tabs, TabList, Tab, TabPanel } from 'react-tabs';
import CsvDownloader from 'react-csv-downloader';
import styles from '../../css/admin/table';
import AdminTableLinks from './AdminTableLinks';
import Pagination from './Pagination';

const cx = classNames.bind(styles);


class UsersFilteredTable extends Component {
	constructor(props) {
		super(props);

		this.state = {
			itensPerPage: 100,
			page: 0
		}

		this.changePage = this.changePage.bind(this);
		this.changeItensPerPage = this.changeItensPerPage.bind(this);
	}

	changePage(page) {
		this.setState({page});
	}

	changeItensPerPage() {
		this.setState({itensPerPage: this.refs.itensPerPage.value});
	}

	render() {
		const { users, role, onDestroy, onEdit } = this.props;
		let dataItens;

		const filteredUsers = users.filter((item, index) => {
			if(item.role === role) {
				return item;
			}
		});

		const initialItem = this.state.page * this.state.itensPerPage;
		const endItem = initialItem + (this.state.itensPerPage - 1);
		const numberOfPages = Math.ceil(filteredUsers.length / this.state.itensPerPage);

		if(filteredUsers && filteredUsers.length > 0) {
			const sortedItens = filteredUsers;
			sortedItens.sort((a, b) => {
				return new Date(b.date) - new Date(a.date);
			});

			dataItens = sortedItens.map((item, index) => {
				if(index >= initialItem && index <= endItem) {
					if(item.role === role) {
						let creationDate = new Date(item.date);
						let month = creationDate.getMonth() + 1;
						let formatedDate = creationDate.getDate() + '/' + month + '/' + creationDate.getFullYear() + ' ' + creationDate.getHours() + ':' + creationDate.getMinutes();
						return (
							<tr key={index}>
								<td className={cx('table-link')}>
									<div className={cx('table-first-column')}>
										<div className={cx('main-info')}>{item.email}</div>
										<AdminTableLinks
											id={item._id}
											index={index}
											data={item}
											onDestroy={onDestroy}
											onEdit={onEdit} />
									</div>
								</td>
								<td>{formatedDate}</td>
							</tr>
						);
					}
				}

			});
		} else {
			dataItens = <tr>
			  <td colSpan="2">Nenhum usuário encontrado.</td>
			</tr>
		}

		return (
			<div className={cx('box-body')}>
				<table className={cx('table', 'table-bordered')}>
					<thead>
						<tr>
							<th>E-mail</th>
							<th>Date</th>
						</tr>
					</thead>
					<tbody>
						{dataItens}
					</tbody>
				</table>

				<Pagination
					page={this.state.page}
					length={numberOfPages}
					goto={this.changePage}
				/>
			</div>
		);
	};
};

UsersFilteredTable.propTypes = {
	users: PropTypes.array,
	role: PropTypes.string,
	onDestroy: PropTypes.func.isRequired,
	onEdit: PropTypes.func.isRequired
};



export default class UsersList extends Component {
	constructor(props) {
		super(props);
	};

	render() {
		const { users, onEdit, onDestroy, onExport } = this.props;
		let usersCsv;
		usersCsv = users
			? users.map((user) => {
				const date = new Date(user.date);
				const formatedDate = `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`
				return {
					nome: user.name,
					email: user.email,
					data: formatedDate,
					regiao: user.profile.city,
					phone: user.profile.phone,
					time: user.profile.team && user.profile.team.name  ? user.profile.team.name : '',
					sexo: user.profile.gender,
				};
			})
			: [];
		const columns = [{
			id: 'nome',
			displayName: 'Nome',
		}, {
			id: 'email',
			displayName: 'E-mail',
		}, {
			id: 'data',
			displayName: 'Data de cadastro',
		}, {
			id: 'regiao',
			displayName: 'Região',
		}, {
			id: 'phone',
			displayName: 'Telefone',
		}, {
			id: 'time',
			displayName: 'Time',
		}, {
			id: 'sexo',
			displayName: 'Sexo',
		}];
		const fileName = 'usuarios';
		return (
			<div className={cx('admin-table-col')}>
				<h2 className={cx('table-title')}>Usuários cadastrados</h2>

				<div className={cx('actions')}>
					<CsvDownloader
						filename={fileName}
						columns={columns}
						datas={usersCsv}
						suffix="true"
					>
						<button className={cx('btn-export')} onClick={onExport}>Exportar CSV</button>
					</CsvDownloader>
				</div>

				<div className={cx('box', 'box-success')}>
					<div className={cx('box-body')}>
						<Tabs defaultIndex={0}>
							<TabList className={cx('tabs-list')}>
								<Tab>Usuários comuns</Tab>
								<Tab>Donos de bares</Tab>
								<Tab>Administradores</Tab>
							</TabList>

							<TabPanel>
								<UsersFilteredTable
									users={users}
									role={"subscriber"} 
									onEdit={onEdit}
									onDestroy={onDestroy}
								/>
							</TabPanel>

							<TabPanel>
								<UsersFilteredTable
									users={users}
									role={"owner"} 
									onEdit={onEdit}
									onDestroy={onDestroy}
								/>
							</TabPanel>

							<TabPanel>
								<UsersFilteredTable
									users={users}
									role={"admin"}
									onEdit={onEdit}
									onDestroy={onDestroy}
								/>
							</TabPanel>
						</Tabs>
					</div>
				</div>
			</div>
		);
	}
};

UsersList.propTypes = {
	users: PropTypes.array.isRequired,
	onDestroy: PropTypes.func.isRequired,
	onEdit: PropTypes.func.isRequired,
};

