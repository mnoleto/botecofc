import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from 'css/admin/selectedList';
const cx = classNames.bind(styles);


class ChampionshipsSelectedListItem extends Component {
	constructor(props) {
		super(props);
		this.removeItem = this.removeItem.bind(this);
	}

	removeItem() {
		const { index, onRemoveItem } = this.props;
		onRemoveItem(index);
	}

	render() {
		const { name } = this.props;

		return (
			<li><a onClick={this.removeItem}><i className={cx('fa', 'fa-close', 'fa-fw')} aria-hidden={true}></i>&nbsp;{name}</a></li>
		);
	}
}

ChampionshipsSelectedListItem.propTypes = {
	index: PropTypes.number,
	id: PropTypes.string,
	name: PropTypes.string,
	onRemoveItem: PropTypes.func
};




class ChampionshipsSelectedList extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { group, removeItem } = this.props;

		let itens;
		if(group.length > 0){
			itens = group.map((item, index) => {
				return (
					<ChampionshipsSelectedListItem key={index}
						id={item.id}
						index={index}
						name={item.name}
						onRemoveItem={removeItem}
						/>
				);
			});
		} else {
			itens = <li className={cx('text-center', 'empty-list')}>Adicione o primeiro!</li>
		}

		return (
			<div className={cx('group-list')}>
				<ul className={cx('list-inline')}>
					{itens}
				</ul>
			</div>
		);
	}
}

ChampionshipsSelectedList.propTypes = {
	group: PropTypes.array,
	removeItem: PropTypes.func
};

export default ChampionshipsSelectedList;