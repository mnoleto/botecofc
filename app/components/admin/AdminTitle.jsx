import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from '../../css/admin/title';

const cx = classNames.bind(styles);

class AdminTitle extends Component {
	constructor(props) {
		super(props);
	};

	render() {
		const { main_title } = this.props;
		return (
			<div className={cx('admin-title')}>
				<h1 className={cx('title')}>{main_title}</h1>
			</div>
		);
	}
};

AdminTitle.propTypes = {
	main_title: PropTypes.string
};

export default AdminTitle;
