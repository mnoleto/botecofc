import classNames from 'classnames/bind';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import InputElement from 'react-input-mask';
import citiesArray from '../../utils/cities';
import Slugify from '../../utils/slugify';
import sortByName from '../../utils/sortByName';
import styles from '../../css/admin/form';
import CheckboxWithLabel from '../CheckboxWithLabel';
import { CitiesSelect, StatesSelect } from '../selects'
import LoadingMessage from './LoadingMessage';
import UploadImage from './UploadImage';

const cx = classNames.bind(styles);

export default class PlacesForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			about: '',
			address: '',
			cards: [],
			city: '',
			cnpj: '',
			email: '',
			facebook: '',
			gallery: [],
			googleplus: '',
			id: '',
			instagram: '',
			logo: {},
			message: '',
			name: '',
			opening: [],
			ownerName: '',
			ownerships: {},
			phone: '',
			cityId: '',
			stateId: '',
			services: [],
			slug: '',
			teamOficial: [],
			twitter: '',
			website: '',
			zipcode: '',
		};

		this.eventHandlers = {};

		this.isEditing = false;
		this.clearImages = false;
		this.isSubmiting = false;

		this.clearForm = this.clearForm.bind(this);
		this.getGalleryObject = this.getGalleryObject.bind(this);
		this.handleCardsChange = this.handleCardsChange.bind(this);
		this.handleFormChange = this.handleFormChange.bind(this);
		this.handleOpeningClick = this.handleOpeningClick.bind(this);
		this.handleServicesChange = this.handleServicesChange.bind(this);
		this.handleTitleChange = this.handleTitleChange.bind(this);
		this.onClearImages = this.onClearImages.bind(this);
		this.onEntrySave = this.onEntrySave.bind(this);
		this.onLogoRemoveImage = this.onLogoRemoveImage.bind(this);
		this.onLogoUploadComplete = this.onLogoUploadComplete.bind(this);
		this.onGalleryUploadComplete = this.onGalleryUploadComplete.bind(this);
		this.onGalleryRemoveImage = this.onGalleryRemoveImage.bind(this);
		this.removeOpeningItem = this.removeOpeningItem.bind(this);
		this.renderContent = this.renderContent.bind(this);
		this.renderMessage = this.renderMessage.bind(this);
		this.handleStateChange = this.handleStateChange.bind(this);
		this.handleCityChange = this.handleCityChange.bind(this);
	};

	componentWillReceiveProps(nextProps) {
		if (nextProps.place && nextProps.place.name) {
			this.setState({
				about: nextProps.place.about || '',
				address: nextProps.place.address || '',
				cards: nextProps.place.cards || [],
				city: nextProps.place.city || '',
				cnpj: nextProps.place.cnpj || '',
				email: nextProps.place.email || '',
				facebook: nextProps.place.facebook || '',
				gallery: nextProps.place.gallery || [],
				googleplus: nextProps.place.googleplus || '',
				id: nextProps.place.id,
				instagram: nextProps.place.instagram || '',
				logo: nextProps.place.logo || {},
				name: nextProps.place.name || '',
				opening: nextProps.place.opening || [],
				ownerName: nextProps.place.ownerName || '',
				ownerships: nextProps.place.ownerships || {},
				phone: nextProps.place.phone || '',
				cityId: nextProps.place.cityId || '',
				stateId: nextProps.place.stateId || '',
				services: nextProps.place.services || [],
				slug: nextProps.place.slug || Slugify(nextProps.place.name),
				teamOficial: nextProps.place.teamOficial || [],
				twitter: nextProps.place.twitter || '',
				website: nextProps.place.website || '',
				zipcode: nextProps.place.zipcode || '',
			});
		}
		this.isEditing = true;
		this.isSubmiting = false;
	}

	clearForm() {
		this.setState({
			about: '',
			address: '',
			cards: [],
			city: '',
			cnpj: '',
			email: '',
			facebook: '',
			gallery: [],
			googleplus: '',
			instagram: '',
			logo: {},
			name: '',
			opening: [],
			ownerName: '',
			ownerships: {},
			phone: '',
			cityId: '0',
			stateId: '0',
			services: [],
			slug: '',
			teamOficial: [],
			twitter: '',
			website: '',
			zipcode: '',
		});
		
		window.scrollTo(0,0);
		this.clearImages = true;
		this.forceUpdate();
	};

	getGalleryObject(index) {
		if(this.state.gallery && this.state.gallery[index]) {
			return this.state.gallery[index];
		} else {
			return [];
		}
	}

	handleCardsChange(val) {
		let checked = (this.state.cards.length > 0) ? this.state.cards.slice() : [];
		if(checked.includes(val)) {
			checked.splice(checked.indexOf(val), 1);
		} else {
			checked.push(val);
		}
		this.setState({cards: checked})
		this.forceUpdate();
	};

	handleFormChange(event) {
		this.setState({[event.target.name]: event.target.value});
	};

	handleOpeningClick(event) {
		const daystart = this.state.daystart;
		const dayend = this.state.dayend;
		const hourstart = this.state.hourstart;
		const hourend = this.state.hourend;

		this.state.opening.push({
			daystart,
			dayend,
			hourstart,
			hourend
		});
		this.forceUpdate();
	};

	handleServicesChange(val) {
		let checked = (this.state.services.length > 0) ? this.state.services.slice() : [];
		if(checked.includes(val)) {
			checked.splice(checked.indexOf(val), 1);
		} else {
			checked.push(val);
		}
		this.setState({services: checked})
		this.forceUpdate();
	};

	handleTitleChange(event) {
		const title = event.target.value;
		this.setState({[event.target.name]: title});
		this.state.slug = Slugify(title);
	}

	handleStateChange(event) {
		this.setState({
			stateId: event.target.value,
		})
	}

	handleCityChange(event) {
		this.setState({
			cityId: event.target.value,
		})
	}

	onClearImages() {
		this.clearImages = false;
	}

	onEntrySave(e) {
		e.preventDefault();

		const { onEntrySave, onEntryChange, place } = this.props;

		if(!this.state.logo.responseText) {
			this.setState({message: 'Faça o upload da logo do bar.'});
			return false;
		}

		if(this.state.name === '') {
			this.setState({message: 'Preencha o campo nome.'});
			return false;
		}

		if(this.state.city === '') {
			this.setState({message: 'Preencha o campo bairro.'});
			return false;
		}

		if (this.state.stateId === '') {
			this.setState({ message: 'Preencha o campo estado.' });
			return false;
		}

		if (this.state.cityId === '') {
			this.setState({ message: 'Preencha o campo cidade.' });
			return false;
		}

		this.setState({message: ''});

		let teamOficial;
		if(this.refs.teamOficial.value === '0') {
			teamOficial = {}
		} else {
			teamOficial = {
				id: this.refs.teamOficial.value,
				name: this.refs.teamOficial.selectedOptions[0].text
			}
		}

		let phoneReplace;
		if(this.state.phone && this.state.phone !== '' && this.state.phone.length > 0) {
			phoneReplace = this.state.phone;
			phoneReplace = phoneReplace.split('_').join('');
			this.setState({phone: phoneReplace});
		}

		this.isSubmiting = true;

		if(place.name){
			onEntryChange({
				...this.state,
			});
		} else {
			onEntrySave({
				...this.state,
				teamOficial: {
					id: this.refs.teamOficial.value,
					name: this.refs.teamOficial.selectedOptions[0].text
				},
			});
		}
		
		this.clearForm();
	};

	onLogoUploadComplete(file) {
		this.state.logo = file;
	};

	onLogoRemoveImage(file) {
		this.setState({logo: {}});
	}

	onGalleryUploadComplete(file, index) {
		let gallery = (this.state.gallery.length > 0) ? this.state.gallery : [];
		gallery[index] = file;
		this.setState({gallery: gallery});
	};

	onGalleryRemoveImage(file) {
		const index = this.state.gallery.findIndex((element) => {
			return (element && element.responseText === file.responseText)
		});
		this.setState({
			gallery: this.state.gallery.map((v, i) => {
				if(i === index) {
					return {};
				} else {
					return v;
				}
			})
		});
	}

	removeOpeningItem(index) {
		this.state.opening.splice(index, 1);
		this.forceUpdate();
	}

	renderContent() {
		if(!this.isSubmiting) {
			const { cities, onOwnershipRemove, states, teams } = this.props;
			const { cityId, stateId } = this.state;

			const filteredCities = cities.filter(city => city.stateId === stateId);

			const alphabeticalTeams = teams;
			alphabeticalTeams.sort(sortByName('name'));
			const teamsItens = alphabeticalTeams.map((item, index) => {
				return (
					<option key={index} value={item.id}>{item.name}</option>
				);
			});

			return (
				<form action="/uploadPlace" ref="adminForm" className={cx('clearfix', 'form-inline', 'container-fluid')} method="post" encType="multipart/form-data" onSubmit={this.onEntrySave} role="form">
					{this.renderMessage()}

					<div className={cx('row')}>
						<div className={cx('upload-field', 'col-md-full')}>
							<UploadImage
								id={'logo'}
								classNames={'upload-logo'}
								clearImages={this.clearImages}
								image={this.state.logo}
								isEditing={this.isEditing}
								postUrl={'/uploadPlace'}
								onClearImages={this.onClearImages}
								onUploadComplete={this.onLogoUploadComplete}
								onRemoveImage={this.onLogoRemoveImage}
								text={'INCLUA A LOGO DO ESTABELECIMENTO'}
								uploadMultiple={false}
							/>
	          </div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="name" className={cx('sr-only')}>Nome *</label>
								<input type="text" name="name" placeholder="Nome *" value={this.state.name || ''} onChange={this.handleTitleChange} />
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="name" className={cx('sr-only')}>Nome do responsável</label>
								<input type="text" name="ownerName" placeholder="Nome do responsável" value={this.state.ownerName || ''} onChange={this.handleFormChange} />
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<label htmlFor="slug" className={cx('sr-only')}>Slug</label>
								<input type="text" name="slug" disabled="disabled" placeholder="Slug" value={this.state.slug || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<label htmlFor="cnpj" className={cx('sr-only')}>CNPJ</label>
								<InputElement name="cnpj" placeholder="CNPJ" mask="999.999.999/9999-99" value={this.state.cnpj || ''} onChange={this.handleFormChange} />
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<label htmlFor="website" className={cx('sr-only')}>Site</label>
								<input type="text" name="website" placeholder="Site" value={this.state.website || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<label htmlFor="email" className={cx('sr-only')}>E-mail</label>
								<input type="text" name="email" placeholder="E-mail" value={this.state.email || ''} onChange={this.handleFormChange} />
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<label htmlFor="address" className={cx('sr-only')}>Endereço</label>
								<input type="text" name="address" placeholder="Endereço" value={this.state.address || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<label htmlFor="city" className={cx('sr-only')}>Bairro *</label>
								<input type="text" name="city" placeholder="Bairro" value={this.state.city || ''} onChange={this.handleFormChange} />
							</div>
						</div>
					</div>


					<div className={cx('row')}>
						<StatesSelect selectedState={stateId} states={states} onChange={this.handleStateChange} />
					</div>
					{stateId !== '' && (
						<div className={cx('row')}>
							<CitiesSelect
								cities={filteredCities}
								selectedCity={cityId}
								onChange={this.handleCityChange}
							/>
						</div>
					)}

					<div className={cx('row')}>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<label htmlFor="zipcode" className={cx('sr-only')}>CEP *</label>
								<InputElement name="zipcode" placeholder="CEP" mask="99.999-999" value={this.state.zipcode || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<label htmlFor="phone" className={cx('sr-only')}>Telefone</label>
								<InputElement name="phone" placeholder="Telefone" mask="(99) 999999999" value={this.state.phone || ''} onChange={this.handleFormChange} />
							</div>
						</div>
					</div>

					<div className={cx('row', 'gallery')}>
						<div className={cx('col-md')}>
							<UploadImage
								id={'gallery0'}
								index={0}
								classNames={'dropzone-gallery'}
								clearImages={this.clearImages}
								image={this.getGalleryObject(0)}
								isEditing={this.isEditing}
								postUrl={'/uploadPlace'}
								onUploadComplete={this.onGalleryUploadComplete}
								onRemoveImage={this.onGalleryRemoveImage}
								onClearImages={this.onClearImages}
								text={''}
								uploadMultiple={false}
							/>
	          </div>
	          <div className={cx('col-md')}>
							<UploadImage
								id={'gallery1'}
								index={1}
								classNames={'dropzone-gallery'}
								clearImages={this.clearImages}
								image={this.getGalleryObject(1)}
								isEditing={this.isEditing}
								postUrl={'/uploadPlace'}
								onUploadComplete={this.onGalleryUploadComplete}
								onRemoveImage={this.onGalleryRemoveImage}
								onClearImages={this.onClearImages}
								text={''}
								uploadMultiple={false}
								/>
						</div>
						<div className={cx('col-md')}>
							<UploadImage
								id={'gallery2'}
								index={2}
								classNames={'dropzone-gallery'}
								clearImages={this.clearImages}
								image={this.getGalleryObject(2)}
								isEditing={this.isEditing}
								postUrl={'/uploadPlace'}
								onUploadComplete={this.onGalleryUploadComplete}
								onRemoveImage={this.onGalleryRemoveImage}
								onClearImages={this.onClearImages}
								text={''}
								uploadMultiple={false}
								/>
						</div>
						<div className={cx('col-md')}>
							<UploadImage
								id={'gallery3'}
								index={3}
								classNames={'dropzone-gallery'}
								clearImages={this.clearImages}
								image={this.getGalleryObject(3)}
								isEditing={this.isEditing}
								postUrl={'/uploadPlace'}
								onUploadComplete={this.onGalleryUploadComplete}
								onRemoveImage={this.onGalleryRemoveImage}
								onClearImages={this.onClearImages}
								text={''}
								uploadMultiple={false}
								/>
						</div>
						<div className={cx('col-md')}>
							<UploadImage
								id={'gallery4'}
								index={4}
								classNames={'dropzone-gallery'}
								clearImages={this.clearImages}
								image={this.getGalleryObject(4)}
								isEditing={this.isEditing}
								postUrl={'/uploadPlace'}
								onUploadComplete={this.onGalleryUploadComplete}
								onRemoveImage={this.onGalleryRemoveImage}
								onClearImages={this.onClearImages}
								text={''}
								uploadMultiple={false}
								/>
						</div>
						<div className={cx('col-md')}>
							<UploadImage
								id={'gallery5'}
								index={5}
								classNames={'dropzone-gallery'}
								clearImages={this.clearImages}
								image={this.getGalleryObject(5)}
								isEditing={this.isEditing}
								postUrl={'/uploadPlace'}
								onUploadComplete={this.onGalleryUploadComplete}
								onRemoveImage={this.onGalleryRemoveImage}
								onClearImages={this.onClearImages}
								text={''}
								uploadMultiple={false}
								/>
	          </div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-full')}>
							<div className={cx('field-group')}>
								<label htmlFor="about">Sobre o boteco <small>máx 400 caracteres</small></label>
								<textarea name="about" ref="about" value={this.state.about || ''} onChange={this.handleFormChange}></textarea>
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<legend>Selecione os serviços</legend>
								<CheckboxWithLabel data={this.state.services} name="services" value="Ar Condicionado" onChange={this.handleServicesChange} />
								<CheckboxWithLabel data={this.state.services} name="services" value="Brinquedoteca" onChange={this.handleServicesChange} />
								<CheckboxWithLabel data={this.state.services} name="services" value="Comanda Individual" onChange={this.handleServicesChange} />
								<CheckboxWithLabel data={this.state.services} name="services" value="Estacionamento Privativo" onChange={this.handleServicesChange} />
								<CheckboxWithLabel data={this.state.services} name="services" value="Manobrista" onChange={this.handleServicesChange} />
								<CheckboxWithLabel data={this.state.services} name="services" value="Mesas ao ar livre" onChange={this.handleServicesChange} />
								<CheckboxWithLabel data={this.state.services} name="services" value="Wifi" onChange={this.handleServicesChange} />
							</div>
						</div>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group')}>
								<legend>Selecione os cartões</legend>
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Visa" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Mastercard" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Maestro" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Elo" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="American Express" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Diners Club" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Vale - Alelo Refeição / Visa Vale" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Vale - Sodexo" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Vale - Ticket Restaurante" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Vale - Vale Card" onChange={this.handleCardsChange} />
								<CheckboxWithLabel data={this.state.cards} name="cards" value="Vale - VR Smart" onChange={this.handleCardsChange} />
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group', 'facebook')}>
								<input type="text" name="facebook" placeholder="url facebook" value={this.state.facebook || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group', 'instagram')}>
								<input type="text" name="instagram" placeholder="url instagram" value={this.state.instagram || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group', 'twitter')}>
								<input type="text" name="twitter" placeholder="url twitter" value={this.state.twitter || ''} onChange={this.handleFormChange} />
							</div>
						</div>
						<div className={cx('col-md-half')}>
							<div className={cx('field-group', 'googleplus')}>
								<input type="text" name="googleplus" placeholder="url g+" value={this.state.googleplus || ''} onChange={this.handleFormChange} />
							</div>
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('field-group', 'col-md-full')}>
							<label htmlFor="name" className={cx('sr-only')}>Ponto de encontro da torcida:</label>
							<select name="teamOficial" ref="teamOficial" value={(this.state.teamOficial) ? this.state.teamOficial.id : '0'} className={cx('full')} onChange={this.handleFormChange}>
								<option value="0">Ponto de encontro da torcida</option>
								{teamsItens}
							</select>
						</div>
					</div>

					{(this.props.place && this.props.place.ownerships && this.props.place.ownerships.name) &&
						<div className={cx('row')}>
							<div className={cx('place-owner')}>
								<h4>DONO DO BAR:</h4>
								{this.props.place.ownerships.name} - <a href={"mailto:" + this.props.place.ownerships.email}>{this.props.place.ownerships.email}</a>
								<button type="button" className={cx('remove-btn')} onClick={onOwnershipRemove}>remover dono</button>
							</div>
						</div>
					}

					<div className={cx('row')}>
						<div className={cx('col-md-full', 'required')}><p className={cx('pull-right')}><small>* campos obrigatórios</small></p></div>
					</div>

					{this.renderMessage()}

					<div className={cx('row')}>
						<input className={cx('submit-btn', 'pull-right')} type="submit" value='Enviar' />
						<input className={cx('clear-btn', 'pull-right')} type="button" value="Limpar" onClick={this.clearForm} />
					</div>
				</form>
			);
		} else {
			return <LoadingMessage />;
		}
	}

	renderMessage() {
		if(this.state.message !== '') {
			return (
				<div className={cx('message')}>
					<i className="zmdi zmdi-block"></i> {this.state.message}
				</div>
			);
		}
	}

	render() {
		return (
			<div className={cx('admin-form-col')}>
				<h2 className={cx('form-title')}>Dados do Boteco</h2>
				{this.renderContent()}
			</div>
		);
	};
}

PlacesForm.propTypes = {
	cities: PropTypes.array,
	onDismissMessage: PropTypes.func,
	onEntryChange: PropTypes.func.isRequired,
	onEntrySave: PropTypes.func.isRequired,
	onOwnershipRemove: PropTypes.func.isRequired,
	place: PropTypes.object,
	states: PropTypes.array,
	teams: PropTypes.array.isRequired,
};
