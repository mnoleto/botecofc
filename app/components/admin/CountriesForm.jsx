import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Slugify from 'utils/slugify';

import LoadingMessage from 'components/admin/LoadingMessage';

import classNames from 'classnames/bind';
import styles from 'css/admin/form';
const cx = classNames.bind(styles);

/*
 * COUNTRIES FORM
 */
export default  class CountriesForm extends Component {
	constructor(props) {
		super(props);

		this.state = {
			id: '',
			name: '',
			slug: '',

			message: ''
		};

		this.eventHandlers = {};

		this.isSubmiting = false;

		this.clearForm = this.clearForm.bind(this);
		this.handleFormChange = this.handleFormChange.bind(this);
		this.handleTitleChange = this.handleTitleChange.bind(this);
		this.onEntrySave = this.onEntrySave.bind(this);

		this.renderContent = this.renderContent.bind(this);
		this.renderMessage = this.renderMessage.bind(this);
	};

	clearForm() {
		this.setState({
			id: '',
			name: '',
			slug: ''
		});
		window.scrollTo(0,0);
	};

	componentWillReceiveProps(nextProps) {
		if (nextProps.country.id) {
			this.setState({
				id: nextProps.country.id,
				name: nextProps.country.name,
				slug: nextProps.country.slug
			});
		}
		this.isSubmiting = false;
	}

	handleFormChange(event) {
		this.setState({[event.target.name]: event.target.value});
	};

	onEntrySave(e) {
		e.preventDefault();
		const { onEntrySave, onEntryChange, country } = this.props;
		const _this = this;
		
		if(this.state.name === '') {
			this.setState({message: 'Preencha o campo nome.'});
			return false;
		}

		this.setState({message: ''});

		this.isSubmiting = true;

		if(country.name){
			onEntryChange({
				'id': this.state.id,
				'name': this.state.name,
				'slug': this.state.slug
			});
		} else {
			onEntrySave({
				'name': this.state.name,
				'slug': this.state.slug
			});

		}
		
		this.clearForm();
	};

	handleTitleChange(event) {
		const title = event.target.value;
		this.setState({[event.target.name]: title});
		this.state.slug = Slugify(title);
	}

	renderContent() {
		if(!this.isSubmiting) {
			return (
				<form className={cx('clearfix', 'form-inline', 'container-fluid')} method="post" encType="multipart/form-data" onSubmit={this.onEntrySave} role="form">
					{this.renderMessage()}
					
					<div className={cx('row')}>
						<div className={cx('field-group', 'col-md-full')}>
							<label htmlFor="name" className={cx('sr-only')}>Nome *</label>
							<input type="text" name="name" placeholder="Nome *" value={this.state.name || ''} onChange={this.handleTitleChange} />
						</div>
					</div>

					<div className={cx('row')}>
						<div className={cx('field-group', 'col-md-full')}>
							<label htmlFor="slug" className={cx('sr-only')}>Slug</label>
							<input type="text" name="slug" disabled="disabled" placeholder="Slug" value={this.state.slug || ''} onChange={this.handleFormChange} />
						</div>
					</div>
				  
					<div className={cx('row')}>
						<div className={cx('col-md-full', 'required')}><p className={cx('pull-right')}><small>* campos obrigatórios</small></p></div>
					</div>

				  	{this.renderMessage()}

					<div className={cx('row')}>
						<input className={cx('submit-btn', 'pull-right')} type="submit" value='Enviar' />
						<input className={cx('clear-btn', 'pull-right')} type="button" value="Limpar" onClick={this.clearForm} />
					</div>
				</form>
			);
		} else {
			return <LoadingMessage />;
		}
	}

	renderMessage() {
		if(this.state.message !== '') {
			return (
				<div className={cx('message')}>
					<i className="zmdi zmdi-block"></i> {this.state.message}
				</div>
			);
		}
	}

	render() {
		return (
			<div className={cx('admin-form-col')}>
				<h2 className={cx('form-title')}>Dados do país</h2>
				{this.renderContent()}
			</div>
		);
	};
}

CountriesForm.propTypes = {
	country:PropTypes.object,
	onEntryChange: PropTypes.func.isRequired,
	onEntrySave: PropTypes.func.isRequired
};

