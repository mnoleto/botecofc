import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from '../../css/admin/table';

const cx = classNames.bind(styles);


/*
 * COUNTRY TABLE ITEM
 */
export default class AdminTableLinks extends Component {
	constructor(props) {
		super(props);
		this.onDestroyClick = this.onDestroyClick.bind(this);
		this.onEditClick = this.onEditClick.bind(this);
	};

	onDestroyClick() {
		const { _id, id, index, onDestroy } = this.props;
		const canDestroy = confirm('Realmente deseja excluir esse registro?');
		if(canDestroy) {
			onDestroy(id, index, _id);
		}
	};

	onEditClick() {
		const { data, index, onEdit } = this.props;
    onEdit(data, index);
	};

	render() {
		const { onDestroy } = this.props;

		return (
			<div className={cx('table-actions')}>
				<a className={cx('edit-link')} onClick={this.onEditClick}>Edit</a>
				{(onDestroy) &&
				<a className={cx('delete-link')} onClick={this.onDestroyClick}>Delete</a>
				}
			</div>
		);
	};
}

AdminTableLinks.propTypes = {
	id: PropTypes.string.isRequired,
	index: PropTypes.number.isRequired,
	data: PropTypes.object.isRequired,
	onDestroy: PropTypes.func,
	onEdit: PropTypes.func.isRequired
};
