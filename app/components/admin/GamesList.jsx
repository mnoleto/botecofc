import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import CsvDownloader from 'react-csv-downloader';
import AdminTableLinks from 'components/admin/AdminTableLinks';
import Pagination from 'components/admin/Pagination';

import classNames from 'classnames/bind';
import styles from 'css/admin/table';
const cx = classNames.bind(styles);


/*
 * ADMIN LIST
 */
export default class GamesList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			itensPerPage: 10,
			page: 0
		}

		this.changePage = this.changePage.bind(this);
	};

	changePage(page) {
		this.setState({page});
	}

	render() {
		const { data, folder, headerTitle, onDestroy, onEdit, onExport } = this.props;
		let dataItens;
		let dataCsv = [];

		const initialItem = this.state.page * this.state.itensPerPage;
		const endItem = initialItem + (this.state.itensPerPage - 1);
		const numberOfPages = Math.ceil(data.length / this.state.itensPerPage);

		if(data.length > 0) {
			const sortData = data;
			sortData.sort((a, b) => {
				if (!a.gameDate || !b.gameDate) return false;
				return new Date(a.gameDate).getTime() - new Date(b.gameDate).getTime();
			});
			dataItens = sortData.map((item, index) => {
				if(index >= initialItem && index <= endItem) {
					const dateFormated = new Date(item.gameDate);
					const month = dateFormated.getMonth() + 1;
					const date = dateFormated.getDate() + '/' + month + '/' + dateFormated.getFullYear();

					if(item.homeTeam && item.homeTeam.id && item.visitingTeam && item.visitingTeam.id && item.championship && item.championship.name) {
						return (
							<tr key={index}>
								<td className={cx('table-link')}>
									<div>
										{item.homeTeam.name} x {item.visitingTeam.name}
										<AdminTableLinks
											id={item.id}
											index={index}
											data={item}
											onDestroy={onDestroy}
											onEdit={onEdit} />
									</div>
								</td>
								<td>{item.championship.name}</td>
								<td>{date}</td>
							</tr>
						);
					} else {
						onDestroy(item.id, index);
					}
				}
			});

			dataCsv = data.map((game) => {
				return {
					homeTeam: game.homeTeam.name,
					visitingTeam: game.visitingTeam.name,
					day: moment(game.gameDate).format('DD-MM').toString(),
					hour: moment(game.gameDate).format('HH[H]mm').toString(),
				};
			});
		} else {
			dataItens = <tr>
			  <td colSpan="2">Nenhum registro encontrado.</td>
			</tr>
		}
		const columns = [{
			id: 'homeTeam',
			displayName: 'Mandante',
		}, {
			id: 'visitingTeam',
			displayName: 'Visitante',
		}, {
			id: 'day',
			displayName: 'Dia',
		}, {
			id: 'hour',
			displayName: 'Horário',
		}];
		const fileName = 'jogos';
		
		return (
			<div className={cx('admin-table-col')}>
				<h2 className={cx('table-title')}>{headerTitle}</h2>

				<div className={cx('actions')}>
					<CsvDownloader
						filename={fileName}
						columns={columns}
						datas={dataCsv}
						suffix="true"
					>
						<button className={cx('btn-export')} onClick={onExport}>Exportar CSV</button>
					</CsvDownloader>
				</div>
				
				<div className={cx('box', 'box-success')}>
					<div className={cx('box-body')}>
					
						<div className={cx('admin-table')}>
							<table className={cx('table', 'table-bordered')}>
								<thead>
									<tr>
										<th>Jogo</th>
										<th>Campeonato</th>
										<th width="20%">Data</th>
									</tr>
								</thead>

								<tbody>
									{dataItens}
								</tbody>
							</table>
						</div>


						<Pagination
							page={this.state.page}
							length={numberOfPages}
							goto={this.changePage}
						/>
					</div>
				</div>
			</div>
		);
	}
}

GamesList.propTypes = {
	data: PropTypes.array.isRequired,
	folder: PropTypes.string,
	headerTitle: PropTypes.string.isRequired,
	onDestroy: PropTypes.func.isRequired,
	onEdit: PropTypes.func.isRequired
};

