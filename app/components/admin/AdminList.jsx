import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import AdminTableLinks from '../admin/AdminTableLinks';
import Pagination from '../admin/Pagination';
import sortByName from '../../utils/sortByName';
import styles from '../../css/admin/table';

const cx = classNames.bind(styles);

/*
 * ADMIN LIST
 */
export default class AdminList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			itensPerPage: 10,
			page: 0
		}

		this.changePage = this.changePage.bind(this);
		this.changeItensPerPage = this.changeItensPerPage.bind(this);
	};

	changePage(page) {
		this.setState({page});
	}

	changeItensPerPage() {
		this.setState({itensPerPage: this.refs.itensPerPage.value});
	}

	render() {
		const { data, headerTitle, onDestroy, onEdit } = this.props;
		let dataItens;

		const initialItem = this.state.page * this.state.itensPerPage;
		const endItem = initialItem + (this.state.itensPerPage - 1);
		const numberOfPages = Math.ceil(data.length / this.state.itensPerPage);

		if(data && data.length > 0) {
			const alphabeticalItens = data;
			alphabeticalItens.sort(sortByName('name'));

			dataItens = alphabeticalItens.map((item, index) => {
				if(index >= initialItem && index <= endItem) {
					const dateFormated = new Date(item.date);
					const month = dateFormated.getMonth() + 1;
					const date = dateFormated.getDate() + '/' + month + '/' + dateFormated.getFullYear();

					return (
						<tr key={index}>
							<td className={cx('table-link')}>
								<div className={cx('table-first-column')}>
									<div className={cx('main-info')}>{item.name}</div>
									<AdminTableLinks
										id={item.id}
										index={index}
										data={item}
										onDestroy={onDestroy}
										onEdit={onEdit} />
								</div>
							</td>
							<td>
								{date}
							</td>
						</tr>
					);
				}

			});
		} else {
			dataItens = <tr>
			  <td colSpan="2">Nenhum registro encontrado.</td>
			</tr>
		}

		return (
			<div className={cx('admin-table-col')}>
				<h2 className={cx('table-title')}>{headerTitle}</h2>
				<div className={cx('box', 'box-success')}>
					<div className={cx('box-body')}>
						<div className={cx('table-header')}>
							<div className={cx('show-entries', 'pull-right', 'form-inline')}>
								<label htmlFor="perpage">Mostrar</label>
								<select ref="itensPerPage" value={this.state.itensPerPage} className={cx('form-control', 'input-sm')} onChange={this.changeItensPerPage}>
									<option value="10">10</option>
									<option value="25">25</option>
									<option value="50">50</option>
								</select>
							</div>
						</div>

						<table className={cx('table', 'table-bordered')}>
							<thead>
								<tr>
									<th>Nome</th>
									<th width="20%">Data</th>
								</tr>
							</thead>

							<tbody>
								{dataItens}
							</tbody>
						</table>

						<Pagination
							page={this.state.page}
							length={numberOfPages}
							goto={this.changePage}
						/>
					</div>
				</div>
			</div>
		);
	}
}

AdminList.propTypes = {
	data: PropTypes.any.isRequired,
	folder: PropTypes.string,
	headerTitle: PropTypes.string.isRequired,
	onDestroy: PropTypes.func,
	onEdit: PropTypes.func.isRequired
};
