import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from 'css/admin/selectedList';

const cx = classNames.bind(styles);


class OpeningSelectedListItem extends Component {
	constructor(props) {
		super(props);

		this.removeItem = this.removeItem.bind(this);
	}

	removeItem() {
		const { index, onRemoveItem } = this.props;
		onRemoveItem(index);
	}

	render() {
		const { daystart, dayend, hourstart, hourend } = this.props;

		return (
			<li>
				<a onClick={this.removeItem}>
					<i className={cx('fa', 'fa-close', 'fa-fw')} aria-hidden={true}></i>&nbsp;
					<span><strong>{daystart}</strong></span>
					{(dayend != '') && <span> a <strong>{dayend}</strong></span>}
					<span> - <strong>{hourstart}</strong> as <strong>{hourend}</strong></span>
				</a>
			</li>
		);
	}
}

OpeningSelectedListItem.propTypes = {
	index: PropTypes.number,
	daystart: PropTypes.string,
	dayend: PropTypes.string,
	hourstart: PropTypes.string,
	hourend: PropTypes.string,
	onRemoveItem: PropTypes.func
};




class OpeningSelectedList extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { group, removeItem } = this.props;

		if(!group) return false;

		let itens;
		if(group.length > 0){
			itens = group.map((item, index) => {
				return (
					<OpeningSelectedListItem 
						key={index}
						index={index}
						daystart={item.daystart}
						dayend={item.dayend}
						hourstart={item.hourstart}
						hourend={item.hourend}
						onRemoveItem={removeItem}
						/>
				);
			});
		} else {
			itens = <li className={cx('text-center', 'empty-list')}>Nenhum funcionamento adicionado.</li>
		}

		return (
			<div className={cx('group-list')}>
				<ul className={cx('list-inline')}>
					{itens}
				</ul>
			</div>
		);
	}
}

OpeningSelectedList.propTypes = {
	group: PropTypes.array,
	removeItem: PropTypes.func
};

export default OpeningSelectedList;
