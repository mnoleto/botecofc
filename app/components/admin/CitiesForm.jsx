import classNames from 'classnames/bind';
import PropTypes from 'prop-types';
import React from 'react';
import { useEffect, useState } from 'react';
import { CitiesSelect, StatesSelect } from '../selects'
import styles from '../../css/admin/form';
import LoadingMessage from './LoadingMessage';

const cx = classNames.bind(styles);

export const CitiesForm = ({
  city,
  cities,
  states,
  onEntryChange,
  onEntrySave,
}) => {
  const [newCity, changeCity] = useState({
    id: '',
    isActive: false,
    name: '',
    stateId: '',
  })

  const [defaultLocale, changeDefaultLocale] = useState({
    cityId: '',
    stateId: '',
  })

  useEffect(() => {
    if (cities != null || cities.length > 0) {
      const defaultCity = cities.find(item => item.isDefault)
      if (defaultCity != null) {
        const defaultState = states.find(item => item.id === defaultCity.stateId)
        changeDefaultLocale({
          cityId: defaultCity.id,
          stateId: defaultState != null ? defaultState.id : '',
        })
      }
    }
  }, [cities, states])

  const [message, changeMessage] = useState('')
  const [isSubmiting, changeIsSubmiting] = useState(false)

  useEffect(() => {
    if (city && city.id) {
      changeCity({
        ...city,
      })
    }
    changeIsSubmiting(false)
  }, [city])

  const clearForm = () => {
    changeCity({
      id: '',
      isActive: false,
      name: '',
      stateId: ''
    });
    changeMessage('')
    changeIsSubmiting(false)
    window.scrollTo(0, 0);
  };

  const handleNameChange = (event) => {
    changeCity({
      ...newCity,
      name: event.target.value,
    })
  };

  const handleIsActive = (event) => {
    changeCity({
      ...newCity,
      isActive: event.target.value === '1',
    })
  }

  const handleDefaultStateChange = (event) => {
    changeDefaultLocale({
      ...defaultLocale,
      stateId: event.target.value,
    })
  }

  const handleStateIdChange = (event) => {
    changeCity({
      ...newCity,
      stateId: event.target.value,
    })
  };

  const onSave = (e) => {
    e.preventDefault();
    if (newCity.name === '') {
      changeMessage('Preencha o campo nome.');
      return false;
    }
    if (newCity.stateId === '' || newCity.stateId === '0') {
      changeMessage('Preencha o campo estado.');
      return false;
    }
    changeMessage('');
    changeIsSubmiting(true)

    if (city.name) {
      onEntryChange({
        id: newCity.id,
        isActive: newCity.isActive,
        name: newCity.name,
        stateId: newCity.stateId,
      })
    } else {
      onEntrySave({
        isDefault: false,
        isActive: newCity.isActive,
        name: newCity.name,
        stateId: newCity.stateId,
      })
    }
    clearForm()
  };

  const handleDefaultCityChange = (event) => {
    const selectedCity = cities.find(item => item.id === event.target.value)
    const oldDefaultCity = cities.find(item => item.id === defaultLocale.cityId)
    changeDefaultLocale({
      ...defaultLocale,
      cityId: event.target.value,
    })

    onEntryChange({
      ...oldDefaultCity,
      isDefault: false,
    })

    onEntryChange({
      ...selectedCity,
      isDefault: true,
    })
  }

  const renderContent = () => {
    if (!isSubmiting) {
      if (!states || states.length === 0) {
        return 'Cadastre um estado antes de cadastrar um cidade.'
      }
      return (
        <form className={cx('clearfix', 'form-inline', 'container-fluid')} method="post" encType="multipart/form-data" onSubmit={onSave} role="form">
          {renderMessage()}

          <div className={cx('row')}>
            <div className={cx('field-group', 'col-md-full')}>
              <label htmlFor="name" className={cx('sr-only')}>Nome *</label>
              <input type="text" name="name" placeholder="Nome *" value={newCity.name || ''} onChange={handleNameChange} />
            </div>
          </div>

          <div className={cx('row')}>
            <StatesSelect
              onChange={handleStateIdChange}
              selectedState={newCity.stateId}
              states={states}
            />
          </div>

          <div className={cx('row')}>
            <label>Ativada para filtro?</label>
            <br />
            <input type="radio" name="active" value="0" checked={!newCity.isActive} onChange={handleIsActive} /> Não
            <br />
            <input type="radio" name="active" value="1" checked={newCity.isActive} onChange={handleIsActive} /> Sim
          </div>

          <div className={cx('row')}>
            <div className={cx('col-md-full', 'required')}><p className={cx('pull-right')}><small>* campos obrigatórios</small></p></div>
          </div>

          {renderMessage()}

          <div className={cx('row')}>
            <input className={cx('submit-btn', 'pull-right')} type="submit" value='Enviar' />
            <input className={cx('clear-btn', 'pull-right')} type="button" value="Limpar" onClick={clearForm} />
          </div>
        </form>
      );
    } else {
      return <LoadingMessage />;
    }
  }

  const renderMessage = () => {
    if (message !== '') {
      return (
        <div className={cx('message')}>
          <i className="zmdi zmdi-block"></i> {message}
        </div>
      );
    }
  }
  const defaultCities = cities.filter(item => item.stateId === defaultLocale.stateId)
  return (
    <div className={cx('admin-form-col')}>
      <h2 className={cx('form-title')}>Dados da cidade</h2>
      {renderContent()}
      <hr/> 
      <h4>Cidade principal</h4>
      {states && states.length > 0 && 
        <StatesSelect
          onChange={handleDefaultStateChange}
          selectedState={defaultLocale.stateId}
          states={states}
        />
      }
      {defaultCities && defaultCities.length > 0 &&
        <CitiesSelect
          onChange={handleDefaultCityChange}
          selectedCity={defaultLocale.cityId}
          cities={defaultCities}
        />
      }
    </div>
  );
}

CitiesForm.propTypes = {
  city: PropTypes.object,
  cities: PropTypes.array,
  states: PropTypes.array,
  onEntryChange: PropTypes.func.isRequired,
  onEntrySave: PropTypes.func.isRequired
};

export default CitiesForm
