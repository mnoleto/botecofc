import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import { fetchChampionships } from '../../actions/championships';
import { fetchCountries } from '../../actions/countries';
import { fetchGames } from '../../actions/games';
import { fetchTeams } from '../../actions/teams';
import { fetchPlaces } from '../../actions/places';
import FilterCountry from '../filters/FilterCountry';
import ListChampionships from '../championships/ListChampionships';
import styles from '../../css/components/championships';

const cx = classNames.bind(styles);

class Championships extends Component {
    static need = [
        fetchChampionships,
        fetchCountries,
        fetchTeams,
        fetchGames,
        fetchPlaces
    ];

    constructor(props) {
        super(props);
        this.state = {
            country: ''
        }

        this.updateCountryFilter = this.updateCountryFilter.bind(this);
    }

    updateCountryFilter(value) {
        const { setVisibilityFilter } = this.props;
        this.state.country = value;
        setVisibilityFilter('SHOW_ONLY', '', value);
    }

    render() {
        const { championships, countries, teams, games } = this.props;
        return (
            <div className={cx('championships')}>
                <FilterCountry
                    countries={countries}
                    onFilter={this.updateCountryFilter}
                />

                <div className={cx('list')}>
                    <ListChampionships
                        title={['Todos os campeonatos']}
                        championships={championships}
                        teams={teams}
                        games={games}
                        showAll={true}
                    />
                </div>
            </div>
        );
    }

};

Championships.propTypes = {
    championships: PropTypes.array.isRequired,
    countries: PropTypes.array.isRequired,
    teams: PropTypes.array.isRequired,
    games: PropTypes.array.isRequired,
    setVisibilityFilter: PropTypes.func.isRequired
};

export default Championships;
