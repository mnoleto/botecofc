import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import sortByName from 'utils/sortByName';
import ChampionshipItem from '../championship/ChampionshipItem';
import ChampionshipGamesModal from '../modais/ChampionshipGamesModal';
import styles from '../../css/components/list';

const cx = classNames.bind(styles);

class ListChampionships extends Component {
	constructor(props) {
		super(props);
		this.state = {
			championship: {}
		};
		this.handleChampionshipSelected = this.handleChampionshipSelected.bind(this);
		this.renderTitle = this.renderTitle.bind(this);
	}

	handleChampionshipSelected(championship) {
		this.setState({'championship': championship});
		document.querySelector( '#championshipGamesModal' ).style.display = 'block';
	}

	renderTitle() {
		const { title } = this.props;

		if(title && title.length > 0) {
			const text = title.map((textOrHTML, index) => {
				return <span key={index}>{textOrHTML}</span>;
			});
			return (
				<header>
					<h2 className={cx('title-list')}>{text}</h2>
				</header>
			);
		}
	}

	render() {
		const { championships, games, teams, showAll } = this.props;
		const _ = this;
		const alphabeticalChampionships = championships;
		alphabeticalChampionships.sort(sortByName('name'));

		let listItem;
		if(alphabeticalChampionships.length > 0) {
			listItem = alphabeticalChampionships.map((championship, index) => {
				if(!showAll && index > 9) {
					return;
				} else {
					return(
						<li key={index}>
							<ChampionshipItem
								championship={championship}
								onSelectChampionship={_.handleChampionshipSelected}
							/>
						</li>
					);
				}
			});
		} else {
			return (
				<li key={'empty-championsip'} className={cx('empty-message')}>
					Nenhum campeonato encontrado.
				</li>
			);
		}

		let button;
		if(!showAll && championships.length >= 9) {
			button = <footer>
					<Link to="/campeonatos" className={cx('btn-more')}>mais campeonatos</Link>
				</footer>
		}
		return (
			<div className={cx('list-container')}>
				{this.renderTitle()}

				<div className={cx('list-content')}>
					<ul className={cx('list')}>
						{listItem}
					</ul>
				</div>

				{button}

        <ChampionshipGamesModal
            id={"championshipGamesModal"}
            championship={this.state.championship}
            teams={teams}
            games={games}
        />
			</div>
		);
	};
};

ListChampionships.propTypes = {
	title: PropTypes.array,
	championships: PropTypes.array.isRequired,
	teams: PropTypes.array.isRequired,
	games: PropTypes.array.isRequired,
	showAll: PropTypes.bool
};

export default ListChampionships;
