import React from 'react';
import PropTypes from 'prop-types';

const Channel = ({ channel }) => (
    <span>{channel.name}</span>
);

Channel.propTypes = {
    channel: PropTypes.object.isRequired,
};

export default Channel;