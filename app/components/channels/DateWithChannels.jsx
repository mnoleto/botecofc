import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import classNames from 'classnames/bind';
import Channel from './Channel';
import styles from '../../css/components/owner';

const cx = classNames.bind(styles);

class Date extends Component {
    getChannel(id, channels) {
        return channels.find(channel => channel.id === id);
    }

    render () {
        const { channels, gameChannels, gameDate } =this.props;

        const date = moment(gameDate);

        let channelsList;
        if (gameChannels && gameChannels.length > 0) {
            channelsList = gameChannels.map((channel, index) => {
                const selectedChannel = this.getChannel(channel, channels);
                if (!selectedChannel) return null;
                return (
                    <span key={channel}>
                        {!!index && ", "}
                        <Channel channel={selectedChannel} />
                    </span>
                );
            }, this);
        }
        return (
            <div className={cx('col-xs-6', 'col-sm-4', 'col-custom-2')}>
                <div className={cx('calendar')}>
                    <div className={cx('weekday')}>{date.format('ddd')}</div>
                    <div className={cx('day-month')}>
                        <div className={cx('day')}>{date.format('DD')}</div>
                        <div className={cx('month')}>{date.format('MMM')}</div>
                    </div>
                </div>
                <div className={cx('detail')}>
                    <div className={cx('hour')}>{date.format('HH[h]mm')}</div>
                    <div className={cx('channels')}>{channelsList}</div>
                </div>
            </div>
        );
    }
    
}

Date.propTypes = {
    gameDate: PropTypes.string.isRequired,
    gameChannels: PropTypes.array,
    channels: PropTypes.array.isRequired,
};

export default Date;
