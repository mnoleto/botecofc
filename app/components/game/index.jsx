import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import Icon from '../Icon';
import FilterCity from '../filters/FilterCity';
import GameTeamItem from '../game/GameTeamItem';
import ListPlaces from '../places/ListPlaces';
import { fetchGames, setVisibilityFilter } from '../../actions/games';
import { fetchTeams } from '../../actions/teams';
import { fetchPlaces } from '../../actions/places';
import { getPlacesByGame } from '../../utils/get';
import styles from '../../css/components/teams';

const cx = classNames.bind(styles);

class Game extends Component {
    static need = [
        fetchGames,
        fetchTeams,
        fetchPlaces,
    ];

    constructor(props) {
        super(props);
        this.state = {
            game: {}
        }
        this.renderGame = this.renderGame.bind(this);
        this.setGameById = this.setGameById.bind(this);
        this.updateCityFilter = this.updateCityFilter.bind(this);
    }

    componentDidMount() {
        const { games, params } = this.props;
        if (params.id !== '') {
            if (params.id !== this.state.gameId && games.length > 0) {
                this.setGameById(params.id);
            }
        }
    }

    setGameById(id) {
        const { games } = this.props;
        const selectedGame = games.find((game) => {
            return (game.id === id);
        });

        if (selectedGame && selectedGame.id !== '') {
            this.setState({
                game: selectedGame
            });
        }
    }

    updateCityFilter(value) {
        const { setVisibilityFilter } = this.props;
        if (value === 'TODO DF') {
            this.setState({ city: '' });
            setVisibilityFilter('SHOW_ALL', '', '', '');
        } else {
            this.setState({ city: value });
            setVisibilityFilter('SHOW_ONLY', '', value, '');
        }
    }

    renderGame() {
        const { places, games, teams } = this.props;
        const _ = this;
        if (this.state.game && this.state.game.id && teams && teams.length > 0) {
            const gameDate = new Date(this.state.game.gameDate);
            const day = gameDate.getDate();
            const month = gameDate.getMonth() + 1;
            const year = gameDate.getFullYear();
            const hour = gameDate.getHours();
            const minutes = gameDate.getMinutes();

            let homeTeam;
            let visitingTeam;

            teams.forEach(team => {
                if (team.id === _.state.game.visitingTeam.id) visitingTeam = team;
                if (team.id === _.state.game.homeTeam.id) homeTeam = team;
            });
            
            return (
                <React.Fragment>
                    <div className={cx('game-box')}>
                        <div className={cx('game-container')}>
                            <div className={cx('game-championship')}>
                                <Icon size={'lg'} name={'championship'}></Icon>
                                {this.state.game.championship.name}
                            </div>

                            <div className={cx('game-item')}>
                                <GameTeamItem image={homeTeam.logo.responseText} name={homeTeam.name} slug={homeTeam.slug} />
                                <div className={cx('team-versus')}>x</div>
                                <GameTeamItem image={visitingTeam.logo.responseText} name={visitingTeam.name} slug={visitingTeam.slug} />
                                <div className={cx('game-date')}>
                                    {((day < 10) ? '0' : '') + day}/{((month < 10) ? '0' : '') + month}/{year} - {hour}H{((minutes < 10) ? '0' : '') + minutes}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className={cx('filters')}></div>

                    <ListPlaces
                        title={['Bares que poderão transmitir esse jogo']}
                        places={getPlacesByGame(places, this.state.game)}
                        games={games}
                        teams={teams}
                        showAll={true}
                        keepOrder={true}
                        showConfirmed={true}
                    />
                </React.Fragment>
            );
        } else {
            return <div>Jogo não encontrado</div>;
        }
    }

    render() {
        return (
            <div className={cx('game')}>{this.renderGame()}</div>
        );
    }
};

Game.propTypes = {
    allplaces: PropTypes.array,
    games: PropTypes.array,
    teams: PropTypes.array.isRequired,
    places: PropTypes.array,
    setVisibilityFilter: PropTypes.func.isRequired,
};

export default Game;