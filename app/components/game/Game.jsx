import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from '../../css/components/owner';

const cx = classNames.bind(styles);

const Game = ({ homeTeam, isMini, visitingTeam, className }) => {
    return (
        <div className={cx('game-logos', className ? className : '', isMini ? 'mini' : '')}>
            {homeTeam && (<div className={cx('team-item')}>
                <img src={(homeTeam.logo ? homeTeam.logo.responseText : '')} alt={homeTeam.name} />
            </div>)}
            <div className={cx('team-versus')}>x</div>
            {visitingTeam && (<div className={cx('team-item')}>
                <img src={(visitingTeam.logo ? visitingTeam.logo.responseText : '')} alt={visitingTeam.name} />
            </div>)}
        </div>
    );
}

Game.propTypes = {
    homeTeam: PropTypes.object.isRequired,
    visitingTeam: PropTypes.object.isRequired,
    className: PropTypes.string,
    isMini: PropTypes.bool,
};

export default Game;
