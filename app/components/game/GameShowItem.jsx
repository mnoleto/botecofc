import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import Game from './Game';
import Icon from '../Icon';
import DateWithChannels from '../channels/DateWithChannels';
import styles from '../../css/components/owner';

const cx = classNames.bind(styles);

class GameShowItem extends Component {
    getTeam(id, teams) {
        return teams.find(team => team.id === id);
    }

    render() {
        const { game, teams, channels, willBroadcast } = this.props;
        if (!teams || !channels || !teams) return null;

        const homeTeam = this.getTeam(game.homeTeam.id, teams);
        const visitingTeam = this.getTeam(game.visitingTeam.id, teams);

        return (
            <div className={cx('game-item')}>
                <div className={cx('owner-manage-container')}>
                    <div className={cx('row')}>
                        <div className={cx('col-xs-6', 'col-sm-4', 'col-sm-offset-1', 'col-custom-1')}>
                            <h3>{game.championship.name}</h3>
                            <div className={cx('checked')}>
                                <Icon
                                    name={'checked'}
                                    size={'3x'}
                                />
                            </div>
                            <Game
                                homeTeam={homeTeam}
                                visitingTeam={visitingTeam}
                            />
                        </div>

                        <DateWithChannels
                            gameDate={game.gameDate}
                            gameChannels={game.channels}
                            channels={channels}
                        />

                        <div className={cx('col-xs-3', 'hidden-xs')}>
                            <button type="button" className={cx('btn-confirmed')}>confirmado</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

GameShowItem.propTypes = {
    game: PropTypes.object.isRequired,
    teams: PropTypes.array.isRequired,
    channels: PropTypes.array.isRequired,
};

export default GameShowItem;