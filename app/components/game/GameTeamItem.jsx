import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from '../../css/components/teams';

const cx = classNames.bind(styles);

const GameTeamItem = ({image, name, slug}) => {
  return (
    <div className={cx('team-item')}>
      <Link to={'/times/' + slug}>
        <img src={image} alt={name} />
        <span>{name}</span>
      </Link>
    </div>
  );
};

GameTeamItem.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired
};

export default GameTeamItem;
