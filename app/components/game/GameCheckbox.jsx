import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from '../../css/components/owner';

const cx = classNames.bind(styles);

export default class GameCheckbox extends React.Component {
    constructor(props) {
        super(props);
        this.onHandleChange = this.onHandleChange.bind(this);
    }

    onHandleChange() {
        const { onChange, value } = this.props;
        onChange(value);
    }

    render() {
        const { value, isChecked } = this.props;
        return (
            <label className={cx('check-row')}>
                <input
                    type="checkbox"
                    value={value}
                    checked={isChecked}
                    onChange={this.onHandleChange}
                    className={cx('check-inline')}
                />
                <span className={cx('checkmark')}></span>
            </label>
        );
    }
}

GameCheckbox.propTypes = {
    isChecked: PropTypes.bool,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
};