import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import GameTeamItem from '../game/GameTeamItem';
import styles from '../../css/components/list';
import iconPlaces from '../../images/icons/places-list.png';
import iconDate from '../../images/icons/calendar-list.png';
import iconDateConfirmed from '../../images/icons/calendar-list-confirmed.png';
import iconConfirmed from '../../images/icons/confirmed.png';

const cx = classNames.bind(styles);

class GameItem extends Component {
	constructor(props) {
		super(props);
		this.renderDate = this.renderDate.bind(this);
		this.renderHour = this.renderHour.bind(this);
		this.renderTeamItem = this.renderTeamItem.bind(this);
	}

	renderDate(date) {
		const gameDate = new Date(date);
		const month = gameDate.getMonth() + 1;
		return ( (gameDate.getDate() < 10) ? '0' : '' ) + gameDate.getDate() + '/' +  ( (month < 9) ? '0' : '' ) + month;
	}

	renderHour(date) {
		const gameDate = new Date(date);
		return (gameDate.getHours()<10?'0':'') + gameDate.getHours() + 'H' + (gameDate.getMinutes()<10?'0':'') + gameDate.getMinutes();
	}

	renderTeamItem(obj) {
		const { teams } = this.props;
		let selected = {};
		if(teams) {
			teams.forEach(team => {
				if(team.id === obj.id) selected = team;
			});
			return (
				<GameTeamItem
					id={selected.id}
					image={(selected.logo) ? selected.logo.responseText : ''}
					name={selected.name}
					slug={selected.slug}
				/>
			)
		}
	}

	render() {
		const {game} = this.props;
		const confirmedClass = game.confirmed ? 'game-item-confirmed' : '';
		const calendarIcon = game.confirmed ? iconDateConfirmed : iconDate;
		return (
			<div className={cx('list-item', 'game-item', confirmedClass)}>
				<div className={cx('list-col-1')}>
					<div className={cx('col-container')}>
						<i className={cx('list-icon', 'list-icon-date')}><img src={calendarIcon} alt="Data" /></i>
						<div className={cx('game-day')}>{this.renderDate(game.gameDate)}</div>
						<div className={cx('game-hour')}>{this.renderHour(game.gameDate)}</div>
					</div>
				</div>

				<div className={cx('list-middle')}>
					<div className={cx('list-middle-container')}>
						{this.renderTeamItem(game.homeTeam)}
						<div className={cx('team-versus')}>x</div>
						{this.renderTeamItem(game.visitingTeam)}
					</div>

					{ (game.confirmed) &&
					<div className={cx('confirmed')}><img src={iconConfirmed} alt="Confirmado" /></div>
					}
				</div>

				<div className={cx('list-col-2', 'places-col')}>
					<div className={cx('col-container')}>
						<Link to={"/jogo/" + game.id} className={cx('btn-places-view')}>
							<i className={cx('list-icon', 'list-icon-place')}><img src={iconPlaces} alt="Visualizar locais" /></i>
							<small>Visualizar</small><br /><strong>locais</strong>
						</Link>
					</div>
				</div>
			</div>
		);
	}
};

GameItem.propTypes = {
	game:PropTypes.object.isRequired,
	teams: PropTypes.array.isRequired
};

export default GameItem;
