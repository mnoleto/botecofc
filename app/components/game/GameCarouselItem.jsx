import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import GameTeamItem from '../game/GameTeamItem';
import styles from '../../css/components/teams';

const cx = classNames.bind(styles);

const GameCarouselItem = ({game, teams}) => {
	const gameDate = new Date(game.gameDate);
	const day = gameDate.getDate();
	const month = gameDate.getMonth() + 1;
	const year = gameDate.getFullYear();
	const hour = gameDate.getHours();
	const minutes = gameDate.getMinutes();

	let homeTeam;
	let visitingTeam;
	teams.forEach((team, index) => {
		if(team.id === game.visitingTeam.id) visitingTeam = team;
		if(team.id === game.homeTeam.id) homeTeam = team;
	});

	return (
		<div className={cx('game-carousel-item')}>
			<GameTeamItem image={homeTeam.logo.responseText} name={homeTeam.name} slug={homeTeam.slug} />
			<div className={cx('team-versus')}>x</div>
			<GameTeamItem image={visitingTeam.logo.responseText} name={visitingTeam.name} slug={visitingTeam.slug} />
			<div className={cx('game-date')}>
				{( (day < 10) ? '0' : '' ) + day}/{( (month < 10) ? '0' : '' ) + month}/{year} - {hour}H{( (minutes < 10) ? '0' : '' ) + minutes}
			</div>
		</div>
	);
};

GameCarouselItem.propTypes = {
	game: PropTypes.object.isRequired,
	teams: PropTypes.array.isRequired
};

export default GameCarouselItem;
