import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import GameCheckbox from './GameCheckbox';
import Game from './Game';
import DateWithChannels from '../channels/DateWithChannels';
import styles from '../../css/components/owner';

const cx = classNames.bind(styles);

class GameSelectItem extends Component {
    constructor(props) {
        super(props);

        this.handleSelect = this.handleSelect.bind(this);
    }

    getTeam(id, teams) {
        return teams.find(team => team.id === id);
    }

    handleSelect() {
        const { addGame, removeGame, game, isChecked } = this.props;
        if (isChecked) {
            removeGame(game);
        } else {
            addGame(game);
        }
    }

    render() {
        const { game, teams, channels, isChecked, showConfirmed } = this.props;
        if (!teams || !channels || !teams) return null;
        
        const homeTeam = this.getTeam(game.homeTeam.id, teams);
        const visitingTeam = this.getTeam(game.visitingTeam.id, teams);
        
        return (
            <div className={cx('game-item')}>
                <div className={cx('owner-manage-container')}>
                    <div className={cx('row')}>
                        <div className={cx('col-xs-6', 'col-sm-4', 'col-sm-offset-1', 'col-custom-1')}>
                            <h3>{game.championship.name}</h3>
                            <GameCheckbox
                                isChecked={isChecked}
                                value={game.id}
                                onChange={this.handleSelect}
                            />
                            <Game
                                homeTeam={homeTeam}
                                visitingTeam={visitingTeam}
                            />
                        </div>

                        <DateWithChannels
                            gameDate={game.gameDate}
                            gameChannels={game.channels}
                            channels={channels}
                        />

                        {showConfirmed &&
                        <div className={cx('col-xs-3', 'hidden-xs')}>
                            <button type="button" className={cx('btn-confirmed')} onClick={this.handleSelect}>confirmado</button>
                        </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

GameSelectItem.propTypes = {
    game: PropTypes.object.isRequired,
    teams: PropTypes.array.isRequired,
    channels: PropTypes.array.isRequired,
    isChecked: PropTypes.bool.isRequired,
    addGame: PropTypes.func.isRequired,
    removeGame: PropTypes.func.isRequired,
    showConfirmed : PropTypes.bool,
};

export default GameSelectItem;