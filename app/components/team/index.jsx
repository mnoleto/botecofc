import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cookie from 'react-cookie';
import classNames from 'classnames/bind';
import {
    fetchTeams,
} from '../../actions/teams';
import { fetchGames } from '../../actions/games';
import { fetchChampionships } from '../../actions/championships';
import { fetchPlaces } from '../../actions/places';
import {
    addPlaceToWatch,
    removePlaceToWatch,
    addTeamToFavorites,
    removeTeamFromFavorites,
} from '../../actions/users';
import TeamTabs from '../team/TeamTabs';
import PlacesToWatchModal from '../modais/PlacesToWatchModal';
import LoginModal from '../modais/LoginModal';
import teamBg from '../../images/bg/team.jpg';
import teamBgMobile from '../../images/bg/team@2x.jpg';
import styles from '../../css/components/teams';

const cx = classNames.bind(styles);

class Team extends Component {
    static need = [
        fetchTeams,
        fetchGames,
        fetchChampionships,
        fetchPlaces
    ];

    constructor(props) {
        super(props);
        this.state = {
            slug: '',
            team: {},
            placesToWatch: [],
        };

        this.addPlaceToWatch = this.addPlaceToWatch.bind(this);
        this.addRecomendation = this.addRecomendation.bind(this);
        this.addFavorite = this.addFavorite.bind(this);
        this.getPlaceById = this.getPlaceById.bind(this);
        this.openPlacesToWatch = this.openPlacesToWatch.bind(this);
        this.removeFavorite = this.removeFavorite.bind(this);
        this.removePlaceToWatch = this.removePlaceToWatch.bind(this);
        this.removeRecomendation = this.removeRecomendation.bind(this);
    }

    componentDidMount() {
        const { teams, params, user } = this.props;

        cookie.remove('returnTo', { path: '/' });
        if (params.slug) {
            if (params.slug !== this.state.slug && teams.length > 0) {
                this.setState({ slug: params.slug });
                this.getTeamBySlug(params.slug);
            }
        }

        if (user && user.authenticated && this.state.team && this.state.team.id) {
            const _ = this;
            if (user.account.watch && user.account.watch.length > 0) {
                user.account.watch.forEach((value, index) => {
                    if (value.team === _.state.team.id) {
                        _.setState({
                            placesToWatch: user.account.watch[index].places
                        });
                    }
                });

            }
        }
        
        if (!this.props.location.query || !this.props.location.query.openModal) return;
        document.querySelector('#placestowatch').style.display = 'block';
    }

    addFavorite(team) {
        const { addTeamToFavorites } = this.props;
        const { account } = this.props.user;
        addTeamToFavorites({
            id: account._id,
            favorite: team,
        });
    }

    addPlaceToWatch(place) {
        const { addPlaceToWatch, incrementScore } = this.props;
        const { account } = this.props.user;
        const { watch } = this.props.user.account;

        let newWatch;
        const index = watch.map(e => e.team).indexOf(this.state.team.id);

        if (index !== -1) {
            let canInsert = true;
            watch[index].places.forEach((v, i) => {
                if (v === place) {
                    canInsert = false;
                }
            });
            if (canInsert) {
                newWatch = watch;
                newWatch[index].places.push(place);
                this.addRecomendation(place);
            }
        } else {
            newWatch = [...watch, {
                team: this.state.team.id,
                places: [place]
            }];
            this.addRecomendation(place);
        }

        if (newWatch) {
            addPlaceToWatch(account._id, newWatch);
            incrementScore(place.id);
        }
    }

    addRecomendation(place) {
        const { userRecomendTeam } = this.props;

        let newRecomendations = [];
        const selectedPlace = this.getPlaceById(place);
        const recomendations = selectedPlace.recomendations;
        let indexTeam;

        if (recomendations) {
            indexTeam = recomendations.map(e => e.team).indexOf(this.state.team.id);
        }

        if (indexTeam !== undefined && indexTeam !== -1) {
            newRecomendations = recomendations;
            newRecomendations[indexTeam].counter = recomendations[indexTeam].counter + 1;
        } else {
            if (recomendations) {
                newRecomendations = [...recomendations, {
                    team: this.state.team.id,
                    counter: 1
                }]
            } else {
                newRecomendations.push({
                    team: this.state.team.id,
                    counter: 1
                });
            }
        }

        userRecomendTeam({
            id: place,
            recomendations: newRecomendations
        });
    }

    getPlaceById(id) {
        const { allplaces } = this.props;
        return allplaces.find(value => value.id === id);
    }

    getTeamBySlug(slug) {
        const { teams, setTeamVisibilityFilter } = this.props;
        let teamSelected;
        teams.some(item => {
            if (item.slug === slug) {
                teamSelected = item;
                return;
            }
        });
        this.setState({ team: teamSelected });
        setTeamVisibilityFilter('SHOW_ONLY', teamSelected.id);
        this.forceUpdate();
    }

    hexToRgb(hex) {
        if (!hex && hex != '') return false;
        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, (m, r, g, b) => {
            return r + r + g + g + b + b;
        });

        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    openPlacesToWatch() {
        const { authenticated } = this.props.user;
        if (authenticated) {
            document.querySelector('#placestowatch').style.display = 'block';
        } else {
            // TODO: recolocar cookie
            // cookie.save('returnTo', window.location.pathname + '?openModal=transmissao', { path: '/' });
            document.querySelector('#loginmodal').style.display = 'block';
        }
    }

    removeFavorite(team) {
        const { account } = this.props.user;
        const { removeTeamFromFavorites } = this.props;
        removeTeamFromFavorites({
            id: account._id,
            favorite: team,
        });
    }

    removeRecomendation(place) {
        const { userRecomendTeam, decrementScore } = this.props;

        let newRecomendations = [];
        const selectedPlace = this.getPlaceById(place);
        const recomendations = selectedPlace.recomendations;
        let indexTeam;

        if (recomendations) {
            indexTeam = recomendations.map(e => e.team).indexOf(this.state.team.id);
        }

        if (indexTeam !== undefined && indexTeam !== -1) {
            newRecomendations = recomendations;
            newRecomendations[indexTeam].counter = recomendations[indexTeam].counter - 1;
        } else {
            if (recomendations) {
                newRecomendations = [...recomendations, {
                    team: this.state.team.id,
                    counter: 0
                }]
            } else {
                newRecomendations.push({
                    team: this.state.team.id,
                    counter: 0
                });
            }
        }

        userRecomendTeam({
            id: place,
            recomendations: newRecomendations
        });
        decrementScore(place.id);
    }

    removePlaceToWatch(place) {
        const { removePlaceToWatch } = this.props;
        const { account } = this.props.user;
        const { watch } = this.props.user.account;

        let newWatch = watch;
        const index = newWatch.map(e =>e.team).indexOf(this.state.team.id);

        if (index != -1) {
            newWatch[index].places.forEach((v, i) => {
                if (v === place) {
                    newWatch[index].places.splice(i, 1);
                }
            });
            this.removeRecomendation(place);
        }

        if (newWatch) {
            removePlaceToWatch(account._id, newWatch);
        }
    }

    render() {
        const { user, games, championships, places, allplaces, teams } = this.props;
        const teamColor = this.hexToRgb(this.state.team.color);
        return (
            <div className={cx('team')}>
                <div className={cx('team-content')}>
                    <figure className={cx('team-logo-lg')}><img src={(this.state.team.logo) ? this.state.team.logo.responseText : ''} alt={this.state.team.name} /></figure>

                    <header className={cx('team-name')}>
                        <h1>{this.state.team.name}</h1>
                    </header>

                    <div className={cx('team-box')}>
                        <button type="button" className={cx('btn')} onClick={this.openPlacesToWatch}>Ajude outros torcedores. Onde você costuma assistir aos jogos do <strong>{this.state.team.name}</strong>?</button>
                    </div>

                    <TeamTabs
                        team={this.state.team}
                        teams={teams}
                        games={games}
                        championships={championships}
                        places={places}
                        user={user}
                        addFavorite={this.addFavorite}
                        removeFavorite={this.removeFavorite}
                    />
                </div>

                <figure className={cx('team-bg', 'visible-xs', 'visible-sm')} style={{
                    background: '-moz-linear-gradient( top, rgba(' + teamColor.r + ', ' + teamColor.g + ', ' + teamColor.b + ', 0.45) 0%, rgba(' + teamColor.r + ', ' + teamColor.g + ', ' + teamColor.b + ', 0.45) 85%,  rgba(255, 255, 255, 1) 100%), url( ' + teamBgMobile + ' )',
                    background: '-webkit-linear-gradient( top, rgba(' + teamColor.r + ', ' + teamColor.g + ', ' + teamColor.b + ', 0.45) 0%, rgba(' + teamColor.r + ', ' + teamColor.g + ', ' + teamColor.b + ', 0.45) 85%,  rgba(255, 255, 255, 1) 100%), url( ' + teamBgMobile + ' )',
                    background: 'linear-gradient( to bottom, rgba(' + teamColor.r + ', ' + teamColor.g + ', ' + teamColor.b + ', 0.45) 0%, rgba(' + teamColor.r + ', ' + teamColor.g + ', ' + teamColor.b + ', 0.45) 85%,  rgba(255, 255, 255, 1) 100%), url( ' + teamBgMobile + ' )',
                    backgroundSize: 'contain',
                    backgroundRepeat: 'no-repeat'
                }}>
                    <img src={teamBgMobile} />
                </figure>

                <figure className={cx('team-bg', 'visible-md', 'visible-lg')} style={{
                    background: '-moz-linear-gradient( top, rgba(' + teamColor.r + ', ' + teamColor.g + ', ' + teamColor.b + ', 0.45) 0%, rgba(' + teamColor.r + ', ' + teamColor.g + ', ' + teamColor.b + ', 0.45) 85%,  rgba(255, 255, 255, 1) 100%), url( ' + teamBg + ' )',
                    background: '-webkit-linear-gradient( top, rgba(' + teamColor.r + ', ' + teamColor.g + ', ' + teamColor.b + ', 0.45) 0%, rgba(' + teamColor.r + ', ' + teamColor.g + ', ' + teamColor.b + ', 0.45) 85%,  rgba(255, 255, 255, 1) 100%), url( ' + teamBg + ' )',
                    background: 'linear-gradient( to bottom, rgba(' + teamColor.r + ', ' + teamColor.g + ', ' + teamColor.b + ', 0.45) 0%, rgba(' + teamColor.r + ', ' + teamColor.g + ', ' + teamColor.b + ', 0.45) 85%,  rgba(255, 255, 255, 1) 100%), url( ' + teamBg + ' )',
                    backgroundSize: 'contain',
                    backgroundRepeat: 'no-repeat'
                }}>
                    <img src={teamBg} />
                </figure>

                {user.authenticated &&
                    <PlacesToWatchModal
                        id={"placestowatch"}
                        places={allplaces}
                        placesToWatch={this.state.placesToWatch}
                        team={this.state.team}
                        addPlaceToWatch={this.addPlaceToWatch}
                        removePlaceToWatch={this.removePlaceToWatch}
                    />
                }

                {!user.authenticated &&
                    <LoginModal
                        id={'loginmodal'}
                    />
                }
            </div>
        );
    }
};

Team.propTypes = {
    user: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
    teams: PropTypes.array.isRequired,
    games: PropTypes.array.isRequired,
    championships: PropTypes.array.isRequired,
    places: PropTypes.array.isRequired,
    allplaces: PropTypes.array.isRequired,
    setTeamVisibilityFilter: PropTypes.func.isRequired,
    addPlaceToWatch: PropTypes.func.isRequired,
    userRecomendTeam: PropTypes.func.isRequired,
    removePlaceToWatch: PropTypes.func.isRequired,
    addTeamToFavorites: PropTypes.func.isRequired,
    removeTeamFromFavorites: PropTypes.func.isRequired,
    incrementScore: PropTypes.func.isRequired,
    decrementScore: PropTypes.func.isRequired
};

export default Team;
