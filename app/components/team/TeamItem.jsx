import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import classNames from 'classnames/bind';
import styles from '../../css/components/list';
import iconDate from '../../images/icons/calendar-white-list.png';
import iconDateGrey from '../../images/icons/calendar-grey-list.png';
import iconGame from '../../images/icons/games-team-list.png';

const cx = classNames.bind(styles);

class TeamItem extends Component {
	constructor(props) {
		super(props);
		this.renderDate = this.renderDate.bind(this);
		this.renderHour = this.renderHour.bind(this);
	}

	renderDate(date) {
		const gameDate = new Date(date);
		const month = gameDate.getMonth() + 1;
		return ( (gameDate.getDate() < 10) ? '0' : '' ) + gameDate.getDate() + '/' +  ( (month < 9) ? '0' : '' ) + month;
	}

	renderHour(date) {
		const gameDate = new Date(date);
		return (gameDate.getHours()<10?'0':'') + gameDate.getHours() + 'H' + (gameDate.getMinutes()<10?'0':'') + gameDate.getMinutes();
	}

	render() {
		const { team, game } = this.props;
		return (
			<div className={cx('list-item', 'team-item')}>
				<div className={cx('list-col-1')}>
					<div className={cx('col-container')}>
						<h3 className={cx('title')}>
							<Link to={'/times/' + team.slug} style={{'color': team.textColor}}>{team.name}</Link>
						</h3>
						{(game) &&
							<div className={cx('next-game-container')}>
								<h4 className={cx('subtitle')} style={{'color': team.textColor}}>Próximo jogo</h4>
								<div className={cx('gameday')}>
									<div className={cx('game-day')} style={{'color': team.textColor}}>{this.renderDate(game.gameDate)}</div>
									<div className={cx('game-hour')} style={{'color': team.textColor}}>{this.renderHour(game.gameDate)}</div>
								</div>
							</div>
						}
						
					</div>
				</div>

				<div className={cx('list-middle')}>
					<div className={cx('list-middle-container')}>
						<Link to={'/times/' + team.slug} className={cx('logo-container')} style={{backgroundColor: team.color}}><img src={team.logo.responseText} alt={team.name} className={cx('logo')} /></Link>
					</div>
				</div>

				<div className={cx('list-col-2')}>
					<div className={cx('col-container')}>
						<Link to={'/times/' + team.slug} role="button" className={cx('btn-game-list')}>
							<i className={cx('list-icon', 'listteam-icon-game')}><img src={iconGame} alt="Lista de jogos" /></i>
							<small>lista de</small><br /><strong>jogos</strong>
						</Link>
					</div>
				</div>
			</div>
		);
	}
};

TeamItem.propTypes = {
	team: PropTypes.object.isRequired,
	game: PropTypes.object
};

export default TeamItem;
