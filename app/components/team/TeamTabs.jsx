import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Tabs, TabList, Tab, TabPanel } from 'react-tabs';
import classNames from 'classnames/bind';
import ListChampionships from '../championships/ListChampionships';
import ListGames from '../games/ListGames';
import ListPlaces from '../places/ListPlaces';
import styles from '../../css/components/tabs';

const cx = classNames.bind(styles);

class TeamTabs extends Component {
	constructor(props) {
		super(props);

		this.state = {
			favorite: false,
		};

		this.handleFavoriteClick = this.handleFavoriteClick.bind(this);
		this.renderFavoriteTab = this.renderFavoriteTab.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		const { team } = this.props;
		if (nextProps.user && nextProps.user.authenticated && team.name && team.name.length > 0) {
			const user = nextProps.user;
			if (user.authenticated && user.account.favorites.length > 0) {
				const _ = this;
				user.account.favorites.forEach((value, index) => {
					if (value.id === team.id) _.setState({ favorite: true });
				});
			}
		}
	}

	handleFavoriteClick() {
		const { team, addFavorite, removeFavorite } = this.props;
		const { authenticated, account } = this.props.user;
		
		if (authenticated) {
			if (this.state.favorite) {
				this.setState({ favorite: false });
				removeFavorite({
					id: team.id,
					name: team.name
				});
			} else {
				this.setState({ favorite: true });
				addFavorite({
					id: team.id,
					name: team.name
				});
			}
			this.forceUpdate();
		} else {
			document.querySelector( '#loginmodal' ).style.display = 'block';
		}
	}

	renderFavoriteTab() {
		const { team, user } = this.props;
		const { authenticated, account } = this.props.user;

		if (authenticated) {
			if (this.state.favorite) {
				return(
					<Tab
						className={cx('tab', 'tab-button', 'tablist-favorite', 'favorite')}
						onClick={this.handleFavoriteClick}>
						<span>Favorito</span>
					</Tab>
				);
			} else {
				return(
					<Tab
						className={cx('tab', 'tab-button', 'tablist-favorite')}
						onClick={this.handleFavoriteClick}>
						<span>Favoritar time</span>
					</Tab>
				);
			}
		} else {
			return(
				<Tab
					className={cx('tab', 'tab-button', 'tablist-favorite')}
					disabled
					onClick={this.handleFavoriteClick}>
					<span>Favoritar time</span>
				</Tab>
			);
		}
		
	}

	render() {
		const { user, championships, places, games, teams } = this.props;

		return (
			<Tabs className={cx('tabs')} defaultIndex={0}>
				<TabList className={cx('tabs-list', 'four-tabs')}>
					<Tab className={cx('tab', 'tablist-games')}><span>Próximos Jogos</span></Tab>
					<Tab className={cx('tab', 'tablist-championships')}><span>Campeonatos</span></Tab>
					<Tab className={cx('tab', 'tablist-oficial')}><span>Botecos oficiais</span></Tab>
					{this.renderFavoriteTab()}
				</TabList>

				<TabPanel className={cx('tabpanel')}>
					<ListGames
						title={['Próximos', <br />, <strong>jogos</strong>]}
						games={games}
						teams={teams}
					/>
				</TabPanel>

				<TabPanel className={cx('tabpanel')}>
					<ListChampionships
						title={['Campeonatos', <br />, <strong>em disputa</strong>]}
						championships={championships}
						teams={teams}
						games={games}
					/>
				</TabPanel>

				<TabPanel className={cx('tabpanel')}>
					<ListPlaces
						title={['Botecos', <br />, <strong>oficiais</strong>]}
						places={places}
						games={games}
						teams={teams}
						showConfirmed={false}
					/>
				</TabPanel>

				<TabPanel className={cx('tabpanel')}>
					<div className={cx('favorite-messsage')}>Adicionado aos favoritos</div>
				</TabPanel>
			</Tabs>
		);
	}
	
};

TeamTabs.propTypes = {
	team: PropTypes.object.isRequired,
	teams: PropTypes.array.isRequired,
	games: PropTypes.array.isRequired,
	championships: PropTypes.array.isRequired,
	places: PropTypes.array.isRequired,
	user: PropTypes.object,
	addFavorite: PropTypes.func.isRequired,
	removeFavorite: PropTypes.func.isRequired
};

export default TeamTabs;
