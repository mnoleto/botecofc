import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import registerImage from '../../images/bg/register@2x.jpg';
import styles from '../../css/components/loginOrRegister';

const cx = classNames.bind(styles);

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: ''
        };
        this.renderMessage = this.renderMessage.bind(this);
    }

    renderMessage() {
        const { message } = this.props.user;
        if (this.state.message != '') {
            return (
                <div className={cx('message')}>
                    <i className="zmdi zmdi-block"></i> {this.state.message}
                </div>
            );
        } else if (message && message.length > 0) {
            return (
                <div className={cx('message')}>
                    <i className="zmdi zmdi-block"></i> {message}
                </div>
            );
        }
    }

    render() {
        return (
            <div className={cx('login')}>
                <figure className={cx('header-image')}>
                    <img src={registerImage} alt="" />
                </figure>

                <div className={cx('container')}>
                    <h1 className={cx('heading')}>Que bom te receber aqui! Se conecte com a gente e reforce o time do Qual Bar. :)</h1>

                    <div className={cx('login-button-group')}>
                        <a href="/auth/facebook" className={cx('btn-more-register', 'btn-facebook')} onClick={this.facebookLogin}>Conectar com <strong>Facebook</strong></a>
                        <a href="/auth/google" className={cx('btn-more-register', 'btn-google')} onClick={this.googleLogin}>Conectar com <strong>Google</strong></a>
                    </div>
                </div>
            </div>
        );
    }
}

Login.propTypes = {
    user: PropTypes.object,
};

export default Login;