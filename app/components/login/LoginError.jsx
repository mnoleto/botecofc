import React, { Component } from 'react';
import classNames from 'classnames/bind';
import registerImage from '../../images/bg/register@2x.jpg';
import iconError from '../../images/icons/error.png';
import styles from '../../css/components/loginOrRegister';

const cx = classNames.bind(styles);

class LoginError extends Component {
	render() {
		return (
			<div className={cx('login')}>
				<figure className={cx('header-image')}>
					<img src={registerImage} alt="" />
				</figure>

				<div className={cx('container')}>
          <div className={cx('icon-error')}><img src={iconError} alt="Error" /></div>
					<h1 className={cx('heading')}>Já existe uma conta cadastrada que pertence a você. Faça o login usando essa conta.</h1>

					<div className={cx('login-button-group')}>
						<a href="/auth/facebook" className={cx('btn-more-register', 'btn-facebook')} onClick={this.facebookLogin}>Conectar com <strong>Facebook</strong></a>
						<a href="/auth/google" className={cx('btn-more-register', 'btn-google')} onClick={this.googleLogin}>Conectar com <strong>Google</strong></a>
					</div>
				</div>
			</div>
		);
	}
}

export default LoginError;
