import expect from 'expect';
import { filterConfirmedGames } from '../../utils/filters';

const games = [];

const places = [];

describe('Filters', () => {
    describe('Confirmed Games', () => {
        it('Should return an empty array if there isnt games loaded', () => {
            expect(filterConfirmedGames(null, null)).toEqual([]);
        });
    });
});