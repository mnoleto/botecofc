import expect from 'expect';
import { sortByFavoriteTeam, sortByGameDate, sortGames } from '../../utils/sort';

let data;

const dataSortedByDate = [
  { id: 109, gameDate: '2018-06-21T03:00:20.000Z' },
  { id: 108, gameDate: '2018-06-22T03:00:20.000Z' },
  { id: 103, gameDate: '2018-06-24T03:00:10.000Z' },
  { id: 107, gameDate: '2018-06-24T03:00:20.000Z' },
  { id: 100, gameDate: '2018-06-24T12:00:10.000Z' },
  { id: 101, gameDate: '2018-06-24T14:00:10.000Z' },
  { id: 104, gameDate: '2018-06-24T18:00:10.000Z' },
  { id: 106, gameDate: '2018-06-24T19:00:10.000Z' },
  { id: 102, gameDate: '2018-06-26T09:00:10.000Z' },
  { id: 105, gameDate: '2018-06-30T21:00:10.000Z' },
];

const dataSortedByFavorite = [
  { id: 107, homeTeam: { id: 8 }, visitingTeam: { id: 123 } },
  { id: 109, homeTeam: { id: 123 }, visitingTeam: { id: 3 } },
  { id: 100, homeTeam: { id: 1 }, visitingTeam: { id: 9 } },
  { id: 101, homeTeam: { id: 2 }, visitingTeam: { id: 10 } },
  { id: 102, homeTeam: { id: 3 }, visitingTeam: { id: 11 } },
  { id: 103, homeTeam: { id: 4 }, visitingTeam: { id: 12 } },
  { id: 104, homeTeam: { id: 5 }, visitingTeam: { id: 13 } },
  { id: 105, homeTeam: { id: 6 }, visitingTeam: { id: 14 } },
  { id: 106, homeTeam: { id: 7 }, visitingTeam: { id: 15 } },
  { id: 108, homeTeam: { id: 9 }, visitingTeam: { id: 2 } },
];

const dataSortedByBoth = [
  { id: 109, homeTeam: { id: 123 }, visitingTeam: { id: 3 }, gameDate: '2018-06-21T03:00:20.000Z' },
  { id: 107, homeTeam: { id: 8 }, visitingTeam: { id: 123 }, gameDate: '2018-06-24T03:00:20.000Z' },
  { id: 108, homeTeam: { id: 9 }, visitingTeam: { id: 2 }, gameDate: '2018-06-22T03:00:20.000Z' },
  { id: 103, homeTeam: { id: 4 }, visitingTeam: { id: 12 }, gameDate: '2018-06-24T03:00:10.000Z' },
  { id: 100, homeTeam: { id: 1 }, visitingTeam: { id: 9 }, gameDate: '2018-06-24T12:00:10.000Z' },
  { id: 101, homeTeam: { id: 2 }, visitingTeam: { id: 10 }, gameDate: '2018-06-24T14:00:10.000Z' },
  { id: 104, homeTeam: { id: 5 }, visitingTeam: { id: 13 }, gameDate: '2018-06-24T18:00:10.000Z' },
  { id: 106, homeTeam: { id: 7 }, visitingTeam: { id: 15 }, gameDate: '2018-06-24T19:00:10.000Z' },
  { id: 102, homeTeam: { id: 3 }, visitingTeam: { id: 11 }, gameDate: '2018-06-26T09:00:10.000Z' },
  { id: 105, homeTeam: { id: 6 }, visitingTeam: { id: 14 }, gameDate: '2018-06-30T21:00:10.000Z' },
];

const favoriteTeam = { id: 123 };

describe('Sort', () => {
  beforeEach(() => {
    data = [
      { id: 100, homeTeam: { id: 1 }, visitingTeam: { id: 9 }, gameDate: '2018-06-24T12:00:10.000Z' },
      { id: 101, homeTeam: { id: 2 }, visitingTeam: { id: 10 }, gameDate: '2018-06-24T14:00:10.000Z' },
      { id: 102, homeTeam: { id: 3 }, visitingTeam: { id: 11 }, gameDate: '2018-06-26T09:00:10.000Z' },
      { id: 103, homeTeam: { id: 4 }, visitingTeam: { id: 12 }, gameDate: '2018-06-24T03:00:10.000Z' },
      { id: 104, homeTeam: { id: 5 }, visitingTeam: { id: 13 }, gameDate: '2018-06-24T18:00:10.000Z' },
      { id: 105, homeTeam: { id: 6 }, visitingTeam: { id: 14 }, gameDate: '2018-06-30T21:00:10.000Z' },
      { id: 106, homeTeam: { id: 7 }, visitingTeam: { id: 15 }, gameDate: '2018-06-24T19:00:10.000Z' },
      { id: 107, homeTeam: { id: 8 }, visitingTeam: { id: 123 }, gameDate: '2018-06-24T03:00:20.000Z' },
      { id: 108, homeTeam: { id: 9 }, visitingTeam: { id: 2 }, gameDate: '2018-06-22T03:00:20.000Z' },
      { id: 109, homeTeam: { id: 123 }, visitingTeam: { id: 3 }, gameDate: '2018-06-21T03:00:20.000Z' },
    ];
  });
  describe('sortByGameDate', () => {
    it('should sort the data', () => {
      const sortedGames = sortByGameDate(data);
      sortedGames.map((game, index) => {
        expect(game.id).toEqual(dataSortedByDate[index].id);
      });
    });
  });

  describe('sortByFavoriteTeam', () => {
    it('should sort the data', () => {
      const sortedGames = sortByFavoriteTeam(data, favoriteTeam);
      sortedGames.map((game, index) => {
        expect(game.id).toEqual(dataSortedByFavorite[index].id);
      });
    });
  });

  describe('sortGames', () => {
    it('returns the data sorted only by date when there is no favorite team', () => {
      const sortedGames = sortGames(data);
      sortedGames.map((game, index) => {
        expect(game.id).toEqual(dataSortedByDate[index].id);
      });
    });

    it('returns the data with the favorite team games first but all games sorted by date', () => {
      const sortedGames = sortGames(data, favoriteTeam);
      sortedGames.map((game, index) => {
        expect(game.id).toEqual(dataSortedByBoth[index].id);
      });
    });
  });
});