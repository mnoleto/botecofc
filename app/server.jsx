import axios from 'axios';
import { createMemoryHistory } from 'history'
import React from 'react';
import { renderToString } from 'react-dom/server';
import { RouterContext, match } from 'react-router';
import { Provider } from 'react-redux';
import createRoutes from 'routes';
import header from './components/Meta';
import preRenderMiddleware from './middlewares/preRenderMiddleware';
import configureStore from './store/configureStore';

const clientConfig = {
  host: process.env.NODE_ENV === 'prod' ? 'https://botecofc.com.br' : 'http://localhost',
  port: process.env.PORT || '3000'
};

// configure baseURL for axios requests (for serverside API calls)
// axios.defaults.baseURL = `https://botecofc.com.br:${clientConfig.port}`;
axios.defaults.baseURL = `${clientConfig.host}:${clientConfig.port}`;

/*
 * Export render function to be used in server/config/routes.js
 * We grab the state passed in from the server and the req object from Express/Koa
 * and pass it into the Router.run function.
 */
export default function render(req, res) {
  const authenticated = req.isAuthenticated();
  const history = createMemoryHistory();
  const user = req.user
  const city = user != null && user.profile != null ? req.user.profile.cityId : ''
  const store = configureStore({
    user: {
      authenticated,
      isWaiting: false,
      isLoading: true,
      message: '',
      isLogin: true,
      termsAccepted : (authenticated ? req.user.profile.terms : false),
      account: (authenticated ? req.user : {}),
      selectedCity: city
    }
  }, history);
  const routes = createRoutes(store);

  /*
   * From the react-router docs:
   *
   * This function is to be used for server-side rendering. It matches a set of routes to
   * a location, without rendering, and calls a callback(err, redirect, props)
   * when it's done.
   *
   * The function will create a `history` for you, passing additional `options` to create it.
   * These options can include `basename` to control the base name for URLs, as well as the pair
   * of `parseQueryString` and `stringifyQuery` to control query string parsing and serializing.
   * You can also pass in an already instantiated `history` object, which can be constructured
   * however you like.
   *
   * The three arguments to the callback function you pass to `match` are:
   * - err:       A javascript Error object if an error occured, `undefined` otherwise.
   * - redirect:  A `Location` object if the route is a redirect, `undefined` otherwise
   * - props:     The props you should pass to the routing context if the route matched,
   *              `undefined` otherwise.
   * If all three parameters are `undefined`, this means that there was no route found matching the
   * given location.
   */
  match({routes, location: req.url}, (err, redirect, props) => {
    if (err) {
      res.status(500).json(err);
    } else if (redirect) {
      res.redirect(302, redirect.pathname + redirect.search);
    } else if (props) {
      // This method waits for all render component
      // promises to resolve before returning to browser
      preRenderMiddleware(
        store.dispatch,
        props.components,
        props.params
      )
      .then(() => {
        const initialState = store.getState();
        const componentHTML = renderToString(
          <Provider store={store}>
            <RouterContext {...props} />
          </Provider>
        );

        res.status(200).send(`
          <!doctype html>
          <html ${header.htmlAttributes.toString()}>
            <head>
              ${header.title.toString()}
              ${header.meta.toString()}
              ${header.link.toString()}
              <link rel="stylesheet" type="text/css" charset="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
              <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
              <style>
                .react-tabs {
                  -webkit-tap-highlight-color: transparent;
                }

                .react-tabs__tab-list {
                  border-bottom: 1px solid #aaa;
                  margin: 0 0 10px;
                  padding: 0;
                }

                .react-tabs__tab {
                  display: inline-block;
                  border: 1px solid transparent;
                  border-bottom: none;
                  bottom: -1px;
                  position: relative;
                  list-style: none;
                  padding: 6px 12px;
                  cursor: pointer;
                }

                .react-tabs__tab--selected {
                  background: #fff;
                  border-color: #aaa;
                  color: black;
                  border-radius: 5px 5px 0 0;
                }

                .react-tabs__tab--disabled {
                  color: GrayText;
                  cursor: default;
                }

                .react-tabs__tab:focus {
                  box-shadow: 0 0 5px hsl(208, 99%, 50%);
                  border-color: hsl(208, 99%, 50%);
                  outline: none;
                }

                .react-tabs__tab:focus:after {
                  content: "";
                  position: absolute;
                  height: 5px;
                  left: -4px;
                  right: -4px;
                  bottom: -5px;
                  background: #fff;
                }

                .react-tabs__tab-panel {
                  display: none;
                }

                .react-tabs__tab-panel--selected {
                  display: block;
                }
                .places-carousel-slick .dotslists .slick-active button {
                  background-color: #fff;
                }
                .home-banners .slick-active button {
                  background-color: #fff;
                  color: #fff;
                }
                .home-banners .slick-active button:before {
                  background-color: #ccc;
                  bottom: 0px;
                  content: '';
                  display: block;
                  height: 7px;
                  left: 0px;
                  position: absolute;
                  top: 0px;
                  width: 0px;
                  -webkit-animation: Bar 6s linear 1;
                  -moz-animation: Bar 6s linear 1;
                  animation: Bar 6s linear 1;
                }
                @-webkit-keyframes Bar {
                  0% {
                    width: 0px;
                  }
                  100% {
                    width: 50px;
                  }
                }

                @-moz-keyframes Bar {
                  0% {
                    width: 0px;
                  }
                  100% {
                    width: 50px;
                  }
                }

                @keyframes Bar {
                  0% {
                    width: 0px;
                  }
                  100% {
                    width: 50px;
                  }
                }
                .react-datepicker__navigation--next,
                .react-datepicker__navigation--previous {
                  font-size: 1px;
                  text-indent: -6000px;
                  line-height: 0;
                }
                .react-datepicker-wrapper, .react-datepicker__input-container {
                    width: 100%;
                }
                .react-datepicker__input-container button {
                  background: transparent;
                  border: 0px;
                  color: #313131;
                  cursor: pointer;
                  display: block;
                  font-size: 18px;
                  outline: none;
                  padding: 0;
                  position: relative;
                  text-align: left;
                  text-transform: uppercase;
                  width: 100%;
                }
                .Select {
                  z-index: 1000;
                }
                .Select-control {
                  height: 50px;
                  border-radius: 10px;
                }
                .Select-value {
                  padding-top: 3px;
                }
                .Select-input {
                  padding-top: 5px;
                }
                .Select-control > *:last-child {
                  padding-right: 15px;
                }
                .Select-control .Clear {
                  padding-right: 10px;
                }
                .Select-arrow-zone {
                  top: 0px !important;
                  width: 27px;
                }
                .Select-placeholder {
                  top: 5px;
                }
                .Select-arrow {
                  background: transparent !important;
                  border: 0px;
                  display; none;
                }
              </style>
              <!--Global site tag (gtag.js) - AdWords: 951463108 -->
              <script async src="https://www.googletagmanager.com/gtag/js?id=AW-951463108"></script>
              <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'AW-951463108');
              </script>

              <script>
                gtag('event', 'page_view', {
                  'send_to': 'AW-951463108',
                  'user_id': 'replace with value'
                });
              </script>
            </head>
            <body>
              <div id="app">${componentHTML}</div>
              <script>window.__INITIAL_STATE__ = ${JSON.stringify(initialState)};</script>
              <script type="text/javascript" charset="utf-8" src="https://code.jquery.com/jquery-2.2.4.min.js" crossorigin="anonymous"></script>
              <script type="text/javascript" charset="utf-8" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
              <script type="text/javascript" charset="utf-8" src="/assets/app.js"></script>
            </body>
          </html>
        `);
      })
      .catch((err) => {
        res.status(500).json(err);
      });
    } else {
      res.sendStatus(404);
    }
  });
}
